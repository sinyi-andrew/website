from flask import Blueprint, render_template, request, Response, redirect, url_for
from flask import jsonify, json

from Model.Exception import *
from Model.Employee import EmployeeInstance
from Model.JWT import JWTInstance
from Model.Member import MemberInstance
from Utility.Status import *

userApi = Blueprint('userApi', __name__)


def init_app(app):
    app.register_blueprint(userApi, url_prefix='/admin/user/api')


@userApi.route('/get/unused/<user_uuid>', methods=['GET'])
def getUnusedByUserUUID(user_uuid):
    try:
        user_id = MemberInstance().getUserIDByUuid(user_uuid)
        unusedList = MemberInstance().getUnusedByUserUUID(user_id)
        return Response(json.dumps({'unusedList': unusedList}), mimetype='application/json')
    except(
            MissingRequiredParametersException,
            TargetIdDoesNotExistException,
            DatabaseFormatWrongException,
            AuthenticateFailedException,
            Exception,
            RuntimeError
    ) as e:
        response = Response()
        response.headers.add('error_code', e.__str__())
        response.status_code = 490
        return response


@userApi.route('/grades', methods=['POST'])
def gradesData():
    try:
        id = request.json["id"] if request.json["id"] is not None else None
        name = request.json["name"] if request.json["name"] is not None else None
        description = request.json["description"] if request.json["description"] is not None else None
        discount = request.json["discount"] if request.json["discount"] is not None else None
        shipping_fee = request.json["shipping_fee"] if request.json["shipping_fee"] is not None else None
        shipping_setting = request.json["shipping_setting"] if request.json["shipping_setting"] is not None else None

        if name is None or description is None or discount is None or shipping_fee is None or shipping_setting is None:
            raise MissingCriticalParametersException()
        else:
            instance = MemberInstance()
            response = instance.gradeData(id, name, description, discount, shipping_fee, shipping_setting)
            return 'success'
    except (
            MissingCriticalParametersException,
            Exception,
            RuntimeError
    ) as e:
        return e.__str__()


@userApi.route('/user', methods=['POST'])
def userData():
    try:
        id = request.json["id"] if request.json["id"] is not None else None
        grade_id = request.json["grade_id"] if request.json["grade_id"] is not None else None
        name = request.json["name"] if request.json["name"] is not None else None
        IDNumber = request.json["IDNumber"] if request.json["IDNumber"] is not None else None
        email = request.json["email"] if request.json["email"] is not None else None
        phone_number = request.json["phone_number"] if request.json["phone_number"] is not None else None
        mobile_phone = request.json["mobile_phone"] if request.json["mobile_phone"] is not None else None
        birthday = request.json["birthday"] if request.json["birthday"] is not None else None
        bloodtype = request.json["bloodtype"] if request.json["bloodtype"] is not None else None
        gender = request.json["gender"] if request.json["gender"] is not None else None
        address = request.json["address"] if request.json["address"] is not None else None
        contact = request.json["contact"] if request.json["contact"] is not None else None
        contact_phone = request.json["contact_phone"] if request.json["contact_phone"] is not None else None
        height = request.json["height"] if request.json["height"] is not None else None
        weight = request.json["weight"] if request.json["weight"] is not None else None
        improve = request.json["improve"] if request.json["improve"] is not None else None
        disease_history = request.json["disease_history"] if request.json["disease_history"] is not None else None
        allergy_history = request.json["allergy_history"] if request.json["allergy_history"] is not None else None
        whereform = request.json["whereform"] if request.json["whereform"] is not None else None
        note = request.json["note"] if request.json["note"] is not None else None

        if name is None or IDNumber is None or email is None or mobile_phone is None or birthday is None or bloodtype is None or gender is None or address is None or contact is None or contact_phone is None:
            raise MissingCriticalParametersException()
        else:
            instance = MemberInstance()
            response = instance.userData(id, grade_id, name, IDNumber, email, phone_number, mobile_phone, birthday, bloodtype, gender, address, contact, contact_phone, height, weight, improve,
                                         disease_history, allergy_history, whereform, note)
            return 'success'
    except (
            MissingCriticalParametersException,
            Exception,
            RuntimeError
    ) as e:
        return e.__str__()


@userApi.route('/point', methods=['POST'])
def userPointData():
    try:
        _token = request.form["_token"] if request.form["_token"] is not None else None
        user_id = request.form["user_id"] if request.form["user_id"] is not None else None
        point = request.form["point"] if request.form["point"] is not None else None

        if _token is None:
            return redirect('/login')
        else:
            user_info = JWTInstance().decode_auth_token(_token)
            input_personnel_grade = int(user_info['grade'])
            personal_uuid = user_info['uuid']
            input_personnel = EmployeeInstance().getEmployeeIDByUuid(personal_uuid)

            function = FunctionTypeStatus.user_point
            permission = EmployeeInstance().getPermissionByGradeIDAndFunctionID(input_personnel_grade, function)
            # permission judgment(need write)
            if int(permission) == PermissionTypeStatus.close or int(permission) == PermissionTypeStatus.read_only:
                raise AuthenticateFailedException()

            if user_id is None or point is None:
                raise MissingCriticalParametersException()
            else:
                instance = MemberInstance()
                response = instance.userPointData(user_id, input_personnel, point)
                return redirect('/admin/user/{}/{}/{}/point/print'.format(user_id, response, input_personnel))
                # return redirect(url_for('user.userPointPrint', uuid=user_id, response=response, employee=input_personnel))
    except (
            MissingCriticalParametersException,
            AuthenticateFailedException,
            Exception,
            RuntimeError
    ) as e:
        return e.__str__()


@userApi.route('/search', methods=['POST'])
def userSearch():
    try:
        search = request.json["search"] if request.json["search"] is not None else None
        if search is None:
            raise MissingCriticalParametersException()
        else:
            instance = MemberInstance()
            response = instance.searchUser(search)
            return Response(json.dumps(response), mimetype='application/json')
    except (
            MissingCriticalParametersException,
            Exception,
            RuntimeError
    ) as e:
        return e.__str__()

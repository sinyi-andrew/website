from flask import Blueprint, render_template, request, Response
from datetime import datetime, date, timedelta
import json

from Model.BusinessRule import BusinessRuleInstance
from Model.Employee import EmployeeInstance
from Model.Product import ProductInstance
from Model.Exception import *
from Utility.Status import *

testApi = Blueprint('testApi', __name__)


def init_app(app):
    app.register_blueprint(testApi, url_prefix='/admin/test/api')


@testApi.route('/test', methods=['GET'])
def test():
    try:
        personal_uuid = '9984aa8e-cf81-476d-a809-538d6f453592' # TODO: input_personnel get from token
        personal_id = EmployeeInstance().getEmployeeIDByUuid(personal_uuid)
        employeeIDList = EmployeeInstance().getEmployeeIDList()
        productList = ProductInstance().getProductList()

        dateToday = date.today()
        numOfUsers = BusinessRuleInstance().getNumOfUsers()
        allUnusedAmount = BusinessRuleInstance().getAllUnusedAmount()
        salesToday = BusinessRuleInstance().getSalesByDate(dateToday)
        salesTodayPersonal = BusinessRuleInstance().getSalesByDateOnlyPersonal(dateToday, personal_id)

        targetYear = int(dateToday.strftime('%Y'))
        targetMonth = int(dateToday.strftime('%m'))
        # targetMonth = int(4)
        salesThisMonth = BusinessRuleInstance().getSalesByMonth(targetYear, targetMonth)
        salesThisMonthPersonal = BusinessRuleInstance().getSalesByMonthOnlyPersonal(targetYear, targetMonth, personal_id)

        salesThisMonthAllPersonal = BusinessRuleInstance().getSalesByMonthAllPersonal(targetYear, targetMonth, employeeIDList)
        salesThisMonthAllDays = BusinessRuleInstance().getSalesByMonthAllDays(targetYear, targetMonth)
        salesThisMonthAllProducts = BusinessRuleInstance().getSalesByMonthAllProducts(targetYear, targetMonth, productList)
        salesThisYearAllMonths = BusinessRuleInstance().getSalesByYearAllMonths(targetYear)

        response = {
            'numOfUsers': numOfUsers,
            'allUnusedAmount': allUnusedAmount,
            'salesToday': salesToday,
            'salesTodayPersonal': salesTodayPersonal,
            'salesThisMonth': salesThisMonth,
            'salesThisMonthPersonal': salesThisMonthPersonal,
            'salesThisMonthAllPersonal': salesThisMonthAllPersonal,
            'salesThisMonthAllDays': salesThisMonthAllDays,
            'salesThisMonthAllProducts': salesThisMonthAllProducts,
            'salesThisYearAllMonths': salesThisYearAllMonths
        }
        return Response(json.dumps(response), mimetype='application/json')
    except(
            MissingRequiredParametersException,
            Exception,
            RuntimeError
    ) as e:
        response = Response()
        response.headers.add('error_code', e.__str__())
        response.status_code = 490
        return response


@testApi.route('/test/getProductDetailByCategoryId', methods=['GET'])
def getProductDetailByCategoryId():
    try:
        response = BusinessRuleInstance().getProductDetailByCategoryIdForFrontend(3) # 3 = service
        return Response(json.dumps(response), mimetype='application/json')
    except(
            MissingRequiredParametersException,
            Exception,
            RuntimeError
    ) as e:
        response = Response()
        response.headers.add('error_code', e.__str__())
        response.status_code = 490
        return response
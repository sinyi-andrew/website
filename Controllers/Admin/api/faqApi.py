from flask import Blueprint, render_template, request, Response, redirect
import json

from Model.Exception import *
from Model.Employee import EmployeeInstance
from Model.JWT import JWTInstance
from Model.Member import MemberInstance
from Model.Faq import FaqInstance
from Utility.Status import *

faqApi = Blueprint('faqApi', __name__)


def init_app(app):
    app.register_blueprint(faqApi, url_prefix='/admin/faq/api')


@faqApi.route('/faq/without/reply', methods=['GET'])
def getFaqWithoutReply():
    try:
        response = FaqInstance().getFaqWithoutReply()
        return Response(json.dumps(response), mimetype='application/json')
    except(
            MissingRequiredParametersException,
            Exception,
            RuntimeError
    ) as e:
        response = Response()
        response.headers.add('error_code', e.__str__())
        response.status_code = 490
        return response


@faqApi.route('/faq', methods=['GET'])
def getFaq():
    try:
        response = FaqInstance().getFaq()
        return Response(json.dumps(response), mimetype='application/json')
    except(
            MissingRequiredParametersException,
            Exception,
            RuntimeError
    ) as e:
        response = Response()
        response.headers.add('error_code', e.__str__())
        response.status_code = 490
        return response


@faqApi.route('/faq', methods=['POST'])
def faqCreate():
    try:
        name = request.json["name"] if request.json["name"] is not None else None
        birthday = request.json["birthday"] if request.json["birthday"] is not None else None
        gender = request.json["gender"] if request.json["gender"] is not None else None
        phone_number = request.json["phone_number"] if request.json["phone_number"] is not None else None
        mobile_phone = request.json["mobile_phone"] if request.json["mobile_phone"] is not None else None
        email = request.json["email"] if request.json["email"] is not None else None
        faq_type = request.json["faq_type"] if request.json["faq_type"] is not None else None
        content = request.json["content"] if request.json["content"] is not None else None

        if name is None or birthday is None or gender is None or phone_number is None or mobile_phone is None or email is None\
                 or faq_type is None or content is None:
            raise MissingRequiredParametersException()
        else:
            faq_id = FaqInstance().create_faq(name, birthday, gender, phone_number, mobile_phone, email, faq_type, content)
            return Response(json.dumps({'faq_id': faq_id}), mimetype='application/json')
    except(
        MissingRequiredParametersException,
        TargetIdDoesNotExistException,
        Exception,
        RuntimeError
    ) as e:
        response = Response()
        response.headers.add('error_code', e.__str__())
        response.status_code = 490
        return response


@faqApi.route('/faq/reply', methods=['POST'])
def faqReply():
    try:
        _token = request.json["_token"] if request.json["_token"] is not None else None
        faq_id = request.json["faq_id"] if request.json["faq_id"] is not None else None
        reply_content = request.json["reply_content"] if request.json["reply_content"] is not None else None

        if _token is None:
            return redirect('/login')
        else:
            user_info = JWTInstance().decode_auth_token(_token)
            input_personnel_grade = int(user_info['grade'])
            personal_uuid = user_info['uuid']
            input_personnel = EmployeeInstance().getEmployeeIDByUuid(personal_uuid)

            function = FunctionTypeStatus.faq
            permission = EmployeeInstance().getPermissionByGradeIDAndFunctionID(input_personnel_grade, function)
            # permission judgment(need write)
            if int(permission) == PermissionTypeStatus.close or int(permission) == PermissionTypeStatus.read_only:
                raise AuthenticateFailedException()

            if faq_id is None or reply_content is None:
                raise MissingRequiredParametersException()
            else:
                faq_id = FaqInstance().reply_faq(input_personnel, faq_id, reply_content)
                return Response(json.dumps({'faq_id': faq_id}), mimetype='application/json')
    except(
        MissingRequiredParametersException,
        TargetIdDoesNotExistException,
        AuthenticateFailedException,
        Exception,
        RuntimeError
    ) as e:
        response = Response()
        response.headers.add('error_code', e.__str__())
        response.status_code = 490
        return response
from flask import Blueprint, render_template, request, Response, redirect
from datetime import datetime, timedelta, date
import json
import xlsxwriter

from Model.Exception import *
from Model.Employee import EmployeeInstance
from Model.Inventory import InventoryInstance
from Model.JWT import JWTInstance
from Model.Member import MemberInstance
from Model.Order import OrderInstance
from Model.Product import ProductInstance
from Utility.config import ConfigSetting
from Utility.Status import *

orderApi = Blueprint('orderApi', __name__)


def init_app(app):
    app.register_blueprint(orderApi, url_prefix='/admin/order/api')


@orderApi.route('/calendar')
def index():
    return render_template('admin/order/calendar.html')


@orderApi.route('/get/order/by/useruuid/<user_uuid>', methods=['GET'])
def getOrderByUserID(user_uuid):
    try:
        user_id = MemberInstance().getUserIDByUuid(user_uuid)
        response = OrderInstance().getOrderByUserID(user_id)
        return Response(json.dumps(response), mimetype='application/json')
    except(
            MissingRequiredParametersException,
            TargetIdDoesNotExistException,
            EmployeeListEmptyException,
            Exception,
            RuntimeError
    ) as e:
        response = Response()
        response.headers.add('error_code', e.__str__())
        response.status_code = 490
        return response


@orderApi.route('/get/orderitems/by/useruuid/<user_uuid>', methods=['GET'])
def getOrderItemsByUserID(user_uuid):
    try:
        user_id = MemberInstance().getUserIDByUuid(user_uuid)
        response = OrderInstance().getOrderItemsByUserID(user_id)
        return Response(json.dumps(response), mimetype='application/json')
    except(
            Exception,
            RuntimeError
    ) as e:
        response = Response()
        response.headers.add('error_code', e.__str__())
        response.status_code = 490
        return response


@orderApi.route('/get/orderitems/by/useruuid/<user_uuid>/without/filter', methods=['GET'])
def getOrderItemsByUserIDWithoutFilter(user_uuid):
    try:
        user_id = MemberInstance().getUserIDByUuid(user_uuid)
        response = OrderInstance().getOrderItemsByUserIDWithoutFilter(user_id)
        return Response(json.dumps(response), mimetype='application/json')
    except(
        TargetIdDoesNotExistException,
        Exception,
        RuntimeError
    ) as e:
        response = Response()
        response.headers.add('error_code', e.__str__())
        response.status_code = 490
        return response


@orderApi.route('/get/order/<order_uuid>', methods=['GET'])
def getOrderByOrderID(order_uuid):
    try:
        order_id = OrderInstance().getOrderIDByOrderUuid(order_uuid)
        response = OrderInstance().getOrderByOrderID(order_id)
        return Response(json.dumps(response), mimetype='application/json')
    except(
            MissingRequiredParametersException,
            TargetIdDoesNotExistException,
            EmployeeListEmptyException,
            Exception,
            RuntimeError
    ) as e:
        response = Response()
        response.headers.add('error_code', e.__str__())
        response.status_code = 490
        return response


@orderApi.route('/order', methods=['GET'])
def getOrder():
    try:
        if request.values["search[value]"] is '':
            response = OrderInstance().getOrder()
        else:
            search = request.values["search[value]"] if request.values["search[value]"] is not None else None
            response = OrderInstance().getOrderBySearch(search)
        return Response(json.dumps(response), mimetype='application/json')
    except(
            MissingRequiredParametersException,
            EmployeeListEmptyException,
            Exception,
            RuntimeError
    ) as e:
        response = Response()
        response.headers.add('error_code', e.__str__())
        response.status_code = 490
        return response


@orderApi.route('/order', methods=['POST'])
def orderCreate():
    try:
        _token = request.json["_token"] if request.json["_token"] is not None else None

        user_uuid = request.json["user_uuid"] if request.json["user_uuid"] is not None else None
        employee_uuid1 = request.json["employee_uuid1"] if request.json["employee_uuid1"] is not None else None
        employee_uuid2 = request.json["employee_uuid2"] if request.json["employee_uuid2"] is not None else None
        payment_type = request.json["payment_type"] if request.json["payment_type"] is not None else None
        shipping_type = request.json["shipping_type"] if request.json["shipping_type"] is not None else None
        shipping_fee = request.json["shipping_fee"] if request.json["shipping_fee"] is not None else None
        input_total_price = request.json["total_price"] if request.json["total_price"] is not None else None
        recipient = request.json["recipient"] if request.json["recipient"] is not None else None
        recipient_phone_number = request.json["recipient_phone_number"] if request.json["recipient_phone_number"] is not None else None
        recipient_deliver_address = request.json["recipient_deliver_address"] if request.json["recipient_deliver_address"] is not None else None
        shipping_date = request.json["shipping_date"] if request.json["shipping_date"] is not None else None
        skus_list = request.json["skus_list"] if request.json["skus_list"] is not None else None
        creditcard_note = request.json["creditcard_note"] if "creditcard_note" in request.json else None

        if _token is None:
            return redirect('/login')
        else:
            user_info = JWTInstance().decode_auth_token(_token)
            input_personnel_grade = int(user_info['grade'])
            personal_uuid = user_info['uuid']
            input_personnel = EmployeeInstance().getEmployeeIDByUuid(personal_uuid)

            function = FunctionTypeStatus.order
            permission = EmployeeInstance().getPermissionByGradeIDAndFunctionID(input_personnel_grade, function)
            # permission judgment(need write)
            if int(permission) == PermissionTypeStatus.close or int(permission) == PermissionTypeStatus.read_only:
                raise AuthenticateFailedException()

            if input_personnel is None or user_uuid is None or employee_uuid1 is None or employee_uuid2 is None or payment_type is None or shipping_type is None\
                     or shipping_fee is None or skus_list == []:
                raise MissingRequiredParametersException()
            else:
                # check user and employee, and get id by uuid
                user_id = MemberInstance().getUserIDByUuid(user_uuid)
                employee_id1 = EmployeeInstance().getEmployeeIDByUuid(employee_uuid1)
                employee_id2 = EmployeeInstance().getEmployeeIDByUuid(employee_uuid2)
                user_discount, shipping_setting = MemberInstance().getDiscountByUserID(user_id)
                # check recipient input when shipping_type is not self, and check shipping_fee is 0 when shipping_type is self
                shipping_fee = OrderInstance().check_recipient(shipping_type, recipient, recipient_phone_number, recipient_deliver_address, shipping_fee)
                # check creditcard_note when payment_type is creditcard (OrdersPaymentTypeStatus.creditcard -> 3)
                creditcard_note = OrderInstance().check_creditcard_note(payment_type, creditcard_note)
                # check skus_list sku_id duplication
                sku_id_list = OrderInstance().check_skus_list_duplication(skus_list)
                # check category7(Experience ticket)
                OrderInstance().checkCategory7Unused(user_id, sku_id_list)
                # processing skus_list and check inventory
                skus_info = OrderInstance().processing_skus_list(skus_list, user_discount, shipping_setting, shipping_fee)
                total_price = skus_info['total_price']
                skus = skus_info['skus_list']

                if input_total_price == total_price:
                    inventory_deduction_list = skus_info['inventory_deduction_list']
                    OrderInstance().check_quota(payment_type, user_uuid, total_price)

                    # Temporarily set to undone, update order_status after create item
                    temp_order_status = 1 # undone
                    order_info = OrderInstance().create_order(input_personnel, user_id, employee_id1, employee_id2, payment_type,
                                                            shipping_type, shipping_fee, temp_order_status, total_price,
                                                            recipient, recipient_phone_number, recipient_deliver_address, shipping_date, creditcard_note)

                    order_id = order_info['order_id']
                    order_status = OrderInstance().create_item(skus, order_id)
                    OrderInstance().update_order_status(order_id, order_status)

                    if payment_type == OrdersPaymentTypeStatus.points:
                        # Deduction points
                        write_employee_id = input_personnel
                        number = 'D' + str(order_id).rjust(8, '0')
                        MemberInstance().createUserPoint(user_uuid, order_id, write_employee_id, number, -total_price)

                    # if immediate receive
                    for i in range(len(inventory_deduction_list)):
                        supplies_id = inventory_deduction_list[i]['supplies_id']
                        quantity = inventory_deduction_list[i]['quantity']
                        note = 'D' + str(order_id).rjust(8, '0') + ': 已從櫃檯取貨'
                        type = inventory_deduction_list[i]['type']
                        InventoryInstance().create_or_update_inventory(input_personnel, None, supplies_id, -quantity, note, type)

                    return Response(json.dumps({'order_info': order_info}), mimetype='application/json')
                else:
                    raise TotalPriceUnmatchException()
    except(
        MissingRequiredParametersException,
        TargetIdDoesNotExistException,
        WrongFormatException,
        InsufficientBalanceException,
        InsufficientInventoryException,
        TotalPriceUnmatchException,
        InvaildSkuInAssembleException,
        AuthenticateFailedException,
        ThisExperienceTicketAlreadyUsedException,
        CreditcardNoteWrongFormatException,
        Exception,
        RuntimeError
    ) as e:
        response = Response()
        response.headers.add('error_code', e.__str__())
        response.status_code = 490
        return response


@orderApi.route('/order', methods=['PUT'])
def orderUpdate():
    try:
        _token = request.json["_token"] if request.json["_token"] is not None else None
        order_uuid = request.json["order_uuid"] if request.json["order_uuid"] is not None else None
        recipient = request.json["recipient"] if request.json["recipient"] is not None else None
        recipient_phone_number = request.json["recipient_phone_number"] if request.json["recipient_phone_number"] is not None else None
        recipient_deliver_address = request.json["recipient_deliver_address"] if request.json["recipient_deliver_address"] is not None else None
        shipping_date = request.json["shipping_date"] if request.json["shipping_date"] is not None else None

        if _token is None:
            return redirect('/login')
        else:
            user_info = JWTInstance().decode_auth_token(_token)
            input_personnel_grade = int(user_info['grade'])
            personal_uuid = user_info['uuid']
            input_personnel = EmployeeInstance().getEmployeeIDByUuid(personal_uuid)
            function = FunctionTypeStatus.order
            permission = EmployeeInstance().getPermissionByGradeIDAndFunctionID(input_personnel_grade, function)
            # permission judgment(need write)
            if int(permission) == PermissionTypeStatus.close or int(permission) == PermissionTypeStatus.read_only:
                raise AuthenticateFailedException()

            if input_personnel is None or order_uuid is None:
                raise MissingRequiredParametersException()
            else:
                order_id = OrderInstance().getOrderIDByOrderUuid(order_uuid)
                order_info = OrderInstance().getOrderInfoByOrderID(order_id)
                shipping_type = order_info['shipping_type']
                shipping_fee = order_info['shipping_fee']
                OrderInstance().check_recipient(shipping_type, recipient, recipient_phone_number, recipient_deliver_address, shipping_fee)
                OrderInstance().update_order(input_personnel, order_id, recipient, recipient_phone_number, recipient_deliver_address, shipping_date)
                return Response(json.dumps({'order_id': order_id}), mimetype='application/json')
    except(
        MissingRequiredParametersException,
        TargetIdDoesNotExistException,
        AuthenticateFailedException,
        Exception,
        RuntimeError
    ) as e:
        response = Response()
        response.headers.add('error_code', e.__str__())
        response.status_code = 490
        return response


# TODO: apart product pick up and service exp_date extension
@orderApi.route('/orderitem', methods=['PUT'])
def orderitemUpdate():
    try:
        _token = request.json["_token"] if request.json["_token"] is not None else None
        order_item_id = request.json["order_item_id"] if request.json["order_item_id"] is not None else None
        used = request.json["used"] if request.json["used"] is not None else None
        exp_date = request.json["exp_date"] if request.json["exp_date"] is not None else None
        note = request.json["note"] if request.json["note"] is not None else None

        if _token is None:
            return redirect('/login')
        else:
            user_info = JWTInstance().decode_auth_token(_token)
            input_personnel_grade = int(user_info['grade'])
            personal_uuid = user_info['uuid']
            input_personnel = EmployeeInstance().getEmployeeIDByUuid(personal_uuid)

            if input_personnel is None or order_item_id is None or used is None or exp_date is None:
                raise MissingRequiredParametersException()
            else:
                order_item_info = OrderInstance().getOrderInfoByOrderItemID(order_item_id)
                # product pickup
                if order_item_info['order_item_category_id'] == ProductCategoriesStatus.product:
                    function = FunctionTypeStatus.order
                    permission = EmployeeInstance().getPermissionByGradeIDAndFunctionID(input_personnel_grade, function)
                    # permission judgment(need write)
                    if int(permission) == PermissionTypeStatus.close or int(permission) == PermissionTypeStatus.read_only:
                        raise AuthenticateFailedException()
                    order_item_id, used_diff = OrderInstance().update_order_item(input_personnel, order_item_id, used, exp_date, note, order_item_info['order_item_category_id'])

                    if used_diff != 0:
                        # inventory deduction
                        supply_id = InventoryInstance().getSupplyIDByProductId(order_item_info['order_item_product_id'])
                        note = 'D' + str(order_item_info['order_id']).rjust(8, '0') + ': 已從櫃檯取貨'
                        InventoryInstance().create_or_update_inventory(input_personnel, None, supply_id, -used_diff, note, InventoryTypeStatus.counter)

                    return Response(json.dumps({'order_item_id': order_item_id}), mimetype='application/json')

                # service exp_date extension
                else:
                    function = FunctionTypeStatus.order_item
                    permission = EmployeeInstance().getPermissionByGradeIDAndFunctionID(input_personnel_grade, function)
                    # permission judgment(need write)
                    if int(permission) == PermissionTypeStatus.close or int(permission) == PermissionTypeStatus.read_only:
                        raise AuthenticateFailedException()
                    order_item_id, used_diff = OrderInstance().update_order_item(input_personnel, order_item_id, used, exp_date, note, order_item_info['order_item_category_id'])
                    return Response(json.dumps({'order_item_id': order_item_id}), mimetype='application/json')
    except(
        MissingRequiredParametersException,
        TargetIdDoesNotExistException,
        WrongFormatException,
        AuthenticateFailedException,
        Exception,
        RuntimeError
    ) as e:
        response = Response()
        response.headers.add('error_code', e.__str__())
        response.status_code = 490
        return response


@orderApi.route('/orderitem/change', methods=['POST'])
def orderitemChange():
    try:
        _token = request.json["_token"] if request.json["_token"] is not None else None
        order_item_id = request.json["order_item_id"] if request.json["order_item_id"] is not None else None
        user_uuid = request.json["user_uuid"] if request.json["user_uuid"] is not None else None
        sku_id = request.json["sku_id"] if request.json["sku_id"] is not None else None
        quantity = request.json["quantity"] if request.json["quantity"] is not None else None
        immediate = request.json["immediate"] if request.json["immediate"] is not None else None
        note = request.json["note"] if request.json["note"] is not None else None

        if _token is None:
            return redirect('/login')
        else:
            user_info = JWTInstance().decode_auth_token(_token)
            input_personnel_grade = int(user_info['grade'])
            personal_uuid = user_info['uuid']
            input_personnel = EmployeeInstance().getEmployeeIDByUuid(personal_uuid)
            function = FunctionTypeStatus.order_item_change
            permission = EmployeeInstance().getPermissionByGradeIDAndFunctionID(input_personnel_grade, function)
            # permission judgment(need write)
            if int(permission) == PermissionTypeStatus.close or int(permission) == PermissionTypeStatus.read_only:
                raise AuthenticateFailedException()

            if input_personnel is None or order_item_id is None or user_uuid is None or sku_id is None or quantity is None\
                    or immediate is None:
                raise MissingRequiredParametersException()
            else:
                # get new sku info
                sku_info = ProductInstance().getSkuInfoBySkuID(sku_id)
                sku_info["quantity"] = quantity
                sku_info["immediate"] = immediate
                sku_info["note"] = note

                # old order_item info
                order_item_info = OrderInstance().getOrderInfoByOrderItemID(order_item_id)
                user_discount, shipping_setting = MemberInstance().getDiscountByUserID(order_item_info['order_user_id'])

                sku_id_list = []
                sku_id_list.append(sku_id)
                # check category7(Experience ticket)
                OrderInstance().checkCategory7Unused(order_item_info['order_user_id'], sku_id_list)

                # check inventory (new_sku_id)(only product category)
                inventory_deduction_list = []
                if sku_info["category_id"] == ProductCategoriesStatus.product:
                    inventory_deduction_list = OrderInstance().processing_sku_id_single(sku_info)

                # action

                # return product used to inventory
                if order_item_info['order_item_product_id'] == ProductCategoriesStatus.product:
                    inventory_return_list = OrderInstance().get_inventory_return_by_sku_id_single(sku_info, order_item_info['order_item_used'])
                    for i in range(len(inventory_return_list)):
                        supplies_id = inventory_return_list[i]['supplies_id']
                        quantity = inventory_return_list[i]['quantity']
                        note = 'D' + str(order_item_info['order_id']).rjust(8, '0') + ': 換貨時，已從櫃檯取回商品'
                        type = inventory_return_list[i]['type']
                        InventoryInstance().create_or_update_inventory(input_personnel, None, supplies_id, quantity, note, type)

                # change order_item
                order_item_id = OrderInstance().change_order_item(input_personnel, sku_info, order_item_info, user_discount)

                # if immediate receive
                for i in range(len(inventory_deduction_list)):
                    supplies_id = inventory_deduction_list[i]['supplies_id']
                    quantity = inventory_deduction_list[i]['quantity']
                    note = 'D' + str(order_item_info['order_id']).rjust(8, '0') + ': 換貨時，已從櫃檯取貨'
                    type = inventory_deduction_list[i]['type']
                    InventoryInstance().create_or_update_inventory(input_personnel, None, supplies_id, -quantity, note, type)

                return Response(json.dumps({'order_item_id': order_item_id}), mimetype='application/json')
    except(
        MissingRequiredParametersException,
        TargetIdDoesNotExistException,
        InsufficientInventoryException,
        WrongFormatException,
        AuthenticateFailedException,
        Exception,
        RuntimeError
    ) as e:
        response = Response()
        response.headers.add('error_code', e.__str__())
        response.status_code = 490
        return response


# TODO: add permission judgment
@orderApi.route('/order/cancel', methods=['GET', 'POST'])
def getOrderCancel():
    response = OrderInstance().getOrderCancel()
    return Response(json.dumps(response), mimetype='application/json')
    # try:
    #     _token = request.json["_token"] if request.json["_token"] is not None else None
    #     if _token is None:
    #         return redirect('/login')
    #     else:
    #         user_info = JWTInstance().decode_auth_token(_token)
    #         input_personnel_grade = int(user_info['grade'])
    #         personal_uuid = user_info['uuid']
    #         input_personnel = EmployeeInstance().getEmployeeIDByUuid(personal_uuid)
    #
    #         function = FunctionTypeStatus.order
    #         permission = EmployeeInstance().getPermissionByGradeIDAndFunctionID(input_personnel_grade, function)
    #         # permission judgment(need read)
    #         if int(permission) == PermissionTypeStatus.close:
    #             raise AuthenticateFailedException()
    #
    #         if input_personnel is None:
    #             raise MissingRequiredParametersException()
    #         else:
    #             response = OrderInstance().getOrderCancel()
    #             return Response(json.dumps(response), mimetype='application/json')
    # except(
    #     MissingRequiredParametersException,
    #     TargetIdDoesNotExistException,
    #     WrongFormatException,
    #     InsufficientBalanceException,
    #     InsufficientInventoryException,
    #     AuthenticateFailedException,
    #     Exception,
    #     RuntimeError
    # ) as e:
    #     response = Response()
    #     response.headers.add('error_code', e.__str__())
    #     response.status_code = 490
    #     return response


@orderApi.route('/order/cancel/<product_serial_number>', methods=['POST'])
def orderCancel(product_serial_number):
    try:
        _token = request.json["_token"] if request.json["_token"] is not None else None
        if _token is None:
            return redirect('/login')
        else:
            user_info = JWTInstance().decode_auth_token(_token)
            input_personnel_grade = int(user_info['grade'])
            personal_uuid = user_info['uuid']
            input_personnel = EmployeeInstance().getEmployeeIDByUuid(personal_uuid)

            function = FunctionTypeStatus.order_cacnel
            permission = EmployeeInstance().getPermissionByGradeIDAndFunctionID(input_personnel_grade, function)
            # permission judgment(need write)
            if int(permission) == PermissionTypeStatus.close or int(permission) == PermissionTypeStatus.read_only:
                raise AuthenticateFailedException()

            refund = 0     # refund not yet

            if input_personnel is None or product_serial_number is None:
                raise MissingRequiredParametersException()
            else:
                # get info
                order_id = OrderInstance().getOrderIDByOrderUuid(product_serial_number)
                order_info = OrderInstance().getOrderInfoByOrderID(order_id)
                user_uuid = MemberInstance().getUuidByUserID(order_info['user_id'])

                # action
                OrderInstance().cancel_order(input_personnel, order_id)

                # return point
                if order_info['payment_type'] == OrdersPaymentTypeStatus.points:
                    # Add points
                    number = 'D' + str(order_id).rjust(8, '0')
                    total_price = order_info['total_price']
                    MemberInstance().createUserPoint(user_uuid, order_id, input_personnel, number, total_price)
                    refund = 1     # already refund

                inventory_addition_list = order_info['inventory_addition_list']
                # return inventory
                for i in range(len(inventory_addition_list)):
                    supplies_id = inventory_addition_list[i]['supplies_id']
                    used = inventory_addition_list[i]['used']
                    note = 'Return of goods.'
                    type = InventoryTypeStatus.counter
                    InventoryInstance().create_or_update_inventory(input_personnel, None, supplies_id, used, note, type)

                OrderInstance().modify_order_refund(order_id, refund)

                return Response(json.dumps({'order_id': order_id}), mimetype='application/json')
    except(
        MissingRequiredParametersException,
        TargetIdDoesNotExistException,
        AuthenticateFailedException,
        Exception,
        RuntimeError
    ) as e:
        response = Response()
        response.headers.add('error_code', e.__str__())
        response.status_code = 490
        return response


@orderApi.route('/order/cancel/refund/<product_serial_number>', methods=['POST'])
def orderCancelRefund(product_serial_number):
    try:
        _token = request.json["_token"] if request.json["_token"] is not None else None
        if _token is None:
            return redirect('/login')
        else:
            user_info = JWTInstance().decode_auth_token(_token)
            input_personnel_grade = int(user_info['grade'])
            personal_uuid = user_info['uuid']
            input_personnel = EmployeeInstance().getEmployeeIDByUuid(personal_uuid)

            function = FunctionTypeStatus.order_cacnel
            permission = EmployeeInstance().getPermissionByGradeIDAndFunctionID(input_personnel_grade, function)
            # permission judgment(need write)
            if int(permission) == PermissionTypeStatus.close or int(permission) == PermissionTypeStatus.read_only:
                raise AuthenticateFailedException()

            refund = request.json["refund"] if request.json["refund"] is not None else None

            if input_personnel is None or product_serial_number is None:
                raise MissingRequiredParametersException()
            else:
                # get info
                order_id = OrderInstance().getOrderIDByOrderUuid(product_serial_number)
                OrderInstance().modify_order_refund(order_id, refund)

                return Response(json.dumps({'order_id': order_id}), mimetype='application/json')
    except(
        MissingRequiredParametersException,
        TargetIdDoesNotExistException,
        AuthenticateFailedException,
        Exception,
        RuntimeError
    ) as e:
        response = Response()
        response.headers.add('error_code', e.__str__())
        response.status_code = 490
        return response


@orderApi.route('/get/event/<event_id>', methods=['GET'])
def getEventByEventID(event_id):
    try:
        response = OrderInstance().getEventByEventID(event_id)
        return Response(json.dumps(response), mimetype='application/json')
    except(
        MissingRequiredParametersException,
        TargetIdDoesNotExistException,
        Exception,
        RuntimeError
    ) as e:
        response = Response()
        response.headers.add('error_code', e.__str__())
        response.status_code = 490
        return response


@orderApi.route('/event/by/userid/<user_uuid>', methods=['GET'])
def getEventByUserID(user_uuid):
    try:
        if user_uuid is not None:
            user_id = MemberInstance().getUserIDByUuid(user_uuid)
            response = OrderInstance().getEventByUserID(user_id)
            return Response(json.dumps(response), mimetype='application/json')
        else:
            raise MissingRequiredParametersException()
    except(
        MissingRequiredParametersException,
        Exception,
        RuntimeError
    ) as e:
        response = Response()
        response.headers.add('error_code', e.__str__())
        response.status_code = 490
        return response


@orderApi.route('/event/<start>/<end>', methods=['GET'])
def getEventWithRange(start, end):
    try:
        response = OrderInstance().getEventWithRange(start, end)
        return Response(json.dumps(response), mimetype='application/json')
    except(
        MissingRequiredParametersException,
        Exception,
        RuntimeError
    ) as e:
        response = Response()
        response.headers.add('error_code', e.__str__())
        response.status_code = 490
        return response


@orderApi.route('/event', methods=['GET'])
def getEvent():
    try:
        response = OrderInstance().getEvent()
        return Response(json.dumps(response), mimetype='application/json')
    except(
        MissingRequiredParametersException,
        Exception,
        RuntimeError
    ) as e:
        response = Response()
        response.headers.add('error_code', e.__str__())
        response.status_code = 490
        return response


@orderApi.route('/event', methods=['POST'])
def eventCreate():
    try:
        _token = request.json["_token"] if request.json["_token"] is not None else None
        event_id = request.json["event_id"] if request.json["event_id"] is not None else None
        user_uuid = request.json["user_uuid"] if request.json["user_uuid"] is not None else None
        order_item_id = request.json["order_item_id"] if request.json["order_item_id"] is not None else None
        product_name = request.json["product_name"] if request.json["product_name"] is not None else None
        start_time = request.json["start_time"] if request.json["start_time"] is not None else None
        end_time = request.json["end_time"] if request.json["end_time"] is not None else None
        note = request.json["note"] if request.json["note"] is not None else None
        event_employee_list = request.json["event_employee_list"] if request.json["event_employee_list"] is not None else None

        if _token is None:
            return redirect('/login')
        else:
            user_info = JWTInstance().decode_auth_token(_token)
            input_personnel_grade = int(user_info['grade'])
            personal_uuid = user_info['uuid']
            input_personnel = EmployeeInstance().getEmployeeIDByUuid(personal_uuid)

            function = FunctionTypeStatus.order
            permission = EmployeeInstance().getPermissionByGradeIDAndFunctionID(input_personnel_grade, function)
            # permission judgment(need write)
            if int(permission) == PermissionTypeStatus.close or int(permission) == PermissionTypeStatus.read_only:
                raise AuthenticateFailedException()

            if input_personnel is None or user_uuid is None or order_item_id is None or start_time is None or \
                            end_time is None or event_employee_list is None:
                raise MissingRequiredParametersException()
            else:
                # create event
                if event_id == "" or event_id is None:
                    user_id = MemberInstance().getUserIDByUuid(user_uuid)
                    # processing event_employee_list
                    event_employee_insert_list = OrderInstance().processing_event_employee_list(order_item_id, event_employee_list)
                    OrderInstance().check_quota_order_item(user_id, order_item_id)
                    # create new data
                    event_id = OrderInstance().create_event(input_personnel, user_id, order_item_id, product_name, start_time, end_time, note)
                    # move to another api
                    # OrderInstance().add_order_item_used(user_id, order_item_id)
                    OrderInstance().create_event_employee(event_employee_insert_list, event_id)
                    return Response(json.dumps({'event_id': event_id}), mimetype='application/json')

                # update event
                else:
                    user_id = MemberInstance().getUserIDByUuid(user_uuid)
                    # processing event_employee_list
                    event_employee_insert_list = OrderInstance().processing_event_employee_list(order_item_id, event_employee_list)
                    OrderInstance().check_quota_order_item(user_id, order_item_id)
                    # action
                    event_id = OrderInstance().update_event(input_personnel, event_id, order_item_id, product_name, start_time, end_time, note)
                    OrderInstance().delete_all_old_event(event_id)
                    OrderInstance().create_event_employee(event_employee_insert_list, event_id)
                    # event_employee_update_list = OrderInstance().processing_event_employee_list_update(order_item_id, event_employee_list)
                    # event_id = OrderInstance().update_event(input_personnel, event_id, start_time, end_time, note)
                    # OrderInstance().update_event_employee(event_employee_update_list)
                    return Response(json.dumps({'event_id': event_id}), mimetype='application/json')
    except(
        MissingRequiredParametersException,
        TargetIdDoesNotExistException,
        WrongFormatException,
        ItemExpiredException,
        InsufficientBalanceException,
        InsufficientInventoryException,
        ServicePartUnmatchException,
        EventAlreadyCheckedByDoctorException,
        AuthenticateFailedException,
        Exception,
        RuntimeError
    ) as e:
        response = Response()
        response.headers.add('error_code', e.__str__())
        response.status_code = 490
        return response


@orderApi.route('/event', methods=['PUT'])
def eventRegistered():
    try:
        _token = request.json["_token"] if request.json["_token"] is not None else None
        event_id = request.json["event_id"] if request.json["event_id"] is not None else None

        if _token is None:
            return redirect('/login')
        else:
            user_info = JWTInstance().decode_auth_token(_token)
            input_personnel_grade = int(user_info['grade'])
            personal_uuid = user_info['uuid']
            input_personnel = EmployeeInstance().getEmployeeIDByUuid(personal_uuid)

            function = FunctionTypeStatus.order
            permission = EmployeeInstance().getPermissionByGradeIDAndFunctionID(input_personnel_grade, function)
            # permission judgment(need write)
            if int(permission) == PermissionTypeStatus.close or int(permission) == PermissionTypeStatus.read_only:
                raise AuthenticateFailedException()

            if input_personnel is None or event_id is None:
                raise MissingRequiredParametersException()
            else:
                event_id = OrderInstance().registered_event(input_personnel, event_id)
                return Response(json.dumps({'event_id': event_id}), mimetype='application/json')
    except(
        MissingRequiredParametersException,
        TargetIdDoesNotExistException,
        WrongFormatException,
        InsufficientBalanceException,
        InsufficientInventoryException,
        ServicePartUnmatchException,
        AuthenticateFailedException,
        Exception,
        RuntimeError
    ) as e:
        response = Response()
        response.headers.add('error_code', e.__str__())
        response.status_code = 490
        return response


@orderApi.route('/event/doctor/check', methods=['PUT'])
def eventDoctorCheck():
    try:
        _token = request.json["_token"] if request.json["_token"] is not None else None
        event_id = request.json["event_id"] if request.json["event_id"] is not None else None

        if _token is None:
            return redirect('/login')
        else:
            user_info = JWTInstance().decode_auth_token(_token)
            input_personnel_grade = int(user_info['grade'])
            personal_uuid = user_info['uuid']
            input_personnel = EmployeeInstance().getEmployeeIDByUuid(personal_uuid)

            function = FunctionTypeStatus.order
            permission = EmployeeInstance().getPermissionByGradeIDAndFunctionID(input_personnel_grade, function)
            # permission judgment(need write)
            if int(permission) == PermissionTypeStatus.close or int(permission) == PermissionTypeStatus.read_only:
                raise AuthenticateFailedException()

            if input_personnel is None or event_id is None:
                raise MissingRequiredParametersException()
            else:
                user_id, order_item_id = OrderInstance().doctor_check_event(input_personnel, event_id)
                OrderInstance().add_order_item_used(user_id, order_item_id)
                return Response(json.dumps({'event_id': event_id}), mimetype='application/json')
    except(
        MissingRequiredParametersException,
        TargetIdDoesNotExistException,
        EventNotYetRegisterException,
        EventAlreadyCanceledException,
        EventAlreadyCheckedByDoctorException,
        AuthenticateFailedException,
        Exception,
        RuntimeError
    ) as e:
        response = Response()
        response.headers.add('error_code', e.__str__())
        response.status_code = 490
        return response


@orderApi.route('/event/cancel/<event_id>', methods=['DELETE'])
def eventCancel(event_id):
    try:
        _token = request.json["_token"] if request.json["_token"] is not None else None
        if _token is None:
            return redirect('/login')
        else:
            user_info = JWTInstance().decode_auth_token(_token)
            input_personnel_grade = int(user_info['grade'])
            personal_uuid = user_info['uuid']
            input_personnel = EmployeeInstance().getEmployeeIDByUuid(personal_uuid)

            function = FunctionTypeStatus.order
            permission = EmployeeInstance().getPermissionByGradeIDAndFunctionID(input_personnel_grade, function)
            # permission judgment(need write)
            if int(permission) == PermissionTypeStatus.close or int(permission) == PermissionTypeStatus.read_only:
                raise AuthenticateFailedException()

            if input_personnel is None or event_id is None:
                raise MissingRequiredParametersException()
            else:
                event_id = OrderInstance().cancel_event(input_personnel, event_id)
                OrderInstance().delete_event_employee(event_id)
                return Response(json.dumps({'event_id': event_id}), mimetype='application/json')
    except(
        MissingRequiredParametersException,
        TargetIdDoesNotExistException,
        PermissionDeniedException,
        AuthenticateFailedException,
        Exception,
        RuntimeError
    ) as e:
        response = Response()
        response.headers.add('error_code', e.__str__())
        response.status_code = 490
        return response


@orderApi.route('/export/excel/order/by/day/<int:year>/<int:month>/<int:day>', methods=['POST'])
def exportExcelOrderByDay(year, month, day):
    try:
        if year is None or month is None or day is None:
            raise MissingRequiredParametersException()
        else:
            # need to correspond Chinese Array, for catch data from function
            idList = ['order_num', 'user_name', 'user_mobile_phone', 'saler_name', 'employee_name', 'input_personnel', 'total_price', 'status_str', 'payment_type_str', 'shipping_type_str', 'date', 'modify_date', 'order_items_single_list', 'order_items_assemble_list']
            # for show name in excel file (title)
            idListName = ['訂單編號', '會員姓名', '會員行動電話', '銷售經理', '銷售醫美師', '訂單經手人', '訂單金額', '訂單狀態', '付款方式', '取貨方式', '訂單下訂時間', '訂單異動時間', '訂單項目(單品)', '訂單項目(組合)']
            day_str = (date(year, month, day)).strftime("%Y%m%d")
            filename = ConfigSetting().UPLOAD_FOLDER + "/" + day_str + "_all_day_order_data.xlsx"
            showname = day_str + "_all_day_order_data.xlsx"

            workbook = xlsxwriter.Workbook(filename)
            worksheet = workbook.add_worksheet()
            red_font = workbook.add_format({'bold': True, 'font_color': 'red'})

            for i in range(len(idListName)):
                worksheet.write(0, i, idListName[i])

            order_all_day_list, income_all_day = OrderInstance().getOrderAllDay(year, month, day)

            for j in range(len(order_all_day_list)):
                for i in range(len(idList)):
                    worksheet.write(j+1, i, order_all_day_list[j][idList[i]].__str__() if order_all_day_list[j][idList[i]] is not None else "")
                    # if idList[i] == 'order_items_single_list':
                    #     worksheet.write(j+1, i, order_all_day_list[j][idList[i]].__str__() if order_all_day_list[j][idList[i]] is not None else "")
                    # elif idList[i] == 'order_items_assemble_list':
                    #     worksheet.write(j+1, i, order_all_day_list[j][idList[i]].__str__() if order_all_day_list[j][idList[i]] is not None else "")
                    # else:
                    #     worksheet.write(j+1, i, order_all_day_list[j][idList[i]].__str__() if order_all_day_list[j][idList[i]] is not None else "")

            # list income
            line_beginning = len(order_all_day_list)+1

            worksheet.write(line_beginning+1, 0, income_all_day['income_cash_str'].__str__() if income_all_day['income_cash_str'].__str__() is not None else "")
            worksheet.write(line_beginning+1, 1, income_all_day['income_cash'].__str__() if income_all_day['income_cash'].__str__() is not None else "")

            worksheet.write(line_beginning+2, 0, income_all_day['income_points_str'].__str__() if income_all_day['income_points_str'].__str__() is not None else "")
            worksheet.write(line_beginning+2, 1, income_all_day['income_points'].__str__() if income_all_day['income_points'].__str__() is not None else "")

            worksheet.write(line_beginning+3, 0, income_all_day['income_creditcard_str'].__str__() if income_all_day['income_creditcard_str'].__str__() is not None else "")
            worksheet.write(line_beginning+3, 1, income_all_day['income_creditcard'].__str__() if income_all_day['income_creditcard'].__str__() is not None else "")

            worksheet.write(line_beginning+4, 0, income_all_day['income_creditloan_str'].__str__() if income_all_day['income_creditloan_str'].__str__() is not None else "")
            worksheet.write(line_beginning+4, 1, income_all_day['income_creditloan'].__str__() if income_all_day['income_creditloan'].__str__() is not None else "")

            worksheet.write(line_beginning+5, 0, income_all_day['income_total_str'].__str__() if income_all_day['income_total_str'].__str__() is not None else "", red_font)
            worksheet.write(line_beginning+5, 1, income_all_day['income_total'].__str__() if income_all_day['income_total'].__str__() is not None else "", red_font)

            workbook.close()
            with open(filename, 'rb') as f:
                return Response(
                    f.read(),
                    mimetype="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                    headers={
                        "Content-disposition": "attachment; filename=" + showname,
                        "Content-type": "application/force-download",
                        "Content-Transfer-Encoding": "Binary"
                    }
                )
    except(
        TargetIdDoesNotExistException,
        EmployeeListEmptyException,
        MissingRequiredParametersException,
        Exception,
        RuntimeError
    ) as e:
        response = Response()
        response.headers.add('error_code', e.__str__())
        response.status_code = 490
        error = {
            'code': [4, 9, 0],
            'str': e.__str__()
        }
        return render_template('404.html', error=error)


@orderApi.route('/export/excel/event/by/day/<int:year>/<int:month>/<int:day>', methods=['POST'])
def exportExcelEventByDay(year, month, day):
    try:
        if year is None or month is None or day is None:
            raise MissingRequiredParametersException()
        else:
            # need to correspond Chinese Array, for catch data from function
            idList = ['user_number', 'user_name', 'user_grade', 'user_mobile_phone', 'doctors', 'managers', 'medical_artists', 'nurses', 'evnet_product', 'status']
            # for show name in excel file (title)
            idListName = ['會員編號', '會員姓名', '會員等級', '會員行動電話', '醫師', '經理', '醫美師', '護士', '預約課程', '預約狀態']
            day_str = (date(year, month, day)).strftime("%Y%m%d")
            filename = ConfigSetting().UPLOAD_FOLDER + "/" + day_str + "_all_day_event_data.xlsx"
            showname = day_str + "_all_day_event_data.xlsx"

            workbook = xlsxwriter.Workbook(filename)
            worksheet = workbook.add_worksheet()

            for i in range(len(idListName)):
                worksheet.write(0, i, idListName[i])

            event_all_day_list = OrderInstance().getEventAllDay(year, month, day)

            for j in range(len(event_all_day_list)):
                for i in range(len(idList)):
                    worksheet.write(j+1, i, event_all_day_list[j][idList[i]].__str__() if event_all_day_list[j][idList[i]] is not None else "")
            workbook.close()
            with open(filename, 'rb') as f:
                return Response(
                    f.read(),
                    mimetype="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                    headers={
                        "Content-disposition": "attachment; filename=" + showname,
                        "Content-type": "application/force-download",
                        "Content-Transfer-Encoding": "Binary"
                    }
                )
    except(
        TargetIdDoesNotExistException,
        EmployeeListEmptyException,
        MissingRequiredParametersException,
        Exception,
        RuntimeError
    ) as e:
        response = Response()
        response.headers.add('error_code', e.__str__())
        response.status_code = 490
        error = {
            'code': [4, 9, 0],
            'str': e.__str__()
        }
        return render_template('404.html', error=error)


@orderApi.route('/export/excel/point/by/day/<int:year>/<int:month>/<int:day>', methods=['POST'])
def exportExcelPointByDay(year, month, day):
    try:
        if year is None or month is None or day is None:
            raise MissingRequiredParametersException()
        else:
            # need to correspond Chinese Array, for catch data from function
            idList = ['user_number', 'user_name', 'user_grade', 'user_mobile_phone', 'input_personnel', 'point', 'point_number', 'order_number', 'date']
            # for show name in excel file (title)
            idListName = ['會員編號', '會員姓名', '會員等級', '會員行動電話', '經手人', '點數', '儲值單號', '訂單單號', '儲值時間']
            day_str = (date(year, month, day)).strftime("%Y%m%d")
            filename = ConfigSetting().UPLOAD_FOLDER + "/" + day_str + "_all_day_point_data.xlsx"
            showname = day_str + "_all_day_point_data.xlsx"

            workbook = xlsxwriter.Workbook(filename)
            worksheet = workbook.add_worksheet()

            for i in range(len(idListName)):
                worksheet.write(0, i, idListName[i])

            point_all_day_list = OrderInstance().getPointAllDay(year, month, day)

            for j in range(len(point_all_day_list)):
                for i in range(len(idList)):
                    worksheet.write(j+1, i, point_all_day_list[j][idList[i]].__str__() if point_all_day_list[j][idList[i]] is not None else "")
            workbook.close()
            with open(filename, 'rb') as f:
                return Response(
                    f.read(),
                    mimetype="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                    headers={
                        "Content-disposition": "attachment; filename=" + showname,
                        "Content-type": "application/force-download",
                        "Content-Transfer-Encoding": "Binary"
                    }
                )
    except(
        TargetIdDoesNotExistException,
        EmployeeListEmptyException,
        MissingRequiredParametersException,
        Exception,
        RuntimeError
    ) as e:
        response = Response()
        response.headers.add('error_code', e.__str__())
        response.status_code = 490
        error = {
            'code': [4, 9, 0],
            'str': e.__str__()
        }
        return render_template('404.html', error=error)


@orderApi.route('/export/excel/supply/by/day/<int:year>/<int:month>/<int:day>', methods=['POST'])
def exportExcelSupplyByDay(year, month, day):
    try:
        if year is None or month is None or day is None:
            raise MissingRequiredParametersException()
        else:
            # only need
            # need to correspond Chinese Array, for catch data from function
            idList = ['supply_name', 'quantity', 'input_personnel', 'note', 'date']
            # for show name in excel file (title)
            idListName = ['庫存類別', '數量', '經手人', '附註', '時間']
            day_str = (date(year, month, day)).strftime("%Y%m%d")
            filename = ConfigSetting().UPLOAD_FOLDER + "/" + day_str + "_all_day_supply_data.xlsx"
            showname = day_str + "_all_day_supply_data.xlsx"

            workbook = xlsxwriter.Workbook(filename)
            worksheet = workbook.add_worksheet()
            red_font = workbook.add_format({'bold': True, 'font_color': 'red'})

            row_no = 0

            for i in range(len(idListName)):
                worksheet.write(row_no, i, idListName[i])
            row_no = row_no + 1

            supply_all_day_list = OrderInstance().getSupplyAllDay(year, month, day)
            dataGetSupplies = InventoryInstance().getSupplies()
            suppliesList = dataGetSupplies['suppliesList']

            for k in range(len(suppliesList)):
                for j in range(len(supply_all_day_list)):
                    if suppliesList[k]['id'] == supply_all_day_list[j]['supplies_id']:
                        for i in range(len(idList)):
                            worksheet.write(row_no, i, supply_all_day_list[j][idList[i]].__str__() if supply_all_day_list[j][idList[i]] is not None else "")
                        row_no = row_no + 1
                    else:
                        continue

                worksheet.write(row_no, 0, suppliesList[k]['name'].__str__() if suppliesList[k]['name'] is not None else "")
                worksheet.write(row_no, 1, suppliesList[k]['num_counter'].__str__() if suppliesList[k]['num_counter'] is not None else "", red_font)
                row_no = row_no + 1
            workbook.close()
            with open(filename, 'rb') as f:
                return Response(
                    f.read(),
                    mimetype="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                    headers={
                        "Content-disposition": "attachment; filename=" + showname,
                        "Content-type": "application/force-download",
                        "Content-Transfer-Encoding": "Binary"
                    }
                )
    except(
        TargetIdDoesNotExistException,
        EmployeeListEmptyException,
        MissingRequiredParametersException,
        Exception,
        RuntimeError
    ) as e:
        response = Response()
        response.headers.add('error_code', e.__str__())
        response.status_code = 490
        error = {
            'code': [4, 9, 0],
            'str': e.__str__()
        }
        return render_template('404.html', error=error)
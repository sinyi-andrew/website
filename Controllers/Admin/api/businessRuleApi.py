from flask import Blueprint, render_template, request, Response
from datetime import datetime, date, timedelta
import json

from Model.BusinessRule import BusinessRuleInstance
from Model.Employee import EmployeeInstance
from Model.Product import ProductInstance
from Model.Exception import *
from Utility.Status import *

businessRuleApi = Blueprint('businessRuleApi', __name__)


def init_app(app):
    app.register_blueprint(businessRuleApi, url_prefix='/admin/businessRule/api')


@businessRuleApi.route('/businessRule', methods=['GET'])
def businessRule():
    try:
        personal_uuid = '9984aa8e-cf81-476d-a809-538d6f453592' # TODO: input_personnel get from token
        personal_id = EmployeeInstance().getEmployeeIDByUuid(personal_uuid)
        employeeList = EmployeeInstance().getEmployeeIDList()
        productList = ProductInstance().getProductList()

        dateToday = date.today()
        numOfUsers = BusinessRuleInstance().getNumOfUsers()
        allUnusedAmount = BusinessRuleInstance().getAllUnusedAmount()
        salesToday = BusinessRuleInstance().getSalesByDate(dateToday)
        salesTodayPersonal = BusinessRuleInstance().getSalesByDateOnlyPersonal(dateToday, personal_id)

        targetYear = int(dateToday.strftime('%Y'))
        targetMonth = int(dateToday.strftime('%m'))
        # targetMonth = int(4)
        salesThisMonth = BusinessRuleInstance().getSalesByMonth(targetYear, targetMonth)
        salesThisMonthPersonal = BusinessRuleInstance().getSalesByMonthOnlyPersonal(targetYear, targetMonth, personal_id)

        salesThisMonthAllPersonal = BusinessRuleInstance().getSalesByMonthAllPersonal(targetYear, targetMonth, employeeList)
        salesThisMonthAllDays = BusinessRuleInstance().getSalesByMonthAllDays(targetYear, targetMonth)
        salesThisMonthAllProducts = BusinessRuleInstance().getSalesByMonthAllProducts(targetYear, targetMonth, productList)
        salesThisYearAllMonths = BusinessRuleInstance().getSalesByYearAllMonths(targetYear)

        response = {
            'numOfUsers': numOfUsers,
            'allUnusedAmount': allUnusedAmount,
            'salesToday': salesToday,
            'salesTodayPersonal': salesTodayPersonal,
            'salesThisMonth': salesThisMonth,
            'salesThisMonthPersonal': salesThisMonthPersonal,
            'salesThisMonthAllPersonal': salesThisMonthAllPersonal,
            'salesThisMonthAllDays': salesThisMonthAllDays,
            'salesThisMonthAllProducts': salesThisMonthAllProducts,
            'salesThisYearAllMonths': salesThisYearAllMonths
        }
        return Response(json.dumps(response), mimetype='application/json')
    except(
            MissingRequiredParametersException,
            Exception,
            RuntimeError
    ) as e:
        response = Response()
        response.headers.add('error_code', e.__str__())
        response.status_code = 490
        return response


@businessRuleApi.route('/get/sales/by/month/all/personal/<int:year>/<int:month>', methods=['GET'])
def getSalesByMonthAllPersonal(year, month):
    try:
        if year is None or month is None:
            raise MissingRequiredParametersException()

        employeeList = EmployeeInstance().getEmployeeIDListOnlySeller()
        targetYear = int(year)
        targetMonth = int(month)
        salesByMonthAllPersonal = BusinessRuleInstance().getSalesByMonthAllPersonal(targetYear, targetMonth, employeeList)
        return Response(json.dumps({'salesByMonthAllPersonal': salesByMonthAllPersonal}), mimetype='application/json')
    except(
            MissingRequiredParametersException,
            Exception,
            RuntimeError
    ) as e:
        response = Response()
        response.headers.add('error_code', e.__str__())
        response.status_code = 490
        return response


@businessRuleApi.route('/get/sales/by/month/all/days/<int:year>/<int:month>', methods=['GET'])
def getSalesByMonthAllDays(year, month):
    try:
        if year is None or month is None:
            raise MissingRequiredParametersException()

        targetYear = int(year)
        targetMonth = int(month)
        salesByMonthAllDays = BusinessRuleInstance().getSalesByMonthAllDays(targetYear, targetMonth)
        return Response(json.dumps({'salesByMonthAllDays': salesByMonthAllDays}), mimetype='application/json')
    except(
            MissingRequiredParametersException,
            Exception,
            RuntimeError
    ) as e:
        response = Response()
        response.headers.add('error_code', e.__str__())
        response.status_code = 490
        return response


@businessRuleApi.route('/get/sales/by/month/all/products/<int:year>/<int:month>', methods=['GET'])
def getSalesByMonthAllProducts(year, month):
    try:
        if year is None or month is None:
            raise MissingRequiredParametersException()

        targetYear = int(year)
        targetMonth = int(month)
        productList = ProductInstance().getProductList()
        salesByMonthAllProducts = BusinessRuleInstance().getSalesByMonthAllProducts(targetYear, targetMonth, productList)
        return Response(json.dumps({'salesByMonthAllProducts': salesByMonthAllProducts}), mimetype='application/json')
    except(
            MissingRequiredParametersException,
            Exception,
            RuntimeError
    ) as e:
        response = Response()
        response.headers.add('error_code', e.__str__())
        response.status_code = 490
        return response


@businessRuleApi.route('/get/sales/by/month/all/services/<int:year>/<int:month>', methods=['GET'])
def getSalesByMonthAllServices(year, month):
    try:
        if year is None:
            raise MissingRequiredParametersException()

        targetYear = int(year)
        targetMonth = int(month)
        productList = ProductInstance().getProductList()
        salesByMonthAllServices = BusinessRuleInstance().getSalesByMonthAllServices(targetYear, targetMonth, productList)
        return Response(json.dumps({'salesByMonthAllServices': salesByMonthAllServices}), mimetype='application/json')
    except(
            MissingRequiredParametersException,
            Exception,
            RuntimeError
    ) as e:
        response = Response()
        response.headers.add('error_code', e.__str__())
        response.status_code = 490
        return response


@businessRuleApi.route('/get/sales/by/year/all/products/<int:year>/<int:month>', methods=['GET'])
def getSalesByYearAllProducts(year, month):
    try:
        if year is None:
            raise MissingRequiredParametersException()

        targetYear = int(year)
        targetMonth = int(month)
        productCategoryList = ProductInstance().getProductCategoryList()
        salesByYearAllProducts = BusinessRuleInstance().getSalesByYearAllProducts(targetYear, targetMonth, productCategoryList)
        return Response(json.dumps({'salesByYearAllProducts': salesByYearAllProducts}), mimetype='application/json')
    except(
            MissingRequiredParametersException,
            Exception,
            RuntimeError
    ) as e:
        response = Response()
        response.headers.add('error_code', e.__str__())
        response.status_code = 490
        return response


@businessRuleApi.route('/get/sales/by/year/all/months/<int:year>/<int:month>', methods=['GET'])
def getSalesByYearAllMonths(year, month):
    try:
        if year is None:
            raise MissingRequiredParametersException()

        targetYear = int(year)
        targetMonth = int(month)
        salesByYearAllMonths = BusinessRuleInstance().getSalesByYearAllMonths(targetYear, targetMonth)
        return Response(json.dumps({'salesByYearAllMonths': salesByYearAllMonths}), mimetype='application/json')
    except(
            MissingRequiredParametersException,
            Exception,
            RuntimeError
    ) as e:
        response = Response()
        response.headers.add('error_code', e.__str__())
        response.status_code = 490
        return response


@businessRuleApi.route('/get/event/num/by/month/all/days/<int:year>/<int:month>', methods=['GET'])
def getEventNumByMonthAllDays(year, month):
    try:
        if year is None or month is None:
            raise MissingRequiredParametersException()

        targetYear = int(year)
        targetMonth = int(month)
        eventNumByMonthAllDays = BusinessRuleInstance().getEventNumByMonthAllDays(targetYear, targetMonth)
        return Response(json.dumps({'eventNumByMonthAllDays': eventNumByMonthAllDays}), mimetype='application/json')
    except(
            MissingRequiredParametersException,
            Exception,
            RuntimeError
    ) as e:
        response = Response()
        response.headers.add('error_code', e.__str__())
        response.status_code = 490
        return response


@businessRuleApi.route('/get/unused/event/num/by/day/all/products', methods=['GET'])
def getUnusedEventNumByDayAllProducts():
    try:
        productList = ProductInstance().getProductList()
        unusedEventNumByDayAllProducts = BusinessRuleInstance().getUnusedEventNumByDayAllProducts(productList)
        return Response(json.dumps({'unusedEventNumByDayAllProducts': unusedEventNumByDayAllProducts}), mimetype='application/json')
    except(
            MissingRequiredParametersException,
            Exception,
            RuntimeError
    ) as e:
        response = Response()
        response.headers.add('error_code', e.__str__())
        response.status_code = 490
        return response
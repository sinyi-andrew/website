from flask import Blueprint, render_template, request, Response, redirect

from Model.Exception import *
from Model.Employee import EmployeeInstance
from Model.JWT import JWTInstance
from Model.Setting import SettingInstance
from Utility.Status import *

settingApi = Blueprint('settingApi', __name__)


def init_app(app):
    app.register_blueprint(settingApi, url_prefix='/admin/setting/api')


@settingApi.route('/brand', methods=['POST'])
def brandUpdate():
    try:
        store_id = request.form["store_id"] if request.form["store_id"] is not None else None
        slogan = request.form["slogan"] if request.form["slogan"] is not None else None
        story = request.form["story"] if request.form["story"] is not None else None
        idea = request.form["idea"] if request.form["idea"] is not None else None

        if slogan is None or story is None or idea is None:
            raise MissingCriticalParametersException()
        else:
            instance = SettingInstance()
            response = instance.updateBrand(store_id, slogan, story, idea)

            return render_template("admin/redirect.html",path='/admin/setting/brand')
    except (
            MissingCriticalParametersException,
            Exception,
            RuntimeError
    ) as e:
        return 'error'


@settingApi.route('/story', methods=['POST'])
def storyUpdate():
    try:
        store_id = request.form["store_id"] if request.form["store_id"] is not None else None
        about = request.form["about"] if request.form["about"] is not None else None

        if about is None:
            raise MissingCriticalParametersException()
        else:
            instance = SettingInstance()
            response = instance.updateAbout(store_id, about)

            return render_template("admin/redirect.html",path='/admin/setting/story')
    except (
            MissingCriticalParametersException,
            Exception,
            RuntimeError
    ) as e:
        return 'error'


# TODO: add permission judgment
@settingApi.route('/news', methods=['POST'])
@settingApi.route('/shares', methods=['POST'])
def newsCreate():
    try:
        store_id = request.form["store_id"] if request.form["store_id"] is not None else None
        types = str(request.form["type"]) if request.form["type"] is not None else None
        title = request.form["title"] if request.form["title"] is not None else None
        content = request.form["content"] if request.form["content"] is not None else None
        author = request.form["author"] if request.form["author"] is not None else None
        status = int(request.form["status"]) if request.form["status"] is not None else None
        if store_id is None or title is None or content is None or types is None or status is None:
            raise MissingCriticalParametersException()

        instance = SettingInstance()
        response = instance.createNews(store_id, types, title, content, author, status)
        print(response)
        return render_template("admin/redirect.html",path='/admin/setting/' + types)
        # _token = request.form["_token"] if request.form["_token"] is not None else None
        # store_id = request.form["store_id"] if request.form["store_id"] is not None else None
        # types = str(request.form["type"]) if request.form["type"] is not None else None
        # title = request.form["title"] if request.form["title"] is not None else None
        # content = request.form["content"] if request.form["content"] is not None else None
        # author = request.form["author"] if request.form["author"] is not None else None
        # status = int(request.form["status"]) if request.form["status"] is not None else None
        #
        # if _token is None:
        #     return redirect('/login')
        # else:
        #     user_info = JWTInstance().decode_auth_token(_token)
        #     input_personnel_grade = int(user_info['grade'])
        #     personal_uuid = user_info['uuid']
        #     input_personnel = EmployeeInstance().getEmployeeIDByUuid(personal_uuid)
        #
        #     function = FunctionTypeStatus.setting
        #     permission = EmployeeInstance().getPermissionByGradeIDAndFunctionID(input_personnel_grade, function)
        #     # permission judgment(need write)
        #     if int(permission) == PermissionTypeStatus.close or int(permission) == PermissionTypeStatus.read_only:
        #         raise AuthenticateFailedException()
        #
        #     if input_personnel is None or store_id is None or title is None or content is None or types is None or status is None:
        #         raise MissingCriticalParametersException()
        #     else:
        #         instance = SettingInstance()
        #         response = instance.createNews(store_id, types, title, content, author, status)
        #         print(response)
        #         return render_template("admin/redirect.html",path='/admin/setting/' + types)
    except (
            MissingCriticalParametersException,
            AuthenticateFailedException,
            Exception,
            RuntimeError
    ) as e:
        return 'error'


# TODO: add permission judgment
@settingApi.route('/news/<id>', methods=['POST'])
@settingApi.route('/shares/<id>', methods=['POST'])
def newsUpdate(id):
    try:
        id = int(id)
        title = request.form["title"] if request.form["title"] is not None else None
        types = str(request.form["type"]) if request.form["type"] is not None else None
        content = request.form["content"] if request.form["content"] is not None else None
        author = request.form["author"] if request.form["author"] is not None else None
        status = int(request.form["status"]) if request.form["status"] is not None else None

        if id is None or title is None or content is None or author is None:
            raise MissingCriticalParametersException()
        else:
            instance = SettingInstance()
            response = instance.updateNews(id, title, content, author, status)
            print(response)
            # return render_template("admin/redirect.html",path='/admin/setting/' + types)
            return render_template("admin/redirect.html",path='/admin/setting/' + types)

        # _token = request.form["_token"] if request.form["_token"] is not None else None
        # id = int(id)
        # title = request.form["title"] if request.form["title"] is not None else None
        # types = str(request.form["type"]) if request.form["type"] is not None else None
        # content = request.form["content"] if request.form["content"] is not None else None
        # author = request.form["author"] if request.form["author"] is not None else None
        # status = int(request.form["status"]) if request.form["status"] is not None else None
        #
        # if _token is None:
        #     return redirect('/login')
        # else:
        #     user_info = JWTInstance().decode_auth_token(_token)
        #     input_personnel_grade = int(user_info['grade'])
        #     personal_uuid = user_info['uuid']
        #     input_personnel = EmployeeInstance().getEmployeeIDByUuid(personal_uuid)
        #
        #     function = FunctionTypeStatus.setting
        #     permission = EmployeeInstance().getPermissionByGradeIDAndFunctionID(input_personnel_grade, function)
        #     # permission judgment(need write)
        #     if int(permission) == PermissionTypeStatus.close or int(permission) == PermissionTypeStatus.read_only:
        #         raise AuthenticateFailedException()
        #
        #     if id is None or title is None or content is None or author is None:
        #         raise MissingCriticalParametersException()
        #     else:
        #         instance = SettingInstance()
        #         response = instance.updateNews(id, title, content, author, status)
        #         print(response)
        #         # return render_template("admin/redirect.html",path='/admin/setting/' + types)
        #         return render_template("admin/redirect.html",path='/admin/setting/' + types)
    except (
            MissingCriticalParametersException,
            AuthenticateFailedException,
            Exception,
            RuntimeError
    ) as e:
        return 'error'


@settingApi.route('/news/destroy/<id>', methods=['GET'])
def newsDestroy(id):
    try:
        id = int(id)

        if id is None:
            raise MissingCriticalParametersException()
        else:
            instance = SettingInstance()
            response = instance.deleteNews(id)
            print(response)
            return render_template("admin/redirect.html",path='/admin/setting/' + response)
    except (
            MissingCriticalParametersException,
            Exception,
            RuntimeError
    ) as e:
        return 'error'


@settingApi.route('/slider/create', methods=['POST'])
def slideCreate():
    try:
        product_id = request.form["product_id"] if request.form["product_id"] is not None else None
        newImgBase64 = request.form["newImgBase64"] if request.form["newImgBase64"] is not None else None
        sort = request.form["sort"] if request.form["sort"] is not None else None
        imgName = request.form["imgName"] if request.form["imgName"] is not None else None

        if newImgBase64 is None or imgName is None:
            raise MissingCriticalParametersException()
        else:
            instance = SettingInstance()
            response = instance.createSlider(product_id, newImgBase64, sort, imgName)
            print(response)
            return render_template("admin/redirect.html",path='/admin/setting/slider')
    except (
            MissingCriticalParametersException,
            Exception,
            RuntimeError
    ) as e:
        return e.__str__()

@settingApi.route('/slider/update', methods=['POST'])
def slideUpdate():
    try:
        id = request.form["id"] if request.form["id"] is not None else None
        product_id = request.form["product_id"] if request.form["product_id"] is not None else None
        newImgBase64 = request.form["newImgBase64"] if request.form["newImgBase64"] is not None else None
        sort = request.form["sort"] if request.form["sort"] is not None else None
        imgName = request.form["imgName"] if request.form["imgName"] is not None else None

        if newImgBase64 is None or imgName is None:
            raise MissingCriticalParametersException()
        else:
            instance = SettingInstance()
            response = instance.updateSlider(id, product_id, newImgBase64, sort, imgName)
            print(response)
            return render_template("admin/redirect.html",path='/admin/setting/slider')
    except (
            MissingCriticalParametersException,
            Exception,
            RuntimeError
    ) as e:
        return e.__str__()

@settingApi.route('/slider/delete', methods=['POST'])
def slideDelete():
    try:
        id = request.json["id"] if "id" in request.json else None

        if id is None:
            raise MissingCriticalParametersException()
        else:
            instance = SettingInstance()
            response = instance.deleteSlider(id)
            print(response)
            return render_template("admin/redirect.html",path='/admin/setting/slider')
    except (
            MissingCriticalParametersException,
            Exception,
            RuntimeError
    ) as e:
        return e.__str__()
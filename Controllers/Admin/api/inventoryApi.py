from flask import Blueprint, render_template, Response, request, redirect
import json

from Model.Exception import *
from Model.Employee import EmployeeInstance
from Model.Inventory import InventoryInstance
from Model.JWT import JWTInstance
from Model.BusinessRule import BusinessRuleInstance
from Utility.Status import *

inventoryApi = Blueprint('inventoryApi', __name__)


def init_app(app):
    app.register_blueprint(inventoryApi, url_prefix='/admin/inventory/api')


@inventoryApi.route('/supplies', methods=['GET'])
def getSupplies():
    try:
        response = InventoryInstance().getSupplies()
        return Response(json.dumps(response), mimetype='application/json')
    except(
        MissingRequiredParametersException,
        Exception,
        RuntimeError
    ) as e:
        response = Response()
        response.headers.add('error_code', e.__str__())
        response.status_code = 490
        return response


@inventoryApi.route('/supplies', methods=['POST'])
def suppliesCreateOrUpdate():
    try:
        _token = request.json["_token"] if request.json["_token"] is not None else None
        supplies_id = request.json["supplies_id"] if request.json["supplies_id"] is not None else None
        products_id = request.json["products_id"] if request.json["products_id"] is not None else None
        name = request.json["name"] if request.json["name"] is not None else None
        cost = request.json["cost"] if request.json["cost"] is not None else None

        if _token is None:
            return redirect('/login')
        else:
            user_info = JWTInstance().decode_auth_token(_token)
            input_personnel_grade = int(user_info['grade'])
            personal_uuid = user_info['uuid']
            input_personnel = EmployeeInstance().getEmployeeIDByUuid(personal_uuid)

            ## remove permission judgment
            # function = FunctionTypeStatus.supplies_warehouse
            # permission = EmployeeInstance().getPermissionByGradeIDAndFunctionID(input_personnel_grade, function)
            # # permission judgment(need write)
            # if int(permission) == PermissionTypeStatus.close or int(permission) == PermissionTypeStatus.read_only:
            #     raise AuthenticateFailedException()

            # products_id -> None: Clinic supplies
            if name is None or cost is None:
                raise MissingRequiredParametersException()
            else:
                response = InventoryInstance().create_or_update_supplies(input_personnel, supplies_id, products_id, name, cost)
                return Response(json.dumps(response), mimetype='application/json')
    except(
        MissingRequiredParametersException,
        TargetIdDoesNotExistException,
        Exception,
        RuntimeError
    ) as e:
        response = Response()
        response.headers.add('error_code', e.__str__())
        response.status_code = 490
        return response


@inventoryApi.route('/get/inventory/by/skuid/<int:sku_id>', methods=['GET'])
def getInventoryBySkuID(sku_id):
    try:
        product_inventory = InventoryInstance().getInventoryBySkuID(sku_id)
        return Response(json.dumps({'product_inventory': product_inventory}), mimetype='application/json')
    except(
        MissingRequiredParametersException,
        Exception,
        RuntimeError
    ) as e:
        response = Response()
        response.headers.add('error_code', e.__str__())
        response.status_code = 490
        return response


@inventoryApi.route('/inventory', methods=['GET'])
def getInventory():
    try:
        response = InventoryInstance().getInventory()
        return Response(json.dumps(response), mimetype='application/json')
    except(
        MissingRequiredParametersException,
        Exception,
        RuntimeError
    ) as e:
        response = Response()
        response.headers.add('error_code', e.__str__())
        response.status_code = 490
        return response


@inventoryApi.route('/inventory/by/supplyID/<int:supply_id>', methods=['GET'])
def getInventoryBySupplyId(supply_id):
    try:
        response = InventoryInstance().getInventoryBySupplyId(supply_id)
        return Response(json.dumps(response), mimetype='application/json')
    except(
        MissingRequiredParametersException,
        Exception,
        RuntimeError
    ) as e:
        response = Response()
        response.headers.add('error_code', e.__str__())
        response.status_code = 490
        return response


@inventoryApi.route('/inventory', methods=['POST'])
def inventoryCreateOrUpdate():
    try:
        _token = request.json["_token"] if request.json["_token"] is not None else None
        inventory_id = request.json["inventory_id"] if request.json["inventory_id"] is not None else None
        supplies_id = request.json["supplies_id"] if request.json["supplies_id"] is not None else None
        quantity = request.json["quantity"] if request.json["quantity"] is not None else None
        note = request.json["note"] if request.json["note"] is not None else None
        type = request.json["type"] if request.json["type"] is not None else None

        if _token is None:
            return redirect('/login')
        else:
            user_info = JWTInstance().decode_auth_token(_token)
            input_personnel_grade = int(user_info['grade'])
            personal_uuid = user_info['uuid']
            input_personnel = EmployeeInstance().getEmployeeIDByUuid(personal_uuid)

            if int(type) == InventoryTypeStatus.warehouse:
                function = FunctionTypeStatus.supplies_warehouse
            else:
                if int(quantity) >= 0:
                    function = FunctionTypeStatus.supplies_warehouse
                else:
                    function = FunctionTypeStatus.supplies_counter

            permission = EmployeeInstance().getPermissionByGradeIDAndFunctionID(input_personnel_grade, function)
            # permission judgment(need write)
            if int(permission) == PermissionTypeStatus.close or int(permission) == PermissionTypeStatus.read_only:
                raise AuthenticateFailedException()

            if supplies_id is None or quantity is None or type is None:
                raise MissingRequiredParametersException()
            else:
                response = InventoryInstance().create_or_update_inventory(input_personnel, inventory_id, supplies_id, quantity,
                                                                          note, type)
                return Response(json.dumps(response), mimetype='application/json')
    except(
        MissingRequiredParametersException,
        TargetIdDoesNotExistException,
        AuthenticateFailedException,
        Exception,
        RuntimeError
    ) as e:
        response = Response()
        response.headers.add('error_code', e.__str__())
        response.status_code = 490
        return response

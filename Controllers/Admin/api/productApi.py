from flask import Blueprint, render_template, Response, request, redirect, url_for
import json

from Model.Exception import *
from Model.Employee import EmployeeInstance
from Model.Product import ProductInstance
from Model.BusinessRule import BusinessRuleInstance
from Model.JWT import JWTInstance
from Utility.Status import *

productApi = Blueprint('productApi', __name__)


def init_app(app):
    app.register_blueprint(productApi, url_prefix='/admin/product/api')


@productApi.route('/get/product/by/category_id/<int:category_id>', methods=['GET'])
def getProductDetail(category_id):
    try:
        if category_id is None:
            raise MissingRequiredParametersException()
        else:
            response = BusinessRuleInstance().getProductDetailByCategoryId(category_id)

            return Response(json.dumps(response), mimetype='application/json')
    except(
            MissingRequiredParametersException,
            TargetIdDoesNotExistException,
            Exception,
            RuntimeError
    ) as e:
        response = Response()
        response.headers.add('error_code', e.__str__())
        response.status_code = 490
        return response


@productApi.route('/get/product/by/product_id/<int:product_id>', methods=['GET'])
def productDetail(product_id):
    try:
        if product_id is None:
            raise MissingRequiredParametersException()
        else:
            response = BusinessRuleInstance().getProductDetailByProductsId(product_id)
            return Response(json.dumps(response), mimetype='application/json')
    except(
            MissingRequiredParametersException,
            TargetIdDoesNotExistException,
            Exception,
            RuntimeError
    ) as e:
        response = Response()
        response.headers.add('error_code', e.__str__())
        response.status_code = 490
        return response


@productApi.route('/get/image/<int:type>/by/<int:id>', methods=['GET'])
def getImageByID(type, id):
    try:
        if type is None or id is None:
            raise MissingRequiredParametersException()

        response = ProductInstance().getImageByID(type, id)
        return Response(json.dumps(response), mimetype='application/json')
    except(
            MissingRequiredParametersException,
            TypeDoesNotExistException,
            Exception,
            RuntimeError
    ) as e:
        response = Response()
        response.headers.add('error_code', e.__str__())
        response.status_code = 490
        return response


@productApi.route('/image', methods=['POST'])
def imageUpdate():
    try:
        type = request.json["type"] if request.json["type"] is not None else None
        id = request.json["id"] if request.json["id"] is not None else None
        image_input = request.json["image"] if request.json["image"] is not None else None

        if type is None or id is None or image_input is None or image_input == '':
            raise MissingRequiredParametersException()

        response = ProductInstance().update_image(type, id, image_input)
        return Response(json.dumps(response), mimetype='application/json')
    except(
            MissingRequiredParametersException,
            TargetIdDoesNotExistException,
            TypeDoesNotExistException,
            Exception,
            RuntimeError
    ) as e:
        response = Response()
        response.headers.add('error_code', e.__str__())
        response.status_code = 490
        return response


@productApi.route('/get/serviceCategory')
def getServiceCategory():
    try:
        response = ProductInstance().getServiceCategiry()
        return Response(json.dumps(response), mimetype='application/json')
    except(
            MissingRequiredParametersException,
            Exception,
            RuntimeError
    ) as e:
        response = Response()
        response.headers.add('error_code', e.__str__())
        response.status_code = 490
        return response


@productApi.route('/get/serviceCategory/by/<int:id>')
def getServiceCategoryByID(id):
    try:
        response = BusinessRuleInstance().getServiceCategoryByID(id)
        return Response(json.dumps(response), mimetype='application/json')
    except(
            MissingRequiredParametersException,
            TargetIdDoesNotExistException,
            Exception,
            RuntimeError
    ) as e:
        response = Response()
        response.headers.add('error_code', e.__str__())
        response.status_code = 490
        return response


'''product1: product part'''


@productApi.route('/product', methods=['POST'])
def productCreateOrUpdate():
    try:
        products_id = request.json["products_id"] if request.json["products_id"] is not None else None
        name = request.json["name"] if request.json["name"] is not None else None
        image = request.json["image"] if request.json["image"] is not None else None
        discount = request.json["discount"] if request.json["discount"] is not None else None
        intro = request.json["intro"] if request.json["intro"] is not None else None
        description = request.json["description"] if request.json["description"] is not None else None
        ingredient = request.json["ingredient"] if request.json["ingredient"] is not None else None
        care = request.json["care"] if request.json["care"] is not None else None
        notice = request.json["notice"] if request.json["notice"] is not None else None
        faq = request.json["faq"] if request.json["faq"] is not None else None
        note = request.json["note"] if request.json["note"] is not None else None
        tip = request.json["tip"] if request.json["tip"] is not None else None
        sort = int(request.json["sort"]) if int(request.json["sort"]) is not None else 99
        status = request.json["status"] if request.json["status"] is not None else None

        if name is None or image is None or discount is None or status is None:
            raise MissingRequiredParametersException()
        else:
            response = ProductInstance().create_or_update_product(products_id, name, image, discount, intro,
                                                                  description, ingredient, care, notice, faq, note,
                                                                  tip, sort, status)
            return Response(json.dumps(response), mimetype='application/json')
    except(
            MissingRequiredParametersException,
            TargetIdDoesNotExistException,
            Exception,
            RuntimeError
    ) as e:
        response = Response()
        response.headers.add('error_code', e.__str__())
        response.status_code = 490
        return response


'''end product1: product part'''
'''product2: assemble part'''


@productApi.route('/assemble', methods=['POST'])
def assembleCreateOrUpdate():
    try:
        assemble_id = request.json["assemble_id"] if request.json["assemble_id"] is not None else None
        name = request.json["name"] if request.json["name"] is not None else None
        image = request.json["image"] if request.json["image"] is not None else None
        discount = request.json["discount"] if request.json["discount"] is not None else None
        intro = request.json["intro"] if request.json["intro"] is not None else None
        description = request.json["description"] if request.json["description"] is not None else None
        ingredient = request.json["ingredient"] if request.json["ingredient"] is not None else None
        care = request.json["care"] if request.json["care"] is not None else None
        notice = request.json["notice"] if request.json["notice"] is not None else None
        faq = request.json["faq"] if request.json["faq"] is not None else None
        note = request.json["note"] if request.json["note"] is not None else None
        tip = request.json["tip"] if request.json["tip"] is not None else None
        sort = request.json["sort"] if request.json["sort"] is not None else 99
        status = request.json["status"] if request.json["status"] is not None else None

        if name is None or image is None or discount is None or status is None:
            raise MissingRequiredParametersException()
        else:
            response = ProductInstance().create_or_update_assemble(assemble_id, name, image, discount, intro,
                                                                   description, ingredient, care, notice, faq, note,
                                                                   tip, sort, status)
            return Response(json.dumps(response), mimetype='application/json')
    except(
            MissingRequiredParametersException,
            TargetIdDoesNotExistException,
            Exception,
            RuntimeError
    ) as e:
        response = Response()
        response.headers.add('error_code', e.__str__())
        response.status_code = 490
        return response


@productApi.route('/assemble/Assemble', methods=['POST'])
def assembleAssemble():
    try:
        id = request.json["id"] if request.json["id"] is not None else None
        assemble_id = request.json["assemble_id"] if request.json["assemble_id"] is not None else None
        sku = request.json["sku"] if request.json["sku"] is not None else None
        sort = request.json["sort"] if "sort" in request.json else 99

        if assemble_id is None or sku is None:
            raise MissingRequiredParametersException()
        else:
            response = ProductInstance().assemble_assemble(id, assemble_id, sku, sort)
            return Response(json.dumps(response), mimetype='application/json')
    except(
            MissingRequiredParametersException,
            TargetIdDoesNotExistException,
            Exception,
            RuntimeError
    ) as e:
        response = Response()
        response.headers.add('error_code', e.__str__())
        response.status_code = 490
        return response


@productApi.route('/assemble/delete', methods=['POST'])
def assembleDelete():
    try:
        id = request.json["id"] if "id" in request.json else None

        if id is None:
            raise MissingRequiredParametersException()
        else:
            response = ProductInstance().delete_assemble(id)
            return Response(json.dumps(response), mimetype='application/json')
    except(
            MissingRequiredParametersException,
            TargetIdDoesNotExistException,
            Exception,
            RuntimeError
    ) as e:
        response = Response()
        response.headers.add('error_code', e.__str__())
        response.status_code = 490
        return response


'''end product2: assemble part'''
'''product3: service part'''


@productApi.route('/service', methods=['POST'])
def serviceCreateOrUpdate():
    try:
        service_id = request.json["service_id"] if "service_id" in request.json else None
        category_id = int(request.json["category_id"]) if "category_id" in request.json else None
        name = request.json["name"] if "name" in request.json else None
        image = request.json["image"] if "image" in request.json else None
        discount = request.json["discount"] if "discount" in request.json else None
        intro = request.json["intro"] if "intro" in request.json else None
        description = request.json["description"] if "description" in request.json else None
        ingredient = request.json["ingredient"] if "ingredient" in request.json else None
        care = request.json["care"] if "care" in request.json else None
        notice = request.json["notice"] if "notice" in request.json else None
        faq = request.json["faq"] if "faq" in request.json else None
        note = request.json["note"] if "note" in request.json else None
        tip = request.json["tip"] if "tip" in request.json else None
        sort = int(request.json["sort"]) if "sort" in request.json else 99
        status = request.json["status"] if request.json["status"] is not None else None

        if category_id is None or name is None or image is None or discount is None or status is None:
            raise MissingRequiredParametersException()
        else:
            response = ProductInstance().create_or_update_service(service_id, category_id, name, image, discount, intro,
                                                                  description, ingredient, care, notice, faq, note,
                                                                  tip, sort, status)
            return Response(json.dumps(response), mimetype='application/json')
    except(
            MissingRequiredParametersException,
            TargetIdDoesNotExistException,
            EmployeeGradeDuplicationException,
            Exception,
            RuntimeError
    ) as e:
        response = Response()
        response.headers.add('error_code', e.__str__())
        response.status_code = 490
        return response


'''end product3: service part'''
'''product4: delete part'''


# @productApi.route('/product/delete', methods=['POST'])
# def productDelete():
#     try:
#         products_id = request.json["products_id"] if "products_id" in request.json else None
#
#         if products_id is None:
#             raise MissingRequiredParametersException()
#         else:
#             response = ProductInstance().delete_product(products_id)
#             return Response(json.dumps(response), mimetype='application/json')
#     except(
#         MissingRequiredParametersException,
#         Exception,
#         RuntimeError
#     ) as e:
#         response = Response()
#         response.headers.add('error_code', e.__str__())
#         response.status_code = 490
#         return response


'''end product4: delete part'''


@productApi.route('/sku/by/user_id/<user_id>', methods=['GET'])
def getUsedCategory7SkuByUserID(user_id):
    try:
        response = ProductInstance().getUsedCategory7SkuByUserID(user_id)
        return Response(json.dumps(response), mimetype='application/json')
    except(
            MissingRequiredParametersException,
            Exception,
            RuntimeError
    ) as e:
        response = Response()
        response.headers.add('error_code', e.__str__())
        response.status_code = 490
        return response


@productApi.route('/sku', methods=['GET'])
def getSku():
    try:
        response = ProductInstance().getSku()
        return Response(json.dumps(response), mimetype='application/json')
    except(
            MissingRequiredParametersException,
            Exception,
            RuntimeError
    ) as e:
        response = Response()
        response.headers.add('error_code', e.__str__())
        response.status_code = 490
        return response


@productApi.route('/get/skudetail/by/inputfilter', methods=['POST'])
def getSkuByInputFilter():
    try:
        if request.json["filter"] is '':
            response = ProductInstance().getSku()
        else:
            filter = request.json["filter"] if request.json["filter"] is not None else None
            response = ProductInstance().getSkuByInputFilter(filter)
        return Response(json.dumps(response), mimetype='application/json')
    except(
            MissingRequiredParametersException,
            Exception,
            RuntimeError
    ) as e:
        response = Response()
        response.headers.add('error_code', e.__str__())
        response.status_code = 490
        return response


@productApi.route('/sku', methods=['POST'])
def skuCreateOrUpdate():
    try:
        sku_id = request.json["sku_id"] if "sku_id" in request.json else None
        products_id = request.json["products_id"] if "products_id" in request.json else None
        number = request.json["number"] if "number" in request.json else None
        price = request.json["price"] if "price" in request.json else None
        expire = request.json["expire"] if "expire" in request.json else None
        only_for_assemable = request.json["only_for_assemable"] if "only_for_assemable" in request.json else None

        if products_id is None or number is None or price is None or expire is None or only_for_assemable is None:
            raise MissingRequiredParametersException()
        else:
            response = ProductInstance().create_or_update_sku(sku_id, products_id, number, price,
                                                              expire, only_for_assemable)
            return Response(json.dumps(response), mimetype='application/json')
    except(
            MissingRequiredParametersException,
            TargetIdDoesNotExistException,
            SkuDuplicationException,
            Exception,
            RuntimeError
    ) as e:
        response = Response()
        response.headers.add('error_code', e.__str__())
        response.status_code = 490
        return response


@productApi.route('/sku/delete', methods=['POST'])
def skuDelete():
    try:
        sku_id = request.json["sku_id"] if "sku_id" in request.json else None

        if sku_id is None:
            raise MissingRequiredParametersException()
        else:
            response = ProductInstance().delete_sku(sku_id)
            return Response(json.dumps(response), mimetype='application/json')
    except(
            MissingRequiredParametersException,
            TargetIdDoesNotExistException,
            Exception,
            RuntimeError
    ) as e:
        response = Response()
        response.headers.add('error_code', e.__str__())
        response.status_code = 490
        return response


@productApi.route('/servicePart', methods=['POST'])
def servicePartCreateOrUpdate():
    try:
        servicepart_id = request.json["servicePart_id"] if "servicePart_id" in request.json else None
        products_id = request.json["products_id"] if "products_id" in request.json else None
        employee_grade = request.json["employee_grade"] if "employee_grade" in request.json else None
        employee_number = request.json["employee_number"] if "employee_number" in request.json else None
        technical_fee = request.json["technical_fee"] if "technical_fee" in request.json else None

        if products_id is None or employee_grade is None or employee_number is None or technical_fee is None:
            raise MissingRequiredParametersException()
        else:
            response = ProductInstance().create_or_update_sp(servicepart_id, products_id, employee_grade, employee_number, technical_fee)
            return Response(json.dumps(response), mimetype='application/json')
    except(
            MissingRequiredParametersException,
            TargetIdDoesNotExistException,
            ServicePartDuplicationException,
            Exception,
            RuntimeError
    ) as e:
        response = Response()
        response.headers.add('error_code', e.__str__())
        response.status_code = 490
        return response


@productApi.route('/servicePart/delete', methods=['POST'])
def servicePartDelete():
    try:
        servicepart_id = request.json["servicepart_id"] if "servicepart_id" in request.json else None

        if servicepart_id is None:
            raise MissingRequiredParametersException()
        else:
            response = ProductInstance().delete_sp(servicepart_id)
            return Response(json.dumps(response), mimetype='application/json')
    except(
            MissingRequiredParametersException,
            TargetIdDoesNotExistException,
            Exception,
            RuntimeError
    ) as e:
        response = Response()
        response.headers.add('error_code', e.__str__())
        response.status_code = 490
        return response


@productApi.route('/service/category/create/or/update', methods=['POST'])
def serviceCategoryCreateOrUpdate():
    try:
        category_id = request.json["category_id"] if "category_id" in request.json else None
        name = request.json["name"] if "name" in request.json else None
        description = request.json["description"] if "description" in request.json else None
        type = request.json["type"] if "type" in request.json else None

        if name is None:
            raise MissingRequiredParametersException()
        else:
            response = ProductInstance().create_or_update_service_categiry(category_id, name, description, type)
            return Response(json.dumps(response), mimetype='application/json')
    except(
            MissingRequiredParametersException,
            TargetIdDoesNotExistException,
            Exception,
            RuntimeError
    ) as e:
        response = Response()
        response.headers.add('error_code', e.__str__())
        response.status_code = 490
        return response


@productApi.route('/experiences/<int:experiences_id>', methods=['GET'])
def getExperiencesOne(experiences_id):
    try:
        response = ProductInstance().getExperiencesOne(experiences_id)
        return Response(json.dumps(response), mimetype='application/json')
    except(
            MissingRequiredParametersException,
            TargetIdDoesNotExistException,
            Exception,
            RuntimeError
    ) as e:
        response = Response()
        response.headers.add('error_code', e.__str__())
        response.status_code = 490
        return response


@productApi.route('/experiences', methods=['GET'])
def getExperiences():
    try:
        response = ProductInstance().getExperiences()
        return Response(json.dumps(response), mimetype='application/json')
    except(
            MissingRequiredParametersException,
            Exception,
            RuntimeError
    ) as e:
        response = Response()
        response.headers.add('error_code', e.__str__())
        response.status_code = 490
        return response


@productApi.route('/experiences', methods=['POST'])
def experiencesCreateOrUpdate():
    try:
        _token = request.json["_token"] if request.json["_token"] is not None else None
        experiences_id = request.json["experiences_id"] if request.json["experiences_id"] is not None else None
        product_id = request.json["product_id"] if request.json["product_id"] is not None else None
        image = request.json["image"] if request.json["image"] is not None else None
        interview = request.json["interview"] if request.json["interview"] is not None else None
        advisory = request.json["advisory"] if request.json["advisory"] is not None else None
        suggest = request.json["suggest"] if request.json["suggest"] is not None else None
        results = request.json["results"] if request.json["results"] is not None else None
        essence = request.json["essence"] if request.json["essence"] is not None else None

        if _token is None:
            return redirect('/login')
        else:
            user_info = JWTInstance().decode_auth_token(_token)
            input_personnel_grade = int(user_info['grade'])
            personal_uuid = user_info['uuid']
            input_personnel = EmployeeInstance().getEmployeeIDByUuid(personal_uuid)

            function = FunctionTypeStatus.setting
            permission = EmployeeInstance().getPermissionByGradeIDAndFunctionID(input_personnel_grade, function)
            # permission judgment(need write)
            if int(permission) == PermissionTypeStatus.close or int(permission) == PermissionTypeStatus.read_only:
                raise AuthenticateFailedException()

            if product_id is None or image is None or essence is None:
                raise MissingRequiredParametersException()
            else:
                response = ProductInstance().create_or_update_experiences(experiences_id, product_id, input_personnel, image, interview,
                                                                          advisory, suggest, results, essence)
                return Response(json.dumps(response), mimetype='application/json')
    except(
            MissingRequiredParametersException,
            TargetIdDoesNotExistException,
            AuthenticateFailedException,
            Exception,
            RuntimeError
    ) as e:
        response = Response()
        response.headers.add('error_code', e.__str__())
        response.status_code = 490
        return response


@productApi.route('/experiences/delete', methods=['POST'])
def experiencesDelete():
    try:
        experiences_id = request.json["experiences_id"] if request.json["experiences_id"] is not None else None

        if experiences_id is None:
            raise MissingRequiredParametersException()
        else:
            response = ProductInstance().delete_experiences(experiences_id)
            return Response(json.dumps(response), mimetype='application/json')
    except(
            MissingRequiredParametersException,
            Exception,
            RuntimeError
    ) as e:
        response = Response()
        response.headers.add('error_code', e.__str__())
        response.status_code = 490
        return response

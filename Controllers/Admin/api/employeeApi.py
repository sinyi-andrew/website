from flask import Blueprint, current_app, render_template, request, Response, redirect, url_for, jsonify, make_response
from flask import jsonify
import json
import jwt
import datetime

from Model.Exception import *
from Model.Employee import EmployeeInstance
from Model.JWT import JWTInstance

employeeApi = Blueprint('employeeApi', __name__)


def init_app(app):
    app.register_blueprint(employeeApi, url_prefix='/admin/employee/api')


@employeeApi.route('/profile', methods=['POST'])
def getProfile():
    try:
        _token = request.json["_token"] if request.json["_token"] is not None else None
        if _token is None:
            return redirect('/login')
        else:
            user_info = JWTInstance().decode_auth_token(_token)
            employee_uuid = user_info['uuid']
            employee_name = user_info['name']
            response = EmployeeInstance().getEmployeeDataByUuid(employee_uuid)

            return Response(json.dumps(response), mimetype='application/json')
    except(
        MissingRequiredParametersException,
        TargetIdDoesNotExistException,
        Exception,
        RuntimeError
    ) as e:
        response = Response()
        response.headers.add('error_code', e.__str__())
        response.status_code = 490
        return response


@employeeApi.route('/calendar', methods=['GET'])
def getCalendar():
    try:
        response = EmployeeInstance().getCalendar()
        return Response(json.dumps(response), mimetype='application/json')
    except(
        MissingRequiredParametersException,
        TargetIdDoesNotExistException,
        Exception,
        RuntimeError
    ) as e:
        response = Response()
        response.headers.add('error_code', e.__str__())
        response.status_code = 490
        return response


@employeeApi.route('/calendar', methods=['POST'])
def calendarCreateOrUpdate():
    try:
        _token = request.json["_token"] if request.json["_token"] is not None else None
        calendar_id = request.json["calendar_id"] if request.json["calendar_id"] is not None else None
        employee_uuid = request.json["employee_uuid"] if request.json["employee_uuid"] is not None else None
        start_time = request.json["datetime_start"] if request.json["datetime_start"] is not None else None
        end_time = request.json["datetime_end"] if request.json["datetime_end"] is not None else None
        note = request.json["note"] if request.json["note"] is not None else None

        if _token is None:
            return redirect('/login')
        else:
            user_info = JWTInstance().decode_auth_token(_token)
            personal_uuid = user_info['uuid']
            input_personnel = EmployeeInstance().getEmployeeIDByUuid(personal_uuid)

            if input_personnel is None or employee_uuid is None or start_time is None or end_time is None:
                raise MissingRequiredParametersException()
            else:
                # create
                if calendar_id == '' or calendar_id is None:
                    employee_id = EmployeeInstance().getEmployeeIDByUuid(employee_uuid)
                    calendar_id = EmployeeInstance().create_calendar(input_personnel, employee_id, start_time, end_time, note)
                    return Response(json.dumps({'calendar_id': calendar_id}), mimetype='application/json')
                # update
                else:
                    calendar_id = EmployeeInstance().update_calendar(calendar_id, input_personnel, start_time, end_time, note)
                    return Response(json.dumps({'calendar_id': calendar_id}), mimetype='application/json')
    except(
        MissingRequiredParametersException,
        TargetIdDoesNotExistException,
        Exception,
        RuntimeError
    ) as e:
        response = Response()
        response.headers.add('error_code', e.__str__())
        response.status_code = 490
        return response


@employeeApi.route('/calendar', methods=['DELETE'])
def calendarDelete():
    try:
        _token = request.json["_token"] if request.json["_token"] is not None else None
        calendar_id = request.json["calendar_id"] if request.json["calendar_id"] is not None else None

        if _token is None:
            return redirect('/login')
        else:
            user_info = JWTInstance().decode_auth_token(_token)
            personal_uuid = user_info['uuid']
            input_personnel = EmployeeInstance().getEmployeeIDByUuid(personal_uuid)

            if input_personnel is None or calendar_id is None:
                raise MissingRequiredParametersException()
            else:
                calendar_id = EmployeeInstance().delete_calendar(input_personnel, calendar_id)
                return Response(json.dumps({'calendar_id': calendar_id}), mimetype='application/json')
    except(
        MissingRequiredParametersException,
        TargetIdDoesNotExistException,
        AuthenticateFailedException,
        Exception,
        RuntimeError
    ) as e:
        response = Response()
        response.headers.add('error_code', e.__str__())
        response.status_code = 490
        return response


@employeeApi.route('/function', methods=['POST'])
def functionCreate():
    try:
        store_id = request.form["store_id"] if request.form["store_id"] is not None else None
        name = request.form["name"] if request.form["name"] is not None else None
        description = str(request.form["description"]) if request.form["description"] is not None else None

        if name is None or description is None or store_id is None:
            raise MissingCriticalParametersException()
        else:
            instance = EmployeeInstance()
            response = instance.createFunction(store_id, name, description)

            return render_template("admin/redirect.html",path='/admin/employee/function/create')
    except (
            MissingCriticalParametersException,
            Exception,
            RuntimeError
    ) as e:
        return 'error'


@employeeApi.route('/role', methods=['POST'])
def roleData():
    try:
        id = request.json["id"] if request.json["id"] is not None else None
        name = request.json["name"] if request.json["name"] is not None else None
        description = request.json["description"] if request.json["description"] is not None else None
        sale = int(request.json["sale"]) if request.json["sale"] is not None else None
        permission = request.json["permission"] if request.json["permission"] is not None else None

        if name is None or description is None or sale is None or permission is None:
            raise MissingCriticalParametersException()
        else:
            instance = EmployeeInstance()
            response = instance.roleData(id, name, description, sale, permission)
            return 'success'
    except (
            DataDoesNotExistException,
            PermissionDeniedException,
            AuthenticateFailedException,
            MissingCriticalParametersException,
            Exception,
            RuntimeError
    ) as e:
        response = Response()
        response.headers.add('error_code', e.__str__())
        response.status_code = 490
        return response


@employeeApi.route('/employee', methods=['POST'])
def employeeData():
    try:
        # r = request.json
        id = request.json["id"] if request.json["id"] is not None else None
        grade_id = int(request.json["grade_id"]) if request.json["grade_id"] is not None else None
        store_id = int(request.json["store_id"]) if request.json["store_id"] is not None else None
        name = request.json["name"] if request.json["name"] is not None else None
        IDNumber = request.json["IDNumber"] if request.json["IDNumber"] is not None else None
        email = request.json["email"] if request.json["email"] is not None else None
        phone_number = request.json["phone_number"] if request.json["phone_number"] is not None else None
        mobile_phone = request.json["mobile_phone"] if request.json["mobile_phone"] is not None else None
        birthday = request.json["birthday"] if request.json["birthday"] is not None else None
        bloodtype = request.json["bloodtype"] if request.json["bloodtype"] is not None else None
        gender = request.json["gender"] if request.json["gender"] is not None else None
        address = request.json["address"] if request.json["address"] is not None else None
        height = request.json["height"] if request.json["height"] is not None else None
        weight = request.json["weight"] if request.json["weight"] is not None else None
        marriage = int(request.json["marriage"]) if request.json["marriage"] is not None else None
        skill = request.json["skill"] if request.json["skill"] is not None else None
        position_change = request.json["position_change"] if request.json["position_change"] is not None else None
        education = request.json["education"] if request.json["education"] is not None else None
        experience = request.json["experience"] if request.json["experience"] is not None else None
        certificate = request.json["certificate"] if request.json["certificate"] is not None else None
        note = request.json["note"] if request.json["note"] is not None else None
        images = request.json["images"] if request.json["images"] is not None else None
        contact = request.json["contact"] if request.json["contact"] is not None else None
        contact_phone = request.json["contact_phone"] if request.json["contact_phone"] is not None else None
        status = request.json["status"] if request.json["status"] is not None else None

        # permission = request.json["permission"] if request.json["permission"] is not None else None

        if name is None or IDNumber is None or email is None or mobile_phone is None or birthday is None or bloodtype is None or gender is None or address is None or contact is None or contact_phone is None or status is None:
            raise MissingCriticalParametersException()
        else:
            instance = EmployeeInstance()
            confirm = instance.idNumbercConfirm(id, IDNumber)
            if confirm != 0:
                return '此身分證字號已被使用'
            else:
                instance = EmployeeInstance()
                response = instance.employeeData(
                    id, grade_id, store_id, name, IDNumber, email, phone_number, mobile_phone, birthday, bloodtype,
                    gender, address, height, weight, marriage, skill, position_change, education, experience,
                    certificate, note, images, contact, contact_phone, status)

            return 'success'
    except (
            EmailDuplicationException,
    ) as e:
        return '此電子信箱已被使用'
    except (
            MissingCriticalParametersException,
            Exception,
            RuntimeError
    ) as e:
        return e.__str__()


@employeeApi.route('/authenticate', methods=['POST'])
def authenticate():
    try:
        account = request.form["account"] if "account" in request.form else None
        password = request.form["password"] if "password" in request.form else None

        if account is None or password is None:
            raise MissingRequiredParametersException()
        else:
            profile = EmployeeInstance().authenticate(account, password)
            token = JWTInstance().encode_auth_token(profile['name'], profile['uuid'])

            response = make_response(render_template('admin/dashboard.html', request=request))
            response.headers.add('Authorization', token, httponly=True)
            return response
    except (
            AuthenticateFailedException,
            MissingRequiredParametersException,
            Exception,
            RuntimeError
    ) as e:
        response = Response()
        response.headers.add('error_code', e.__str__())
        response.status_code = 490
        error = {
            'code': [4, 9, 0],
            'str': e.__str__()
        }
        return render_template('404.html', error=error)


@employeeApi.route('/search', methods=['POST'])
def employeeSearch():
    try:
        search = request.json["search"] if request.json["search"] is not None else None
        if search is None:
            raise MissingCriticalParametersException()
        else:
            instance = EmployeeInstance()
            response = instance.searchEmployee(search)
            return Response(json.dumps(response), mimetype='application/json')
    except (
            MissingCriticalParametersException,
            Exception,
            RuntimeError
    ) as e:
        return e.__str__()


@employeeApi.route('/update/password', methods=['POST'])
def updatePassword():
    try:
        number = request.json["number"] if request.json["number"] is not None else None
        old_password = request.json["old_password"] if request.json["old_password"] is not None else None
        new_password = request.json["new_password"] if request.json["new_password"] is not None else None
        if number is None or old_password is None or new_password is None:
            raise MissingCriticalParametersException()
        else:
            instance = EmployeeInstance()
            response = instance.updatePassword(number, old_password, new_password)
            return Response(json.dumps(response), mimetype='application/json')
    except (
            MissingCriticalParametersException,
            AuthenticateFailedException,
            Exception,
            RuntimeError
    ) as e:
        return e.__str__()


@employeeApi.route('/api/account/password/forget', methods=['POST'])
def api_account_password_forget():
    try:
        email = request.json["email"] if "email" in request.json else None

        if email is None:
            raise MissingRequiredParametersException()

        return Response(json.dumps(EmployeeInstance().send_forgetpassword_email(email)), mimetype='application/json')
    except (
        MissingRequiredParametersException,
        TargetIdDoesNotExistException,
        Exception,
        RuntimeError
    ) as e:
        response = Response()
        response.headers.add('error_code', e.__str__())
        response.status_code = 490
        return response


@employeeApi.route('/forget-password/authenticate', methods=['GET'])
def authenticate_token_for_forget_password_request():
    try:
        token = request.args.get("token") if request.args.get("token") is not None else None

        if token is None:
            raise MissingCriticalParametersException()
        else:
            check_result = EmployeeInstance().authenticate_forgetword_token(token)
            if check_result is None:
                raise TokenAuthenticationIsFailedException()
            else:
                return render_template('admin/password.html', employee_id=check_result["employee_id"])
    except(
        TokenAuthenticationIsFailedException,
        MissingCriticalParametersException,
        Exception,
        RuntimeError
    ) as e:
        response = Response()
        response.headers.add('error_code', e.__str__())
        response.status_code = 490
        error = {
            'code': [4, 9, 0],
            'str': e.__str__()
        }
        return render_template('404.html', error=error)


@employeeApi.route('/api/account/password', methods=['POST'])
def api_account_password():
    try:
        employee_id = request.json["employee_id"] if "employee_id" in request.json else None
        password = request.json["password"] if "password" in request.json else None
        token = request.json["token"] if "token" in request.json else None

        if employee_id is None or password is None or token is None:
            raise MissingRequiredParametersException()

        return Response(json.dumps(EmployeeInstance().password(employee_id, password, token)), mimetype='application/json')
    except (
        MissingRequiredParametersException,
        TokenAuthenticationIsFailedException,
        TargetIdDoesNotExistException,
        Exception,
        RuntimeError
    ) as e:
        response = Response()
        response.headers.add('error_code', e.__str__())
        response.status_code = 490
        return response
from flask import Blueprint, render_template, jsonify, request, Response, redirect
import json
from Model.Exception import *
from Model.Member import MemberInstance
from Model.JWT import JWTInstance
from Model.Employee import EmployeeInstance
from Utility.Status import *
from Utility.config import TenantSetting


user = Blueprint('user', __name__)


def init_app(app):
    app.register_blueprint(user, url_prefix='/admin/user')


TenantSetting = TenantSetting.infomation


@user.route('/grades', methods=['GET', 'POST'])
def userGradesList():
    try:
        _token = request.form["_token"] if "_token" in request.form else None
        if _token is None:
            return redirect('/login')
        else:
            user_info = JWTInstance().decode_auth_token(_token)
            grade = int(user_info['grade'])
            all_grade = EmployeeInstance().getPermissionByGradeID(grade)
            instance = MemberInstance()
            data = instance.getRoleList()
            return render_template('admin/user/role.html', grade=grade, all_grade=all_grade, data=data, TenantSetting=TenantSetting)
    except (
            AuthenticateFailedException,
            MissingRequiredParametersException,
            Exception,
            RuntimeError
    ) as e:
        response = Response()
        response.headers.add('error_code', e.__str__())
        response.status_code = 490
        error = {
            'code': [4, 9, 0],
            'str': e.__str__()
        }
        return render_template('404.html', error=error)


@user.route('/grades/create', methods=['GET', 'POST'])
def userGradesCreate():
    try:
        _token = request.form["_token"] if "_token" in request.form else None
        if _token is None:
            return redirect('/login')
        else:
            user_info = JWTInstance().decode_auth_token(_token)
            grade = int(user_info['grade'])
            all_grade = EmployeeInstance().getPermissionByGradeID(grade)
            return render_template('admin/user/roleForm.html', grade=grade, all_grade=all_grade, TenantSetting=TenantSetting)
    except (
            AuthenticateFailedException,
            MissingRequiredParametersException,
            Exception,
            RuntimeError
    ) as e:
        response = Response()
        response.headers.add('error_code', e.__str__())
        response.status_code = 490
        error = {
            'code': [4, 9, 0],
            'str': e.__str__()
        }
        return render_template('404.html', error=error)


@user.route('/grades/<id>', methods=['GET', 'POST'])
def userGradesData(id):
    try:
        _token = request.form["_token"] if "_token" in request.form else None
        if _token is None:
            return redirect('/login')
        else:
            user_info = JWTInstance().decode_auth_token(_token)
            grade = int(user_info['grade'])
            all_grade = EmployeeInstance().getPermissionByGradeID(grade)
            update = '1'
            instance = MemberInstance()
            data = instance.getGradeData(id)
            return render_template('admin/user/roleForm.html', grade=grade, all_grade=all_grade, update=update, data=data, TenantSetting=TenantSetting)
    except (
            AuthenticateFailedException,
            MissingRequiredParametersException,
            Exception,
            RuntimeError
    ) as e:
        response = Response()
        response.headers.add('error_code', e.__str__())
        response.status_code = 490
        error = {
            'code': [4, 9, 0],
            'str': e.__str__()
        }
        return render_template('404.html', error=error)


@user.route('/list', methods=['GET', 'POST'])
def userList():
    try:
        _token = request.form["_token"] if "_token" in request.form else None
        if _token is None:
            return redirect('/login')
        else:
            user_info = JWTInstance().decode_auth_token(_token)
            grade = int(user_info['grade'])
            all_grade = EmployeeInstance().getPermissionByGradeID(grade)

            data = MemberInstance().getUserList()
            return render_template('admin/user/user.html', grade=grade, all_grade=all_grade, data=data, user=user, TenantSetting=TenantSetting)
    except (
            AuthenticateFailedException,
            MissingRequiredParametersException,
            Exception,
            RuntimeError
    ) as e:
        response = Response()
        response.headers.add('error_code', e.__str__())
        response.status_code = 490
        error = {
            'code': [4, 9, 0],
            'str': e.__str__()
        }
        return render_template('404.html', error=error)


@user.route('/create', methods=['GET', 'POST'])
def userCreate():
    try:
        _token = request.form["_token"] if "_token" in request.form else None
        if _token is None:
            return redirect('/login')
        else:
            user_info = JWTInstance().decode_auth_token(_token)
            grade = int(user_info['grade'])
            all_grade = EmployeeInstance().getPermissionByGradeID(grade)
            data = MemberInstance().getRoleList()
            return render_template('admin/user/userForm.html', grade=grade, all_grade=all_grade, data=data, TenantSetting=TenantSetting)
    except (
            AuthenticateFailedException,
            MissingRequiredParametersException,
            Exception,
            RuntimeError
    ) as e:
        response = Response()
        response.headers.add('error_code', e.__str__())
        response.status_code = 490
        error = {
            'code': [4, 9, 0],
            'str': e.__str__()
        }
        return render_template('404.html', error=error)


@user.route('/<uuid>', methods=['GET', 'POST'])
def userData(uuid):
    try:
        _token = request.form["_token"] if "_token" in request.form else None
        if _token is None:
            return redirect('/login')
        else:
            user_info = JWTInstance().decode_auth_token(_token)
            grade = int(user_info['grade'])
            all_grade = EmployeeInstance().getPermissionByGradeID(grade)
            update = '1'
            grades = MemberInstance().getRoleList()
            data = MemberInstance().getUserData(uuid)
            return render_template('admin/user/userData.html', grade=grade, all_grade=all_grade, update=update, grades=grades, data=data, TenantSetting=TenantSetting)
    except (
            AuthenticateFailedException,
            MissingRequiredParametersException,
            Exception,
            RuntimeError
    ) as e:
        response = Response()
        response.headers.add('error_code', e.__str__())
        response.status_code = 490
        error = {
            'code': [4, 9, 0],
            'str': e.__str__()
        }
        return render_template('404.html', error=error)

@user.route('/printUser/<uuid>', methods=['GET', 'POST'])
def printUser(uuid):
    try:
        _token = request.form["_token"] if "_token" in request.form else None
        if _token is None:
            return redirect('/login')
        else:
            user_info = JWTInstance().decode_auth_token(_token)
            grade = int(user_info['grade'])
            all_grade = EmployeeInstance().getPermissionByGradeID(grade)
            grades = MemberInstance().getRoleList()
            data = MemberInstance().getUserData(uuid)
            return render_template('admin/user/printUser.html', grade=grade, all_grade=all_grade, grades=grades, data=data, TenantSetting=TenantSetting)
    except (
            AuthenticateFailedException,
            MissingRequiredParametersException,
            Exception,
            RuntimeError
    ) as e:
        response = Response()
        response.headers.add('error_code', e.__str__())
        response.status_code = 490
        error = {
            'code': [4, 9, 0],
            'str': e.__str__()
        }
        return render_template('404.html', error=error)


@user.route('/<uuid>/edit', methods=['GET', 'POST'])
def userDataEdit(uuid):
    try:
        _token = request.form["_token"] if "_token" in request.form else None
        if _token is None:
            return redirect('/login')
        else:
            user_info = JWTInstance().decode_auth_token(_token)
            grade = int(user_info['grade'])
            all_grade = EmployeeInstance().getPermissionByGradeID(grade)
            update = '1'
            grades = MemberInstance().getRoleList()
            data = MemberInstance().getUserData(uuid)
            return render_template('admin/user/userForm.html', grade=grade, all_grade=all_grade, update=update, grades=grades, data=data, TenantSetting=TenantSetting)
    except (
            AuthenticateFailedException,
            MissingRequiredParametersException,
            Exception,
            RuntimeError
    ) as e:
        response = Response()
        response.headers.add('error_code', e.__str__())
        response.status_code = 490
        error = {
            'code': [4, 9, 0],
            'str': e.__str__()
        }
        return render_template('404.html', error=error)


@user.route('/<uuid>/point', methods=['GET', 'POST'])
def userPoint(uuid):
    try:
        _token = request.form["_token"] if "_token" in request.form else None
        if _token is None:
            return redirect('/login')
        else:
            user_info = JWTInstance().decode_auth_token(_token)
            grade = int(user_info['grade'])
            all_grade = EmployeeInstance().getPermissionByGradeID(grade)
            member = MemberInstance().getUserData(uuid)
            data = MemberInstance().getUserpoint(uuid)
            return render_template('admin/user/point.html', grade=grade, all_grade=all_grade, member=member, data=data, TenantSetting=TenantSetting)
    except (
            AuthenticateFailedException,
            MissingRequiredParametersException,
            Exception,
            RuntimeError
    ) as e:
        response = Response()
        response.headers.add('error_code', e.__str__())
        response.status_code = 490
        error = {
            'code': [4, 9, 0],
            'str': e.__str__()
        }
        return render_template('404.html', error=error)


@user.route('/<uuid>/point/add', methods=['GET', 'POST'])
def userPointEdit(uuid):
    try:
        _token = request.form["_token"] if "_token" in request.form else None
        if _token is None:
            return redirect('/login')
        else:
            user_info = JWTInstance().decode_auth_token(_token)
            grade = int(user_info['grade'])
            all_grade = EmployeeInstance().getPermissionByGradeID(grade)
            member = MemberInstance().getUserData(uuid)
            return render_template('admin/user/pointForm.html', grade=grade, all_grade=all_grade, member=member, TenantSetting=TenantSetting)
    except (
            AuthenticateFailedException,
            MissingRequiredParametersException,
            Exception,
            RuntimeError
    ) as e:
        response = Response()
        response.headers.add('error_code', e.__str__())
        response.status_code = 490
        error = {
            'code': [4, 9, 0],
            'str': e.__str__()
        }
        return render_template('404.html', error=error)


@user.route('/<uuid>/<response>/<employee>/point/print', methods=['GET', 'POST'])
def userPointPrint(uuid,response,employee):
    try:
        # _token = request.form["_token"] if "_token" in request.form else None
        # if _token is None:
        if uuid is None or response is None or employee is None:
            return redirect('/login')
        else:
            # user_info = JWTInstance().decode_auth_token(_token)
            # grade = int(user_info['grade'])
            # all_grade = EmployeeInstance().getPermissionByGradeID(grade)
            emp = EmployeeInstance().getEmployeeData(employee)
            member = MemberInstance().getUserData(uuid)
            data = MemberInstance().getUserpoint(uuid)
            return render_template('admin/user/printPoint.html', member=member, employee=emp, response=response, data=data, TenantSetting=TenantSetting)#, grade=grade, all_grade=all_grade
    except (
            AuthenticateFailedException,
            MissingRequiredParametersException,
            Exception,
            RuntimeError
    ) as e:
        response = Response()
        response.headers.add('error_code', e.__str__())
        response.status_code = 490
        error = {
            'code': [4, 9, 0],
            'str': e.__str__()
        }
        return render_template('404.html', error=error)
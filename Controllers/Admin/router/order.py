from flask import Blueprint, render_template, Response, redirect, request
import json

from Model.Exception import *
from Model.Order import OrderInstance
from Model.Product import ProductInstance
from Model.Employee import EmployeeInstance
from Model.JWT import JWTInstance
from Model.BusinessRule import BusinessRuleInstance
from Model.Order import OrderInstance
from Utility.config import TenantSetting


order = Blueprint('order', __name__)
from Model.Exception import *


def init_app(app):
    app.register_blueprint(order, url_prefix='/admin/order')


TenantSetting = TenantSetting.infomation


@order.route('/list', methods=['GET', 'POST'])
def orderIndex():
    try:
        _token = request.form["_token"] if "_token" in request.form else None
        if _token is None:
            return redirect('/login')
        else:
            user_info = JWTInstance().decode_auth_token(_token)
            grade = int(user_info['grade'])
            all_grade = EmployeeInstance().getPermissionByGradeID(grade)
            orderList = OrderInstance().getOrder()

            return render_template('admin/order/index.html', grade=grade, all_grade=all_grade, orderList=orderList, TenantSetting=TenantSetting)
    except (
            AuthenticateFailedException,
            MissingRequiredParametersException,
            Exception,
            RuntimeError
    ) as e:
        response = Response()
        response.headers.add('error_code', e.__str__())
        response.status_code = 490
        error = {
            'code': [4, 9, 0],
            'str': e.__str__()
        }
        return render_template('404.html', error=error)

@order.route('/cancelList', methods=['GET', 'POST'])
def orderCancel():
    try:
        _token = request.form["_token"] if "_token" in request.form else None
        if _token is None:
            return redirect('/login')
        else:
            user_info = JWTInstance().decode_auth_token(_token)
            grade = int(user_info['grade'])
            all_grade = EmployeeInstance().getPermissionByGradeID(grade)

            return render_template('admin/order/cancel.html', grade=grade, all_grade=all_grade, TenantSetting=TenantSetting)
    except (
            AuthenticateFailedException,
            MissingRequiredParametersException,
            Exception,
            RuntimeError
    ) as e:
        response = Response()
        response.headers.add('error_code', e.__str__())
        response.status_code = 490
        error = {
            'code': [4, 9, 0],
            'str': e.__str__()
        }
        return render_template('404.html', error=error)

@order.route('/<order_uuid>', methods=['GET', 'POST'])
def getOrderByOrderUUID(order_uuid):
    try:
        _token = request.form["_token"] if "_token" in request.form else None
        if _token is None:
            return redirect('/login')
        else:
            user_info = JWTInstance().decode_auth_token(_token)
            grade = int(user_info['grade'])
            all_grade = EmployeeInstance().getPermissionByGradeID(grade)
            order_id = OrderInstance().getOrderIDByOrderUuid(order_uuid)
            response = OrderInstance().getOrderByOrderID(order_id)
            return render_template('admin/order/order.html', grade=grade, all_grade=all_grade, data=response, uuid=order_uuid, TenantSetting=TenantSetting)
            # return Response(json.dumps(response), mimetype='application/json')
    except (
        AuthenticateFailedException,
        MissingRequiredParametersException,
        TargetIdDoesNotExistException,
        EmployeeListEmptyException,
        Exception,
        RuntimeError
    ) as e:
        response = Response()
        response.headers.add('error_code', e.__str__())
        response.status_code = 490
        error = {
            'code': [4, 9, 0],
            'str': e.__str__()
        }
        return render_template('404.html', error=error)


@order.route('/print/<order_uuid>', methods=['GET', 'POST'])
def printOrderByOrderUUID(order_uuid):
    try:
        _token = request.form["_token"] if "_token" in request.form else None
        if _token is None:
            return redirect('/login')
        else:
            user_info = JWTInstance().decode_auth_token(_token)
            grade = int(user_info['grade'])
            all_grade = EmployeeInstance().getPermissionByGradeID(grade)
            order_id = OrderInstance().getOrderIDByOrderUuid(order_uuid)
            response = OrderInstance().getOrderByOrderID(order_id)
            return render_template('admin/order/print-order.html', grade=grade, all_grade=all_grade, data=response, TenantSetting=TenantSetting)
            # return Response(json.dumps(response), mimetype='application/json')
    except (
        AuthenticateFailedException,
        MissingRequiredParametersException,
        TargetIdDoesNotExistException,
        EmployeeListEmptyException,
        Exception,
        RuntimeError
    ) as e:
        response = Response()
        response.headers.add('error_code', e.__str__())
        response.status_code = 490
        error = {
            'code': [4, 9, 0],
            'str': e.__str__()
        }
        return render_template('404.html', error=error)

@order.route('/printPickup/<order_uuid>', methods=['GET', 'POST'])
def printPickupByOrderUUID(order_uuid):
    try:
        _token = request.form["_token"] if "_token" in request.form else None
        if _token is None:
            return redirect('/login')
        else:
            user_info = JWTInstance().decode_auth_token(_token)
            grade = int(user_info['grade'])
            all_grade = EmployeeInstance().getPermissionByGradeID(grade)
            order_id = OrderInstance().getOrderIDByOrderUuid(order_uuid)
            response = OrderInstance().getOrderByOrderID(order_id)
            return render_template('admin/order/printPickup.html', grade=grade, all_grade=all_grade, data=response, TenantSetting=TenantSetting)
            # return Response(json.dumps(response), mimetype='application/json')
    except (
        AuthenticateFailedException,
        MissingRequiredParametersException,
        TargetIdDoesNotExistException,
        EmployeeListEmptyException,
        Exception,
        RuntimeError
    ) as e:
        response = Response()
        response.headers.add('error_code', e.__str__())
        response.status_code = 490
        error = {
            'code': [4, 9, 0],
            'str': e.__str__()
        }
        return render_template('404.html', error=error)


@order.route('/calendar', methods=['GET', 'POST'])
def calendar():
    try:
        _token = request.form["_token"] if "_token" in request.form else None
        if _token is None:
            return redirect('/login')
        else:
            user_info = JWTInstance().decode_auth_token(_token)
            grade = int(user_info['grade'])
            all_grade = EmployeeInstance().getPermissionByGradeID(grade)
            employee = EmployeeInstance().getEmployeeList()
            roles = EmployeeInstance().getRoleList()
            return render_template('admin/order/calendar.html', grade=grade, all_grade=all_grade, employee=employee, roles=roles, TenantSetting=TenantSetting)
    except (
            AuthenticateFailedException,
            MissingRequiredParametersException,
            Exception,
            RuntimeError
    ) as e:
        response = Response()
        response.headers.add('error_code', e.__str__())
        response.status_code = 490
        error = {
            'code': [4, 9, 0],
            'str': e.__str__()
        }
        return render_template('404.html', error=error)


@order.route('/taskboard', methods=['GET', 'POST'])
def taskboard():
    try:
        _token = request.form["_token"] if "_token" in request.form else None
        if _token is None:
            return redirect('/login')
        else:
            user_info = JWTInstance().decode_auth_token(_token)
            grade = int(user_info['grade'])
            all_grade = EmployeeInstance().getPermissionByGradeID(grade)
            employee = EmployeeInstance().getEmployeeList()
            roles = EmployeeInstance().getRoleList()
            return render_template('admin/order/taskboard.html', grade=grade, all_grade=all_grade, employee=employee, roles=roles, TenantSetting=TenantSetting)
    except (
            AuthenticateFailedException,
            MissingRequiredParametersException,
            Exception,
            RuntimeError
    ) as e:
        response = Response()
        response.headers.add('error_code', e.__str__())
        response.status_code = 490
        error = {
            'code': [4, 9, 0],
            'str': e.__str__()
        }
        return render_template('404.html', error=error)

# @order.route('/calendarJWT', methods=['GET', 'POST'])
# def calendarJWT():
#     return render_template('admin/order/calendarJWT.html', TenantSetting=TenantSetting)

# @order.route('/calendarIframe', methods=['GET', 'POST'])
# def calendarIframe():
#     try:
#         _token = request.form["_token"] if "_token" in request.form else None
#         if _token is None:
#             return redirect('/login')
#         else:
#             user_info = JWTInstance().decode_auth_token(_token)
#             grade = int(user_info['grade'])
#             all_grade = EmployeeInstance().getPermissionByGradeID(grade)
#             employee = EmployeeInstance().getEmployeeList()
#             roles = EmployeeInstance().getRoleList()
#             return render_template('admin/order/calendarIframe.html', grade=grade, all_grade=all_grade, employee=employee, roles=roles, TenantSetting=TenantSetting)
#     except (
#             AuthenticateFailedException,
#             MissingRequiredParametersException,
#             Exception,
#             RuntimeError
#     ) as e:
#         response = Response()
#         response.headers.add('error_code', e.__str__())
#         response.status_code = 490
#         error = {
#             'code': [4, 9, 0],
#             'str': e.__str__()
#         }
#         return render_template('404.html', error=error)


@order.route('/create', methods=['GET', 'POST'])
def orderCreate():
    try:
        _token = request.form["_token"] if "_token" in request.form else None
        if _token is None:
            return redirect('/login')
        else:
            user_info = JWTInstance().decode_auth_token(_token)
            grade = int(user_info['grade'])
            all_grade = EmployeeInstance().getPermissionByGradeID(grade)
            return render_template('admin/order/orderForm.html', grade=grade, all_grade=all_grade, TenantSetting=TenantSetting)
    except (
            AuthenticateFailedException,
            MissingRequiredParametersException,
            Exception,
            RuntimeError
    ) as e:
        response = Response()
        response.headers.add('error_code', e.__str__())
        response.status_code = 490
        error = {
            'code': [4, 9, 0],
            'str': e.__str__()
        }
        return render_template('404.html', error=error)


@order.route('/memberOrder', methods=['GET', 'POST'])
def memberOrder():
    try:
        _token = request.form["_token"] if "_token" in request.form else None
        if _token is None:
            return redirect('/login')
        else:
            user_info = JWTInstance().decode_auth_token(_token)
            grade = int(user_info['grade'])
            all_grade = EmployeeInstance().getPermissionByGradeID(grade)
            return render_template('admin/order/memberOrder.html', grade=grade, all_grade=all_grade, TenantSetting=TenantSetting)
    except (
            AuthenticateFailedException,
            MissingRequiredParametersException,
            Exception,
            RuntimeError
    ) as e:
        response = Response()
        response.headers.add('error_code', e.__str__())
        response.status_code = 490
        error = {
            'code': [4, 9, 0],
            'str': e.__str__()
        }
        return render_template('404.html', error=error)

@order.route('/managerOrder', methods=['GET', 'POST'])
def managerOrder():
    try:
        _token = request.form["_token"] if "_token" in request.form else None
        if _token is None:
            return redirect('/login')
        else:
            user_info = JWTInstance().decode_auth_token(_token)
            grade = int(user_info['grade'])
            all_grade = EmployeeInstance().getPermissionByGradeID(grade)
            list = ProductInstance().getSku()
            return render_template('admin/order/managerOrder.html', grade=grade, all_grade=all_grade, list=list, TenantSetting=TenantSetting)
    except (
            AuthenticateFailedException,
            MissingRequiredParametersException,
            Exception,
            RuntimeError
    ) as e:
        response = Response()
        response.headers.add('error_code', e.__str__())
        response.status_code = 490
        error = {
            'code': [4, 9, 0],
            'str': e.__str__()
        }
        return render_template('404.html', error=error)

@order.route('/reservation', methods=['GET', 'POST'])
def orderReservation():
    try:
        _token = request.form["_token"] if "_token" in request.form else None
        if _token is None:
            return redirect('/login')
        else:
            user_info = JWTInstance().decode_auth_token(_token)
            grade = int(user_info['grade'])
            all_grade = EmployeeInstance().getPermissionByGradeID(grade)
            employee = EmployeeInstance().getEmployeeList()
            roles = EmployeeInstance().getRoleList()
            user = EmployeeInstance().getEmployeeDataByUuid(user_info['uuid'])

            return render_template('admin/order/reservation.html', grade=grade, all_grade=all_grade, employee=employee, roles=roles, user=user, TenantSetting=TenantSetting)
    except (
            AuthenticateFailedException,
            MissingRequiredParametersException,
            Exception,
            RuntimeError
    ) as e:
        response = Response()
        response.headers.add('error_code', e.__str__())
        response.status_code = 490
        error = {
            'code': [4, 9, 0],
            'str': e.__str__()
        }
        return render_template('404.html', error=error)

@order.route('/printReservation', methods=['GET', 'POST'])
def printReservation():
    try:
        _token = request.form["_token"] if "_token" in request.form else None
        if _token is None:
            return redirect('/login')
        else:
            user_info = JWTInstance().decode_auth_token(_token)
            grade = int(user_info['grade'])
            employee = EmployeeInstance().getEmployeeList()

            return render_template('admin/order/printReservation.html', grade=grade, employee=employee, TenantSetting=TenantSetting)
    except (
            AuthenticateFailedException,
            MissingRequiredParametersException,
            Exception,
            RuntimeError
    ) as e:
        response = Response()
        response.headers.add('error_code', e.__str__())
        response.status_code = 490
        error = {
            'code': [4, 9, 0],
            'str': e.__str__()
        }
        return render_template('404.html', error=error)

@order.route('/memberReservation/<user_number>', methods=['GET', 'POST'])
def orderMemberReservation(user_number):
    try:
        _token = request.form["_token"] if "_token" in request.form else None
        if _token is None:
            return redirect('/login')
        else:
            user_info = JWTInstance().decode_auth_token(_token)
            grade = int(user_info['grade'])
            all_grade = EmployeeInstance().getPermissionByGradeID(grade)
            employee = EmployeeInstance().getEmployeeList()
            roles = EmployeeInstance().getRoleList()

            return render_template('admin/order/memberReservation.html', grade=grade, all_grade=all_grade, employee=employee, roles=roles, user_number=user_number, TenantSetting=TenantSetting)
    except (
            AuthenticateFailedException,
            MissingRequiredParametersException,
            Exception,
            RuntimeError
    ) as e:
        response = Response()
        response.headers.add('error_code', e.__str__())
        response.status_code = 490
        error = {
            'code': [4, 9, 0],
            'str': e.__str__()
        }
        return render_template('404.html', error=error)
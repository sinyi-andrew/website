from flask import Blueprint, render_template, request, redirect, Response
import json

from Model.Exception import *
from Model.Employee import EmployeeInstance
from Model.JWT import JWTInstance
from Utility.config import TenantSetting


employee = Blueprint('employee', __name__)


TenantSetting = TenantSetting.infomation


def init_app(app):
    app.register_blueprint(employee, url_prefix='/admin/employee')


# @employee.route('/function/create', methods=['GET', 'POST'])
# def FunctionCreate():
#     try:
#         _token = request.form["_token"] if "_token" in request.form else None
#         if _token is None:
#             return redirect('/login')
#         else:
#             user_info = JWTInstance().decode_auth_token(_token)
#             grade = int(user_info['grade'])
#             all_grade = EmployeeInstance().getPermissionByGradeID(grade)
#             return render_template('admin/employee/functionForm.html', grade=grade, all_grade=all_grade, TenantSetting=TenantSetting)
#     except (
#             AuthenticateFailedException,
#             MissingRequiredParametersException,
#             Exception,
#             RuntimeError
#     ) as e:
#         response = Response()
#         response.headers.add('error_code', e.__str__())
#         response.status_code = 490
#         error = {
#             'code': [4, 9, 0],
#             'str': e.__str__()
#         }
#         return render_template('404.html', error=error)


@employee.route('/roles', methods=['GET', 'POST'])
def employeeGradesList():
    try:
        _token = request.form["_token"] if "_token" in request.form else None
        if _token is None:
            return redirect('/login')
        else:
            user_info = JWTInstance().decode_auth_token(_token)
            grade = int(user_info['grade'])
            all_grade = EmployeeInstance().getPermissionByGradeID(grade)

            instance = EmployeeInstance()
            data = instance.getRoleList()
            instance = EmployeeInstance()
            functions = instance.getFunctionData()
            return render_template('admin/employee/role.html', grade=grade, all_grade=all_grade, data=data, functions=functions, TenantSetting=TenantSetting)
    except (
            AuthenticateFailedException,
            MissingRequiredParametersException,
            Exception,
            RuntimeError
    ) as e:
        response = Response()
        response.headers.add('error_code', e.__str__())
        response.status_code = 490
        error = {
            'code': [4, 9, 0],
            'str': e.__str__()
        }
        return render_template('404.html', error=error)


@employee.route('/role/create', methods=['GET', 'POST'])
def employeeGradesCreate():
    try:
        _token = request.form["_token"] if "_token" in request.form else None
        if _token is None:
            return redirect('/login')
        else:
            user_info = JWTInstance().decode_auth_token(_token)
            grade = int(user_info['grade'])
            all_grade = EmployeeInstance().getPermissionByGradeID(grade)
            instance = EmployeeInstance()
            data = instance.getFunctionData()
            return render_template('admin/employee/roleForm.html', grade=grade, all_grade=all_grade, data=data, TenantSetting=TenantSetting)
    except (
            AuthenticateFailedException,
            MissingRequiredParametersException,
            Exception,
            RuntimeError
    ) as e:
        response = Response()
        response.headers.add('error_code', e.__str__())
        response.status_code = 490
        error = {
            'code': [4, 9, 0],
            'str': e.__str__()
        }
        return render_template('404.html', error=error)


@employee.route('/role/<id>', methods=['GET', 'POST'])
def employeeGradesData(id):
    try:
        _token = request.form["_token"] if "_token" in request.form else None
        if _token is None:
            return redirect('/login')
        else:
            user_info = JWTInstance().decode_auth_token(_token)
            grade = int(user_info['grade'])
            all_grade = EmployeeInstance().getPermissionByGradeID(grade)
            update = '1'
            instance = EmployeeInstance()
            data = instance.getRoleData(id)
            instance = EmployeeInstance()
            functions = instance.getFunctionData()
            return render_template('admin/employee/roleForm.html', grade=grade, all_grade=all_grade, update=update, data=data, functions=functions, TenantSetting=TenantSetting)
    except (
            AuthenticateFailedException,
            MissingRequiredParametersException,
            Exception,
            RuntimeError
    ) as e:
        response = Response()
        response.headers.add('error_code', e.__str__())
        response.status_code = 490
        error = {
            'code': [4, 9, 0],
            'str': e.__str__()
        }
        return render_template('404.html', error=error)


@employee.route('/list', methods=['GET', 'POST'])
def employeeList():
    try:
        _token = request.form["_token"] if "_token" in request.form else None
        if _token is None:
            return redirect('/login')
        else:
            user_info = JWTInstance().decode_auth_token(_token)
            grade = int(user_info['grade'])
            all_grade = EmployeeInstance().getPermissionByGradeID(grade)
            personal_uuid = user_info['uuid']

            instance = EmployeeInstance()
            data = instance.getEmployeeList()
            instance = EmployeeInstance()
            roles = instance.getRoleList()
            return render_template('admin/employee/employee.html', grade=grade, all_grade=all_grade, data=data, roles=roles, TenantSetting=TenantSetting)
    except (
            AuthenticateFailedException,
            MissingRequiredParametersException,
            Exception,
            RuntimeError
    ) as e:
        response = Response()
        response.headers.add('error_code', e.__str__())
        response.status_code = 490
        error = {
            'code': [4, 9, 0],
            'str': e.__str__()
        }
        return render_template('404.html', error=error)

@employee.route('/create', methods=['GET', 'POST'])
def employeeCreate():
    try:
        _token = request.form["_token"] if "_token" in request.form else None
        if _token is None:
            return redirect('/login')
        else:
            user_info = JWTInstance().decode_auth_token(_token)
            grade = int(user_info['grade'])
            all_grade = EmployeeInstance().getPermissionByGradeID(grade)
            instance = EmployeeInstance()
            data = instance.getRoleList()
            return render_template('admin/employee/employeeForm.html', grade=grade, all_grade=all_grade, data=data, TenantSetting=TenantSetting)
    except (
            AuthenticateFailedException,
            MissingRequiredParametersException,
            Exception,
            RuntimeError
    ) as e:
        response = Response()
        response.headers.add('error_code', e.__str__())
        response.status_code = 490
        error = {
            'code': [4, 9, 0],
            'str': e.__str__()
        }
        return render_template('404.html', error=error)


@employee.route('/<id>', methods=['GET', 'POST'])
def employeeData(id):
    try:
        _token = request.form["_token"] if "_token" in request.form else None
        if _token is None:
            return redirect('/login')
        else:
            user_info = JWTInstance().decode_auth_token(_token)
            grade = int(user_info['grade'])
            all_grade = EmployeeInstance().getPermissionByGradeID(grade)
            update = '1'
            instance = EmployeeInstance()
            data = instance.getEmployeeData(id)
            instance = EmployeeInstance()
            roles = instance.getRoleList()
            return render_template('admin/employee/employeeForm.html', grade=grade, all_grade=all_grade, update=update, data=data, roles=roles, TenantSetting=TenantSetting)
    except (
            AuthenticateFailedException,
            MissingRequiredParametersException,
            Exception,
            RuntimeError
    ) as e:
        response = Response()
        response.headers.add('error_code', e.__str__())
        response.status_code = 490
        error = {
            'code': [4, 9, 0],
            'str': e.__str__()
        }
        return render_template('404.html', error=error)


@employee.route('/calendar', methods=['GET', 'POST'])
def calendar():
    try:
        _token = request.form["_token"] if "_token" in request.form else None
        if _token is None:
            return redirect('/login')
        else:
            user_info = JWTInstance().decode_auth_token(_token)
            grade = int(user_info['grade'])
            all_grade = EmployeeInstance().getPermissionByGradeID(grade)
            employee = EmployeeInstance().getEmployeeList()
            roles = EmployeeInstance().getRoleList()
            return render_template('admin/employee/calendar.html', grade=grade, all_grade=all_grade, employee=employee, roles=roles, TenantSetting=TenantSetting)
    except (
            AuthenticateFailedException,
            MissingRequiredParametersException,
            Exception,
            RuntimeError
    ) as e:
        response = Response()
        response.headers.add('error_code', e.__str__())
        response.status_code = 490
        error = {
            'code': [4, 9, 0],
            'str': e.__str__()
        }
        return render_template('404.html', error=error)


# @employee.route('/calendarJWT', methods=['GET', 'POST'])
# def calendarJWT():
#     return render_template('admin/employee/calendarJWT.html', TenantSetting=TenantSetting)


# @employee.route('/calendarIframe', methods=['GET', 'POST'])
# def calendarIframe():
#     try:
#         _token = request.form["_token"] if "_token" in request.form else None
#         if _token is None:
#             return redirect('/login')
#         else:
#             user_info = JWTInstance().decode_auth_token(_token)
#             grade = int(user_info['grade'])
#             all_grade = EmployeeInstance().getPermissionByGradeID(grade)
#             employee = EmployeeInstance().getEmployeeList()
#             roles = EmployeeInstance().getRoleList()
#             return render_template('admin/employee/calendarIframe.html', grade=grade, all_grade=all_grade, employee=employee, roles=roles, TenantSetting=TenantSetting)
#     except (
#             AuthenticateFailedException,
#             MissingRequiredParametersException,
#             Exception,
#             RuntimeError
#     ) as e:
#         response = Response()
#         response.headers.add('error_code', e.__str__())
#         response.status_code = 490
#         error = {
#             'code': [4, 9, 0],
#             'str': e.__str__()
#         }
#         return render_template('404.html', error=error)
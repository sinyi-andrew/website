from flask import Blueprint, render_template, request, redirect, Response
import json

from Model.Exception import *
from Model.Employee import EmployeeInstance
from Model.Inventory import InventoryInstance
from Model.JWT import JWTInstance
from Model.BusinessRule import BusinessRuleInstance
from Utility.config import TenantSetting


supply = Blueprint('supply', __name__)


def init_app(app):
    app.register_blueprint(supply, url_prefix='/admin/supply')


TenantSetting = TenantSetting.infomation


@supply.route('/list', methods=['GET', 'POST'])
def List():
    try:
        _token = request.form["_token"] if "_token" in request.form else None
        if _token is None:
            return redirect('/login')
        else:
            user_info = JWTInstance().decode_auth_token(_token)
            grade = int(user_info['grade'])
            all_grade = EmployeeInstance().getPermissionByGradeID(grade)

            instance = InventoryInstance()
            data = instance.getSupplies()
            return render_template('admin/supply/supply.html', grade=grade, all_grade=all_grade, data=data, TenantSetting=TenantSetting)
    except (
            AuthenticateFailedException,
            MissingRequiredParametersException,
            Exception,
            RuntimeError
    ) as e:
        response = Response()
        response.headers.add('error_code', e.__str__())
        response.status_code = 490
        error = {
            'code': [4, 9, 0],
            'str': e.__str__()
        }
        return render_template('404.html', error=error)


@supply.route('/supplyCreate', methods=['GET', 'POST'])
def supplyCreate():
    try:
        _token = request.form["_token"] if "_token" in request.form else None
        if _token is None:
            return redirect('/login')
        else:
            user_info = JWTInstance().decode_auth_token(_token)
            grade = int(user_info['grade'])
            all_grade = EmployeeInstance().getPermissionByGradeID(grade)
            product = InventoryInstance().getSupplies()
            list = BusinessRuleInstance().getProductDetailByCategoryId(1)
            return render_template('admin/supply/supplyCreate.html', grade=grade, all_grade=all_grade, product=product, list=list, update=0, TenantSetting=TenantSetting)
    except (
            AuthenticateFailedException,
            MissingRequiredParametersException,
            Exception,
            RuntimeError
    ) as e:
        response = Response()
        response.headers.add('error_code', e.__str__())
        response.status_code = 490
        error = {
            'code': [4, 9, 0],
            'str': e.__str__()
        }
        return render_template('404.html', error=error)


@supply.route('/supplyEdit/<id>/<name>/<pname>/<cost>', methods=['GET', 'POST'])
def supplyEdit(id,name,pname,cost):
    try:
        _token = request.form["_token"] if "_token" in request.form else None
        if _token is None:
            return redirect('/login')
        else:
            user_info = JWTInstance().decode_auth_token(_token)
            grade = int(user_info['grade'])
            all_grade = EmployeeInstance().getPermissionByGradeID(grade)
            product = InventoryInstance().getSupplies()
            list = BusinessRuleInstance().getProductDetailByCategoryId(1)
            return render_template('admin/supply/supplyCreate.html', grade=grade, all_grade=all_grade, update=1, product=product, list=list, id=id, name=name, pname=pname, cost=cost, TenantSetting=TenantSetting)
    except (
            AuthenticateFailedException,
            MissingRequiredParametersException,
            Exception,
            RuntimeError
    ) as e:
        response = Response()
        response.headers.add('error_code', e.__str__())
        response.status_code = 490
        error = {
            'code': [4, 9, 0],
            'str': e.__str__()
        }
        return render_template('404.html', error=error)


@supply.route('/inventory/<id>', methods=['GET', 'POST'])
def inventory(id):
    try:
        _token = request.form["_token"] if "_token" in request.form else None
        if _token is None:
            return redirect('/login')
        else:
            user_info = JWTInstance().decode_auth_token(_token)
            grade = int(user_info['grade'])
            all_grade = EmployeeInstance().getPermissionByGradeID(grade)
            product = InventoryInstance().getSuppliesById(id)
            data = InventoryInstance().getInventoryBySupplyId(id)
            return render_template('admin/supply/inventory.html', grade=grade, all_grade=all_grade, data=data, product=product, id=id, TenantSetting=TenantSetting)
    except (
            AuthenticateFailedException,
            MissingRequiredParametersException,
            Exception,
            RuntimeError
    ) as e:
        response = Response()
        response.headers.add('error_code', e.__str__())
        response.status_code = 490
        error = {
            'code': [4, 9, 0],
            'str': e.__str__()
        }
        return render_template('404.html', error=error)


@supply.route('/inventoryCreate/<supplyId>', methods=['GET', 'POST'])
def inventoryCreate(supplyId):
    try:
        _token = request.form["_token"] if "_token" in request.form else None
        if _token is None:
            return redirect('/login')
        else:
            user_info = JWTInstance().decode_auth_token(_token)
            grade = int(user_info['grade'])
            all_grade = EmployeeInstance().getPermissionByGradeID(grade)
            return render_template('admin/supply/inventoryCreate.html', grade=grade, all_grade=all_grade, update=0, supplyId=supplyId, TenantSetting=TenantSetting)
    except (
            AuthenticateFailedException,
            MissingRequiredParametersException,
            Exception,
            RuntimeError
    ) as e:
        response = Response()
        response.headers.add('error_code', e.__str__())
        response.status_code = 490
        error = {
            'code': [4, 9, 0],
            'str': e.__str__()
        }
        return render_template('404.html', error=error)
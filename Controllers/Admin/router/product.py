from flask import Blueprint, render_template, url_for, redirect, Response, request
import json

from Model.Exception import *
from Model.JWT import JWTInstance
from Model.Product import ProductInstance
from Model.BusinessRule import BusinessRuleInstance
from Model.Employee import EmployeeInstance
from Utility.config import TenantSetting


product = Blueprint('product', __name__)


def init_app(app):
    app.register_blueprint(product, url_prefix='/admin/product')


TenantSetting = TenantSetting.infomation


@product.route('/list', methods=['GET', 'POST'])
def productList():
    try:
        _token = request.form["_token"] if "_token" in request.form else None
        if _token is None:
            return redirect('/login')
        else:
            user_info = JWTInstance().decode_auth_token(_token)
            grade = int(user_info['grade'])
            all_grade = EmployeeInstance().getPermissionByGradeID(grade)

            list = BusinessRuleInstance().getProductDetailByCategoryId(1)  # 1 = product
            assemble = BusinessRuleInstance().getProductDetailByCategoryId(2)  # 2 = assemble
            return render_template('admin/product/productList.html', grade=grade, all_grade=all_grade, list=list, assemble=assemble, TenantSetting=TenantSetting)
            # return render_template('admin/product/list.html', grade=grade, all_grade=all_grade, data=data, TenantSetting=TenantSetting)
    except (
            AuthenticateFailedException,
            MissingRequiredParametersException,
            Exception,
            RuntimeError
    ) as e:
        response = Response()
        response.headers.add('error_code', e.__str__())
        response.status_code = 490
        error = {
            'code': [4, 9, 0],
            'str': e.__str__()
        }
        return render_template('404.html', error=error)


@product.route('/productCreate', methods=['GET', 'POST'])
def productCreate():
    try:
        _token = request.form["_token"] if "_token" in request.form else None
        if _token is None:
            return redirect('/login')
        else:
            user_info = JWTInstance().decode_auth_token(_token)
            grade = int(user_info['grade'])
            all_grade = EmployeeInstance().getPermissionByGradeID(grade)
            list = ProductInstance().getSku()
            return render_template('admin/product/newOrEditProduct.html', grade=grade, all_grade=all_grade, update=0, type=0, list=list, TenantSetting=TenantSetting)
    except (
            AuthenticateFailedException,
            MissingRequiredParametersException,
            Exception,
            RuntimeError
    ) as e:
        response = Response()
        response.headers.add('error_code', e.__str__())
        response.status_code = 490
        error = {
            'code': [4, 9, 0],
            'str': e.__str__()
        }
        return render_template('404.html', error=error)


@product.route('/productEdit/<id>', methods=['GET', 'POST'])
def productEdit(id):
    try:
        _token = request.form["_token"] if "_token" in request.form else None
        if _token is None:
            return redirect('/login')
        else:
            user_info = JWTInstance().decode_auth_token(_token)
            grade = int(user_info['grade'])
            all_grade = EmployeeInstance().getPermissionByGradeID(grade)
            data = BusinessRuleInstance().getProductDetailByProductsId(id)
            list = ProductInstance().getSku()
            return render_template('admin/product/newOrEditProduct.html', grade=grade, all_grade=all_grade, update=1, data=data, list=list, TenantSetting=TenantSetting)
    except (
            AuthenticateFailedException,
            MissingRequiredParametersException,
            Exception,
            RuntimeError
    ) as e:
        response = Response()
        response.headers.add('error_code', e.__str__())
        response.status_code = 490
        error = {
            'code': [4, 9, 0],
            'str': e.__str__()
        }
        return render_template('404.html', error=error)


@product.route('/productView/<id>', methods=['GET', 'POST'])
def productView(id):
    try:
        _token = request.form["_token"] if "_token" in request.form else None
        if _token is None:
            return redirect('/login')
        else:
            user_info = JWTInstance().decode_auth_token(_token)
            grade = int(user_info['grade'])
            all_grade = EmployeeInstance().getPermissionByGradeID(grade)
            data = BusinessRuleInstance().getProductDetailByProductsId(id)
            list = ProductInstance().getSku()
            return render_template('admin/product/viewProduct.html', grade=grade, all_grade=all_grade, data=data, list=list, TenantSetting=TenantSetting)
    except (
            AuthenticateFailedException,
            MissingRequiredParametersException,
            Exception,
            RuntimeError
    ) as e:
        response = Response()
        response.headers.add('error_code', e.__str__())
        response.status_code = 490
        error = {
            'code': [4, 9, 0],
            'str': e.__str__()
        }
        return render_template('404.html', error=error)


@product.route('/assembleCreate', methods=['GET', 'POST'])
def assembleCreate():
    try:
        _token = request.form["_token"] if "_token" in request.form else None
        if _token is None:
            return redirect('/login')
        else:
            user_info = JWTInstance().decode_auth_token(_token)
            grade = int(user_info['grade'])
            all_grade = EmployeeInstance().getPermissionByGradeID(grade)
            list = ProductInstance().getSku()
            return render_template('admin/product/newOrEditAssemble.html', grade=grade, all_grade=all_grade, update=0, type=0, list=list, TenantSetting=TenantSetting)
    except (
            AuthenticateFailedException,
            MissingRequiredParametersException,
            Exception,
            RuntimeError
    ) as e:
        response = Response()
        response.headers.add('error_code', e.__str__())
        response.status_code = 490
        error = {
            'code': [4, 9, 0],
            'str': e.__str__()
        }
        return render_template('404.html', error=error)


@product.route('/assembleEdit/<id>', methods=['GET', 'POST'])
def assembleEdit(id):
    try:
        _token = request.form["_token"] if "_token" in request.form else None
        if _token is None:
            return redirect('/login')
        else:
            user_info = JWTInstance().decode_auth_token(_token)
            grade = int(user_info['grade'])
            all_grade = EmployeeInstance().getPermissionByGradeID(grade)
            data = BusinessRuleInstance().getProductDetailByProductsId(id)
            list = ProductInstance().getSku()
            return render_template('admin/product/newOrEditAssemble.html', grade=grade, all_grade=all_grade, update=1, data=data, list=list, TenantSetting=TenantSetting)
    except (
            AuthenticateFailedException,
            MissingRequiredParametersException,
            Exception,
            RuntimeError
    ) as e:
        response = Response()
        response.headers.add('error_code', e.__str__())
        response.status_code = 490
        error = {
            'code': [4, 9, 0],
            'str': e.__str__()
        }
        return render_template('404.html', error=error)


@product.route('/assembleView/<id>', methods=['GET', 'POST'])
def assembleView(id):
    try:
        _token = request.form["_token"] if "_token" in request.form else None
        if _token is None:
            return redirect('/login')
        else:
            user_info = JWTInstance().decode_auth_token(_token)
            grade = int(user_info['grade'])
            all_grade = EmployeeInstance().getPermissionByGradeID(grade)
            data = BusinessRuleInstance().getProductDetailByProductsId(id)
            list = ProductInstance().getSku()
            return render_template('admin/product/viewAssemble.html', grade=grade, all_grade=all_grade, data=data, list=list, TenantSetting=TenantSetting)
    except (
            AuthenticateFailedException,
            MissingRequiredParametersException,
            Exception,
            RuntimeError
    ) as e:
        response = Response()
        response.headers.add('error_code', e.__str__())
        response.status_code = 490
        error = {
            'code': [4, 9, 0],
            'str': e.__str__()
        }
        return render_template('404.html', error=error)


@product.route('/stock', methods=['GET', 'POST'])
def productStock():
    try:
        _token = request.form["_token"] if "_token" in request.form else None
        if _token is None:
            return redirect('/login')
        else:
            user_info = JWTInstance().decode_auth_token(_token)
            grade = int(user_info['grade'])
            all_grade = EmployeeInstance().getPermissionByGradeID(grade)
            list = BusinessRuleInstance().getProductDetailByCategoryId(1)  # 1 = product
            return render_template('admin/product/stock.html', grade=grade, all_grade=all_grade, list=list, TenantSetting=TenantSetting)
    except (
            AuthenticateFailedException,
            MissingRequiredParametersException,
            Exception,
            RuntimeError
    ) as e:
        response = Response()
        response.headers.add('error_code', e.__str__())
        response.status_code = 490
        error = {
            'code': [4, 9, 0],
            'str': e.__str__()
        }
        return render_template('404.html', error=error)


# service type

@product.route('/serviceType', methods=['GET', 'POST'])
def productServiceType():
    try:
        _token = request.form["_token"] if "_token" in request.form else None
        if _token is None:
            return redirect('/login')
        else:
            user_info = JWTInstance().decode_auth_token(_token)
            grade = int(user_info['grade'])
            all_grade = EmployeeInstance().getPermissionByGradeID(grade)
            type = ProductInstance().getServiceCategiry()
            return render_template('admin/product/serviceType.html', grade=grade, all_grade=all_grade, type=type, TenantSetting=TenantSetting)
    except (
            AuthenticateFailedException,
            MissingRequiredParametersException,
            Exception,
            RuntimeError
    ) as e:
        response = Response()
        response.headers.add('error_code', e.__str__())
        response.status_code = 490
        error = {
            'code': [4, 9, 0],
            'str': e.__str__()
        }
        return render_template('404.html', error=error)


@product.route('/ServiceTypeCreate', methods=['GET', 'POST'])
def productServiceTypeCreate():
    try:
        _token = request.form["_token"] if "_token" in request.form else None
        if _token is None:
            return redirect('/login')
        else:
            user_info = JWTInstance().decode_auth_token(_token)
            grade = int(user_info['grade'])
            all_grade = EmployeeInstance().getPermissionByGradeID(grade)
            return render_template('admin/product/newOrEditServiceType.html', grade=grade, all_grade=all_grade, update=0, TenantSetting=TenantSetting)
    except (
            AuthenticateFailedException,
            MissingRequiredParametersException,
            Exception,
            RuntimeError
    ) as e:
        response = Response()
        response.headers.add('error_code', e.__str__())
        response.status_code = 490
        error = {
            'code': [4, 9, 0],
            'str': e.__str__()
        }
        return render_template('404.html', error=error)


@product.route('/ServiceTypeEdit/<id>', methods=['GET', 'POST'])
def productServiceTypeEdit(id):
    try:
        _token = request.form["_token"] if "_token" in request.form else None
        if _token is None:
            return redirect('/login')
        else:
            user_info = JWTInstance().decode_auth_token(_token)
            grade = int(user_info['grade'])
            all_grade = EmployeeInstance().getPermissionByGradeID(grade)
            data = BusinessRuleInstance().getServiceCategoryByID(id)
            return render_template('admin/product/newOrEditServiceType.html', grade=grade, all_grade=all_grade, update=1, data=data, TenantSetting=TenantSetting)
    except (
            AuthenticateFailedException,
            MissingRequiredParametersException,
            Exception,
            RuntimeError
    ) as e:
        response = Response()
        response.headers.add('error_code', e.__str__())
        response.status_code = 490
        error = {
            'code': [4, 9, 0],
            'str': e.__str__()
        }
        return render_template('404.html', error=error)


# service

@product.route('/serviceList', methods=['GET', 'POST'])
def productService():
    try:
        _token = request.form["_token"] if "_token" in request.form else None
        if _token is None:
            return redirect('/login')
        else:
            user_info = JWTInstance().decode_auth_token(_token)
            grade = int(user_info['grade'])
            all_grade = EmployeeInstance().getPermissionByGradeID(grade)
            type = ProductInstance().getServiceCategiry()
            list = BusinessRuleInstance().getProductDetailByCategoryId(3)  # 3 = service
            return render_template('admin/product/serviceList.html', grade=grade, all_grade=all_grade, list=list, type=type, TenantSetting=TenantSetting)
    except (
            AuthenticateFailedException,
            MissingRequiredParametersException,
            Exception,
            RuntimeError
    ) as e:
        response = Response()
        response.headers.add('error_code', e.__str__())
        response.status_code = 490
        error = {
            'code': [4, 9, 0],
            'str': e.__str__()
        }
        return render_template('404.html', error=error)


@product.route('/productServiceCreate', methods=['GET', 'POST'])
def productServiceCreate():
    try:
        _token = request.form["_token"] if "_token" in request.form else None
        if _token is None:
            return redirect('/login')
        else:
            user_info = JWTInstance().decode_auth_token(_token)
            grade = int(user_info['grade'])
            all_grade = EmployeeInstance().getPermissionByGradeID(grade)
            type = ProductInstance().getServiceCategiry()
            roles = EmployeeInstance().getRoleList()
            return render_template('admin/product/newOrEditService.html', grade=grade, all_grade=all_grade, update=0, type=type, roles=roles, TenantSetting=TenantSetting)
    except (
            AuthenticateFailedException,
            MissingRequiredParametersException,
            Exception,
            RuntimeError
    ) as e:
        response = Response()
        response.headers.add('error_code', e.__str__())
        response.status_code = 490
        error = {
            'code': [4, 9, 0],
            'str': e.__str__()
        }
        return render_template('404.html', error=error)


@product.route('/productServiceEdit/<id>', methods=['GET', 'POST'])
def productServiceEdit(id):
    try:
        _token = request.form["_token"] if "_token" in request.form else None
        if _token is None:
            return redirect('/login')
        else:
            user_info = JWTInstance().decode_auth_token(_token)
            grade = int(user_info['grade'])
            all_grade = EmployeeInstance().getPermissionByGradeID(grade)
            type = ProductInstance().getServiceCategiry()
            data = BusinessRuleInstance().getProductDetailByProductsId(id)
            roles = EmployeeInstance().getRoleList()
            return render_template('admin/product/newOrEditService.html', grade=grade, all_grade=all_grade, update=1, data=data, type=type, roles=roles, TenantSetting=TenantSetting)
    except (
            AuthenticateFailedException,
            MissingRequiredParametersException,
            Exception,
            RuntimeError
    ) as e:
        response = Response()
        response.headers.add('error_code', e.__str__())
        response.status_code = 490
        error = {
            'code': [4, 9, 0],
            'str': e.__str__()
        }
        return render_template('404.html', error=error)


@product.route('/productServiceView/<id>', methods=['GET', 'POST'])
def productServiceView(id):
    try:
        _token = request.form["_token"] if "_token" in request.form else None
        if _token is None:
            return redirect('/login')
        else:
            user_info = JWTInstance().decode_auth_token(_token)
            grade = int(user_info['grade'])
            all_grade = EmployeeInstance().getPermissionByGradeID(grade)
            type = ProductInstance().getServiceCategiry()
            data = BusinessRuleInstance().getProductDetailByProductsId(id)
            roles = EmployeeInstance().getRoleList()
            return render_template('admin/product/viewService.html', grade=grade, all_grade=all_grade, data=data, type=type, roles=roles, TenantSetting=TenantSetting)
    except (
            AuthenticateFailedException,
            MissingRequiredParametersException,
            Exception,
            RuntimeError
    ) as e:
        response = Response()
        response.headers.add('error_code', e.__str__())
        response.status_code = 490
        error = {
            'code': [4, 9, 0],
            'str': e.__str__()
        }
        return render_template('404.html', error=error)


# service case

@product.route('/successCase/list', methods=['GET', 'POST'])
def productServiceCase():
    try:
        _token = request.form["_token"] if "_token" in request.form else None
        if _token is None:
            return redirect('/login')
        else:
            user_info = JWTInstance().decode_auth_token(_token)
            grade = int(user_info['grade'])
            all_grade = EmployeeInstance().getPermissionByGradeID(grade)
            list = ProductInstance().getExperiences()
            return render_template('admin/product/serviceCaseList.html', grade=grade, all_grade=all_grade, list=list, type=type, TenantSetting=TenantSetting)
    except (
            AuthenticateFailedException,
            MissingRequiredParametersException,
            Exception,
            RuntimeError
    ) as e:
        response = Response()
        response.headers.add('error_code', e.__str__())
        response.status_code = 490
        error = {
            'code': [4, 9, 0],
            'str': e.__str__()
        }
        return render_template('404.html', error=error)


@product.route('/productServiceCaseCreate', methods=['GET', 'POST'])
def productServiceCaseCreate():
    try:
        _token = request.form["_token"] if "_token" in request.form else None
        if _token is None:
            return redirect('/login')
        else:
            user_info = JWTInstance().decode_auth_token(_token)
            grade = int(user_info['grade'])
            all_grade = EmployeeInstance().getPermissionByGradeID(grade)
            type = BusinessRuleInstance().getProductDetailByCategoryId(3)  # 3 = service
            return render_template('admin/product/newOrEditServiceCase.html', grade=grade, all_grade=all_grade, update=0, type=type, TenantSetting=TenantSetting)
    except (
            AuthenticateFailedException,
            MissingRequiredParametersException,
            Exception,
            RuntimeError
    ) as e:
        response = Response()
        response.headers.add('error_code', e.__str__())
        response.status_code = 490
        error = {
            'code': [4, 9, 0],
            'str': e.__str__()
        }
        return render_template('404.html', error=error)


@product.route('/productServiceCaseEdit/<id>', methods=['GET', 'POST'])
def productServiceCaseEdit(id):
    try:
        _token = request.form["_token"] if "_token" in request.form else None
        if _token is None:
            return redirect('/login')
        else:
            user_info = JWTInstance().decode_auth_token(_token)
            grade = int(user_info['grade'])
            all_grade = EmployeeInstance().getPermissionByGradeID(grade)
            type = BusinessRuleInstance().getProductDetailByCategoryId(3)
            data = ProductInstance().getExperiencesOne(id)
            return render_template('admin/product/newOrEditServiceCase.html', grade=grade, all_grade=all_grade, update=1, data=data, type=type, TenantSetting=TenantSetting)
    except (
            AuthenticateFailedException,
            MissingRequiredParametersException,
            Exception,
            RuntimeError
    ) as e:
        response = Response()
        response.headers.add('error_code', e.__str__())
        response.status_code = 490
        error = {
            'code': [4, 9, 0],
            'str': e.__str__()
        }
        return render_template('404.html', error=error)
from flask import Blueprint, render_template, Response, redirect, request
import json

from Model.Exception import *
from Model.Employee import EmployeeInstance
from Model.Faq import FaqInstance
from Model.JWT import JWTInstance
from Utility.config import TenantSetting


faq = Blueprint('faq', __name__)

def init_app(app):
    app.register_blueprint(faq, url_prefix='/admin/faq')


TenantSetting = TenantSetting.infomation


@faq.route('/index', methods=['GET', 'POST'])
def faqIndex():
    try:
        _token = request.form["_token"] if "_token" in request.form else None
        if _token is None:
            return redirect('/login')
        else:
            user_info = JWTInstance().decode_auth_token(_token)
            grade = int(user_info['grade'])
            all_grade = EmployeeInstance().getPermissionByGradeID(grade)

            data = FaqInstance().getFaq()
            return render_template('admin/faq/index.html', grade=grade, all_grade=all_grade, data=data, TenantSetting=TenantSetting)
    except (
            AuthenticateFailedException,
            MissingRequiredParametersException,
            Exception,
            RuntimeError
    ) as e:
        response = Response()
        response.headers.add('error_code', e.__str__())
        response.status_code = 490
        error = {
            'code': [4, 9, 0],
            'str': e.__str__()
        }
        return render_template('404.html', error=error)


@faq.route('/list', methods=['GET', 'POST'])
def unReply():
    try:
        _token = request.form["_token"] if "_token" in request.form else None
        if _token is None:
            return redirect('/login')
        else:
            user_info = JWTInstance().decode_auth_token(_token)
            grade = int(user_info['grade'])
            all_grade = EmployeeInstance().getPermissionByGradeID(grade)
            data = FaqInstance().getFaqWithoutReply()
            return render_template('admin/faq/list.html', grade=grade, all_grade=all_grade, data=data, TenantSetting=TenantSetting)
    except (
            AuthenticateFailedException,
            MissingRequiredParametersException,
            Exception,
            RuntimeError
    ) as e:
        response = Response()
        response.headers.add('error_code', e.__str__())
        response.status_code = 490
        error = {
            'code': [4, 9, 0],
            'str': e.__str__()
        }
        return render_template('404.html', error=error)


@faq.route('/reply/<id>', methods=['GET', 'POST'])
def reply(id):
    try:
        _token = request.form["_token"] if "_token" in request.form else None
        if _token is None:
            return redirect('/login')
        else:
            user_info = JWTInstance().decode_auth_token(_token)
            grade = int(user_info['grade'])
            all_grade = EmployeeInstance().getPermissionByGradeID(grade)
            data = FaqInstance().getFaqWithoutReply()
            return render_template('admin/faq/reply.html', grade=grade, all_grade=all_grade, id=id, data=data, TenantSetting=TenantSetting)
    except (
            AuthenticateFailedException,
            MissingRequiredParametersException,
            Exception,
            RuntimeError
    ) as e:
        response = Response()
        response.headers.add('error_code', e.__str__())
        response.status_code = 490
        error = {
            'code': [4, 9, 0],
            'str': e.__str__()
        }
        return render_template('404.html', error=error)
from flask import Blueprint, render_template, redirect, url_for, escape, request, Response, make_response
from datetime import datetime, date, timedelta
import json

from Model.BusinessRule import BusinessRuleInstance
from Model.Exception import *
from Model.Employee import EmployeeInstance
from Model.JWT import JWTInstance
from Model.Setting import SettingInstance
from Model.Product import ProductInstance
from Utility.config import TenantSetting

adminn = Blueprint('admin', __name__)


def init_app(app):
    app.register_blueprint(adminn, url_prefix='/')


TenantSetting = TenantSetting.infomation


@adminn.route('login', methods=['GET', 'POST'])
def login():
    return render_template("admin/login.html", TenantSetting=TenantSetting)

@adminn.route('redirect/<path>', methods=['GET', 'POST'])
def redirect(path):
    return render_template("admin/redirect.html",path=path, TenantSetting=TenantSetting)

@adminn.route('admin/dashboard', methods=['GET', 'POST'])
def admin():
    try:
        _token = request.form["_token"] if "_token" in request.form else None
        if _token is None:
            return redirect('/login')
        else:
            user_info = JWTInstance().decode_auth_token(_token)
            grade = int(user_info['grade'])
            all_grade = EmployeeInstance().getPermissionByGradeID(grade)
            personal_uuid = user_info['uuid']
            personal_id = EmployeeInstance().getEmployeeIDByUuid(personal_uuid)
            products = ProductInstance().getSku()

            dateToday = date.today()
            numOfUsers = BusinessRuleInstance().getNumOfUsers()
            allUnusedAmount = BusinessRuleInstance().getAllUnusedAmount()
            salesToday = BusinessRuleInstance().getSalesByDate(dateToday)
            salesTodayPersonal = BusinessRuleInstance().getSalesByDateOnlyPersonal(dateToday, personal_id)

            thisYear = int(dateToday.strftime('%Y'))
            thisMonth = int(dateToday.strftime('%m'))
            salesThisMonth = BusinessRuleInstance().getSalesByMonth(thisYear, thisMonth)
            salesThisMonthPersonal = BusinessRuleInstance().getSalesByMonthOnlyPersonal(thisYear, thisMonth, personal_id)

            return render_template(
                "admin/dashboard.html",
                grade=grade,
                all_grade=all_grade,
                products=products,
                numOfUsers=numOfUsers,
                allUnusedAmount=allUnusedAmount,
                salesToday=salesToday,
                salesTodayPersonal=salesTodayPersonal,
                salesThisMonth=salesThisMonth,
                salesThisMonthPersonal=salesThisMonthPersonal,
                TenantSetting=TenantSetting
            )
    except (JWTAuthenticateFailedException) as e:
        return redirect('/login')
    except (
            AuthenticateFailedException,
            MissingRequiredParametersException,
            GradeDoesNotExistException,
            TargetIdDoesNotExistException,
            Exception,
            RuntimeError
    ) as e:
        response = Response()
        response.headers.add('error_code', e.__str__())
        response.status_code = 490
        error = {
            'code': [4, 9, 0],
            'str': e.__str__()
        }
        # return response
        return render_template('404.html', error=error)


@adminn.route('authenticate', methods=['POST'])
def authenticate():
    try:
        account = request.json["account"] if "account" in request.json else None
        password = request.json["password"] if "password" in request.json else None

        if account is None or password is None:
            raise MissingRequiredParametersException()
        else:
            profile = EmployeeInstance().authenticate(account, password)
            token = JWTInstance().encode_auth_token(profile['name'], profile['uuid'], profile['grade_id'])
            # response = make_response(render_template('admin/dashboard.html'))
            # response.headers.add('Authorization', _token, httponly=True)
            # return response

            return Response(json.dumps({'token': 'Bearer {}'.format(token)}), mimetype='application/json')
    except (
            AuthenticateFailedException,
            MissingRequiredParametersException,
            Exception,
            RuntimeError
    ) as e:
        response = Response()
        response.headers.add('error_code', e.__str__())
        response.status_code = 490
        return response


@adminn.route('check/password', methods=['POST'])
def checkPassword():
    try:
        uuid = '9984aa8e-cf81-476d-a809-538d6f453592'   # TODO: remove hard code
        password = request.json["password"] if "password" in request.json else None

        if uuid is None or password is None:
            raise MissingRequiredParametersException()
        else:
            response = EmployeeInstance().checkPassword(uuid, password)
            return Response(json.dumps(response), mimetype='application/json')
    except (
        AuthenticateFailedException,
        MissingRequiredParametersException,
        Exception,
        RuntimeError
    ) as e:
        response = Response()
        response.headers.add('error_code', e.__str__())
        response.status_code = 490

        error = {
            'code': [4, 9, 0],
            'str': e.__str__()
        }
        return render_template('404.html', error=error)

from flask import Blueprint, render_template, session, redirect, url_for, escape, request, Response
import json

from Model.Exception import *
from Model.Setting import SettingInstance
from Model.BusinessRule import BusinessRuleInstance
from Model.Product import ProductInstance
from Model.Employee import EmployeeInstance
from Utility.config import TenantSetting


frontend = Blueprint('frontend', __name__)


def init_app(app):
    app.register_blueprint(frontend, url_prefix='/')


TenantSetting = TenantSetting.infomation


@frontend.route('/')
@frontend.route('home')
def index():
    sliderList = SettingInstance().getSliderList()
    news = SettingInstance().getNewsListForFrontend(1, 'news')
    product = BusinessRuleInstance().getProductDetailByCategoryIdForFrontend(1) # 1 = product
    assemble = BusinessRuleInstance().getProductDetailByCategoryIdForFrontend(2) # 2 = assemble
    service = BusinessRuleInstance().getProductDetailByCategoryIdForFrontend(3) # 3 = service
    case = ProductInstance().getExperiences()
    employee = EmployeeInstance().getEmployeeList()
    roles = EmployeeInstance().getRoleList()
    shares = SettingInstance().getNewsList(1, 'shares')
    
    return render_template('index.html', reloadFunction='indexReload()', sliderList=sliderList, news=news, product=product, assemble=assemble, service=service, case=case, shares=shares,employee=employee,roles=roles, TenantSetting=TenantSetting)


@frontend.route('about')
def about():
    sliderList = SettingInstance().getSliderList()
    brand = SettingInstance().getSettingData(1)
    return render_template('about.html', href='about', reloadFunction='aboutReload()', sliderList=sliderList, brand=brand, TenantSetting=TenantSetting)

@frontend.route('newsList')
def newsList():
    news = SettingInstance().getNewsList(1, 'news')
    return render_template('extend/newsList.html', news=news, TenantSetting=TenantSetting)

@frontend.route('product')
def product():
    sliderList = SettingInstance().getSliderList()
    product = BusinessRuleInstance().getProductDetailByCategoryIdForFrontend(1) # 1 = product
    assemble = BusinessRuleInstance().getProductDetailByCategoryIdForFrontend(2) # 2 = assemble
    return render_template('product.html', href='product', reloadFunction='productReload()', sliderList=sliderList, product=product, assemble=assemble, TenantSetting=TenantSetting)


@frontend.route('product/item/<id>')
def productItem(id):
    data = BusinessRuleInstance().getProductDetailByProductsId(id)
    return render_template('extend/productModal.html', data=data, TenantSetting=TenantSetting)


@frontend.route('service')
def service():
    sliderList = SettingInstance().getSliderList()
    service = BusinessRuleInstance().getProductDetailByCategoryIdForFrontend(3) # 3 = service
    type = ProductInstance().getServiceCategiryForFrontend()
    return render_template('service.html', href='service', reloadFunction='serviceReload()', sliderList=sliderList, service=service, type=type, TenantSetting=TenantSetting)


@frontend.route('service/item/<id>')
def serviceItem(id):
    data = BusinessRuleInstance().getProductDetailByProductsId(id)
    return render_template('extend/serviceModal.html', data=data, TenantSetting=TenantSetting)


@frontend.route('cast')
def cast():
    sliderList = SettingInstance().getSliderList()
    employee = EmployeeInstance().getEmployeeList()
    roles = EmployeeInstance().getRoleList()
    return render_template('cast.html', href='cast', reloadFunction='castReloadI()', sliderList=sliderList,employee=employee,roles=roles, TenantSetting=TenantSetting)


@frontend.route('cast/item/<id>')
def castItem(id):
    data = EmployeeInstance().getEmployeeData(id)
    roles = EmployeeInstance().getRoleList()
    return render_template('extend/castModal.html', data=data,roles=roles, TenantSetting=TenantSetting)


@frontend.route('case')
def case():
    sliderList = SettingInstance().getSliderList()
    case = ProductInstance().getExperiences()
    return render_template('case.html', href='case', reloadFunction='', sliderList=sliderList , case=case, TenantSetting=TenantSetting)


@frontend.route('case/item/<id>')
def caseItem(id):
    data = ProductInstance().getExperiencesOne(id)
    return render_template('extend/caseModal.html', data=data, TenantSetting=TenantSetting)


@frontend.route('knows')
def knows():
    sliderList = SettingInstance().getSliderList()
    shares = SettingInstance().getNewsListForFrontend(1, 'shares')
    return render_template('knows.html', href='knows', reloadFunction='knowsReload()', sliderList=sliderList, shares=shares, TenantSetting=TenantSetting)


@frontend.route('knows/item')
def knowsItem():
    return render_template('extend/knowsModal.html', TenantSetting=TenantSetting)


@frontend.route('svg')
def svg():
    return render_template('svg.html', TenantSetting=TenantSetting)

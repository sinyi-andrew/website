__author__ = 'PC-015'

from sqlalchemy import *
from Database import DatabaseFactory
from sqlalchemy.dialects.mysql import *

db_factory = DatabaseFactory()
engine = db_factory.get_db_engine()
meta = MetaData()

employee_calendar = Table('employee_calendar', meta,
                          Column('id', Integer, primary_key=True, autoincrement=True),
                          Column('employee_id', Integer, nullable=False),
                          Column('date', DateTime(timezone=True), nullable=False),
                          )
employee_calendar.create(engine)

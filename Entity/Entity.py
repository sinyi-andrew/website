from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import *
from sqlalchemy.dialects.mysql import *
from sqlalchemy import ForeignKey
from sqlalchemy.orm import relationship

base = declarative_base()


class StoreEntity(base):
    __tablename__ = "stores"
    id = Column('id', Integer, primary_key=True, autoincrement=True)
    name = Column('name', NVARCHAR(50), nullable=False)
    address = Column('address', NVARCHAR(128), nullable=False)
    description = Column('description', NVARCHAR(128), nullable=False)


class FunctionEntity(base):
    __tablename__ = "functions"
    id = Column('id', Integer, primary_key=True, autoincrement=True)
    store_id = Column('store_id', Integer, nullable=False)
    name = Column('name', NVARCHAR(50), nullable=False)
    description = Column('description', NVARCHAR(128), nullable=False)


class PermissionEntity(base):
    __tablename__ = "permissions"
    id = Column('id', Integer, primary_key=True, autoincrement=True)
    store_id = Column('store_id', Integer, nullable=False)
    grade_id = Column('grade_id', Integer, nullable=False)
    function_id = Column('function_id', Integer, nullable=False)
    permission = Column('permission', Integer, nullable=False)


class EmployeeGradesEntity(base):
    __tablename__ = "employee_grades"
    id = Column('id', Integer, primary_key=True, autoincrement=True)
    name = Column('name', NVARCHAR(50), nullable=False)
    description = Column('description', NVARCHAR(128), nullable=False)
    sale = Column('sale', BOOLEAN, nullable=False)


class EmployeesEntity(base):
    __tablename__ = "employees"
    id = Column('id', Integer, primary_key=True, autoincrement=True)
    employee_number = Column('employee_number', VARCHAR(8), nullable=True)
    grade_id = Column('grade_id', Integer, nullable=False)
    store_id = Column('store_id', Integer, nullable=False)
    uuid = Column('uuid', NVARCHAR(40), nullable=False, unique=True)
    name = Column('name', NVARCHAR(50), nullable=False)
    IDNumber = Column('IDNumber', NVARCHAR(50), nullable=False, unique=True)
    email = Column('email', NVARCHAR(128), nullable=False, unique=True)
    password = Column('password', NVARCHAR(256), nullable=False)
    phone_number = Column('phone_number', NVARCHAR(40))
    mobile_phone = Column('mobile_phone', NVARCHAR(40), nullable=False)
    birthday = Column('birthday', DateTime(timezone=True), nullable=False)
    bloodtype = Column('bloodtype', NVARCHAR(10), nullable=False)
    gender = Column('gender', NVARCHAR(10), nullable=False)
    address = Column('address', NVARCHAR(128), nullable=False)
    height = Column('height', NVARCHAR(20))
    weight = Column('weight', NVARCHAR(20))
    marriage = Column('marriage', BOOLEAN)
    skill = Column('skill', LONGTEXT)
    position_change = Column('position_change', LONGTEXT)
    education = Column('education', LONGTEXT)
    experience = Column('experience', LONGTEXT)
    certificate = Column('certificate', LONGTEXT)
    note = Column('note', NVARCHAR(256))
    contact = Column('contact', NVARCHAR(128), nullable=False)
    contact_phone = Column('contact_phone', NVARCHAR(128), nullable=False)
    create_date = Column('create_date', DateTime(timezone=True), nullable=False)
    status = Column('status', BOOLEAN, nullable=False)
    image = Column('image', LONGTEXT)


class EmployeeCalendarEntity(base):
    __tablename__ = "employee_calendar"
    id = Column('id', Integer, primary_key=True, autoincrement=True)
    employee_id = Column('employee_id', Integer, nullable=False)
    start_time = Column('start_time', NVARCHAR(30), nullable=False)
    end_time = Column('end_time', NVARCHAR(30), nullable=False)
    input_personnel = Column('input_personnel', Integer, nullable=False)
    status = Column('status', Integer, nullable=False)
    note = Column('note', NVARCHAR(256))


class UserGradesEntity(base):
    __tablename__ = "user_grades"
    id = Column('id', Integer, primary_key=True, autoincrement=True)
    name = Column('name', NVARCHAR(50), nullable=False)
    description = Column('description', NVARCHAR(128), nullable=False)
    discount = Column('discount', Float, nullable=False)
    shipping_fee = Column('shipping_fee', Integer, nullable=False)
    shipping_setting = Column('shipping_setting', Integer, nullable=False)


class UsersEntity(base):
    __tablename__ = "users"
    id = Column('id', Integer, primary_key=True, autoincrement=True)
    user_number = Column('user_number', VARCHAR(8), nullable=False)
    grade_id = Column('grade_id', Integer, nullable=False)
    uuid = Column('uuid', NVARCHAR(40), nullable=False, unique=True)
    name = Column('name', NVARCHAR(50), nullable=False)
    IDNumber = Column('IDNumber', NVARCHAR(50), nullable=False, unique=True)
    email = Column('email', NVARCHAR(128), nullable=False)
    phone_number = Column('phone_number', NVARCHAR(40))
    mobile_phone = Column('mobile_phone', NVARCHAR(40), nullable=False)
    birthday = Column('birthday', DateTime(timezone=True), nullable=False)
    bloodtype = Column('bloodtype', NVARCHAR(10), nullable=False)
    gender = Column('gender', NVARCHAR(30), nullable=False)
    address = Column('address', NVARCHAR(128), nullable=False)
    contact = Column('contact', NVARCHAR(128), nullable=False)
    contact_phone = Column('contact_phone', NVARCHAR(128), nullable=False)
    height = Column('height', NVARCHAR(20))
    weight = Column('weight', NVARCHAR(20))
    improve = Column('improve', LONGTEXT)
    disease_history = Column('disease_history', LONGTEXT)
    allergy_history = Column('allergy_history', LONGTEXT)
    whereform = Column('whereform', NVARCHAR(128))
    note = Column('note', NVARCHAR(256))
    create_date = Column('create_date', DateTime(timezone=True), nullable=False)


class UserAccountsEntity(base):
    __tablename__ = "user_accounts"
    id = Column('id', Integer, primary_key=True, autoincrement=True)
    user_id = Column('user_id', Integer, nullable=False)
    order_id = Column('order_id', Integer)
    employee_id = Column('employee_id', Integer, nullable=False)
    point = Column('point', NVARCHAR(50), nullable=False)
    number = Column('number', NVARCHAR(128))
    date = Column('date', DateTime(timezone=True), nullable=False)


class ProductCategoriesEntity(base):
    __tablename__ = "product_categories"
    # id 1: product
    # id 2: assemble
    # id 3~...: service
    id = Column('id', Integer, primary_key=True, autoincrement=True)
    name = Column('name', NVARCHAR(50), nullable=False)
    description = Column('description', NVARCHAR(128), nullable=False)
    type = Column('type', NVARCHAR(128), nullable=False)


class SkusEntity(base):
    __tablename__ = "skus"
    id = Column('id', Integer, primary_key=True, autoincrement=True)
    sku_number = Column('sku_number', VARCHAR(11), nullable=True)
    product_id = Column('product_id', Integer, nullable=False)
    number = Column('number', Integer, nullable=False)
    price = Column('price', Integer, nullable=False)
    expire = Column('expire', Integer)
    product_serial_number = Column('product_serial_number', NVARCHAR(40), nullable=False)
    only_for_assemable = Column('only_for_assemable', BOOLEAN, nullable=False)
    status = Column('status', BOOLEAN, nullable=False)


class ServicePartsEntity(base):
    __tablename__ = "service_parts"
    id = Column('id', Integer, primary_key=True, autoincrement=True)
    product_id = Column('product_id', Integer, nullable=False)
    employee_grade = Column('employee_grade_id', Integer, nullable=False)
    number = Column('number', Integer, nullable=False)
    technical_fee = Column('technical_fee', Integer, nullable=False)


class AssembleEntity(base):
    __tablename__ = "assemble"
    id = Column('id', Integer, primary_key=True, autoincrement=True)
    parent_sku_id = Column('parent_sku_id', Integer, nullable=False)
    sku_id = Column('sku_id', Integer, nullable=False)
    sort = Column('sort', INTEGER, nullable=False)
    delete_time = Column('delete_time', DateTime(timezone=True))


class ProductsEntity(base):
    __tablename__ = "products"
    id = Column('id', Integer, primary_key=True, autoincrement=True)
    category_id = Column('category_id', Integer, nullable=False)
    name = Column('name', NVARCHAR(128), nullable=False)
    image = Column('image', LONGTEXT, nullable=False)
    discount = Column('discount', BOOLEAN)
    intro = Column('intro', LONGTEXT)
    description = Column('description', LONGTEXT)
    ingredient = Column('ingredient', LONGTEXT)
    care = Column('care', LONGTEXT)
    notice = Column('notice', LONGTEXT)
    faq = Column('faq', LONGTEXT)
    note = Column('note', LONGTEXT)
    tip = Column('tip', LONGTEXT)
    sort = Column('sort', INTEGER, nullable=False)
    status = Column('status', BOOLEAN, nullable=False)


class ExperiencesEntity(base):
    __tablename__ = "experiences"
    id = Column('id', Integer, primary_key=True, autoincrement=True)
    product_id = Column('product_id', Integer, nullable=False)
    employee_id = Column('employee_id', Integer, nullable=False)
    image = Column('image', LONGTEXT, nullable=False)
    interview = Column('interview', LONGTEXT)
    advisory = Column('advisory', LONGTEXT)
    suggest = Column('suggest', LONGTEXT)
    results = Column('results', LONGTEXT)
    essence = Column('essence', BOOLEAN, nullable=False)
    status = Column('status', BOOLEAN, nullable=False)
    sort = Column('sort', INTEGER, nullable=False)


class MainSlidersEntity(base):
    __tablename__ = "main_sliders"
    id = Column('id', Integer, primary_key=True, autoincrement=True)
    product_id = Column('product_id', Integer, nullable=False)
    image = Column('image', LONGTEXT, nullable=False)
    sort = Column('sort', NVARCHAR(50), nullable=False)
    description = Column('description', LONGTEXT)
    delete_time = Column('delete_time', DateTime(timezone=True))


class OrdersEntity(base):
    __tablename__ = "orders"
    id = Column('id', Integer, primary_key=True, autoincrement=True)
    order_number = Column('order_number', VARCHAR(11), nullable=True)
    store_id = Column('store_id', Integer, nullable=False)
    user_id = Column('user_id', Integer, nullable=False)
    saler_id = Column('saler_id', Integer, nullable=False)
    employee_id = Column('employee_id', Integer, nullable=False)
    total_price = Column('total_price', Integer, nullable=False)
    payment_type = Column('payment_type', Integer, nullable=False)
    shipping_type = Column('shipping_type', Integer, nullable=False)
    shipping_fee = Column('shipping_fee', Integer, nullable=False)
    status = Column('status', Integer, nullable=False)
    date = Column('date', DateTime(timezone=True), nullable=False)
    product_serial_number = Column('product_serial_number', VARCHAR(40), nullable=False)
    input_personnel = Column('input_personnel', Integer, nullable=False)
    cancel_employee_id = Column('cancel_employee_id', Integer)
    recipient = Column('recipient', NVARCHAR(50), nullable=False)
    recipient_phone_number = Column('recipient_phone_number', NVARCHAR(40), nullable=False)
    recipient_deliver_address = Column('recipient_deliver_address', NVARCHAR(128), nullable=False)
    shipping_date = Column('shipping_date', DateTime(timezone=True), nullable=False)
    refund = Column('refund', BOOLEAN)
    modify_date = Column('modify_date', DateTime(timezone=True), nullable=False)
    sales_reocrd_date = Column('sales_reocrd_date', DateTime(timezone=True), nullable=False)
    creditcard_note = Column('creditcard_note', NVARCHAR(20))


class OrderItemsEntity(base):
    __tablename__ = "order_items"
    id = Column('id', Integer, primary_key=True, autoincrement=True)
    order_id = Column('order_id', Integer, nullable=False)
    sku_id = Column('sku_id', Integer, nullable=False)
    price = Column('price', Integer, nullable=False)
    number = Column('number', Integer, nullable=False)
    quantity = Column('quantity', Integer, nullable=False)
    total_amount = Column('total_amount', Integer, nullable=False)
    status = Column('status', Integer, nullable=False)
    date = Column('date', DateTime(timezone=True), nullable=False)
    used = Column('used', Integer, nullable=False)
    exp_date = Column('exp_date', Date, nullable=False)
    name = Column('name', NVARCHAR(128), nullable=False)
    note = Column('note', NVARCHAR(256))
    input_personnel = Column('input_personnel', Integer)
    price_diff = Column('price_diff', Integer)


class EventsEntity(base):
    __tablename__ = "events"
    id = Column('id', Integer, primary_key=True, autoincrement=True)
    store_id = Column('store_id', Integer, nullable=False)
    order_item_id = Column('order_item_id', Integer, nullable=False)
    user_id = Column('user_id', Integer, nullable=False)
    product_name = Column('product_name', NVARCHAR(50), nullable=False)
    start_time = Column('start_time', DateTime(timezone=True), nullable=False)
    end_time = Column('end_time', DateTime(timezone=True), nullable=False)
    note = Column('note', NVARCHAR(256))
    input_personnel = Column('input_personnel', Integer, nullable=False)
    status = Column('status', Integer, nullable=False)
    registered = Column('registered', Integer)
    doctor_check = Column('doctor_check', Integer)
    cancel_employee_id = Column('cancel_employee_id', Integer)


class EventEmployeeEntity(base):
    __tablename__ = "event_employee"
    id = Column('id', Integer, primary_key=True, autoincrement=True)
    event_id = Column('event_id', Integer, nullable=False)
    grade_id = Column('grade_id', Integer, nullable=False)
    employee_id = Column('employee_id', Integer, nullable=False)


class SuppliesEntity(base):
    __tablename__ = "supplies"
    id = Column('id', Integer, primary_key=True, autoincrement=True)
    name = Column('name', NVARCHAR(50), nullable=False)
    cost = Column('cost', Integer, nullable=False)
    product_id = Column('product_id', Integer)
    employee_id = Column('employee_id', Integer, nullable=False)


class SuppliesInventoryEntity(base):
    __tablename__ = "supplies_inventory"
    id = Column('id', Integer, primary_key=True, autoincrement=True)
    supplies_id = Column('supplies_id', Integer, nullable=False)
    employee_id = Column('employee_id', Integer, nullable=False)
    quantity = Column('quantity', Integer, nullable=False)
    note = Column('note', NVARCHAR(128), nullable=False)
    date = Column('date', DateTime(timezone=True), nullable=False)
    type = Column('type', BOOLEAN, nullable=False)
    current_single_cost = Column('current_single_cost', Integer, nullable=False)


class SettingsEntity(base):
    __tablename__ = "settings"
    id = Column('id', Integer, primary_key=True, autoincrement=True)
    store_id = Column('store_id', Integer, nullable=False)
    title = Column('title', LONGTEXT, nullable=False)
    story = Column('story', LONGTEXT, nullable=False)
    slogan = Column('slogan', LONGTEXT, nullable=False)
    idea = Column('idea', LONGTEXT, nullable=False)
    about = Column('about', LONGTEXT, nullable=False)
    date = Column('date', DateTime(timezone=True), nullable=False)


class NewsEntity(base):
    __tablename__ = "news"
    id = Column('id', Integer, primary_key=True, autoincrement=True)
    store_id = Column('store_id', Integer, nullable=False)
    type = Column('type', NVARCHAR(50), nullable=False)
    title = Column('title', NVARCHAR(128), nullable=False)
    content = Column('content', LONGTEXT, nullable=False)
    author = Column('author', NVARCHAR(128))
    status = Column('status', BOOLEAN, nullable=False)
    date = Column('date', Date, nullable=False)
    delete_time = Column('delete_time', DateTime(timezone=True))
    input_personnel = Column('input_personnel', Integer, nullable=False)


class FaqsEntity(base):
    __tablename__ = "faqs"
    id = Column('id', Integer, primary_key=True, autoincrement=True)
    store_id = Column('store_id', Integer, nullable=False)
    type = Column('type', NVARCHAR(50), nullable=False)
    name = Column('name', NVARCHAR(50), nullable=False)
    email = Column('email', NVARCHAR(128), nullable=False)
    phone_number = Column('phone_number', NVARCHAR(20))
    mobile_phone = Column('mobile_phone', NVARCHAR(20), nullable=False)
    birthday = Column('birthday', Date, nullable=False)
    gender = Column('gender', Integer, nullable=False)
    content = Column('content', LONGTEXT, nullable=False)
    date = Column('date', DateTime(timezone=True), nullable=False)
    reply = Column('reply', BOOLEAN, nullable=False)
    reply_content = Column('reply_content', LONGTEXT)
    reply_personnel = Column('reply_personnel', Integer)


class AuthenticationEntity(base):
    __tablename__ = "authentication"
    id = Column('id', INTEGER, primary_key=True, autoincrement=True)
    auth_token = Column('auth_token', VARCHAR(40), nullable=False)
    auth_valid_date = Column('auth_valid_date', Integer, nullable=False)
    auth_create_date = Column('auth_create_date', Integer, nullable=False)
    auth_account_id = Column('auth_account_id', INTEGER, nullable=False)
    auth_flag = Column('auth_flag', Boolean, nullable=False)
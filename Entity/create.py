__author__ = 'PC-015'

from sqlalchemy import *
from Database import DatabaseFactory
from sqlalchemy.dialects.mysql import *

db_factory = DatabaseFactory()
engine = db_factory.get_db_engine()
meta = MetaData()

stores = Table('stores', meta,
               Column('id', Integer, primary_key=True, autoincrement=True),
               Column('name', NVARCHAR(50), nullable=False),
               Column('address', NVARCHAR(128), nullable=False),
               Column('description', NVARCHAR(128), nullable=False),
               )
stores.create(engine)

functions = Table('functions', meta,
                  Column('id', Integer, primary_key=True, autoincrement=True),
                  Column('store_id', Integer, nullable=False),
                  Column('name', NVARCHAR(50), nullable=False),
                  Column('description', NVARCHAR(128), nullable=False),
                  )
functions.create(engine)

permissions = Table('permissions', meta,
                    Column('id', Integer, primary_key=True, autoincrement=True),
                    Column('store_id', Integer, nullable=False),
                    Column('grade_id', Integer, nullable=False),
                    Column('function_id', Integer, nullable=False),
                    Column('permission', Integer, nullable=False),
                    )
permissions.create(engine)

employee_grades = Table('employee_grades', meta,
                        Column('id', Integer, primary_key=True, autoincrement=True),
                        Column('name', NVARCHAR(50), nullable=False),
                        Column('description', NVARCHAR(128), nullable=False),
                        Column('sale', BOOLEAN, nullable=False),
                        )
employee_grades.create(engine)

employees = Table('employees', meta,
                  Column('id', Integer, primary_key=True, autoincrement=True),
                  Column('employee_number', VARCHAR(8), nullable=False),
                  Column('grade_id', Integer, nullable=False),
                  Column('store_id', Integer, nullable=False),
                  Column('uuid', NVARCHAR(40), nullable=False, unique=True),
                  Column('name', NVARCHAR(50), nullable=False),
                  Column('IDNumber', NVARCHAR(50), nullable=False, unique=True),
                  Column('email', NVARCHAR(128), nullable=False),
                  Column('password', NVARCHAR(256), nullable=False),
                  Column('phone_number', NVARCHAR(40)),
                  Column('mobile_phone', NVARCHAR(40), nullable=False),
                  Column('birthday', Date, nullable=False),
                  Column('bloodtype', NVARCHAR(10), nullable=False),
                  Column('gender', NVARCHAR(10), nullable=False),
                  Column('address', NVARCHAR(128), nullable=False),
                  Column('height', NVARCHAR(20)),
                  Column('weight', NVARCHAR(20)),
                  Column('marriage', BOOLEAN, nullable=False),
                  Column('skill', LONGTEXT),
                  Column('position_change', LONGTEXT),
                  Column('education', LONGTEXT),
                  Column('experience', LONGTEXT),
                  Column('certificate', LONGTEXT),
                  Column('note', NVARCHAR(256)),
                  Column('contact', NVARCHAR(128), nullable=False),
                  Column('contact_phone', NVARCHAR(128), nullable=False),
                  Column('create_date', DateTime(timezone=True), nullable=False),
                  Column('status', BOOLEAN, nullable=False),
                  Column('image', LONGTEXT)
                  )
employees.create(engine)

employee_calendar = Table('employee_calendar', meta,
                          Column('id', Integer, primary_key=True, autoincrement=True),
                          Column('employee_id', Integer, nullable=False),
                          Column('start_time', NVARCHAR(30), nullable=False),
                          Column('end_time', NVARCHAR(30), nullable=False),
                          Column('input_personnel', Integer, nullable=False),
                          Column('status', Integer, nullable=False),
                          Column('note', NVARCHAR(256))
                          )
employee_calendar.create(engine)

user_grades = Table('user_grades', meta,
                    Column('id', Integer, primary_key=True, autoincrement=True),
                    Column('name', NVARCHAR(50), nullable=False),
                    Column('description', NVARCHAR(128), nullable=False),
                    Column('discount', Float, nullable=False),
                    Column('shipping_fee', Integer, nullable=False),
                    Column('shipping_setting', Integer, nullable=False)
                    )
user_grades.create(engine)

users = Table('users', meta,
              Column('id', Integer, primary_key=True, autoincrement=True),
              Column('user_number', VARCHAR(8), nullable=False),
              Column('grade_id', Integer, nullable=False),
              Column('uuid', NVARCHAR(40), nullable=False, unique=True),
              Column('name', NVARCHAR(50), nullable=False),
              Column('IDNumber', NVARCHAR(50), nullable=False, unique=True),
              Column('email', NVARCHAR(128), nullable=False),
              Column('phone_number', NVARCHAR(40)),
              Column('mobile_phone', NVARCHAR(40), nullable=False),
              Column('birthday', Date, nullable=False),
              Column('bloodtype', NVARCHAR(10), nullable=False),
              Column('gender', NVARCHAR(30), nullable=False),
              Column('address', NVARCHAR(128), nullable=False),
              Column('contact', NVARCHAR(128), nullable=False),
              Column('contact_phone', NVARCHAR(128), nullable=False),
              Column('height', NVARCHAR(20)),
              Column('weight', NVARCHAR(20)),
              Column('improve', LONGTEXT),
              Column('disease_history', LONGTEXT),
              Column('allergy_history', LONGTEXT),
              Column('whereform', NVARCHAR(128)),
              Column('note', NVARCHAR(256)),
              Column('create_date', DateTime(timezone=True), nullable=False),
              )
users.create(engine)

user_accounts = Table('user_accounts', meta,
                      Column('id', Integer, primary_key=True, autoincrement=True),
                      Column('user_id', Integer, nullable=False),
                      Column('order_id', Integer),
                      Column('employee_id', Integer, nullable=False),
                      Column('point', NVARCHAR(50), nullable=False),
                      Column('number', NVARCHAR(128)),
                      Column('date', DateTime(timezone=True), nullable=False),
                      )
user_accounts.create(engine)

product_categories = Table('product_categories', meta,
                           Column('id', Integer, primary_key=True, autoincrement=True),
                           Column('name', NVARCHAR(50), nullable=False),
                           Column('description', NVARCHAR(128), nullable=False),
                           Column('type', NVARCHAR(128), nullable=False)
                           )
product_categories.create(engine)

skus = Table('skus', meta,
             Column('id', Integer, primary_key=True, autoincrement=True),
             Column('sku_number', VARCHAR(11), nullable=True),
             Column('product_id', Integer, nullable=False),
             Column('number', Integer, nullable=False),
             Column('price', Integer, nullable=False),
             Column('expire', Integer),
             Column('product_id', Integer),
             Column('product_serial_number', NVARCHAR(40), nullable=False),
             Column('only_for_assemable', BOOLEAN, nullable=False),
             Column('status', BOOLEAN, nullable=False)
             )
skus.create(engine)

service_parts = Table('service_parts', meta,
                      Column('id', Integer, primary_key=True, autoincrement=True),
                      Column('product_id', Integer, nullable=False),
                      Column('employee_grade_id', Integer, nullable=False),
                      Column('number', Integer, nullable=False),
                      Column('technical_fee', Integer, nullable=False)
                      )
service_parts.create(engine)

assemble = Table('assemble', meta,
                 Column('id', Integer, primary_key=True, autoincrement=True),
                 Column('parent_sku_id', Integer, nullable=False),
                 Column('sku_id', Integer, nullable=False),
                 Column('sort', INTEGER, nullable=False),
                 Column('delete_time', DateTime(timezone=True))
                 )
assemble.create(engine)

products = Table('products', meta,
                 Column('id', Integer, primary_key=True, autoincrement=True),
                 Column('category_id', Integer, nullable=False),
                 Column('name', NVARCHAR(128), nullable=False),
                 Column('image', LONGTEXT, nullable=False),
                 Column('discount', BOOLEAN),
                 Column('intro', LONGTEXT),
                 Column('description', LONGTEXT),
                 Column('ingredient', LONGTEXT),
                 Column('care', LONGTEXT),
                 Column('notice', LONGTEXT),
                 Column('faq', LONGTEXT),
                 Column('note', LONGTEXT),
                 Column('tip', LONGTEXT),
                 Column('sort', INTEGER, nullable=False),
                 Column('status', BOOLEAN, nullable=False)
                 )
products.create(engine)

experiences = Table('experiences', meta,
                    Column('id', Integer, primary_key=True, autoincrement=True),
                    Column('product_id', Integer, nullable=False),
                    Column('employee_id', Integer, nullable=False),
                    Column('image', LONGTEXT, nullable=False),
                    Column('interview', LONGTEXT),
                    Column('advisory', LONGTEXT),
                    Column('suggest', LONGTEXT),
                    Column('results', LONGTEXT),
                    Column('essence', BOOLEAN, nullable=False),
                    Column('status', BOOLEAN, nullable=False)
                    )
experiences.create(engine)

main_sliders = Table('main_sliders', meta,
                     Column('id', Integer, primary_key=True, autoincrement=True),
                     Column('product_id', Integer, nullable=False),
                     Column('image', LONGTEXT, nullable=False),
                     Column('sort', NVARCHAR(50), nullable=False),
                     Column('description', LONGTEXT),
                     Column('delete_time', DateTime(timezone=True))
                     )
main_sliders.create(engine)

orders = Table('orders', meta,
               Column('id', Integer, primary_key=True, autoincrement=True),
               Column('order_number', VARCHAR(11), nullable=True),
               Column('store_id', Integer, nullable=False),
               Column('user_id', Integer, nullable=False),
               Column('saler_id', Integer, nullable=False),
               Column('employee_id', Integer, nullable=False),
               Column('total_price', Integer, nullable=False),
               Column('payment_type', Integer, nullable=False),
               Column('shipping_type', Integer, nullable=False),
               Column('shipping_fee', Integer, nullable=False),
               Column('status', Integer, nullable=False),
               Column('date', DateTime(timezone=True), nullable=False),
               Column('product_serial_number', VARCHAR(40), nullable=False),
               Column('input_personnel', Integer, nullable=False),
               Column('cancel_employee_id', Integer),
               Column('recipient', NVARCHAR(50)),
               Column('recipient_phone_number', NVARCHAR(40)),
               Column('recipient_deliver_address', NVARCHAR(128)),
               Column('shipping_date', DateTime(timezone=True)),
               Column('refund', BOOLEAN),
               Column('modify_date', DateTime(timezone=True), nullable=False),
               Column('sales_reocrd_date', DateTime(timezone=True), nullable=False)
               )
orders.create(engine)

order_items = Table('order_items', meta,
                    Column('id', Integer, primary_key=True, autoincrement=True),
                    Column('order_id', Integer, nullable=False),
                    Column('sku_id', Integer, nullable=False),
                    Column('price', Integer, nullable=False),
                    Column('number', Integer, nullable=False),
                    Column('quantity', Integer, nullable=False),
                    Column('total_amount', Integer, nullable=False),
                    Column('status', Integer, nullable=False),
                    Column('date', DateTime(timezone=True), nullable=False),
                    Column('used', Integer, nullable=False),
                    Column('exp_date', Date, nullable=False),
                    Column('name', NVARCHAR(128), nullable=False),
                    Column('note', NVARCHAR(256)),
                    Column('input_personnel', Integer),
                    Column('price_diff', Integer)
                    )
order_items.create(engine)

events = Table('events', meta,
               Column('id', Integer, primary_key=True, autoincrement=True),
               Column('store_id', Integer, nullable=False),
               Column('order_item_id', Integer, nullable=False),
               Column('user_id', Integer, nullable=False),
               Column('product_name', NVARCHAR(50), nullable=False),
               Column('start_time', DateTime(timezone=True), nullable=False),
               Column('end_time', DateTime(timezone=True), nullable=False),
               Column('note', NVARCHAR(256)),
               Column('input_personnel', Integer, nullable=False),
               Column('status', Integer, nullable=False),
               Column('registered', Integer),
               Column('doctor_check', Integer),
               Column('cancel_employee_id', Integer)
               )
events.create(engine)

event_employee = Table('event_employee', meta,
                       Column('id', Integer, primary_key=True, autoincrement=True),
                       Column('event_id', Integer, nullable=False),
                       Column('grade_id', Integer, nullable=False),
                       Column('employee_id', Integer, nullable=False)
                       )
event_employee.create(engine)

supplies = Table('supplies', meta,
                 Column('id', Integer, primary_key=True, autoincrement=True),
                 Column('name', NVARCHAR(50), nullable=False),
                 Column('cost', Integer, nullable=False),
                 Column('product_id', Integer),
                 Column('employee_id', Integer, nullable=False)
                 )
supplies.create(engine)

supplies_inventory = Table('supplies_inventory', meta,
                           Column('id', Integer, primary_key=True, autoincrement=True),
                           Column('supplies_id', Integer, nullable=False),
                           Column('employee_id', Integer, nullable=False),
                           Column('quantity', Integer, nullable=False),
                           Column('note', NVARCHAR(128), nullable=False),
                           Column('date', DateTime(timezone=True), nullable=False),
                           Column('type', BOOLEAN, nullable=False),
                           Column('current_single_cost', Integer, nullable=False)
                           )
supplies_inventory.create(engine)

settings = Table('settings', meta,
                 Column('id', Integer, primary_key=True, autoincrement=True),
                 Column('store_id', Integer, nullable=False),
                 Column('title', LONGTEXT, nullable=False),
                 Column('story', LONGTEXT, nullable=False),
                 Column('slogan', LONGTEXT, nullable=False),
                 Column('idea', LONGTEXT, nullable=False),
                 Column('about', LONGTEXT, nullable=False),
                 Column('date', DateTime(timezone=True), nullable=False),
                 )
settings.create(engine)

news = Table('news', meta,
             Column('id', Integer, primary_key=True, autoincrement=True),
             Column('store_id', Integer, nullable=False),
             Column('type', NVARCHAR(50), nullable=False),
             Column('title', NVARCHAR(128), nullable=False),
             Column('content', LONGTEXT, nullable=False),
             Column('author', NVARCHAR(128)),
             Column('status', BOOLEAN, nullable=False),
             Column('date', Date, nullable=False),
             Column('delete_time', DateTime(timezone=True)),
             Column('input_personnel', Integer, nullable=False)
             )
news.create(engine)

faqs = Table('faqs', meta,
             Column('id', Integer, primary_key=True, autoincrement=True),
             Column('store_id', Integer, nullable=False),
             Column('type', NVARCHAR(50), nullable=False),
             Column('name', NVARCHAR(50), nullable=False),
             Column('email', NVARCHAR(128), nullable=False),
             Column('phone_number', NVARCHAR(20)),
             Column('mobile_phone', NVARCHAR(20), nullable=False),
             Column('birthday', Date, nullable=False),
             Column('gender', Integer, nullable=False),
             Column('content', LONGTEXT, nullable=False),
             Column('date', DateTime(timezone=True), nullable=False),
             Column('reply', BOOLEAN, nullable=False),
             Column('reply_content', LONGTEXT),
             Column('reply_personnel', Integer)
             )
faqs.create(engine)

'''Table declaration'''
authentication = Table('authentication', meta,
    Column('id', INTEGER, primary_key=True, nullable=False, autoincrement=True),
    Column('auth_token', VARCHAR(40), nullable=False),
    Column('auth_valid_date', Integer, nullable=False),
    Column('auth_create_date', Integer, nullable=False),
    Column('auth_account_id', INTEGER, nullable=False),
    Column('auth_flag', Boolean, nullable=False)
)
authentication.create(engine)
# -*- coding: UTF-8 -*-
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.header import Header
from email.utils import formataddr


class EmailHelper():

    office365_user = None
    office365_pwd = None
    smtpserver = None
    email_from = None

    bcc = None

    def __init__(self):
        email_serverhost = 'smtp.gmail.com'
        email_account = "diazywomen@gmail.com"
        email_password = "diazy1215"

        self.office365_user = email_account
        self.office365_pwd = email_password
        self.smtpserver = smtplib.SMTP(email_serverhost, 587)
        self.smtpserver.ehlo()
        self.smtpserver.starttls()
        self.smtpserver.login(email_account, email_password)

    def reply_faq_mail(self, faq_type, user_name, user_email, user_phone_number, user_mobile_phone, faq_content, faq_date, reply_content):
        msg = MIMEMultipart('alternative')
        msg['Subject'] = "[回覆] 黛子線上客戶詢問電子郵件"
        msg['From'] = 'diazywomen@gmail.com'
        msg['To'] = user_email

        html = """\
            <p>[回覆] 線上客戶詢問電子郵件</p>
            <p></p>
            <p>客戶姓名：{0}</p>
            <p>聯絡電話：{1}</p>
            <p>行動電話：{2}</p>
            <p>聯絡電子郵件：{3}</p>
            <p>詢問類別：{4}</p>
            <p>詢問內容：{5}</p>
            <p>詢問時間：{6}</p>
            <p>回覆內容：{7}</p>
           """.format(user_name, user_phone_number, user_mobile_phone, user_email, faq_type, faq_content, faq_date, reply_content)
        p1 = MIMEText(html, 'html')
        msg.attach(p1)
        self.smtpserver.sendmail('diazywomen@gmail.com', user_email, msg.as_string())
        self.smtpserver.quit()

    def send_forget_password_email(self, target_email, employee_name, link):
        msg = MIMEMultipart('alternative')
        msg['Subject'] = "忘記密碼重新設定通知信"
        msg['From'] = 'diazywomen@gmail.com'
        msg['To'] = target_email
        html = """\
            <p style="margin-left:0pt;font-family: 新細明體;">
            您向 「Diazy」查詢您的密碼。<br />
            您的帳號：%s<br />
            <br />
            如果要重設密碼請點選以下連結<br />
            <a href="%s">%s</a><br />
            此信件為系統發出，請勿直接回覆，感謝您的配合。<br />
            若有任何問題，歡迎再來信或來電詢問。<br />
            """ % (target_email, link, link)
        p1 = MIMEText(html, 'html')
        msg.attach(p1)
        self.smtpserver.sendmail('diazywomen@gmail.com', target_email, msg.as_string())
        self.smtpserver.quit()

    def send_employee_activation_notification(self, employee_email, employee_name, employee_number, activate_link):
        msg = MIMEMultipart('alternative')
        msg['Subject'] = "黛子醫美診所【帳號啟用通知】"
        msg['From'] = 'diazywomen@gmail.com'
        msg['To'] = employee_email
        html = """\
            <p>{1}您好：</p>
            <p>歡迎加入黛子醫美診所團隊。</p>
            <p>&nbsp;</p>
            <p>您的員工編號：{2}</p>
            <p>您的帳號：{0}</p>
            <p>您的密碼：diazy</p>
            <p>請於登入後修改密碼。</p>
            <p>&nbsp;</p>
            <p>&nbsp;</p>
            <p>此信件為系統自動寄出，請勿直接回覆，感謝您的配合。</p>
            <p>若有任何問題，歡迎再來信(diazywomen@gmail.com)或來電(07-2410111)詢問。</p>
           """.format(employee_email, employee_name, employee_number)

        p1 = MIMEText(html, 'html')
        msg.attach(p1)
        self.smtpserver.sendmail('diazywomen@gmail.com', employee_email, msg.as_string())
        self.smtpserver.quit()


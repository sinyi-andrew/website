from flask import session, redirect, url_for
from webob import Request
from Model.JWT import JWTInstance


class SimpleMiddleWare(object):
    def __init__(self, app):
        self.app = app

    def __call__(self, environ, start_response):
        return self.app(environ, start_response)

class FunctionTypeStatus:
    setting = 1     # all_grade 0
    user = 2        # all_grade 1
    faq = 3         # all_grade 2
    employee = 4    # all_grade 3
    product = 5     # all_grade 4
    supplies_warehouse = 6  # all_grade 5
    supplies_counter = 7    # all_grade 6
    order = 8       # all_grade 7
    order_item = 9  # all_grade 8
    user_point = 10 # all_grade 9
    doctor_check = 11   # all_grade 10
    order_item_change = 12   # all_grade 11
    order_cacnel = 13   # all_grade 12
    dashboard = 14      # all_grade 13
    user_create = 15    # all_grade 14


class PermissionTypeStatus:
    close = 0
    read_only = 4
    write_and_read = 6


class NewsStatus:
    activity = 1
    off = 0


class ProductStatus:
    activity = 1
    off = 0


class SkuStatus:
    activity = 1
    deleted = 0


class ExperiencesStatus:
    activity = 1
    deleted = 0


class ProductCategoriesStatus:
    product = 1
    assemble = 2
    experience_ticket = 7
    service_starting_value = 3


class ImageTypeStatus:
    products = 1
    experiences = 2
    mainsliders = 3
    employees = 4


class InventoryTypeStatus:
    warehouse = 0
    counter = 1


class OrdersTypeStatus:
    deleted = 0
    undone = 1
    overdue = 2
    completed = 3


class OrdersCancelTypeStatus:
    nocancel = 0
    canceled = 1


class OrdersRefundStatus:
    norefund = 0
    refund = 1


class OrdersPaymentTypeStatus:
    cash = 1
    points = 2
    creditcard = 3
    creditloan = 4


class OrdersShippingTypeStatus:
    self = 1
    homedelivery = 2
    conveniencestore = 3


class OrdersItemTypeStatus:
    unfinished = 0
    completed = 1


class CheckQuotaTypeStatus:
    unfinished = 0
    completed = 1


class EmployeeStatus:
    activity = 1
    deleted = 0


class EmployeeGradeStatus:
    admin = 1
    manager = 2
    medical_artist = 3
    doctor = 4
    nurse = 5


class EmployeeCalendarStatus:
    activity = 1
    deleted = 0


class EventStatus:
    deleted = 0
    activity = 1
    registered = 2
    doctor_check = 3


# class EventRegisteredStatus:
#     unregistered = 0
#     registered = 1


class FaqStatus:
    noreply = 0
    replied = 1


class FaqTypeStatus:
    ordering_product = 1
    reservation_consultation = 2


class GenderStatus:
    female = 0
    male = 1
    other = 2


class AuthenticationFlagStatus:
    unused = 0
    used = 1

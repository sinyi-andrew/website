// 'use strict';
/*! highcharts.js - v1.0.0
* http://admindesigns.com/
* Copyright (c) 2018 Admin Designs;*/


/* Demo theme functions. Required for
 * Settings Pane and misc functions */
// var demoHighCharts = function () {

    $('.chartYear').datetimepicker({
        format: 'YYYY',
        pickTime: false,
        minViewMode: 2,
        defaultDate: moment(),
        maxDate: moment()
    });
    $('.chartMonth').datetimepicker({
        format: 'YYYY-MM',
        pickTime: false,
        minViewMode: 1,
        defaultDate: moment(),
        maxDate: moment()
    });

    // Define chart color patterns
    var highColors = [adColors.bgDangerLr, adColors.bgWarningLr, adColors.bgSystemLr, adColors.bgPrimaryLr, adColors.bgInfoLr, adColors.bgAlertLr,
         adColors.bgSuccessLr, adColors.bgDarkLr
    ];

    // Color Library we used to grab a random color
    var sparkColors = {
        "primary": [adColors.bgPrimary, adColors.bgPrimaryLr, adColors.bgPrimaryDr],
        "info": [adColors.bgInfo, adColors.bgInfoLr, adColors.bgInfoDr],
        "warning": [adColors.bgWarning, adColors.bgWarningLr, adColors.bgWarningDr],
        "danger": [adColors.bgDanger, adColors.bgDangerLr, adColors.bgDangerDr],
        "success": [adColors.bgSuccess, adColors.bgSuccessLr, adColors.bgSuccessDr],
        "alert": [adColors.bgAlert, adColors.bgAlertLr, adColors.bgAlertDr],
        "system": [adColors.bgSystem, adColors.bgSystemLr, adColors.bgSystemDr]
    };


    // Column Charts

    $('#saleAmountMonth').change(function () {
        var date = $(this).val().split('-')
        var $quantity = [], $date = [], $value = []
        $.ajax({
            url: '/api/business-report/company/6641ec95-3144-42be-9038-b5cc1b8dd599/selling-trend/order/number/year/'+date[0]+'/'+date[1],
            type: 'GET',
            datatype: 'json',
            async: false,
            contentType: "application/json",
            success: function (data) {
                for (var i = 0; i < data.OrderNumberList.length; i++) {
                    $date.push(data.OrderNumberList[i].Date)
                    $value.push(data.OrderNumberList[i].Revenue)
                    $quantity.push(data.OrderNumberList[i].Quantity)
                }
            },
            error: function (xhr, textStatus, thownError) {
                swal({
                    title: '發生錯誤',
                    text: xhr.getResponseHeader("error_code"),
                    type: 'error'
                })
            }
        });
        $('#saleAmount').highcharts({
            colors: highColors,
            chart: {
                // backgroundColor: '#f9f9f9',
                // className: 'br-r',
                // type: 'line',
                // zoomType: 'x',
                // panning: true,
                // panKey: 'shift',
                // marginTop: 25,
                // marginRight: 1,
                type: ''
            },
            title: {
                text: ''
            },

            xAxis: {
                categories: $date
            },
            yAxis: [{
                className: 'highcharts-color-0',
                title: {
                    text: '元',
                    rotation: 0,
                    style: {
                        fontSize: '18px'
                    }
                }
            }, {
                className: 'highcharts-color-1',
                opposite: true,
                title: {
                    text: '件',
                    rotation: 0,
                    style: {
                        fontSize: '18px'
                    }
                }
            }],

            plotOptions: {
                column: {
                    borderRadius: 5
                }
            },

            series: [{
                name: '當月業績',
                data: $value,
                // type: 'column',
                showInLegend: false
            }, {
                name: '當月銷售數量',
                data: $quantity,
                yAxis: 1,
                // type: 'column',
                showInLegend: false
            }]
        });
    })
    $('#saleAmountMonth').trigger('change')


    $('#orderAmountMonth').change(function () {
        var date = $(this).val().split('-')
        var $date = [], $value = [], $quantity = []
        $.ajax({
            url: '/api/business-report/company/6641ec95-3144-42be-9038-b5cc1b8dd599/selling-trend/order/number/'+date[0]+'/'+date[1],
            type: 'GET',
            datatype: 'json',
            async: false,
            contentType: "application/json",
            success: function (data) {
                for (var i = 0; i < data.OrderNumberList.length; i++) {
                    $date.push(i+1)
                    $value.push(data.OrderNumberList[i].Revenue)
                    $quantity.push(data.OrderNumberList[i].Quantity)
                }
            },
            error: function (xhr, textStatus, thownError) {
                swal({
                    title: '發生錯誤',
                    text: xhr.getResponseHeader("error_code"),
                    type: 'error'
                })
            }
        });
        $('#orderAmount').highcharts({
            colors: highColors,
            chart: {
                type: ''
            },

            title: {
                text: ''
            },

            xAxis: {
                categories: $date
            },
            yAxis: [{
                className: 'highcharts-color-0',
                title: {
                    text: '元',
                    rotation: 0,
                    style: {
                        fontSize: '18px'
                    }
                }
            }, {
                className: 'highcharts-color-1',
                opposite: true,
                title: {
                    text: '件',
                    rotation: 0,
                    style: {
                        fontSize: '18px'
                    }
                }
            }],
            // tooltip: {
            //     headerFormat: '<table>',
            //     pointFormat: '<tr><td style="padding:0">當日銷售 <b>{point.y}</b> {series.name}</td></tr>',
            //     footerFormat: '</table>',
            //     shared: true,
            //     useHTML: true
            // },

            plotOptions: {
                column: {
                    borderRadius: 5
                }
            },

            series: [{
                name: '當日業績',
                data: $value,
                // type: 'column',
                showInLegend: false
            }, {
                name: '當日銷售數量',
                data: $quantity,
                yAxis: 1,
                // type: 'column',
                showInLegend: false
            }]
        });
    })
    $('#orderAmountMonth').trigger('change')


    $('#productRankMonth').change(function () {
        var date = $(this).val().split('-')
        var $productName = [], $value = [], $quantity = []
        $.ajax({
            url: '/api/business-report/company/6641ec95-3144-42be-9038-b5cc1b8dd599/selling-trend/product/rank/'+date[0]+'/'+date[1]+'/1',
            type: 'GET',
            datatype: 'json',
            async: false,
            contentType: "application/json",
            success: function (data) {
                for (var i = 0; i < data.ProductRankList.length; i++) {
                    $productName.push(data.ProductRankList[i].ProductName)
                    $value.push(data.ProductRankList[i].Revenue)
                    $quantity.push(data.ProductRankList[i].SellingQuantity)
                }
            },
            error: function (xhr, textStatus, thownError) {
                swal({
                    title: '發生錯誤',
                    text: xhr.getResponseHeader("error_code"),
                    type: 'error'
                })
            }
        });
        $('#productRank').highcharts({
            chart: {
                type: ''
            },

            title: {
                text: ''
            },

            xAxis: {
                categories: $productName,
                // labels: {
                //     rotation: -45,
                //     style: {
                //         fontSize: '13px',
                //         fontFamily: 'Verdana, sans-serif'
                //     }
                // }
            },
            yAxis: [{
                className: 'highcharts-color-0',
                title: {
                    text: '元',
                    rotation: 0,
                    style: {
                        fontSize: '18px'
                    }
                }
            }, {
                className: 'highcharts-color-1',
                opposite: true,
                title: {
                    text: '件',
                    rotation: 0,
                    style: {
                        fontSize: '18px'
                    }
                }
            }],
            tooltip: {
                headerFormat: '<table>',
                pointFormat: '<tr><td style="padding:0">累計銷售 <b>{point.y}</b> {series.name}</td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
            },

            plotOptions: {
                column: {
                    borderRadius: 5
                }
            },

            series: [{
                name: '元',
                data: $value,
                type: 'column',
                showInLegend: false
            }, {
                name: '件',
                data: $quantity,
                yAxis: 1,
                type: 'column',
                showInLegend: false
            }]

        });
    })
    $('#productRankMonth').trigger('change')


    $('#dealerSaleMonth').change(function () {
        var date = $(this).val().split('-')
        var $productName = [], $value = [], $quantity = []
        $.ajax({
            url: '/api/business-report/company/6641ec95-3144-42be-9038-b5cc1b8dd599/selling-trend/reseller/rank/'+date[0]+'/'+date[1]+'/1',
            type: 'GET',
            datatype: 'json',
            async: false,
            contentType: "application/json",
            success: function (data) {
                for (var i = 0; i < data.ResellerRankList.length; i++) {
                    $productName.push(data.ResellerRankList[i].CustomerName)
                    $value.push(data.ResellerRankList[i].Revenue)
                    $quantity.push(data.ResellerRankList[i].Quantity)
                }
                console.log($quantity)
            },
            error: function (xhr, textStatus, thownError) {
                swal({
                    title: '發生錯誤',
                    text: xhr.getResponseHeader("error_code"),
                    type: 'error'
                })
            }
        });
        $('#dealerSale').highcharts({
            chart: {
                type: ''
            },

            title: {
                text: ''
            },

            xAxis: {
                categories: $productName,
                // labels: {
                //     rotation: -45,
                //     style: {
                //         fontSize: '13px',
                //         fontFamily: 'Verdana, sans-serif'
                //     }
                // }
            },
            yAxis: [{
                className: 'highcharts-color-0',
                title: {
                    text: '元',
                    rotation: 0,
                    style: {
                        fontSize: '18px'
                    }
                }
            }, {
                className: 'highcharts-color-1',
                opposite: true,
                title: {
                    text: '件',
                    rotation: 0,
                    style: {
                        fontSize: '18px'
                    }
                }
            }],
            tooltip: {
                headerFormat: '<table>',
                pointFormat: '<tr><td style="padding:0">累計銷售 <b>{point.y}</b> {series.name}</td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
            },

            plotOptions: {
                column: {
                    borderRadius: 5
                }
            },

            series: [{
                name: '元',
                data: $value,
                type: 'column',
                showInLegend: false
            }, {
                name: '件',
                data: $quantity,
                yAxis: 1,
                type: 'column',
                showInLegend: false
            }]

        });
    })
    $('#dealerSaleMonth').trigger('change')

        














from flask import Flask, render_template, url_for, request
from Utility.middleware import SimpleMiddleWare

app = Flask(__name__)
app.config['SECRET_KEY'] = 'DiazyProject#SecretKey#54706525'
app.wsgi_app = SimpleMiddleWare(app.wsgi_app)

from Controllers import frontend
from Controllers.Admin import admin

from Controllers.Admin.router import user, order, setting, product, employee, supply, faq
from Controllers.Admin.api import userApi, orderApi, settingApi, productApi, employeeApi, inventoryApi, faqApi, businessRuleApi, testApi

admin.init_app(app)
frontend.init_app(app)
user.init_app(app)
order.init_app(app)
product.init_app(app)
supply.init_app(app)
setting.init_app(app)
employee.init_app(app)
faq.init_app(app)

userApi.init_app(app)
orderApi.init_app(app)
settingApi.init_app(app)
productApi.init_app(app)
inventoryApi.init_app(app)
employeeApi.init_app(app)
faqApi.init_app(app)
businessRuleApi.init_app(app)
testApi.init_app(app)

if __name__ == '__main__':
    app.run()

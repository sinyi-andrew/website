from Entity.Entity import *
from Model.Exception import *
from Model.SuperModel import SuperModel
from Utility.Status import *

from flask import current_app
import jwt
from datetime import datetime, timedelta
from sqlalchemy.sql import func

global engine
global connection
global trans


class JWTInstance(SuperModel):
    session = None

    def __init__(self):
        super(JWTInstance, self).__init__()

    def encode_auth_token(self, name, uuid, grade_id):
        try:
            token = jwt.encode(
                {
                    'name': name,
                    'uuid': uuid,
                    'grade': grade_id
                    # 'exp': datetime.utcnow() + timedelta(minutes=30)
                    # 'exp': datetime.timezone(+8) + timedelta(minutes=30)
                }, current_app.config['SECRET_KEY']
                , algorithm='HS256')
            return token.decode('UTF-8')
        except (
                Exception,
                RuntimeError
        ) as e:
            raise e

    def decode_auth_token(self, auth_token):
        try:
            self.session = super(JWTInstance, self).get_session()

            pre = auth_token.split(' ')[0]
            if pre == 'Bearer':
                token = auth_token.split(' ')[1]
                user = jwt.decode(token, current_app.config['SECRET_KEY'])  # , algorithms=['HS256'], options={'verify_exp': False}

                employeesEntity = self.session.query(
                    EmployeesEntity
                ).filter(
                    EmployeesEntity.uuid == user['uuid'],
                    EmployeesEntity.status == EmployeeStatus.activity
                ).scalar()
                # pass
                if employeesEntity is not None:
                    user_info = {
                        'uuid': user['uuid'],
                        'name': user['name'],
                        'grade': user['grade']
                    }
                    return user_info
                # failed
                else:
                    raise JWTAuthenticateFailedException()
            else:
                raise JWTAuthenticateFailedException()
            # original code
            #     else:
            #         raise AuthenticateFailedException()
            # else:
            #     raise jwt.ExpiredSignatureError()
        except AuthenticateFailedException as e:
            return e.__str__()
        except JWTAuthenticateFailedException as e:
            raise JWTAuthenticateFailedException()
        except jwt.ExpiredSignatureError:
            return 'Signature expired. Please log in again.'
        except jwt.InvalidTokenError:
            return 'Invalid token. Please log in again.'
        except Exception as e:
            return e.__str__()
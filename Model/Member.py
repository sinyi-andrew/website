from Model.SuperModel import SuperModel
from Entity.Entity import *
from Model.Exception import *
from Utility.Status import *
import uuid
import json
from datetime import datetime, timedelta
from sqlalchemy.sql import func

global engine
global connection
global trans


class MemberInstance(SuperModel):
    session = None

    def __init__(self):
        super(MemberInstance, self).__init__()

    def getUnusedByUserUUID(self, user_id):
        unusedList = []
        try:
            self.session = super(MemberInstance, self).get_session()

            ordersEntity = self.session.query(
                # func.sum(OrderItemsEntity.total_amount).label('total_amount'),
                # func.sum(OrderItemsEntity.used).label('used'),
                OrderItemsEntity.total_amount.label('total_amount'),
                OrderItemsEntity.used.label('used'),
                OrdersEntity.order_number.label('order_number'),
                SkusEntity.product_id.label('product_id'),
                ProductsEntity.name.label('product_name')
            ).filter(
                OrdersEntity.user_id == int(user_id),
                OrdersEntity.user_id == UsersEntity.id,
                OrdersEntity.status != OrdersTypeStatus.deleted,
                OrdersEntity.id == OrderItemsEntity.order_id,
                OrderItemsEntity.sku_id == SkusEntity.id,
                SkusEntity.product_id == ProductsEntity.id
            # ).group_by(
            #     SkusEntity.product_id
            ).order_by(
                SkusEntity.product_id
            ).all()

            if ordersEntity != []:
                product_id_lists = []
                product_name_lists = []
                for i in range(len(ordersEntity)):
                    product_id_lists.append(ordersEntity[i].product_id)
                    product_name_lists.append(ordersEntity[i].product_name)
                product_id_list = list(set(product_id_lists))
                product_name_list = list(set(product_name_lists))

                for i in range(len(product_id_list)):
                    order_number_list = []
                    unused_num = 0
                    total_num = 0
                    used_num = 0
                    for j in range(len(ordersEntity)):
                        if product_id_list[i] != ordersEntity[j].product_id:
                            continue
                        else:
                            if ordersEntity[j].order_number is not None:
                                order_number_list.append(ordersEntity[j].order_number)
                            else:
                                order_number_list.append('')

                            if ordersEntity[j].total_amount is not None:
                                # total_amount = int(ordersEntity[j].total_amount)
                                total_num = total_num + int(ordersEntity[j].total_amount)
                            else:
                                # total_amount = 0
                                total_num = total_num

                            if ordersEntity[j].used is not None:
                                # used = int(ordersEntity[j].used)
                                used_num = used_num + int(ordersEntity[j].used)
                            else:
                                # used = 0
                                used_num = used_num

                            if total_num >= used_num:
                                unused_num = total_num - used_num
                            else:
                                raise DatabaseFormatWrongException()

                    rec = {
                        'product_name': product_name_list[i],
                        'total_amount': total_num,
                        'unused_num': unused_num,
                        'order_number_list': order_number_list
                    }
                    unusedList.append(rec)
            return unusedList
        except (
            DatabaseFormatWrongException,
            Exception,
            RuntimeError
        ) as e:
            raise e
        finally:
            self.session.close()
            self.connection.close()

    def getRoleList(self):

        try:
            self.session = super(MemberInstance, self).get_session()
            _data = self.session.query(UserGradesEntity).all()

            if _data is None:
                raise DataDoesNotExistException()
            else:
                result = _data
            return _data
        except (
                AuthenticateFailedException,
                RuntimeError
        ) as e:
            raise e
        finally:
            self.session.close()
            self.connection.close()

    def getGradeData(self, id):
        try:
            self.session = super(MemberInstance, self).get_session()
            _data = self.session.query(UserGradesEntity).filter(
                UserGradesEntity.id == id
            ).scalar()

            if _data is None:
                raise DataDoesNotExistException()
            else:
                result = _data
            return result
        except (
                AuthenticateFailedException,
                RuntimeError
        ) as e:
            raise e
        finally:
            self.session.close()
            self.connection.close()

    def gradeData(self, id, name, description, discount, shipping_fee, shipping_setting):
        response = {}
        try:
            if id == "" or id is None:  # new
                self.session = super(MemberInstance, self).get_session()
                data = UserGradesEntity()

                data.name = name
                data.description = description
                data.discount = discount
                data.shipping_fee = shipping_fee
                data.shipping_setting = shipping_setting

                self.session.add(data)
                self.session.commit()

            else:  # update
                self.session = super(MemberInstance, self).get_session()
                data = self.session.query(UserGradesEntity).filter(UserGradesEntity.id == id).scalar()
                if data is None:
                    raise DataDoesNotExistException()
                else:
                    data.name = name
                    data.description = description
                    data.discount = discount
                    data.shipping_fee = shipping_fee
                    data.shipping_setting = shipping_setting
                    self.session.flush()
                    self.session.commit()

            response_data = {
                'id': self.session.query(UserGradesEntity).filter(
                    UserGradesEntity.id == data.id
                ).scalar()
            }

            return response_data
        except (
                AuthenticateFailedException,
                RuntimeError
        ) as e:
            raise e
        finally:
            self.session.close()
            self.connection.close()

    def idNumbercConfirm(self, IDNumber):
        response = {}
        try:
            self.session = super(MemberInstance, self).get_session()
            data = self.session.query(EmployeesEntity).filter(
                EmployeesEntity.IDNumber == IDNumber
            ).count()
            return data
        except (
                AuthenticateFailedException,
                RuntimeError
        ) as e:
            raise e
        finally:
            self.session.close()
            self.connection.close()

    def getUserList(self):
        try:
            self.session = super(MemberInstance, self).get_session()
            _data = self.session.query(UsersEntity).all()

            if _data is None:
                raise DataDoesNotExistException()
            else:
                for data in _data:
                    point = self.session.query(func.sum(UserAccountsEntity.point)).filter(
                        UserAccountsEntity.user_id == data.id
                    ).scalar()

                    if point is not None:
                        data.point = point

                result = _data
            return result
        except (
                AuthenticateFailedException,
                RuntimeError
        ) as e:
            raise e
        finally:
            self.session.close()
            self.connection.close()

    def userData(self, id, grade_id, name, IDNumber, email, phone_number, mobile_phone, birthday, bloodtype, gender, address, contact, contact_phone, height, weight, improve, disease_history,
                 allergy_history, whereform, note):
        response = {}
        try:
            if id == "" or id is None:  # new
                self.session = super(MemberInstance, self).get_session()
                data = UsersEntity()

                data.grade_id = grade_id
                data.uuid = str(uuid.uuid4())
                data.name = name
                data.IDNumber = IDNumber
                data.email = email
                data.phone_number = phone_number
                data.mobile_phone = mobile_phone
                data.birthday = birthday
                data.bloodtype = bloodtype
                data.gender = gender
                data.address = address
                data.contact = contact
                data.contact_phone = contact_phone
                data.height = height
                data.weight = weight
                data.improve = json.dumps(improve)
                data.disease_history = disease_history
                data.allergy_history = allergy_history
                data.whereform = whereform
                data.note = note
                data.create_date = datetime.now()

                self.session.add(data)
                self.session.commit()
                user_id_str = str(data.id)
                data.user_number = 'M' + user_id_str.rjust(6, '0')
                self.session.commit()
                user_id = data.id

            else:  # update
                self.session = super(MemberInstance, self).get_session()
                data = self.session.query(UsersEntity).filter(UsersEntity.id == id).scalar()
                if data is None:
                    raise DataDoesNotExistException()
                else:

                    data.grade_id = grade_id
                    data.name = name
                    data.IDNumber = IDNumber
                    data.email = email
                    data.phone_number = phone_number
                    data.mobile_phone = mobile_phone
                    data.birthday = birthday
                    data.bloodtype = bloodtype
                    data.gender = gender
                    data.address = address
                    data.contact = contact
                    data.contact_phone = contact_phone
                    data.height = height
                    data.weight = weight
                    data.improve = json.dumps(improve)
                    data.disease_history = disease_history
                    data.allergy_history = allergy_history
                    data.whereform = whereform
                    data.note = note

                    self.session.flush()
                    self.session.commit()

            response_data = {
                'id': self.session.query(UsersEntity).filter(
                    UsersEntity.id == data.id
                ).scalar()
            }

            return response_data
        except (
                AuthenticateFailedException,
                RuntimeError
        ) as e:
            raise e
        finally:
            self.session.close()
            self.connection.close()

    def getUserData(self, uuid):
        try:
            self.session = super(MemberInstance, self).get_session()
            _data = self.session.query(UsersEntity).filter(
                UsersEntity.uuid == uuid
            ).scalar()

            if _data is None:
                raise DataDoesNotExistException()
            else:

                point = self.session.query(func.sum(UserAccountsEntity.point)).filter(
                    UserAccountsEntity.user_id == _data.id
                ).scalar()

                if _data.improve is not None:
                    _data.improve = json.loads(_data.improve)

                if point is not None:
                    _data.point = point
                else:
                    _data.point = 0  ###add
                result = _data
            return result
        except (
                AuthenticateFailedException,
                RuntimeError
        ) as e:
            raise e
        finally:
            self.session.close()
            self.connection.close()

    def getUserpoint(self, uuid):
        try:
            self.session = super(MemberInstance, self).get_session()
            user = self.session.query(UsersEntity).filter(
                UsersEntity.uuid == uuid
            ).scalar()
            _data = self.session.query(UserAccountsEntity).filter(
                UserAccountsEntity.user_id == user.id
            ).all()

            if _data is None:
                raise DataDoesNotExistException()
            else:
                result = _data

            return result
        except (
                AuthenticateFailedException,
                RuntimeError
        ) as e:
            raise e
        finally:
            self.session.close()
            self.connection.close()

    def userPointData(self, user_id, employee_id, point):
        response = []
        try:
            self.session = super(MemberInstance, self).get_session()
            user = self.session.query(UsersEntity).filter(
                UsersEntity.uuid == user_id
            ).scalar()
            if user is not None:
                number = self.session.query(UserAccountsEntity).filter(
                    UserAccountsEntity.user_id == user.id,
                    UserAccountsEntity.number.isnot(None)
                ).count()
                data = UserAccountsEntity()
                data.user_id = user.id
                data.employee_id = employee_id
                data.point = point
                number_str = str(number)
                data.number = 'P' + number_str.rjust(8, '0')
                data.date = datetime.now()


                self.session.add(data)
                self.session.commit()

            response_data = {
                'id': self.session.query(UserAccountsEntity).filter(
                    UserAccountsEntity.id == data.id
                ).scalar()
            }
            return data.number
        except (
                AuthenticateFailedException,
                RuntimeError
        ) as e:
            raise e
        finally:
            self.session.close()
            self.connection.close()

    def createUserPoint(self, uuid, order_id, employee_id, number, point):
        try:
            self.session = super(MemberInstance, self).get_session()

            user = self.session.query(UsersEntity).filter(
                UsersEntity.uuid == uuid
            ).scalar()
            if user is not None:
                data = UserAccountsEntity()
                data.user_id = user.id
                data.order_id = order_id
                data.employee_id = employee_id
                data.point = point
                data.number = number
                data.date = datetime.now()

                self.session.add(data)
                self.session.commit()
                response_data = {
                    'id': data.id
                }
                return response_data
        except (
                AuthenticateFailedException,
                RuntimeError
        ) as e:
            raise e
        finally:
            self.session.close()
            self.connection.close()

    def searchUser(self, search):
        try:
            self.session = super(MemberInstance, self).get_session()

            user = self.session.query(
                UsersEntity.id.label('id'),
                UsersEntity.uuid.label('uuid'),
                UsersEntity.user_number.label('user_num'),
                UsersEntity.name.label('name'),
                UsersEntity.mobile_phone.label('mobile_phone'),
                UsersEntity.IDNumber.label('ID'),
                UsersEntity.address.label('address'),
                UserGradesEntity.name.label('gname'),
                UserGradesEntity.discount.label('discount'),
                UserGradesEntity.shipping_fee.label('shipping_fee'),
                UserGradesEntity.shipping_setting.label('shipping_setting'),
            ).filter(
                or_(UsersEntity.user_number == search,
                    UsersEntity.IDNumber == search,
                    ),
                UsersEntity.grade_id == UserGradesEntity.id
            ).first()

            if user is not None:
                discount_str = self.disCount(user.discount)
                point = self.session.query(func.sum(UserAccountsEntity.point)).filter(
                    UserAccountsEntity.user_id == user.id
                ).scalar()
                if point is None:
                    point = 0
                response_data = {
                    'code': 0,
                    'uuid': user.uuid if user.uuid is not None else "",
                    'user_number': user.user_num if user.user_num is not None else "",
                    'user_name': user.name if user.name is not None else "",
                    'user_phone': user.mobile_phone if user.mobile_phone is not None else "",
                    'user_point': point if point is not None else "",
                    'user_grade': user.gname if user.gname is not None else "",
                    'user_address': user.address if user.address is not None else "",
                    'user_discount': user.discount if user.discount is not None else "",
                    'user_discount_str': discount_str if discount_str is not None else "",
                    'user_shipping_fee': user.shipping_fee if user.shipping_fee is not None else "",
                    'user_shipping_setting': user.shipping_setting if user.shipping_setting is not None else "",
                }
            else:
                response_data = {
                    'code': 1001,
                    'msg': '查無會員帳號資料，請重新輸入!'
                }
            return response_data
        except (
                AuthenticateFailedException,
                RuntimeError
        ) as e:
            raise e
        finally:
            self.session.close()
            self.connection.close()

    def getUserIDByUuid(self, uuid):
        try:
            self.session = super(MemberInstance, self).get_session()
            usersEntity = self.session.query(UsersEntity).filter(
                UsersEntity.uuid == uuid
            ).scalar()

            if usersEntity is None:
                raise TargetIdDoesNotExistException()
            else:
                return usersEntity.id
        except (
                TargetIdDoesNotExistException,
                Exception,
                RuntimeError
        ) as e:
            raise e
        finally:
            self.session.close()
            self.connection.close()

    def getUuidByUserID(self, user_id):
        try:
            self.session = super(MemberInstance, self).get_session()
            usersEntity = self.session.query(UsersEntity).filter(
                UsersEntity.id == user_id
            ).scalar()

            if usersEntity is None:
                raise TargetIdDoesNotExistException()
            else:
                return usersEntity.uuid
        except (
                TargetIdDoesNotExistException,
                Exception,
                RuntimeError
        ) as e:
            raise e
        finally:
            self.session.close()
            self.connection.close()

    def getDiscountByUserID(self, user_id):
        try:
            self.session = super(MemberInstance, self).get_session()

            usersEntity = self.session.query(UsersEntity).filter(
                UsersEntity.id == int(user_id)
            ).scalar()

            if usersEntity is None:
                raise TargetIdDoesNotExistException()
            else:
                userGradesEntity = self.session.query(UserGradesEntity).filter(
                    UserGradesEntity.id == int(usersEntity.grade_id)
                ).scalar()
                if usersEntity is None:
                    raise TargetIdDoesNotExistException()
                else:
                    return userGradesEntity.discount, userGradesEntity.shipping_setting
        except (
                TargetIdDoesNotExistException,
                Exception,
                RuntimeError
        ) as e:
            raise e
        finally:
            self.session.close()
            self.connection.close()

    def disCount(self, value):
        return {
            '1': '原價',
            '0.99': '九九折',
            '0.98': '九八折',
            '0.97': '九七折',
            '0.96': '九六折',
            '0.95': '九五折',
            '0.94': '九四折',
            '0.93': '九三折',
            '0.92': '九二折',
            '0.91': '九一折',
            '0.9': '九折',
            '0.89': '八九折',
            '0.88': '八八折',
            '0.87': '八七折',
            '0.86': '八六折',
            '0.85': '八五折',
            '0.84': '八四折',
            '0.83': '八三折',
            '0.82': '八二折',
            '0.81': '八一折',
            '0.8': '八折',
            '0.79': '七九折',
            '0.78': '七八折',
            '0.77': '七七折',
            '0.76': '七六折',
            '0.75': '七五折',
            '0.74': '七四折',
            '0.73': '七三折',
            '0.72': '七二折',
            '0.71': '七一折',
            '0.7': '七折',
            '0.69': '六九折',
            '0.68': '六八折',
            '0.67': '六七折',
            '0.66': '六六折',
            '0.65': '六五折',
            '0.64': '六四折',
            '0.63': '六三折',
            '0.62': '六二折',
            '0.61': '六一折',
            '0.6': '六折',
            '0.59': '五九折',
            '0.58': '五八折',
            '0.57': '五七折',
            '0.56': '五六折',
            '0.55': '五五折',
            '0.54': '五四折',
            '0.53': '五三折',
            '0.52': '五二折',
            '0.51': '五一折',
            '0.5': '五折',
            '0.49': '四九折',
            '0.48': '四八折',
            '0.47': '四七折',
            '0.46': '四六折',
            '0.45': '四五折',
            '0.44': '四四折',
            '0.43': '四三折',
            '0.42': '四二折',
            '0.41': '四一折',
            '0.4': '四折',
            '0.39': '三九折',
            '0.38': '三八折',
            '0.37': '三七折',
            '0.36': '三六折',
            '0.35': '三五折',
            '0.34': '三四折',
            '0.33': '三三折',
            '0.32': '三二折',
            '0.31': '三一折',
            '0.3': '三折',
            '0.29': '二九折',
            '0.28': '二八折',
            '0.27': '二七折',
            '0.26': '二六折',
            '0.25': '二五折',
            '0.24': '二四折',
            '0.23': '二三折',
            '0.22': '二二折',
            '0.21': '二一折',
            '0.2': '二折',
            '0.19': '一九折',
            '0.18': '一八折',
            '0.17': '一七折',
            '0.16': '一六折',
            '0.15': '一五折',
            '0.14': '一四折',
            '0.13': '一三折',
            '0.12': '一二折',
            '0.11': '一一折',
            '0.1': '一折',

        }.get(value, 'InputError')
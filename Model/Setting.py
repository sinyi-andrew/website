import hashlib
import uuid
import json
from datetime import datetime, timedelta
from sqlalchemy.sql import func

from Model.SuperModel import SuperModel
from Entity.Entity import *
from Model.Exception import *
from Utility.Status import *

global engine
global connection
global trans


class SettingInstance(SuperModel):
    session = None

    def __init__(self):
        super(SettingInstance, self).__init__()

    def createData(self, store_id, slogan, story, idea, about):
        response = {}
        try:
            self.session = super(SettingInstance, self).get_session()
            data = SettingsEntity()
            data.store_id = store_id
            data.title = 'Diazy'
            data.slogan = slogan
            data.story = story
            data.idea = idea
            data.about = about
            data.date = datetime.now()

            self.session.add(data)
            self.session.commit()

            response_data = {
                'id': self.session.query(SettingsEntity).filter(
                    SettingsEntity.store_id == store_id
                ).scalar().id
            }

            return response_data
        except (
                AuthenticateFailedException,
                RuntimeError
        ) as e:
            raise e
        finally:
            self.session.close()
            self.connection.close()

    def getSettingData(self, store_id):
        response = {}
        try:
            self.session = super(SettingInstance, self).get_session()
            _setting = self.session.query(SettingsEntity).filter(
                SettingsEntity.store_id == store_id
            ).scalar()

            if _setting is None:
                raise DataDoesNotExistException()
            else:
                result = _setting
            return _setting
        except (
                AuthenticateFailedException,
                RuntimeError
        ) as e:
            raise e
        finally:
            self.session.close()
            self.connection.close()

    def updateBrand(self, store_id, slogan, story, idea):
        response = {}
        try:
            self.session = super(SettingInstance, self).get_session()
            _setting = self.session.query(SettingsEntity).filter(SettingsEntity.store_id == store_id).scalar()

            if _setting is None:
                raise DataDoesNotExistException()
            else:

                _setting.store_id = store_id
                _setting.slogan = slogan
                _setting.story = story
                _setting.idea = idea
                _setting.date = datetime.now()

                self.session.flush()
                self.session.commit()

                response_data = {
                    'id': self.session.query(SettingsEntity).filter(
                        SettingsEntity.store_id == store_id
                    ).scalar().id
                }
                return response_data
        except (
                AuthenticateFailedException,
                RuntimeError
        ) as e:
            raise e
        finally:
            self.session.close()
            self.connection.close()

    def updateAbout(self, store_id, about):
        response = {}
        try:
            self.session = super(SettingInstance, self).get_session()
            _setting = self.session.query(SettingsEntity).filter(SettingsEntity.store_id == store_id).scalar()

            if _setting is None:
                raise DataDoesNotExistException()
            else:

                _setting.store_id = store_id
                _setting.about = about
                _setting.date = datetime.now()

                self.session.flush()
                self.session.commit()

                response_data = {
                    'id': self.session.query(SettingsEntity).filter(
                        SettingsEntity.store_id == store_id
                    ).scalar().id
                }
                return response_data
        except (
                AuthenticateFailedException,
                RuntimeError
        ) as e:
            raise e
        finally:
            self.session.close()
            self.connection.close()

    # 讀取新聞列表 多筆
    def getNewsListForFrontend(self, store_id, type):
        response = {}
        try:
            self.session = super(SettingInstance, self).get_session()
            _setting = self.session.query(NewsEntity).filter(
                NewsEntity.store_id == store_id,
                NewsEntity.type == type,
                NewsEntity.delete_time.is_(None),
                NewsEntity.status == NewsStatus.activity
            ).all()

            if _setting is None:
                raise DataDoesNotExistException()
            else:
                result = _setting
            return result
        except (
                AuthenticateFailedException,
                RuntimeError
        ) as e:
            raise e
        finally:
            self.session.close()
            self.connection.close()

    # 讀取新聞列表 多筆 for backend
    def getNewsList(self, store_id, type):
        response = {}
        try:
            self.session = super(SettingInstance, self).get_session()
            _setting = self.session.query(NewsEntity).filter(
                NewsEntity.store_id == store_id,
                NewsEntity.type == type,
                NewsEntity.delete_time.is_(None)
            ).all()

            if _setting is None:
                raise DataDoesNotExistException()
            else:
                result = _setting
            return result
        except (
                AuthenticateFailedException,
                RuntimeError
        ) as e:
            raise e
        finally:
            self.session.close()
            self.connection.close()

    # 讀取新聞 單筆
    def getNewsData(self, id):
        response = {}
        try:
            self.session = super(SettingInstance, self).get_session()

            _setting = self.session.query(NewsEntity).filter(
                NewsEntity.id == id
            ).scalar()

            if _setting is None:
                raise DataDoesNotExistException()
            else:
                result = _setting
            return result
        except (
                AuthenticateFailedException,
                RuntimeError
        ) as e:
            raise e
        finally:
            self.session.close()
            self.connection.close()

    def createNews(self, store_id, types, title, content, author, status):
        response = {}
        try:
            self.session = super(SettingInstance, self).get_session()
            data = NewsEntity()
            data.store_id = store_id
            data.type = types
            data.title = title
            data.content = content
            data.author = author
            data.status = status
            data.date = datetime.now()

            self.session.add(data)
            self.session.commit()

            response = {
                'id': self.session.query(NewsEntity).filter(
                    SettingsEntity.store_id == store_id
                ).all()
            }

            return response
        except (
                AuthenticateFailedException,
                RuntimeError
        ) as e:
            raise e
        finally:
            self.session.close()
            self.connection.close()

    def updateNews(self, id, title, content, author, status):
        response = {}
        try:
            self.session = super(SettingInstance, self).get_session()
            _setting = self.session.query(NewsEntity).filter(NewsEntity.id == id).scalar()

            if _setting is None:
                raise DataDoesNotExistException()
            else:

                _setting.title = title
                _setting.content = content
                _setting.author = author
                _setting.status = status

                self.session.flush()
                self.session.commit()

                response_data = {
                    'id': self.session.query(NewsEntity).filter(
                        NewsEntity.id == id
                    ).scalar()
                }
                return response_data
        except (
                AuthenticateFailedException,
                RuntimeError
        ) as e:
            raise e
        finally:
            self.session.close()
            self.connection.close()

    def deleteNews(self, id):
        response = {}
        try:
            self.session = super(SettingInstance, self).get_session()
            _setting = self.session.query(NewsEntity).filter(NewsEntity.id == id).scalar()

            if _setting is None:
                raise DataDoesNotExistException()
            else:

                _setting.delete_time = datetime.now()

                self.session.flush()
                self.session.commit()

                response_data = _setting.type
                return response_data
        except (
                AuthenticateFailedException,
                RuntimeError
        ) as e:
            raise e
        finally:
            self.session.close()
            self.connection.close()

    # 讀取slider列表 多筆
    def getSliderList(self):
        response = {}
        try:
            self.session = super(SettingInstance, self).get_session()
            item = self.session.query(MainSlidersEntity).filter(
                MainSlidersEntity.delete_time.is_(None)
            ).all()

            if item is None:
                raise DataDoesNotExistException()
            else:
                slider_list = []
                for i in range(len(item)):
                    rec = {
                        'id': item[i].id,
                        'product_id': item[i].product_id,
                        'image': item[i].image,
                        'sort': item[i].sort,
                        'description': item[i].description
                    }
                    slider_list.append(rec)
                response = {'sliders': slider_list}
            return response
        except (
                AuthenticateFailedException,
                RuntimeError
        ) as e:
            raise e
        finally:
            self.session.close()
            self.connection.close()

    def createSlider(self, product_id, newImgBase64, sort, imgName):
        response = {}
        try:
            self.session = super(SettingInstance, self).get_session()
            data = MainSlidersEntity()
            data.product_id = product_id
            data.image = newImgBase64
            data.sort = sort
            data.description = imgName

            self.session.add(data)
            self.session.commit()

            response = {
                'id': data.id
            }

            return response
        except (
                AuthenticateFailedException,
                RuntimeError
        ) as e:
            raise e
        finally:
            self.session.close()
            self.connection.close()

    def updateSlider(self, id, product_id, newImgBase64, sort, imgName):
        response = {}
        try:
            self.session = super(SettingInstance, self).get_session()
            item = self.session.query(MainSlidersEntity).filter(MainSlidersEntity.id == id).scalar()

            if item is None:
                raise DataDoesNotExistException()
            else:
                item.id = id
                item.product_id = product_id
                item.image = newImgBase64
                item.sort = sort
                item.description = imgName

                self.session.flush()
                self.session.commit()

                response_data = {
                    'id': self.session.query(MainSlidersEntity).filter(
                        MainSlidersEntity.id == id
                    ).scalar()
                }
                return response_data
        except (
                AuthenticateFailedException,
                RuntimeError
        ) as e:
            raise e
        finally:
            self.session.close()
            self.connection.close()

    def deleteSlider(self, id):
        response = {}
        try:
            self.session = super(SettingInstance, self).get_session()
            item = self.session.query(MainSlidersEntity).filter(MainSlidersEntity.id == id).scalar()

            if item is None:
                raise DataDoesNotExistException()
            else:
                item.delete_time = datetime.now()

                self.session.flush()
                self.session.commit()

                response_data = True
                return response_data
        except (
                AuthenticateFailedException,
                RuntimeError
        ) as e:
            raise e
        finally:
            self.session.close()
            self.connection.close()

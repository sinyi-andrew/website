# -*- coding: utf-8 -*-
from datetime import datetime, timedelta
import uuid
from sqlalchemy.sql import func

from Entity.Entity import *
from Utility.Status import *
from Model.Exception import *
from Model.SuperModel import SuperModel

global engine
global connection
global trans


class ProductInstance(SuperModel):
    session = None

    def __init__(self):
        super(ProductInstance, self).__init__()

    def getImageByID(self, type, id):
        try:
            self.session = super(ProductInstance, self).get_session()

            if type == ImageTypeStatus.products:
                type_name = 'products'

                image = self.session.query(
                    ProductsEntity.image.label('image')
                ).filter(
                    ProductsEntity.id == id
                ).scalar()

            elif type == ImageTypeStatus.experiences:
                type_name = 'experiences'

                image = self.session.query(
                    ExperiencesEntity.image.label('image')
                ).filter(
                    ExperiencesEntity.id == id,
                    ExperiencesEntity.status == ExperiencesStatus.activity
                ).scalar()

            elif type == ImageTypeStatus.mainsliders:
                type_name = 'mainsliders'

                image = self.session.query(
                    MainSlidersEntity.image.label('image')
                ).filter(
                    MainSlidersEntity.id == id,
                    MainSlidersEntity.delete_time == None
                ).scalar()

            elif type == ImageTypeStatus.employees:
                type_name = 'employees'

                image = self.session.query(
                    EmployeesEntity.image.label('image')
                ).filter(
                    EmployeesEntity.id == id,
                    EmployeesEntity.status == EmployeeStatus.activity
                ).scalar()

            else:
                raise TypeDoesNotExistException()

            if image is not None:
                response = {
                    'id': id if id is not None else "",
                    'type': type if type is not None else "",
                    'type_name': type_name if type_name is not None else "",
                    'image': image if image is not None else ""
                }
                return response
            else:
                raise TargetIdDoesNotExistException()
        except (
            TargetIdDoesNotExistException,
            TypeDoesNotExistException,
            Exception,
            RuntimeError
        ) as e:
            raise e
        finally:
            self.session.close()
            self.connection.close()

    def update_image(self, type, id, image_input):
        try:
            self.session = super(ProductInstance, self).get_session()

            if type == ImageTypeStatus.products:
                image = self.session.query(
                    ProductsEntity
                ).filter(
                    ProductsEntity.id == id
                ).first()
            elif type == ImageTypeStatus.experiences:
                image = self.session.query(
                    ExperiencesEntity
                ).filter(
                    ExperiencesEntity.id == id,
                    ExperiencesEntity.status == ExperiencesStatus.activity
                ).first()
            elif type == ImageTypeStatus.mainsliders:
                image = self.session.query(
                    MainSlidersEntity
                ).filter(
                    MainSlidersEntity.id == id,
                    MainSlidersEntity.delete_time is not None
                ).first()
            elif type == ImageTypeStatus.employees:
                image = self.session.query(
                    EmployeesEntity
                ).filter(
                    EmployeesEntity.id == id,
                    EmployeesEntity.status == EmployeeStatus.activity
                ).first()
            else:
                raise TypeDoesNotExistException()

            if image is not None:
                image.image = image_input
                self.session.commit()
                return {'id': id}
            else:
                raise TargetIdDoesNotExistException()
        except (
            TypeDoesNotExistException,
            TargetIdDoesNotExistException,
            Exception,
            RuntimeError
        ) as e:
            raise e
        finally:
            self.session.close()
            self.connection.close()

    def getServiceCategiryForFrontend(self):
        productCategoriesList = []
        try:
            self.session = super(ProductInstance, self).get_session()

            productCategoriesEntity = self.session.query(
                ProductCategoriesEntity
            ).filter(
                ~ProductCategoriesEntity.id.in_([ProductCategoriesStatus.product, ProductCategoriesStatus.assemble, ProductCategoriesStatus.experience_ticket])
            ).all()

            if productCategoriesEntity is not None:
                for i in range(len(productCategoriesEntity)):
                    rec = {
                        'id': productCategoriesEntity[i].id if productCategoriesEntity[i].id is not None else "",
                        'name': productCategoriesEntity[i].name if productCategoriesEntity[i].name is not None else "",
                        'description': productCategoriesEntity[i].description if productCategoriesEntity[i].description is not None else "",
                        'type': productCategoriesEntity[i].type if productCategoriesEntity[i].type is not None else ""
                    }
                    productCategoriesList.append(rec)
            else:
                productCategoriesList = []
            return {"productCategoriesList": productCategoriesList}
        except (
                Exception,
                RuntimeError
        ) as e:
            raise e
        finally:
            self.session.close()
            self.connection.close()

    def getServiceCategiry(self):
        productCategoriesList = []
        try:
            self.session = super(ProductInstance, self).get_session()

            productCategoriesEntity = self.session.query(
                ProductCategoriesEntity
            ).filter(

            ).all()

            if productCategoriesEntity is not None:
                for i in range(len(productCategoriesEntity)):
                    rec = {
                        'id': productCategoriesEntity[i].id if productCategoriesEntity[i].id is not None else "",
                        'name': productCategoriesEntity[i].name if productCategoriesEntity[i].name is not None else "",
                        'description': productCategoriesEntity[i].description if productCategoriesEntity[i].description is not None else "",
                        'type': productCategoriesEntity[i].type if productCategoriesEntity[i].type is not None else ""
                    }
                    productCategoriesList.append(rec)
            else:
                productCategoriesList = []
            return {"productCategoriesList": productCategoriesList}
        except (
                Exception,
                RuntimeError
        ) as e:
            raise e
        finally:
            self.session.close()
            self.connection.close()

    # new product create
    def create_or_update_product(self, products_id, name, image, discount, intro, description, ingredient,
                                 care, notice, faq, note, tip, sort, status):
        try:
            self.session = super(ProductInstance, self).get_session()

            # create new product
            if products_id == "" or products_id is None:

                productsEntity = ProductsEntity()
                productsEntity.category_id = 1
                productsEntity.name = name
                productsEntity.image = image
                productsEntity.discount = discount
                productsEntity.intro = intro
                productsEntity.description = description
                productsEntity.ingredient = ingredient
                productsEntity.care = care
                productsEntity.notice = notice
                productsEntity.faq = faq
                productsEntity.note = note
                productsEntity.tip = tip
                productsEntity.sort = sort
                productsEntity.status = status
                self.session.add(productsEntity)
                self.session.commit()
                response = productsEntity.id

            # update product
            else:
                productsEntity = self.session.query(ProductsEntity).filter(
                    ProductsEntity.id == int(products_id)
                ).scalar()

                if productsEntity is not None:

                    productsEntity.name = name
                    productsEntity.image = image
                    productsEntity.discount = discount
                    productsEntity.intro = intro
                    productsEntity.description = description
                    productsEntity.ingredient = ingredient
                    productsEntity.care = care
                    productsEntity.notice = notice
                    productsEntity.faq = faq
                    productsEntity.note = note
                    productsEntity.tip = tip
                    productsEntity.sort = sort
                    productsEntity.status = status
                    self.session.commit()
                    response = products_id
                else:
                    raise TargetIdDoesNotExistException()
            return {"products_id": response}
        except (
                TargetIdDoesNotExistException,
                Exception,
                RuntimeError
        ) as e:
            raise e
        finally:
            self.session.close()
            self.connection.close()

    # delete product
    def delete_product(self, products_id):
        try:
            self.session = super(ProductInstance, self).get_session()

            productsEntity = self.session.query(ProductsEntity).filter(
                ProductsEntity.id == int(products_id)
            ).scalar()

            if productsEntity is not None:
                productsEntity.status = ProductStatus.deleted
                self.session.commit()

                return {"products_id": products_id}
            else:
                raise TargetIdDoesNotExistException()
        except (
                TargetIdDoesNotExistException,
                Exception,
                RuntimeError
        ) as e:
            raise e
        finally:
            self.session.close()
            self.connection.close()

    # new assemble create
    def create_or_update_assemble(self, assemble_id, name, image, discount, intro, description, ingredient,
                                  care, notice, faq, note, tip, sort, status):
        try:
            self.session = super(ProductInstance, self).get_session()

            # create new assemble
            if assemble_id == "" or assemble_id is None:
                productsEntity = ProductsEntity()
                productsEntity.category_id = 2
                productsEntity.name = name
                productsEntity.image = image
                productsEntity.discount = discount
                productsEntity.intro = intro
                productsEntity.description = description
                productsEntity.ingredient = ingredient
                productsEntity.care = care
                productsEntity.notice = notice
                productsEntity.faq = faq
                productsEntity.note = note
                productsEntity.tip = tip
                productsEntity.sort = sort
                productsEntity.status = status
                self.session.add(productsEntity)
                self.session.commit()
                response = productsEntity.id

            # update product
            else:
                productsEntity = self.session.query(ProductsEntity).filter(
                    ProductsEntity.id == int(assemble_id)
                ).scalar()
                if productsEntity is not None:
                    productsEntity.name = name
                    productsEntity.image = image
                    productsEntity.discount = discount
                    productsEntity.intro = intro
                    productsEntity.description = description
                    productsEntity.ingredient = ingredient
                    productsEntity.care = care
                    productsEntity.notice = notice
                    productsEntity.faq = faq
                    productsEntity.note = note
                    productsEntity.tip = tip
                    productsEntity.sort = sort
                    productsEntity.status = status
                    self.session.commit()
                    response = assemble_id
                else:
                    raise TargetIdDoesNotExistException()
            return {"assemble_id": response}
        except (
                TargetIdDoesNotExistException,
                Exception,
                RuntimeError
        ) as e:
            raise e
        finally:
            self.session.close()
            self.connection.close()

    # assemble assemble
    def assemble_assemble(self, id, assemble_id, sku, sort):
        try:
            self.session = super(ProductInstance, self).get_session()

            # create new assemble assemble
            if id == "" or id is None:
                # check deplication (product_id & sku_id are the same)
                checkAssembleEntity = self.session.query(AssembleEntity).filter(
                    AssembleEntity.parent_sku_id == int(assemble_id),
                    AssembleEntity.sku_id == int(sku),
                    AssembleEntity.delete_time == None
                ).scalar()
                if checkAssembleEntity is None:
                    assembleEntity = AssembleEntity()
                    assembleEntity.parent_sku_id = assemble_id
                    assembleEntity.sku_id = sku
                    assembleEntity.sort = sort
                    self.session.add(assembleEntity)
                    self.session.commit()
                    return {"assemble_id": assembleEntity.id}
                else:
                    raise AssembleDuplicationException()

            # update assemble assemble
            else:
                assembleEntity = self.session.query(AssembleEntity).filter(
                    AssembleEntity.id == int(id),
                    AssembleEntity.delete_time == None
                ).scalar()
                if assembleEntity is not None:
                    # check deplication (product_id & sku_id are the same)
                    if assembleEntity.sku_id == sku:
                        assembleEntity.sort = sort
                        self.session.commit()
                        response = assembleEntity.id
                    else:
                        checkAssembleEntity = self.session.query(AssembleEntity).filter(
                            AssembleEntity.parent_sku_id == int(assemble_id),
                            AssembleEntity.sku_id == int(sku),
                            AssembleEntity.delete_time == None
                        ).scalar()
                        if checkAssembleEntity is None:
                            assembleEntity.sku_id = sku
                            assembleEntity.sort = sort
                            self.session.commit()
                            response = assembleEntity.id
                        else:
                            raise AssembleDuplicationException()
                else:
                    raise TargetIdDoesNotExistException()
            return {"assemble_id": response}
        except (
                TargetIdDoesNotExistException,
                AssembleDuplicationException,
                Exception,
                RuntimeError
        ) as e:
            raise e
        finally:
            self.session.close()
            self.connection.close()

    # delete assemble
    def delete_assemble(self, id):
        try:
            self.session = super(ProductInstance, self).get_session()

            assembleEntity = self.session.query(AssembleEntity).filter(
                AssembleEntity.id == int(id)
            ).scalar()

            if assembleEntity is not None:

                assembleEntity.delete_time = datetime.now()
                self.session.commit()

                return {"id": id}
            else:
                raise TargetIdDoesNotExistException()
        except (
                TargetIdDoesNotExistException,
                Exception,
                RuntimeError
        ) as e:
            raise e
        finally:
            self.session.close()
            self.connection.close()

    # new service create
    def create_or_update_service(self, service_id, category_id, name, image, discount, intro, description, ingredient,
                                 care, notice, faq, note, tip, sort, status):
        try:
            self.session = super(ProductInstance, self).get_session()

            # create new service
            if service_id == "" or service_id is None:

                productsEntity = ProductsEntity()
                productsEntity.category_id = category_id
                productsEntity.name = name
                productsEntity.image = image
                productsEntity.discount = discount
                productsEntity.intro = intro
                productsEntity.description = description
                productsEntity.ingredient = ingredient
                productsEntity.care = care
                productsEntity.notice = notice
                productsEntity.faq = faq
                productsEntity.note = note
                productsEntity.tip = tip
                productsEntity.sort = sort
                productsEntity.status = status
                self.session.add(productsEntity)
                self.session.commit()

                response = productsEntity.id

            # update product
            else:
                if int(category_id) < ProductCategoriesStatus.service_starting_value:
                    raise TargetIdDoesNotExistException()
                productsEntity = self.session.query(ProductsEntity).filter(
                    ProductsEntity.id == int(service_id)
                ).scalar()

                if productsEntity is not None:
                    productsEntity.category_id = category_id
                    productsEntity.name = name
                    productsEntity.image = image
                    productsEntity.discount = discount
                    productsEntity.intro = intro
                    productsEntity.description = description
                    productsEntity.ingredient = ingredient
                    productsEntity.care = care
                    productsEntity.notice = notice
                    productsEntity.faq = faq
                    productsEntity.note = note
                    productsEntity.tip = tip
                    productsEntity.sort = sort
                    productsEntity.status = status
                    self.session.commit()

                    response = service_id
                else:
                    raise TargetIdDoesNotExistException()
            return {"service_id": response}
        except (
                TargetIdDoesNotExistException,
                EmployeeGradeDuplicationException,
                Exception,
                RuntimeError
        ) as e:
            raise e
        finally:
            self.session.close()
            self.connection.close()

    def getUsedCategory7SkuByUserID(self, user_id):
        usedCategory7SkuIDList = []
        try:
            self.session = super(ProductInstance, self).get_session()

            skusEntity = self.session.query(
                SkusEntity.sku_number.label('sku_number'),
            ).filter(
                OrdersEntity.user_id == int(user_id),
                OrdersEntity.id == OrderItemsEntity.order_id,
                OrdersEntity.status != OrdersTypeStatus.deleted,
                OrderItemsEntity.sku_id == SkusEntity.id,
                SkusEntity.product_id == ProductsEntity.id,
                ProductsEntity.category_id == ProductCategoriesStatus.experience_ticket
            ).all()

            if skusEntity != []:
                for i in range(len(skusEntity)):
                    if skusEntity[i].sku_number not in usedCategory7SkuIDList:
                        usedCategory7SkuIDList.append(skusEntity[i].sku_number)
            else:
                usedCategory7SkuIDList = []
            return {"usedCategory7SkuIDList": usedCategory7SkuIDList}
        except (
                Exception,
                RuntimeError
        ) as e:
            raise e
        finally:
            self.session.close()
            self.connection.close()

    def getSku(self):
        skusList = []
        try:
            self.session = super(ProductInstance, self).get_session()

            skusEntity = self.session.query(
                SkusEntity.id.label('id'),
                SkusEntity.sku_number.label('sku_number'),
                SkusEntity.product_id.label('product_id'),
                SkusEntity.number.label('number'),
                SkusEntity.price.label('price'),
                SkusEntity.expire.label('expire'),
                SkusEntity.only_for_assemable.label('only_for_assemable'),
                SkusEntity.status.label('status'),
                ProductsEntity.category_id.label('category_id'),
                ProductsEntity.name.label('product_name')
            ).filter(
                SkusEntity.status == SkuStatus.activity,
                ProductsEntity.id == SkusEntity.product_id,
                ProductsEntity.category_id is not 2
            ).all()

            if skusEntity != []:
                for i in range(len(skusEntity)):
                    if skusEntity[i].category_id == 2:
                        continue
                    else:
                        rec = {
                            'id': skusEntity[i].id if skusEntity[i].id is not None else "",
                            'sku_number': skusEntity[i].sku_number if skusEntity[i].sku_number is not None else "",
                            'product_id': skusEntity[i].product_id if skusEntity[i].product_id is not None else "",
                            'number': skusEntity[i].number if skusEntity[i].number is not None else "",
                            'price': skusEntity[i].price if skusEntity[i].price is not None else "",
                            'expire': skusEntity[i].expire if skusEntity[i].expire is not None else "",
                            'only_for_assemable': 1 if skusEntity[i].only_for_assemable is True else 0,
                            'status': 1 if skusEntity[i].status is True else 0,
                            'product_name': skusEntity[i].product_name if skusEntity[i].product_name is not None else ""
                        }
                        skusList.append(rec)
            else:
                skusList = []
            return {"skusList": skusList}
        except (
                Exception,
                RuntimeError
        ) as e:
            raise e
        finally:
            self.session.close()
            self.connection.close()

    def getSkuInfoBySkuID(self, sku_id):
        try:
            self.session = super(ProductInstance, self).get_session()

            skusEntity = self.session.query(
                SkusEntity.id.label('id'),
                SkusEntity.sku_number.label('sku_number'),
                SkusEntity.product_id.label('product_id'),
                SkusEntity.number.label('number'),
                SkusEntity.price.label('price'),
                SkusEntity.expire.label('expire'),
                SkusEntity.only_for_assemable.label('only_for_assemable'),
                SkusEntity.status.label('status'),
                ProductsEntity.category_id.label('category_id'),
                ProductsEntity.name.label('product_name'),
                ProductsEntity.discount.label('discount')
            ).filter(
                SkusEntity.id == int(sku_id),
                ProductsEntity.id == SkusEntity.product_id
            ).first()

            if skusEntity is not None:
                sku_info = {
                    'skus_id': skusEntity.id if skusEntity.id is not None else "",
                    'sku_number': skusEntity.sku_number if skusEntity.sku_number is not None else "",
                    'product_id': skusEntity.product_id if skusEntity.product_id is not None else "",
                    'skus_number': skusEntity.number if skusEntity.number is not None else "",
                    'skus_price': skusEntity.price if skusEntity.price is not None else "",
                    'expire': skusEntity.expire if skusEntity.expire is not None else "",
                    'only_for_assemable': 1 if skusEntity.only_for_assemable is True else 0,
                    'status': 1 if skusEntity.status is True else 0,
                    'product_name': skusEntity.product_name if skusEntity.product_name is not None else "",
                    'category_id': skusEntity.category_id if skusEntity.category_id is not None else "",
                    'discount': skusEntity.discount if skusEntity.discount is not None else ""
                }
                return sku_info
            else:
                raise TargetIdDoesNotExistException()
        except (
                TargetIdDoesNotExistException,
                Exception,
                RuntimeError
        ) as e:
            raise e
        finally:
            self.session.close()
            self.connection.close()

    def getProductCategoryList(self):
        productCategoryList = []
        try:
            self.session = super(ProductInstance, self).get_session()

            productCategoriesEntity = self.session.query(
                ProductCategoriesEntity
            ).filter(
                ProductCategoriesEntity.id != ProductCategoriesStatus.assemble
            ).all()

            if productCategoriesEntity != []:
                for i in range(len(productCategoriesEntity)):
                    if productCategoriesEntity[i].id == 2:
                        continue
                    else:
                        productCategoryList.append({
                            'category_id': productCategoriesEntity[i].id,
                            'category_name': productCategoriesEntity[i].name
                        })
            return productCategoryList
        except (
                Exception,
                RuntimeError
        ) as e:
            raise e
        finally:
            self.session.close()
            self.connection.close()

    def getProductList(self):
        productList = []
        try:
            self.session = super(ProductInstance, self).get_session()

            productsEntity = self.session.query(
                ProductsEntity.id.label('product_id'),
                ProductsEntity.name.label('product_name'),
                ProductsEntity.category_id.label('category_id'),
                ProductCategoriesEntity.name.label('category_name')
            ).filter(
                ProductsEntity.category_id != ProductCategoriesStatus.assemble,
                ProductsEntity.category_id == ProductCategoriesEntity.id
            ).all()

            if productsEntity != []:
                for i in range(len(productsEntity)):
                    if productsEntity[i].category_id == 2:
                        continue
                    else:
                        productList.append({
                            'product_id': productsEntity[i].product_id,
                            'product_name': productsEntity[i].product_name,
                            'category_id': productsEntity[i].category_id,
                            'category_name': productsEntity[i].category_name
                        })
            return productList
        except (
                Exception,
                RuntimeError
        ) as e:
            raise e
        finally:
            self.session.close()
            self.connection.close()

    def getSkuByInputFilter(self, filter):
        skusList = []
        try:
            self.session = super(ProductInstance, self).get_session()

            skusEntity = self.session.query(
                SkusEntity.id.label('id'),
                SkusEntity.sku_number.label('sku_number'),
                SkusEntity.product_id.label('product_id'),
                SkusEntity.number.label('number'),
                SkusEntity.price.label('price'),
                SkusEntity.expire.label('expire'),
                SkusEntity.only_for_assemable.label('only_for_assemable'),
                SkusEntity.status.label('status'),
                ProductsEntity.category_id.label('category_id'),
                ProductsEntity.name.label('product_name'),
                ProductsEntity.discount.label('discount')
            ).filter(
                SkusEntity.status == SkuStatus.activity,
                ProductsEntity.id == SkusEntity.product_id,
                SkusEntity.only_for_assemable == 0,
                or_(
                    SkusEntity.sku_number.like("%" + filter + "%"),
                    ProductsEntity.name.like("%" + filter + "%")
                )
            ).all()

            if skusEntity != []:
                assemble_skus_list = []
                for i in range(len(skusEntity)):
                    # if category is assemble, collect all assemble skus
                    if skusEntity[i].category_id == ProductCategoriesStatus.assemble:

                        assembleEntity = self.session.query(AssembleEntity).filter(
                            AssembleEntity.parent_sku_id == skusEntity[i].id,
                            AssembleEntity.delete_time == None
                        ).order_by(AssembleEntity.sort.asc()).all()

                        if assembleEntity != []:
                            for j in range(len(assembleEntity)):

                                assembleSkusEntity = self.session.query(
                                    SkusEntity.id.label('sku_id'),
                                    SkusEntity.sku_number.label('sku_number'),
                                    SkusEntity.number.label('number'),
                                    SkusEntity.price.label('price'),
                                    SkusEntity.expire.label('expire'),
                                    SkusEntity.only_for_assemable.label('only_for_assemable'),
                                    SkusEntity.status.label('status'),
                                    ProductsEntity.name.label('product_name')
                                ).filter(
                                    SkusEntity.id == assembleEntity[j].sku_id,
                                    ProductsEntity.id == SkusEntity.product_id,
                                ).first()

                                if assembleSkusEntity is not None:

                                    if assembleSkusEntity.product_name and assembleSkusEntity.number:
                                        name = assembleSkusEntity.product_name + ' * ' + str(assembleSkusEntity.number)
                                    rec_assemble = {
                                        'sku_id': assembleSkusEntity.sku_id if assembleSkusEntity.sku_id is not None else "",
                                        'sku_number': assembleSkusEntity.sku_number if assembleSkusEntity.sku_number is not None else "",
                                        'number': assembleSkusEntity.number if assembleSkusEntity.number is not None else "",
                                        'price': assembleSkusEntity.price if assembleSkusEntity.price is not None else "",
                                        'expire': assembleSkusEntity.expire if assembleSkusEntity.expire is not None else "",
                                        'only_for_assemable': assembleSkusEntity.only_for_assemable if assembleSkusEntity.only_for_assemable is not None else "",
                                        'status': 1 if assembleSkusEntity.status is True else 0,
                                        'name': name if name is not None else ""
                                    }
                                    assemble_skus_list.append(rec_assemble)
                    rec = {
                        'sku_id': skusEntity[i].id if skusEntity[i].id is not None else "",
                        'sku_number': skusEntity[i].sku_number if skusEntity[i].sku_number is not None else "",
                        'product_id': skusEntity[i].product_id if skusEntity[i].product_id is not None else "",
                        'number': skusEntity[i].number if skusEntity[i].number is not None else "",
                        'price': skusEntity[i].price if skusEntity[i].price is not None else "",
                        'expire': skusEntity[i].expire if skusEntity[i].expire is not None else "",
                        'only_for_assemable': 1 if skusEntity[i].only_for_assemable is True else 0,
                        'status': 1 if skusEntity[i].status is True else 0,
                        'product_name': skusEntity[i].product_name if skusEntity[i].product_name is not None else "",
                        'category_id': skusEntity[i].category_id if skusEntity[i].category_id is not None else "",
                        'discount': skusEntity[i].discount if skusEntity[i].discount is not None else "",
                        'assemble_skus_list': assemble_skus_list
                    }
                    skusList.append(rec)
            else:
                skusList = []
            return {"data": skusList}
        except (
                Exception,
                RuntimeError
        ) as e:
            raise e
        finally:
            self.session.close()
            self.connection.close()

    # new sku create or update
    def create_or_update_sku(self, sku_id, products_id, number, price, expire, only_for_assemable):
        try:
            self.session = super(ProductInstance, self).get_session()

            # create new sku
            if sku_id == "" or sku_id is None:
                productsEntity = self.session.query(ProductsEntity).filter(
                    ProductsEntity.id == products_id
                ).scalar()
                if productsEntity is not None:
                    # check deplication (product_id & number & status are the same)
                    checkSkusEntity = self.session.query(SkusEntity).filter(
                        SkusEntity.product_id == int(products_id),
                        SkusEntity.number == int(number),
                        SkusEntity.status == SkuStatus.activity
                    ).scalar()
                    if checkSkusEntity is None:
                        skusEntity = SkusEntity()
                        skusEntity.product_id = products_id
                        skusEntity.number = number
                        skusEntity.price = price
                        skusEntity.expire = expire
                        skusEntity.product_serial_number = str(uuid.uuid4())
                        skusEntity.only_for_assemable = only_for_assemable
                        skusEntity.status = SkuStatus.activity
                        self.session.add(skusEntity)
                        self.session.commit()
                        sku_id_str = str(skusEntity.id)
                        skusEntity.sku_number = 'S' + sku_id_str.rjust(8, '0')
                        self.session.commit()
                        response = skusEntity.id
                    else:
                        raise SkuDuplicationException()
                else:
                    raise TargetIdDoesNotExistException()
            # update category
            else:
                skusEntity = self.session.query(SkusEntity).filter(
                    SkusEntity.id == int(sku_id),
                    SkusEntity.status == SkuStatus.activity
                ).scalar()
                if skusEntity is not None:
                    # check deplication (product_id & number & status are the same)
                    if skusEntity.number == number:
                        skusEntity.price = price
                        skusEntity.expire = expire
                        skusEntity.only_for_assemable = only_for_assemable
                        self.session.commit()
                        response = skusEntity.id
                    else:
                        checkSkusEntity = self.session.query(SkusEntity).filter(
                            SkusEntity.product_id == int(products_id),
                            SkusEntity.number == int(number),
                            SkusEntity.status == SkuStatus.activity
                        ).scalar()
                        if checkSkusEntity is None:
                            skusEntity.number = number
                            skusEntity.price = price
                            skusEntity.expire = expire
                            skusEntity.only_for_assemable = only_for_assemable
                            self.session.commit()
                            response = skusEntity.id
                        else:
                            raise SkuDuplicationException()
                else:
                    raise TargetIdDoesNotExistException()
            return {"sku_id": response}
        except (
                TargetIdDoesNotExistException,
                SkuDuplicationException,
                Exception,
                RuntimeError
        ) as e:
            raise e
        finally:
            self.session.close()
            self.connection.close()

    # delete sku
    def delete_sku(self, sku_id):
        try:
            self.session = super(ProductInstance, self).get_session()

            skusEntity = self.session.query(SkusEntity).filter(
                SkusEntity.id == int(sku_id),
                SkusEntity.status == SkuStatus.activity
            ).scalar()

            if skusEntity is not None:

                skusEntity.status = SkuStatus.deleted
                self.session.commit()

                return {"sku_id": sku_id}
            else:
                raise TargetIdDoesNotExistException()
        except (
                TargetIdDoesNotExistException,
                Exception,
                RuntimeError
        ) as e:
            raise e
        finally:
            self.session.close()
            self.connection.close()

    # new sp create or update
    def create_or_update_sp(self, servicepart_id, products_id, employee_grade, employee_number, technical_fee):
        try:
            self.session = super(ProductInstance, self).get_session()

            # create new service part
            if servicepart_id == "" or servicepart_id is None:
                productsEntity = self.session.query(ProductsEntity).filter(
                    ProductsEntity.id == products_id
                ).scalar()
                if productsEntity is not None:
                    # check deplication (product_id & employee_grade are the same)
                    checkServicePartsEntity = self.session.query(ServicePartsEntity).filter(
                        ServicePartsEntity.product_id == int(products_id),
                        ServicePartsEntity.employee_grade == int(employee_grade)
                    ).first()
                    if checkServicePartsEntity is None:
                        servicePartsEntity = ServicePartsEntity()
                        servicePartsEntity.product_id = products_id
                        servicePartsEntity.employee_grade = employee_grade
                        servicePartsEntity.number = employee_number
                        servicePartsEntity.technical_fee = technical_fee
                        self.session.add(servicePartsEntity)
                        self.session.commit()
                        response = servicePartsEntity.id
                    else:
                        raise ServicePartDuplicationException()
                else:
                    raise TargetIdDoesNotExistException()

            # update service part
            else:
                servicePartsEntity = self.session.query(ServicePartsEntity).filter(
                    ServicePartsEntity.id == int(servicepart_id)
                ).scalar()
                if servicePartsEntity is not None:
                    # check deplication (product_id & employee_grade are the same)
                    if servicePartsEntity.employee_grade == employee_grade:
                        servicePartsEntity.number = employee_number
                        servicePartsEntity.technical_fee = technical_fee
                        self.session.commit()
                        response = servicePartsEntity.id
                    else:
                        checkServicePartsEntity = self.session.query(ServicePartsEntity).filter(
                            ServicePartsEntity.product_id == int(products_id),
                            ServicePartsEntity.employee_grade == int(employee_grade)
                        ).first()
                        if checkServicePartsEntity is None:
                            servicePartsEntity.employee_grade = employee_grade
                            servicePartsEntity.number = employee_number
                            servicePartsEntity.technical_fee = technical_fee
                            self.session.commit()
                            response = servicePartsEntity.id
                        else:
                            raise ServicePartDuplicationException()
                else:
                    raise TargetIdDoesNotExistException()
            return {"serviceParts_id": response}
        except (
                TargetIdDoesNotExistException,
                ServicePartDuplicationException,
                Exception,
                RuntimeError
        ) as e:
            raise e
        finally:
            self.session.close()
            self.connection.close()

    # delete sp
    def delete_sp(self, servicepart_id):
        try:
            self.session = super(ProductInstance, self).get_session()

            servicePartsEntity = self.session.query(ServicePartsEntity).filter(
                ServicePartsEntity.id == int(servicepart_id)
            ).scalar()

            if servicePartsEntity is not None:

                self.session.delete(servicePartsEntity)
                self.session.commit()

                return {"serviceParts_id": servicepart_id}
            else:
                raise TargetIdDoesNotExistException()
        except (
                TargetIdDoesNotExistException,
                Exception,
                RuntimeError
        ) as e:
            raise e
        finally:
            self.session.close()
            self.connection.close()

    # new service categiry create
    def create_or_update_service_categiry(self, category_id, name, description, type):
        try:
            self.session = super(ProductInstance, self).get_session()

            # create new category
            if category_id == "" or category_id is None:

                productCategoriesEntity = ProductCategoriesEntity()
                productCategoriesEntity.name = name
                productCategoriesEntity.description = description
                productCategoriesEntity.type = type
                self.session.add(productCategoriesEntity)
                self.session.commit()

                response = productCategoriesEntity.id

            # update category
            else:
                productCategoriesEntity = self.session.query(ProductCategoriesEntity).filter(
                    ProductCategoriesEntity.id == int(category_id)
                ).scalar()

                if productCategoriesEntity is not None:
                    productCategoriesEntity.name = name
                    productCategoriesEntity.description = description
                    productCategoriesEntity.type = type
                    self.session.commit()

                    response = productCategoriesEntity.id
                else:
                    raise TargetIdDoesNotExistException()

            return {"category_id": response}
        except (
                TargetIdDoesNotExistException,
                Exception,
                RuntimeError
        ) as e:
            raise e
        finally:
            self.session.close()
            self.connection.close()

    def getExperiences(self):
        experiencesList = []
        try:
            self.session = super(ProductInstance, self).get_session()

            experiencesEntity = self.session.query(
                ExperiencesEntity.id.label('id'),
                ExperiencesEntity.product_id.label('product_id'),
                ExperiencesEntity.employee_id.label('employee_id'),
                ExperiencesEntity.image.label('image'),
                ExperiencesEntity.interview.label('interview'),
                ExperiencesEntity.advisory.label('advisory'),
                ExperiencesEntity.suggest.label('suggest'),
                ExperiencesEntity.results.label('results'),
                ExperiencesEntity.essence.label('essence'),
                ProductsEntity.name.label('product_name'),
                EmployeesEntity.name.label('employee_name'),
                ExperiencesEntity.status.label('status')
            ).filter(
                ExperiencesEntity.status == ExperiencesStatus.activity,
                ExperiencesEntity.product_id == ProductsEntity.id,
                ExperiencesEntity.employee_id == EmployeesEntity.id
            ).all()

            if experiencesEntity != []:
                for i in range(len(experiencesEntity)):
                    rec = {
                        'id': experiencesEntity[i].id if experiencesEntity[i].id is not None else "",
                        'product_id': experiencesEntity[i].product_id if experiencesEntity[i].product_id is not None else "",
                        'product_name': experiencesEntity[i].product_name if experiencesEntity[i].product_name is not None else "",
                        'employee_id': experiencesEntity[i].employee_id if experiencesEntity[i].employee_id is not None else "",
                        'employee_name': experiencesEntity[i].employee_name if experiencesEntity[i].employee_name is not None else "",
                        'interview': experiencesEntity[i].interview if experiencesEntity[i].interview is not None else "",
                        'advisory': experiencesEntity[i].advisory if experiencesEntity[i].advisory is not None else "",
                        'suggest': experiencesEntity[i].suggest if experiencesEntity[i].suggest is not None else "",
                        'results': experiencesEntity[i].results if experiencesEntity[i].results is not None else "",
                        'essence': experiencesEntity[i].essence if experiencesEntity[i].essence is not None else "",
                        'status': experiencesEntity[i].status if experiencesEntity[i].status is not None else ""
                    }
                    experiencesList.append(rec)
            else:
                experiencesList = []
            return {"experiencesList": experiencesList}
        except (
                Exception,
                RuntimeError
        ) as e:
            raise e
        finally:
            self.session.close()
            self.connection.close()

    def getExperiencesOne(self, experiences_id):
        try:
            self.session = super(ProductInstance, self).get_session()

            experiencesEntity = self.session.query(
                ExperiencesEntity.id.label('id'),
                ExperiencesEntity.product_id.label('product_id'),
                ExperiencesEntity.employee_id.label('employee_id'),
                ExperiencesEntity.image.label('image'),
                ExperiencesEntity.interview.label('interview'),
                ExperiencesEntity.advisory.label('advisory'),
                ExperiencesEntity.suggest.label('suggest'),
                ExperiencesEntity.results.label('results'),
                ExperiencesEntity.essence.label('essence'),
                ProductsEntity.name.label('product_name'),
                EmployeesEntity.name.label('employee_name'),
                ExperiencesEntity.sort.label('sort'),
                ExperiencesEntity.status.label('status')
            ).filter(
                ExperiencesEntity.status == ExperiencesStatus.activity,
                ExperiencesEntity.product_id == ProductsEntity.id,
                ExperiencesEntity.employee_id == EmployeesEntity.id,
                ExperiencesEntity.id == experiences_id
            ).all()

            if experiencesEntity is not None:
                for i in range(len(experiencesEntity)):
                    rec = {
                        'id': experiencesEntity[i].id if experiencesEntity[i].id is not None else "",
                        'product_id': experiencesEntity[i].product_id if experiencesEntity[i].product_id is not None else "",
                        'product_name': experiencesEntity[i].product_name if experiencesEntity[i].product_name is not None else "",
                        'employee_id': experiencesEntity[i].employee_id if experiencesEntity[i].employee_id is not None else "",
                        'employee_name': experiencesEntity[i].employee_name if experiencesEntity[i].employee_name is not None else "",
                        'image': experiencesEntity[i].image if experiencesEntity[i].image is not None else "",
                        'interview': experiencesEntity[i].interview if experiencesEntity[i].interview is not None else "",
                        'advisory': experiencesEntity[i].advisory if experiencesEntity[i].advisory is not None else "",
                        'suggest': experiencesEntity[i].suggest if experiencesEntity[i].suggest is not None else "",
                        'results': experiencesEntity[i].results if experiencesEntity[i].results is not None else "",
                        'essence': experiencesEntity[i].essence if experiencesEntity[i].essence is not None else "",
                        'sort': experiencesEntity[i].sort if experiencesEntity[i].sort is not None else "",
                        'status': experiencesEntity[i].status if experiencesEntity[i].status is not None else ""
                    }
                return {"experiences": rec}
            else:
                raise TargetIdDoesNotExistException()
        except (
                TargetIdDoesNotExistException,
                Exception,
                RuntimeError
        ) as e:
            raise e
        finally:
            self.session.close()
            self.connection.close()

    # new experiences create
    def create_or_update_experiences(self, experiences_id, product_id, input_personnel, image, interview, advisory, suggest, results,
                                     essence):
        try:
            self.session = super(ProductInstance, self).get_session()

            # create new product
            if experiences_id == "" or experiences_id is None:

                productsEntity = self.session.query(ProductsEntity).filter(
                    ProductsEntity.id == product_id
                ).scalar()
                if productsEntity is not None:

                    experiencesEntity = ExperiencesEntity()
                    experiencesEntity.product_id = product_id
                    experiencesEntity.employee_id = input_personnel
                    experiencesEntity.image = image
                    experiencesEntity.interview = interview
                    experiencesEntity.advisory = advisory
                    experiencesEntity.suggest = suggest
                    experiencesEntity.results = results
                    experiencesEntity.essence = essence
                    experiencesEntity.status = ExperiencesStatus.activity
                    self.session.add(experiencesEntity)
                    self.session.commit()
                    response = experiencesEntity.id
                else:
                    raise TargetIdDoesNotExistException()

            # update product
            else:
                experiencesEntity = self.session.query(ExperiencesEntity).filter(
                    ExperiencesEntity.id == int(experiences_id)
                ).scalar()

                if experiencesEntity is not None:

                    experiencesEntity.employee_id = input_personnel
                    experiencesEntity.image = image
                    experiencesEntity.interview = interview
                    experiencesEntity.advisory = advisory
                    experiencesEntity.suggest = suggest
                    experiencesEntity.results = results
                    experiencesEntity.essence = essence
                    self.session.commit()

                    response = experiencesEntity.id
                else:
                    raise TargetIdDoesNotExistException()
            return {"experiences_id": response}
        except (
                TargetIdDoesNotExistException,
                Exception,
                RuntimeError
        ) as e:
            raise e
        finally:
            self.session.close()
            self.connection.close()

    # delete experiences
    def delete_experiences(self, experiences_id):
        try:
            self.session = super(ProductInstance, self).get_session()

            experiencesEntity = self.session.query(ExperiencesEntity).filter(
                ExperiencesEntity.id == int(experiences_id),
                ExperiencesEntity.id == ExperiencesStatus.activity
            ).scalar()

            if experiencesEntity is not None:

                experiencesEntity.status = ExperiencesStatus.deleted
                self.session.commit()

                return {"experiences_id": experiences_id}
            else:
                raise TargetIdDoesNotExistException()
        except (
                TargetIdDoesNotExistException,
                Exception,
                RuntimeError
        ) as e:
            raise e
        finally:
            self.session.close()
            self.connection.close()

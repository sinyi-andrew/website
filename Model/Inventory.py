# -*- coding: utf-8 -*-
from datetime import datetime
import uuid
from sqlalchemy.sql import func

from Entity.Entity import *
from Utility.Status import *
from Model.Exception import *
from Model.SuperModel import SuperModel

global engine
global connection
global trans


class InventoryInstance(SuperModel):
    session = None

    def __init__(self):
        super(InventoryInstance, self).__init__()

    def getSupplies(self):
        suppliesList = []
        try:
            self.session = super(InventoryInstance, self).get_session()

            suppliesEntity = self.session.query(SuppliesEntity).filter().all()

            if suppliesEntity is not None:
                for i in range(len(suppliesEntity)):
                    num_warehouse = 0
                    num_counter = 0

                    test = suppliesEntity[i].id

                    sum = self.session.query(
                        SuppliesEntity.id.label('Supplie_id'),
                        SuppliesEntity.name.label('Supplie_name'),
                        SuppliesInventoryEntity.type.label('type'),
                        func.sum(SuppliesInventoryEntity.quantity).label('total')
                    ).filter(
                        SuppliesInventoryEntity.supplies_id == suppliesEntity[i].id,
                        SuppliesInventoryEntity.supplies_id == SuppliesEntity.id
                    ).group_by(
                        SuppliesInventoryEntity.type,
                        SuppliesInventoryEntity.supplies_id
                    ).all()

                    if sum != []:
                        for j in range(len(sum)):
                            if sum[j].type == InventoryTypeStatus.warehouse:
                                num_warehouse = int(sum[j].total)
                            elif sum[j].type == InventoryTypeStatus.counter:
                                num_counter = int(sum[j].total)

                    if suppliesEntity[i].product_id is not None:
                        productsEntity = self.session.query(
                            ProductsEntity.name.label('product_name')
                        ).filter(
                            ProductsEntity.id == suppliesEntity[i].product_id
                        ).first()

                        if productsEntity is not None:
                            product_name = productsEntity.product_name
                        else:
                            product_name = ""
                        rec = {
                            'id': suppliesEntity[i].id if suppliesEntity[i].id is not None else "",
                            'name': suppliesEntity[i].name if suppliesEntity[i].name is not None else "",
                            'cost': suppliesEntity[i].cost if suppliesEntity[i].cost is not None else "",
                            'product_id': suppliesEntity[i].product_id if suppliesEntity[i].product_id is not None else "",
                            'product_name': product_name if product_name is not None else "",
                            'num_warehouse': num_warehouse if num_warehouse is not None else 0,
                            'num_counter': num_counter if num_counter is not None else 0
                        }
                        suppliesList.append(rec)
                    else:
                        rec = {
                            'id': suppliesEntity[i].id if suppliesEntity[i].id is not None else "",
                            'name': suppliesEntity[i].name if suppliesEntity[i].name is not None else "",
                            'cost': suppliesEntity[i].cost if suppliesEntity[i].cost is not None else "",
                            'product_id': "",
                            'product_name': "庫存消耗品",
                            'num_warehouse': num_warehouse if num_warehouse is not None else 0,
                            'num_counter': num_counter if num_counter is not None else 0
                        }
                        suppliesList.append(rec)
            else:
                suppliesList = []
            return {"suppliesList": suppliesList}
        except (
                Exception,
                RuntimeError
        ) as e:
            raise e
        finally:
            self.session.close()
            self.connection.close()

    def getSuppliesById(self, id):
        suppliesList = []
        try:
            self.session = super(InventoryInstance, self).get_session()
            supply = self.session.query(SuppliesEntity).filter(
                SuppliesEntity.id == id
            ).first()
            product = self.session.query(ProductsEntity).filter(
                ProductsEntity.id == supply.product_id
            ).first()

            return product
        except (
                Exception,
                RuntimeError
        ) as e:
            raise e
        finally:
            self.session.close()
            self.connection.close()

    def getSupplyIDByProductId(self, product_id):
        try:
            self.session = super(InventoryInstance, self).get_session()

            suppliesEntity = self.session.query(SuppliesEntity).filter(
                SuppliesEntity.product_id == int(product_id)
            ).scalar()

            if suppliesEntity is not None:
                return suppliesEntity.id
            else:
                raise TargetIdDoesNotExistException()
        except (
            TargetIdDoesNotExistException,
            Exception,
            RuntimeError
        ) as e:
            raise e
        finally:
            self.session.close()
            self.connection.close()

    # supplies create/update
    def create_or_update_supplies(self, input_personnel, supplies_id, products_id, name, cost):
        try:
            self.session = super(InventoryInstance, self).get_session()

            # create new supplies
            if supplies_id == "" or supplies_id is None:
                # products_id -> None: Clinic supplies
                if products_id is None or products_id == "":
                    suppliesEntity = SuppliesEntity()
                    suppliesEntity.name = name
                    suppliesEntity.cost = cost
                    suppliesEntity.employee_id = input_personnel
                    self.session.add(suppliesEntity)
                    self.session.commit()
                    response = suppliesEntity.id

                # product supplies
                else:
                    productsEntity = self.session.query(ProductsEntity).filter(
                        ProductsEntity.id == int(products_id),
                        ProductsEntity.category_id == ProductCategoriesStatus.product
                    ).scalar()
                    if productsEntity is not None:
                        checksuppliesEntity = self.session.query(SuppliesEntity).filter(
                            SuppliesEntity.product_id == int(products_id)
                        ).scalar()
                        if checksuppliesEntity is None:
                            suppliesEntity = SuppliesEntity()
                            suppliesEntity.name = name
                            suppliesEntity.cost = cost
                            suppliesEntity.product_id = int(products_id)
                            suppliesEntity.employee_id = input_personnel
                            self.session.add(suppliesEntity)
                            self.session.commit()
                            response = suppliesEntity.id
                        else:
                            raise SuppliesDuplicationException()
                    else:
                        raise TargetIdDoesNotExistException()

            # update supplies inventory
            else:
                suppliesEntity = self.session.query(SuppliesEntity).filter(
                    SuppliesEntity.id == int(supplies_id)
                ).scalar()
                if suppliesEntity is not None:
                    # check deplication (product_id is the same)
                    # product_id no change
                    if suppliesEntity.product_id == products_id:
                        suppliesEntity.name = name
                        suppliesEntity.cost = cost
                        self.session.commit()
                        response = products_id
                    else:
                        checksuppliesEntity = self.session.query(SuppliesEntity).filter(
                            SuppliesEntity.product_id == int(products_id)
                        ).scalar()
                        if checksuppliesEntity is None:
                            suppliesEntity.name = name
                            suppliesEntity.cost = cost
                            suppliesEntity.product_id = products_id
                            self.session.commit()
                            response = products_id
                        else:
                            raise SuppliesDuplicationException()
                else:
                    raise TargetIdDoesNotExistException()
            return {"supplies_id": response}
        except (
                SuppliesDuplicationException,
                TargetIdDoesNotExistException,
                Exception,
                RuntimeError
        ) as e:
            raise e
        finally:
            self.session.close()
            self.connection.close()

    def getInventoryBySkuID(self, sku_id):
        try:
            self.session = super(InventoryInstance, self).get_session()

            skusEntity = self.session.query(
                ProductsEntity.id.label('product_id')
            ).filter(
                SkusEntity.id == int(sku_id),
                SkusEntity.product_id == ProductsEntity.id
            ).first()

            if skusEntity is not None:
                if skusEntity.product_id is not None:

                    suppliesEntity = self.session.query(SuppliesEntity).filter(
                        SuppliesEntity.product_id == int(skusEntity.product_id)
                    ).scalar()
                    if suppliesEntity is not None:
                        sum = self.session.query(func.sum(SuppliesInventoryEntity.quantity)).filter(
                            SuppliesInventoryEntity.supplies_id == suppliesEntity.id
                        ).scalar()
                        if sum is not None:
                            num_inventory = int(sum)
                        else:
                            num_inventory = 0
                        return num_inventory
                    else:
                        raise TargetIdDoesNotExistException()
                else:
                    raise TargetIdDoesNotExistException()
            else:
                raise TargetIdDoesNotExistException()
        except (
                TargetIdDoesNotExistException,
                Exception,
                RuntimeError
        ) as e:
            raise e
        finally:
            self.session.close()
            self.connection.close()

    def getInventory(self):
        warehouseList = []
        counterList = []
        try:
            self.session = super(InventoryInstance, self).get_session()
            warehouse = self.session.query(
                SuppliesInventoryEntity.id.label('suppliesInventor_id'),
                SuppliesInventoryEntity.quantity.label('quantity'),
                SuppliesInventoryEntity.note.label('note'),
                SuppliesInventoryEntity.date.label('date'),
                SuppliesEntity.name.label('supplies_name'),
                EmployeesEntity.name.label('employee_name')
            ).filter(
                SuppliesInventoryEntity.supplies_id == SuppliesEntity.id,
                SuppliesInventoryEntity.employee_id == EmployeesEntity.id,
                SuppliesInventoryEntity.type == InventoryTypeStatus.warehouse
            ).order_by(
                SuppliesInventoryEntity.supplies_id.asc()
            ).all()
            if warehouse != []:
                counter = self.session.query(
                    SuppliesInventoryEntity.id.label('suppliesInventor_id'),
                    SuppliesInventoryEntity.quantity.label('quantity'),
                    SuppliesInventoryEntity.note.label('note'),
                    SuppliesInventoryEntity.date.label('date'),
                    SuppliesEntity.name.label('supplies_name'),
                    EmployeesEntity.name.label('employee_name')
                ).filter(
                    SuppliesInventoryEntity.supplies_id == SuppliesEntity.id,
                    SuppliesInventoryEntity.employee_id == EmployeesEntity.id,
                    SuppliesInventoryEntity.type == InventoryTypeStatus.counter
                ).order_by(
                    SuppliesInventoryEntity.supplies_id.asc()
                ).all()
                if counter != []:
                    results = []
                    results.append(warehouse)
                    results.append(counter)
                    # for put warehouseList and counterList
                    warehouse_first = InventoryTypeStatus.warehouse
                    for result in results:
                        for i in range(len(result)):
                            rec = {
                                'suppliesInventor_id': result[i].suppliesInventor_id if result[i].suppliesInventor_id is not None else "",
                                'quantity': result[i].quantity if result[i].quantity is not None else "",
                                'note': result[i].note if result[i].note is not None else "",
                                'date': result[i].date.__str__() if result[i].date.__str__() is not None else "",
                                'supplies_name': result[i].supplies_name if result[i].supplies_name is not None else "",
                                'employee_name': result[i].employee_name if result[i].employee_name is not None else ""
                            }
                            if warehouse_first == InventoryTypeStatus.warehouse:
                                warehouseList.append(rec)
                            elif warehouse_first == InventoryTypeStatus.counter:
                                counterList.append(rec)
                        warehouse_first = InventoryTypeStatus.counter
                else:
                    counterList = []
            else:
                warehouseList = []
            return {
                "warehouseList": warehouseList,
                "counterList": counterList
            }
        except (
                Exception,
                RuntimeError
        ) as e:
            raise e
        finally:
            self.session.close()
            self.connection.close()

    def getInventoryBySupplyId(self, supply_id):
        inventoryList = []
        reslist = []
        try:
            self.session = super(InventoryInstance, self).get_session()
            inventoryList = self.session.query(SuppliesInventoryEntity).filter(
                SuppliesInventoryEntity.supplies_id == supply_id
            ).all()
            if inventoryList != []:
                for i in range(len(inventoryList)):
                    employee = self.session.query(
                        EmployeesEntity.employee_number.label('number'),
                        EmployeesEntity.name.label('name')
                    ).filter(
                        EmployeesEntity.id == inventoryList[i].employee_id
                    ).first()
                    res = {
                        'id': inventoryList[i].id if inventoryList[i].id is not None else "",
                        'note': inventoryList[i].note if inventoryList[i].note is not None else "",
                        'quantity': inventoryList[i].quantity if inventoryList[i].quantity is not None else "",
                        'current_single_cost': inventoryList[i].current_single_cost if inventoryList[i].current_single_cost is not None else "",
                        'type': '倉庫' if inventoryList[i].type is not True else "櫃台",
                        'date': inventoryList[i].date.__str__() if inventoryList[i].date.__str__() is not None else "",
                        'employee_number': employee.number if employee.number is not None else "",
                        'employee_name': employee.name if employee.name is not None else ""
                    }
                    reslist.append(res)
            return {"data": reslist}
        except (
                Exception,
                RuntimeError
        ) as e:
            raise e
        finally:
            self.session.close()
            self.connection.close()

    # inventory create/update
    def create_or_update_inventory(self, input_personnel, inventory_id, supplies_id, quantity, note, type):
        try:
            self.session = super(InventoryInstance, self).get_session()

            # create new inventory
            if inventory_id == "" or inventory_id is None:
                suppliesEntity = self.session.query(SuppliesEntity).filter(
                    SuppliesEntity.id == int(supplies_id)
                ).scalar()
                if suppliesEntity is not None:
                    suppliesInventoryEntity = SuppliesInventoryEntity()
                    suppliesInventoryEntity.supplies_id = supplies_id
                    suppliesInventoryEntity.employee_id = input_personnel
                    suppliesInventoryEntity.quantity = quantity
                    suppliesInventoryEntity.note = note
                    suppliesInventoryEntity.date = datetime.now()
                    suppliesInventoryEntity.type = type
                    suppliesInventoryEntity.current_single_cost = suppliesEntity.cost
                    self.session.add(suppliesInventoryEntity)
                    self.session.commit()
                    response = suppliesInventoryEntity.id
                else:
                    raise TargetIdDoesNotExistException()

            # update supplies inventory
            else:
                suppliesInventoryEntity = self.session.query(SuppliesInventoryEntity).filter(
                    SuppliesInventoryEntity.id == int(inventory_id)
                ).scalar()
                if suppliesInventoryEntity is not None:
                    # supplies_id and type(Warehouse or Counter inventory) can not be changed
                    suppliesInventoryEntity.employee_id = input_personnel
                    suppliesInventoryEntity.quantity = quantity
                    suppliesInventoryEntity.note = note
                    suppliesInventoryEntity.date = datetime.now()
                    self.session.commit()
                    response = suppliesInventoryEntity.id
                else:
                    raise TargetIdDoesNotExistException()
            return {"inventory_id": response}
        except (
                SuppliesDuplicationException,
                TargetIdDoesNotExistException,
                Exception,
                RuntimeError
        ) as e:
            raise e
        finally:
            self.session.close()
            self.connection.close()

# -*- coding: utf-8 -*-
from datetime import datetime
from sqlalchemy.sql import func

from Entity.Entity import *
from Utility.Status import *
from Model.Exception import *
from Model.Member import MemberInstance
from Model.SuperModel import SuperModel
from Utility.Email import EmailHelper

global engine
global connection
global trans


class FaqInstance(SuperModel):
    session = None

    def __init__(self):
        super(FaqInstance, self).__init__()

    def getFaqWithoutReply(self):
        faqList = []
        try:
            self.session = super(FaqInstance, self).get_session()

            faqsEntity = self.session.query(FaqsEntity).filter(
                FaqsEntity.reply == FaqStatus.noreply
            ).all()

            if faqsEntity != []:
                for i in range(len(faqsEntity)):

                    if faqsEntity[i].gender == GenderStatus.female:
                        gender_str = '女士'
                    elif faqsEntity[i].gender == GenderStatus.male:
                        gender_str = '男士'
                    elif faqsEntity[i].gender == GenderStatus.other:
                        gender_str = '其他'
                    else:
                        gender_str = ''

                    if faqsEntity[i].reply == FaqStatus.noreply:
                        reply_str = '未回覆'
                    elif faqsEntity[i].reply == FaqStatus.replied:
                        reply_str = '已回覆'
                    else:
                        reply_str = ''

                    if faqsEntity[i].reply_personnel is not None and faqsEntity[i].reply_personnel != '':
                        employeesEntity = self.session.query(EmployeesEntity).filter(
                            EmployeesEntity.id == int(faqsEntity[i].reply_personnel)
                        ).scalar()
                        if employeesEntity is not None:
                            reply_personnel_str = employeesEntity.name
                        else:
                            reply_personnel_str = ''
                    else:
                        reply_personnel_str = ''

                    rec = {
                        'faq_id': faqsEntity[i].id if faqsEntity[i].id is not None else "",
                        'type': '產品訂購' if faqsEntity[i].type is 'product' else "預約諮詢",
                        'name': faqsEntity[i].name if faqsEntity[i].name is not None else "",
                        'email': faqsEntity[i].email if faqsEntity[i].email is not None else "",
                        'phone_number': faqsEntity[i].phone_number if faqsEntity[i].phone_number is not None else "",
                        'mobile_phone': faqsEntity[i].mobile_phone if faqsEntity[i].mobile_phone is not None else "",
                        'birthday': faqsEntity[i].birthday.__str__() if faqsEntity[i].birthday.__str__() is not None else "",
                        'gender': faqsEntity[i].gender if faqsEntity[i].gender is not None else "",
                        'gender_str': gender_str,
                        'content': faqsEntity[i].content if faqsEntity[i].content is not None else "",
                        'date': faqsEntity[i].date.__str__() if faqsEntity[i].date.__str__() is not None else "",
                        'reply': faqsEntity[i].reply if faqsEntity[i].reply is not None else "",
                        'reply_str': reply_str,
                        'reply_content': faqsEntity[i].reply_content if faqsEntity[i].reply_content is not None else "",
                        'reply_personnel': faqsEntity[i].reply_personnel if faqsEntity[i].reply_personnel is not None else "",
                        'reply_personnel_str': reply_personnel_str
                    }
                    faqList.append(rec)
            return {'data': faqList}
        except (
                Exception,
                RuntimeError
        ) as e:
            raise e
        finally:
            self.session.close()
            self.connection.close()

    def getFaq(self):
        faqList = []
        try:
            self.session = super(FaqInstance, self).get_session()
            
            faqsEntity = self.session.query(FaqsEntity).filter().all()

            if faqsEntity != []:
                for i in range(len(faqsEntity)):

                    if faqsEntity[i].gender == GenderStatus.female:
                        gender_str = '女士'
                    elif faqsEntity[i].gender == GenderStatus.male:
                        gender_str = '男士'
                    elif faqsEntity[i].gender == GenderStatus.other:
                        gender_str = '其他'
                    else:
                        gender_str = ''

                    if faqsEntity[i].reply == FaqStatus.noreply:
                        reply_str = '未回覆'
                    elif faqsEntity[i].reply == FaqStatus.replied:
                        reply_str = '已回覆'
                    else:
                        reply_str = ''

                    if faqsEntity[i].reply_personnel is not None and faqsEntity[i].reply_personnel != '':
                        employeesEntity = self.session.query(EmployeesEntity).filter(
                            EmployeesEntity.id == int(faqsEntity[i].reply_personnel)
                        ).scalar()
                        if employeesEntity is not None:
                            reply_personnel_str = employeesEntity.name
                        else:
                            reply_personnel_str = ''
                    else:
                        reply_personnel_str = ''

                    rec = {
                        'faq_id': faqsEntity[i].id if faqsEntity[i].id is not None else "",
                        'type': faqsEntity[i].type if faqsEntity[i].type is not None else "",
                        'name': faqsEntity[i].name if faqsEntity[i].name is not None else "",
                        'email': faqsEntity[i].email if faqsEntity[i].email is not None else "",
                        'phone_number': faqsEntity[i].phone_number if faqsEntity[i].phone_number is not None else "",
                        'mobile_phone': faqsEntity[i].mobile_phone if faqsEntity[i].mobile_phone is not None else "",
                        'birthday': faqsEntity[i].birthday.__str__() if faqsEntity[i].birthday.__str__() is not None else "",
                        'gender': faqsEntity[i].gender if faqsEntity[i].gender is not None else "",
                        'gender_str': gender_str,
                        'content': faqsEntity[i].content if faqsEntity[i].content is not None else "",
                        'date': faqsEntity[i].date.__str__() if faqsEntity[i].date.__str__() is not None else "",
                        'reply': faqsEntity[i].reply if faqsEntity[i].reply is not None else "",
                        'reply_str': reply_str,
                        'reply_content': faqsEntity[i].reply_content if faqsEntity[i].reply_content is not None else "",
                        'reply_personnel': faqsEntity[i].reply_personnel if faqsEntity[i].reply_personnel is not None else "",
                        'reply_personnel_str': reply_personnel_str
                    }
                    faqList.append(rec)
            return {'data': faqList}
        except (
                Exception,
                RuntimeError
        ) as e:
            raise e
        finally:
            self.session.close()
            self.connection.close()

    # new faq create
    def create_faq(self, name, birthday, gender, phone_number, mobile_phone, email, faq_type, content):
        try:
            self.session = super(FaqInstance, self).get_session()

            newFaqs = FaqsEntity()
            newFaqs.store_id = 1  # multiple tenant
            newFaqs.type = faq_type
            newFaqs.name = name
            newFaqs.email = email
            newFaqs.phone_number = phone_number
            newFaqs.mobile_phone = mobile_phone
            newFaqs.birthday = birthday
            newFaqs.gender = gender
            newFaqs.content = content
            newFaqs.date = datetime.now()
            newFaqs.reply = FaqStatus.noreply
            self.session.add(newFaqs)
            self.session.commit()
            faq_id = newFaqs.id
            return faq_id
        except (
                Exception,
                RuntimeError
        ) as e:
            raise e
        finally:
            self.session.close()
            self.connection.close()

    def reply_faq(self, input_personnel, faq_id, reply_content):
        try:
            self.session = super(FaqInstance, self).get_session()

            faqsEntity = self.session.query(
                FaqsEntity
            ).filter(
                FaqsEntity.id == int(faq_id)
            ).scalar()

            if faqsEntity is not None:
                faqsEntity.reply = FaqStatus.replied
                faqsEntity.reply_content = reply_content
                faqsEntity.reply_personnel = input_personnel
                self.session.commit()

                faq_id = faqsEntity.id
                faq_type = faqsEntity.type
                user_name = faqsEntity.name
                user_email = faqsEntity.email
                user_phone_number = faqsEntity.phone_number
                user_mobile_phone = faqsEntity.mobile_phone
                user_birthday = faqsEntity.birthday
                user_gender = faqsEntity.gender
                faq_content = faqsEntity.content
                faq_date = faqsEntity.date.__str__()

                EmailHelper().reply_faq_mail(faq_type, user_name, user_email, user_phone_number, user_mobile_phone,
                                             faq_content, faq_date, reply_content)
                return faq_id
            else:
                raise TargetIdDoesNotExistException()
        except (
                TargetIdDoesNotExistException,
                Exception,
                RuntimeError
        ) as e:
            raise e
        finally:
            self.session.close()
            self.connection.close()
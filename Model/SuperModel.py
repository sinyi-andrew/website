# -*- coding: utf-8 -*-
from sqlalchemy.orm import sessionmaker
from Database import DatabaseFactory


#Class Description
class SuperModel():

    db_factory = None
    engine = None
    connection = None
    trans = None

    def __init__(self):

        self.db_factory = DatabaseFactory()
        self.engine = self.db_factory.get_db_engine()
        try:
            self.connection = self.engine.connect()
        except Exception as error:
            raise error

    def get_trans(self):
        if self.connection is None:
            self.connection = self.engine.connect()

        self.trans = self.connection.begin()
        return self.trans

    #Method description
    def get_session(self):
        if self.connection is None:
            self.connection = self.engine.connect()

        Session = sessionmaker(bind=self.connection)
        session = Session()
        return session


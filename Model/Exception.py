class TokenAuthenticationIsFailedException(Exception):
    def __init__(self):
        self.value = "TokenAuthenticationIsFailed"

    def __str__(self):
        return repr(self.value)


class AuthenticateFailedException(Exception):
    def __init__(self):
        self.value = "AuthenticateFailed"

    def __str__(self):
        return repr(self.value)


class JWTAuthenticateFailedException(Exception):
    def __init__(self):
        self.value = "JWTAuthenticateFailed"

    def __str__(self):
        return repr(self.value)


class MissingCriticalParametersException(Exception):
    def __init__(self):
        self.value = "MissingCriticalParameters"

    def __str__(self):
        return repr(self.value)


class MissingRequiredParametersException(Exception):
    def __init__(self):
        self.value = "MissingRequiredParameters"

    def __str__(self):
        return repr(self.value)


class DataDoesNotExistException(Exception):
    def __init__(self):
        self.value = "DataDoesNotExistException"

    def __str__(self):
        return repr(self.value)


class TargetIdDoesNotExistException(Exception):
    def __init__(self):
        self.value = "TargetIdDoesNotExistException"

    def __str__(self):
        return repr(self.value)


class TypeDoesNotExistException(Exception):
    def __init__(self):
        self.value = "TypeDoesNotExist"

    def __str__(self):
        return repr(self.value)


class IdDuplicationException(Exception):
    def __init__(self):
        self.value = "IdDuplication"

    def __str__(self):
        return repr(self.value)


class EmailDuplicationException(Exception):
    def __init__(self):
        self.value = "EmailDuplication"

    def __str__(self):
        return repr(self.value)


class ThisExperienceTicketAlreadyUsedException(Exception):
    def __init__(self):
        self.value = "ThisExperienceTicketAlreadyUsed"

    def __str__(self):
        return repr(self.value)


class AssembleDuplicationException(Exception):
    def __init__(self):
        self.value = "AssembleDuplication"

    def __str__(self):
        return repr(self.value)


class SkuDuplicationException(Exception):
    def __init__(self):
        self.value = "SkuDuplication"

    def __str__(self):
        return repr(self.value)


class ServicePartDuplicationException(Exception):
    def __init__(self):
        self.value = "ServicePartDuplication"

    def __str__(self):
        return repr(self.value)


class SuppliesDuplicationException(Exception):
    def __init__(self):
        self.value = "SuppliesDuplication"

    def __str__(self):
        return repr(self.value)


class EmployeeGradeDuplicationException(Exception):
    def __init__(self):
        self.value = "EmployeeGradeDuplication"

    def __str__(self):
        return repr(self.value)


class EmployeeCalendarDuplicationException(Exception):
    def __init__(self):
        self.value = "EmployeeCalendarDuplication"

    def __str__(self):
        return repr(self.value)


class DatatExistException(Exception):
    def __init__(self):
        self.value = "DatatExistError"

    def __str__(self):
        return repr(self.value)


class DatabaseFormatWrongException(Exception):
    def __init__(self):
        self.value = "DatabaseFormatWrong"

    def __str__(self):
        return repr(self.value)


class WrongFormatException(Exception):
    def __init__(self):
        self.value = "WrongFormat"

    def __str__(self):
        return repr(self.value)


class CreditcardNoteWrongFormatException(Exception):
    def __init__(self):
        self.value = "CreditcardNoteWrongFormat"

    def __str__(self):
        return repr(self.value)


class RecipientWrongFormatException(Exception):
    def __init__(self):
        self.value = "RecipientWrongFormat"

    def __str__(self):
        return repr(self.value)


class EmployeeListEmptyException(Exception):
    def __init__(self):
        self.value = "EmployeeListEmpty"

    def __str__(self):
        return repr(self.value)


class InsufficientBalanceException(Exception):
    def __init__(self):
        self.value = "InsufficientBalance"

    def __str__(self):
        return repr(self.value)


class ItemExpiredException(Exception):
    def __init__(self):
        self.value = "ItemExpired"

    def __str__(self):
        return repr(self.value)


class InsufficientInventoryException(Exception):
    def __init__(self):
        self.value = "InsufficientInventory"

    def __str__(self):
        return repr(self.value)


class PermissionDeniedException(Exception):
    def __init__(self):
        self.value = "PermissionDenied"

    def __str__(self):
        return repr(self.value)


class ServicePartUnmatchException(Exception):
    def __init__(self):
        self.value = "ServicePartUnmatch"

    def __str__(self):
        return repr(self.value)


class TotalPriceUnmatchException(Exception):
    def __init__(self):
        self.value = "TotalPriceUnmatch"

    def __str__(self):
        return repr(self.value)


class InvaildSkuInAssembleException(Exception):
    def __init__(self):
        self.value = "InvaildSkuInAssemble"

    def __str__(self):
        return repr(self.value)


class WrongUsedInputException(Exception):
    def __init__(self):
        self.value = "WrongUsedInput"

    def __str__(self):
        return repr(self.value)


class EventAlreadyRegisteredException(Exception):
    def __init__(self):
        self.value = "EventAlreadyRegistered"

    def __str__(self):
        return repr(self.value)


class EventAlreadyCheckedByDoctorException(Exception):
    def __init__(self):
        self.value = "EventAlreadyCheckedByDoctor"

    def __str__(self):
        return repr(self.value)


class EventNotYetRegisterException(Exception):
    def __init__(self):
        self.value = "EventNotYetRegister"

    def __str__(self):
        return repr(self.value)


class EventAlreadyCanceledException(Exception):
    def __init__(self):
        self.value = "EventAlreadyCanceled"

    def __str__(self):
        return repr(self.value)


class GradeDoesNotExistException(Exception):
    def __init__(self):
        self.value = "GradeDoesNotExist"

    def __str__(self):
        return repr(self.value)

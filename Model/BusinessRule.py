# -*- coding: utf-8 -*-
import calendar
from datetime import datetime, date, timedelta
import uuid
from sqlalchemy.sql import func
from sqlalchemy import desc

from Entity.Entity import *
from Utility.Status import *

from Model.Exception import *
from Model.SuperModel import SuperModel

global engine
global connection
global trans


class BusinessRuleInstance(SuperModel):
    session = None

    def __init__(self):
        super(BusinessRuleInstance, self).__init__()

    # def example:
    # def getXxxByYyy(self, Yyy)

    def getNumOfUsers(self):
        try:
            self.session = super(BusinessRuleInstance, self).get_session()

            usersEntity = self.session.query(UsersEntity).filter().count()

            if usersEntity is not None:
                numOfUsers = usersEntity
            else:
                numOfUsers = 0
            return numOfUsers
        except (
                Exception,
                RuntimeError
        ) as e:
            raise e
        finally:
            self.session.close()
            self.connection.close()

    def getSalesByDate(self, dateToday):
        try:
            self.session = super(BusinessRuleInstance, self).get_session()

            sales = self.session.query(
                OrdersEntity.status.label('status'),
                OrdersEntity.date.label('date'),
                func.sum(OrdersEntity.total_price).label('total_price')
            ).filter(
                cast(OrdersEntity.sales_reocrd_date, DATE) == dateToday
            ).group_by(
                OrdersEntity.status
            ).all()

            salesToday = 0
            if sales != []:
                for i in range(len(sales)):
                    # deleted order
                    if sales[i].status == OrdersTypeStatus.deleted:
                        order_date = sales[i].date.date()
                        if order_date == dateToday:
                            pass
                        else:
                            salesToday = salesToday + -(int(sales[i].total_price))
                    # activity order
                    else:
                        salesToday = salesToday + (int(sales[i].total_price))
            else:
                salesToday = 0
            return salesToday
        except (
                Exception,
                RuntimeError
        ) as e:
            raise e
        finally:
            self.session.close()
            self.connection.close()

    def getSalesByDateOnlyPersonal(self, dateToday, personal_id):
        try:
            self.session = super(BusinessRuleInstance, self).get_session()

            sales = self.session.query(
                OrdersEntity.status.label('status'),
                OrdersEntity.date.label('date'),
                func.sum(OrdersEntity.total_price).label('total_price')
            ).filter(
                cast(OrdersEntity.sales_reocrd_date, DATE) == dateToday,
                or_(
                    OrdersEntity.saler_id == personal_id,
                    OrdersEntity.employee_id == personal_id
                )
            ).group_by(
                OrdersEntity.status
            ).all()

            salesToday = 0
            if sales != []:
                for i in range(len(sales)):
                    # deleted order
                    if sales[i].status == OrdersTypeStatus.deleted:
                        order_date = sales[i].date.date()
                        if order_date == dateToday:
                            salesToday = salesToday + 0
                        else:
                            salesToday = salesToday + -(int(sales[i].total_price))
                    # activity order
                    else:
                        salesToday = salesToday + (int(sales[i].total_price))
            else:
                salesToday = 0
            return salesToday
        except (
                Exception,
                RuntimeError
        ) as e:
            raise e
        finally:
            self.session.close()
            self.connection.close()

    def getSalesByMonth(self, targetYear, targetMonth):
        try:
            self.session = super(BusinessRuleInstance, self).get_session()

            sales = self.session.query(
                OrdersEntity.status.label('status'),
                OrdersEntity.date.label('date'),
                func.sum(OrdersEntity.total_price).label('total_price')
            ).filter(
                extract('year', OrdersEntity.sales_reocrd_date) == int(targetYear),
                extract('month', OrdersEntity.sales_reocrd_date) == int(targetMonth)
            ).group_by(
                OrdersEntity.status
            ).all()

            salesByMonth = 0
            if sales != []:
                for i in range(len(sales)):
                    # deleted order
                    if sales[i].status == OrdersTypeStatus.deleted:
                        order_date_year = sales[i].date.year
                        order_date_month = sales[i].date.month
                        if order_date_year == targetYear and order_date_month == targetMonth:
                            pass
                        else:
                            salesByMonth = salesByMonth + -(int(sales[i].total_price))
                    # activity order
                    else:
                        salesByMonth = salesByMonth + (int(sales[i].total_price))
            else:
                salesByMonth = 0
            return salesByMonth
        except (
                Exception,
                RuntimeError
        ) as e:
            raise e
        finally:
            self.session.close()
            self.connection.close()

    def getSalesByMonthOnlyPersonal(self, targetYear, targetMonth, personal_id):
        try:
            self.session = super(BusinessRuleInstance, self).get_session()

            sales = self.session.query(
                OrdersEntity.status.label('status'),
                OrdersEntity.date.label('date'),
                func.sum(OrdersEntity.total_price).label('total_price')
            ).filter(
                extract('year', OrdersEntity.sales_reocrd_date) == int(targetYear),
                extract('month', OrdersEntity.sales_reocrd_date) == int(targetMonth),
                or_(
                    OrdersEntity.saler_id == personal_id,
                    OrdersEntity.employee_id == personal_id
                )
            ).group_by(
                OrdersEntity.status
            ).all()

            salesByMonthPersonal = 0
            if sales != []:
                for i in range(len(sales)):
                    # deleted order
                    if sales[i].status == OrdersTypeStatus.deleted:
                        order_date_year = sales[i].date.year
                        order_date_month = sales[i].date.month
                        if order_date_year == targetYear and order_date_month == targetMonth:
                            pass
                        else:
                            salesByMonthPersonal = salesByMonthPersonal + -(int(sales[i].total_price))
                    # activity order
                    else:
                        salesByMonthPersonal = salesByMonthPersonal + (int(sales[i].total_price))
            else:
                salesByMonthPersonal = 0
            return salesByMonthPersonal
        except (
                Exception,
                RuntimeError
        ) as e:
            raise e
        finally:
            self.session.close()
            self.connection.close()

    def getSalesByMonthAllPersonal(self, targetYear, targetMonth, employeeList):
        salesByMonthAllPersonal = []
        try:
            self.session = super(BusinessRuleInstance, self).get_session()

            employeeIDList = employeeList['employeeIDList']
            employeeNameList = employeeList['employeeNameList']

            for i in range(len(employeeIDList)):

                sales = self.session.query(
                    func.sum(OrdersEntity.total_price)
                ).filter(
                    extract('year', OrdersEntity.date) == int(targetYear),
                    extract('month', OrdersEntity.date) == int(targetMonth),
                    or_(
                        OrdersEntity.saler_id == int(employeeIDList[i]),
                        OrdersEntity.employee_id == int(employeeIDList[i])
                    ),
                    OrdersEntity.cancel_employee_id == None
                ).scalar()

                if sales is not None:
                    rec = {
                        'employeeID': employeeIDList[i],
                        'employeeName': employeeNameList[i],
                        'sales': int(sales)
                    }
                else:
                    rec = {
                        'employeeID': employeeIDList[i],
                        'employeeName': employeeNameList[i],
                        'sales': 0
                    }
                salesByMonthAllPersonal.append(rec)
            return salesByMonthAllPersonal
        except (
                Exception,
                RuntimeError
        ) as e:
            raise e
        finally:
            self.session.close()
            self.connection.close()

    def getSalesByMonthAllDays(self, targetYear, targetMonth):
        salesByMonthAllDays = []
        try:
            self.session = super(BusinessRuleInstance, self).get_session()

            # list all days in target month
            num_days = calendar.monthrange(targetYear, targetMonth)[1]
            days = [(date(targetYear, targetMonth, day)).strftime("%Y-%m-%d") for day in range(1, num_days+1)]

            sales = self.session.query(
                cast(OrdersEntity.date, DATE).label('date'),
                func.sum(OrdersEntity.total_price).label('total_price')
            ).filter(
                extract('year', OrdersEntity.date) == int(targetYear),
                extract('month', OrdersEntity.date) == int(targetMonth),
                OrdersEntity.cancel_employee_id == None
            ).group_by(cast(OrdersEntity.date, DATE)).all()

            if sales != []:
                dateSalesListByMonth = []
                totalSalesListByMonth = []
                for i in range(len(sales)):
                    dateSalesListByMonth.append((sales[i].date).strftime("%Y-%m-%d"))
                    totalSalesListByMonth.append(int(sales[i].total_price))

                for i in range(len(days)):
                    # if days in sales[i].date:
                    if days[i] in dateSalesListByMonth:
                        sales_index = dateSalesListByMonth.index(days[i])
                        rec = {
                            'date': dateSalesListByMonth[sales_index],
                            'total_price': int(totalSalesListByMonth[sales_index])
                        }
                    else:
                        rec = {
                            'date': days[i],
                            'total_price': 0
                        }
                    salesByMonthAllDays.append(rec)
            else:
                for i in range(len(days)):
                    rec = {
                        'date': days[i],
                        'total_price': 0
                    }
                    salesByMonthAllDays.append(rec)
            return salesByMonthAllDays
        except (
                Exception,
                RuntimeError
        ) as e:
            raise e
        finally:
            self.session.close()
            self.connection.close()

    def getSalesByYearAllMonths(self, targetYear, targetMonth):
        salesByYearAllMonths = []
        try:
            self.session = super(BusinessRuleInstance, self).get_session()

            # list all months in target month
            last_year_month = [(date(targetYear-1, month, 1)).strftime("%Y-%m") for month in range(targetMonth+1, 12+1)]
            this_year_month = [(date(targetYear, month, 1)).strftime("%Y-%m") for month in range(1, targetMonth+1)]
            months = last_year_month + this_year_month

            sales = self.session.query(
                extract('year', OrdersEntity.date).label('year'),
                extract('month', OrdersEntity.date).label('month'),
                cast(OrdersEntity.date, DATE).label('date'),
                func.sum(OrdersEntity.total_price).label('total_price')
            ).filter(
                or_(extract('year', OrdersEntity.date) == int(targetYear - 1),
                    extract('year', OrdersEntity.date) == int(targetYear)
                    ),
                OrdersEntity.cancel_employee_id == None,
                OrdersEntity.id == OrderItemsEntity.order_id,
                OrderItemsEntity.name == ""
            ).group_by(
                extract('year', OrdersEntity.date),
                extract('month', OrdersEntity.date)
            ).all()

            if sales != []:
                monthSalesListByYear = []
                totalSalesListByYear = []
                for i in range(len(sales)):
                    monthSalesListByYear.append(date(sales[i].year, sales[i].month, 1).strftime("%Y-%m"))
                    totalSalesListByYear.append(int(sales[i].total_price))

                for i in range(len(months)):
                    # if months in sales[i].date:
                    if months[i] in monthSalesListByYear:
                        sales_index = monthSalesListByYear.index(months[i])
                        rec = {
                            'month': monthSalesListByYear[sales_index],
                            'total_price': int(totalSalesListByYear[sales_index])
                        }
                    else:
                        rec = {
                            'month': months[i],
                            'total_price': 0
                        }
                    salesByYearAllMonths.append(rec)
            else:
                for i in range(len(months)):
                    rec = {
                        'month': months[i],
                        'total_price': 0
                    }
                    salesByYearAllMonths.append(rec)
            return salesByYearAllMonths
        except (
                Exception,
                RuntimeError
        ) as e:
            raise e
        finally:
            self.session.close()
            self.connection.close()

    def getSalesByMonthAllProducts(self, targetYear, targetMonth, productList):
        salesByMonthAllProducts = []
        try:
            self.session = super(BusinessRuleInstance, self).get_session()

            sales = self.session.query(
                OrderItemsEntity.sku_id.label('sku_id'),
                SkusEntity.product_id.label('product_id'),
                ProductsEntity.category_id.label('category_id'),
                ProductsEntity.name.label('product_name'),
                ProductCategoriesEntity.name.label('category_name'),
                func.sum(OrdersEntity.total_price).label('total_price')
            ).filter(
                extract('year', OrdersEntity.date) == int(targetYear),
                extract('month', OrdersEntity.date) == int(targetMonth),
                OrdersEntity.cancel_employee_id == None,
                OrdersEntity.id == OrderItemsEntity.order_id,
                OrderItemsEntity.name == "",
                OrderItemsEntity.sku_id == SkusEntity.id,
                SkusEntity.product_id == ProductsEntity.id,
                ProductsEntity.category_id == ProductCategoriesEntity.id
            ).group_by(
                SkusEntity.product_id
            ).all()

            if sales != []:
                sku_id_list = []
                product_id_list = []
                product_name_list = []
                category_id_list = []
                category_name_list = []
                total_price_list = []
                for i in range(len(sales)):
                    sku_id_list.append(int(sales[i].sku_id))
                    product_id_list.append(int(sales[i].product_id))
                    product_name_list.append(sales[i].product_name)
                    category_id_list.append(sales[i].category_id)
                    category_name_list.append(sales[i].category_name)
                    total_price_list.append(int(sales[i].total_price))

                for i in range(len(productList)):
                    if productList[i]['product_id'] in product_id_list:
                        product_index = product_id_list.index(productList[i]['product_id'])

                        salesByMonthAllProducts.append({
                            'category_id': category_id_list[product_index],
                            'category_name': category_name_list[product_index],
                            'product_id': product_id_list[product_index],
                            'product_name': product_name_list[product_index],
                            'total_price': int(total_price_list[product_index])
                        })

                    else:
                        salesByMonthAllProducts.append({
                            'product_id': productList[i]['product_id'],
                            'product_name': productList[i]['product_name'],
                            'category_id': productList[i]['category_id'],
                            'category_name': productList[i]['category_name'],
                            'total_price': 0
                        })
            else:
                for i in range(len(productList)):
                    salesByMonthAllProducts.append({
                        'product_id': productList[i]['product_id'],
                        'product_name': productList[i]['product_name'],
                        'category_id': productList[i]['category_id'],
                        'category_name': productList[i]['category_name'],
                        'total_price': 0
                    })
            return salesByMonthAllProducts
        except (
                Exception,
                RuntimeError
        ) as e:
            raise e
        finally:
            self.session.close()
            self.connection.close()

    def getSalesByMonthAllServices(self, targetYear, targetMonth, productList):
        salesByMonthAllServices = []
        try:
            self.session = super(BusinessRuleInstance, self).get_session()

            # list all months in target month
            last_year_month = [(date(targetYear-1, month, 1)).strftime("%Y-%m") for month in range(targetMonth+1, 12+1)]
            this_year_month = [(date(targetYear, month, 1)).strftime("%Y-%m") for month in range(1, targetMonth+1)]
            months = last_year_month + this_year_month

            sales = self.session.query(
                extract('year', OrdersEntity.date).label('year'),
                extract('month', OrdersEntity.date).label('month'),
                OrderItemsEntity.sku_id.label('sku_id'),
                SkusEntity.product_id.label('product_id'),
                ProductsEntity.name.label('product_name'),
                func.sum(OrdersEntity.total_price).label('total_price')
            ).filter(
                or_(extract('year', OrdersEntity.date) == int(targetYear - 1),
                    extract('year', OrdersEntity.date) == int(targetYear)
                    ),
                OrdersEntity.cancel_employee_id == None,
                OrdersEntity.id == OrderItemsEntity.order_id,
                OrderItemsEntity.name == "",
                OrderItemsEntity.sku_id == SkusEntity.id,
                SkusEntity.product_id == ProductsEntity.id
            ).group_by(
                SkusEntity.product_id,
                extract('year', OrdersEntity.date),
                extract('month', OrdersEntity.date)
            ).all()

            if sales != []:
                sales_result = []
                for i in range(len(sales)):
                    sales_result.append({
                        'month': (date(int(sales[i].year), int(sales[i].month), 1)).strftime("%Y-%m"),
                        'sku_id': int(sales[i].sku_id),
                        'product_id': int(sales[i].product_id),
                        'product_name': sales[i].product_name,
                        'total_price': int(sales[i].total_price)
                    })

                for i in range(len(productList)):
                    if productList[i]['category_id'] == ProductCategoriesStatus.product or productList[i]['category_id'] == ProductCategoriesStatus.assemble:
                        continue
                    else:
                        salesByYearSingleProducts = []
                        for j in range(len(months)):
                            judgment_data_in_result = False
                            for k in range(len(sales_result)):
                                if sales_result[k]['month'] == months[j] and sales_result[k]['product_id'] == productList[i]['product_id']:
                                    judgment_data_in_result = True
                                    result = k

                            if judgment_data_in_result:
                                salesByYearSingleProducts.append({
                                    'month': sales_result[result]['month'],
                                    'product_id': sales_result[result]['product_id'],
                                    'product_name': sales_result[result]['product_name'],
                                    'total_price': sales_result[result]['total_price']
                                })
                            else:
                                salesByYearSingleProducts.append({
                                    'month': months[j],
                                    'product_id': productList[i]['product_id'],
                                    'product_name': productList[i]['product_name'],
                                    'total_price': 0
                                })
                        salesByMonthAllServices.append({
                            'product_name': productList[i]['product_name'],
                            'category_id': productList[i]['category_id'],
                            'salesByYearSingleProducts': salesByYearSingleProducts
                        })
            else:
                for i in range(len(productList)):
                    for j in range(len(months)):
                        salesByYearSingleProducts = []
                        salesByYearSingleProducts.append({
                            'month': months[j],
                            'product_id': productList[i]['product_id'],
                            'product_name': productList[i]['product_name'],
                            'total_price': 0
                        })
                    salesByMonthAllServices.append({
                            'product_name': productList[i]['product_name'],
                            'category_id': productList[i]['category_id'],
                            'salesByYearSingleProducts': salesByYearSingleProducts
                    })
            return salesByMonthAllServices
        except (
                Exception,
                RuntimeError
        ) as e:
            raise e
        finally:
            self.session.close()
            self.connection.close()

    def getSalesByYearAllProducts(self, targetYear, targetMonth, productCategoryList):
        salesByYearAllProducts = []
        try:
            self.session = super(BusinessRuleInstance, self).get_session()

            # list all months in target month
            last_year_month = [(date(targetYear-1, month, 1)).strftime("%Y-%m") for month in range(targetMonth+1, 12+1)]
            this_year_month = [(date(targetYear, month, 1)).strftime("%Y-%m") for month in range(1, targetMonth+1)]
            months = last_year_month + this_year_month

            sales = self.session.query(
                extract('year', OrdersEntity.date).label('year'),
                extract('month', OrdersEntity.date).label('month'),
                ProductsEntity.category_id.label('category_id'),
                ProductCategoriesEntity.name.label('category_name'),
                func.sum(OrdersEntity.total_price).label('total_price')
            ).filter(
                or_(extract('year', OrdersEntity.date) == int(targetYear - 1),
                    extract('year', OrdersEntity.date) == int(targetYear)
                    ),
                OrdersEntity.cancel_employee_id == None,
                OrdersEntity.id == OrderItemsEntity.order_id,
                OrderItemsEntity.name == "",
                OrderItemsEntity.sku_id == SkusEntity.id,
                SkusEntity.product_id == ProductsEntity.id,
                ProductsEntity.category_id == ProductCategoriesEntity.id
            ).group_by(
                ProductsEntity.category_id,
                extract('year', OrdersEntity.date),
                extract('month', OrdersEntity.date)
            ).all()

            if sales != []:
                sales_result = []
                for i in range(len(sales)):
                    sales_result.append({
                        'month': (date(int(sales[i].year), int(sales[i].month), 1)).strftime("%Y-%m"),
                        'category_id': int(sales[i].category_id),
                        'category_name': sales[i].category_name,
                        'total_price': int(sales[i].total_price)
                    })

                for i in range(len(productCategoryList)):
                    salesByYearSingleProducts = []
                    for j in range(len(months)):
                        judgment_data_in_result = False
                        for k in range(len(sales_result)):
                            if sales_result[k]['month'] == months[j] and sales_result[k]['category_id'] == productCategoryList[i]['category_id']:
                                judgment_data_in_result = True
                                result = k

                        if judgment_data_in_result:
                            salesByYearSingleProducts.append({
                                'month': sales_result[result]['month'],
                                'category_id': sales_result[result]['category_id'],
                                'category_name': sales_result[result]['category_name'],
                                'total_price': sales_result[result]['total_price']
                            })
                        else:
                            salesByYearSingleProducts.append({
                                'month': months[j],
                                'category_id': productCategoryList[i]['category_id'],
                                'category_name': productCategoryList[i]['category_name'],
                                'total_price': 0
                            })
                    salesByYearAllProducts.append(salesByYearSingleProducts)
            else:
                for i in range(len(productCategoryList)):
                    for j in range(len(months)):
                        salesByYearSingleProducts = []
                        salesByYearSingleProducts.append({
                            'month': months[j],
                            'category_id': productCategoryList[i]['category_id'],
                            'category_name': productCategoryList[i]['category_name'],
                            'total_price': 0
                        })
                    salesByYearAllProducts.append(salesByYearSingleProducts)
            return salesByYearAllProducts
        except (
                Exception,
                RuntimeError
        ) as e:
            raise e
        finally:
            self.session.close()
            self.connection.close()

    def getAllUnusedAmount(self):
        try:
            self.session = super(BusinessRuleInstance, self).get_session()

            point = self.session.query(
                func.sum(UserAccountsEntity.point)
            ).filter(

            ).scalar()

            if point is not None:
                allUnusedAmount = int(point)
            else:
                allUnusedAmount = 0
            return allUnusedAmount
        except (
                Exception,
                RuntimeError
        ) as e:
            raise e
        finally:
            self.session.close()
            self.connection.close()

    def getProductDetailByCategoryId(self, category_id):
        products_list = []
        try:
            self.session = super(BusinessRuleInstance, self).get_session()

            if category_id == ProductCategoriesStatus.product or category_id == ProductCategoriesStatus.assemble:
                products = self.session.query(
                    ProductCategoriesEntity.id.label('categories_id'),
                    ProductCategoriesEntity.name.label('categories_name'),
                    ProductsEntity.id.label('products_id'),
                    ProductsEntity.name.label('products_name'),
                    ProductsEntity.discount.label('products_discount'),
                    ProductsEntity.intro.label('products_intro'),
                    ProductsEntity.description.label('products_description'),
                    ProductsEntity.ingredient.label('products_ingredient'),
                    ProductsEntity.care.label('products_care'),
                    ProductsEntity.notice.label('products_notice'),
                    ProductsEntity.faq.label('products_faq'),
                    ProductsEntity.note.label('products_note'),
                    ProductsEntity.tip.label('products_tip'),
                    ProductsEntity.status.label('products_status')
                ).filter(
                    ProductCategoriesEntity.id == ProductsEntity.category_id,
                    ProductCategoriesEntity.id == category_id
                ).order_by(ProductsEntity.sort).all()
            else:
                products = self.session.query(
                    ProductCategoriesEntity.id.label('categories_id'),
                    ProductCategoriesEntity.name.label('categories_name'),
                    ProductsEntity.id.label('products_id'),
                    ProductsEntity.name.label('products_name'),
                    ProductsEntity.discount.label('products_discount'),
                    ProductsEntity.intro.label('products_intro'),
                    ProductsEntity.description.label('products_description'),
                    ProductsEntity.ingredient.label('products_ingredient'),
                    ProductsEntity.care.label('products_care'),
                    ProductsEntity.notice.label('products_notice'),
                    ProductsEntity.faq.label('products_faq'),
                    ProductsEntity.note.label('products_note'),
                    ProductsEntity.tip.label('products_tip'),
                    ProductsEntity.status.label('products_status')
                ).filter(
                    ProductCategoriesEntity.id == ProductsEntity.category_id,
                    ~ProductCategoriesEntity.id.in_([1, 2])
                ).order_by(ProductsEntity.sort).all()

            if products != []:
                for i in range(len(products)):
                    if products[i].products_id is not None:
                        skus_list = []
                        skusEntity = self.session.query(SkusEntity).filter(
                            SkusEntity.product_id == products[i].products_id,
                            SkusEntity.status == SkuStatus.activity
                        ).all()
                        for j in range(len(skusEntity)):
                            recSkus = {
                                'skus_id': skusEntity[j].id if skusEntity[j].id is not None else "",
                                'number': skusEntity[j].number if skusEntity[j].number is not None else 1,
                                'price': skusEntity[j].price if skusEntity[j].price is not None else 0,
                                'expire': skusEntity[j].expire if skusEntity[j].expire is not None else 0,
                                'product_serial_number': skusEntity[j].product_serial_number if skusEntity[j].product_serial_number is not None else "",
                                'only_for_assemable': skusEntity[j].only_for_assemable if skusEntity[j].only_for_assemable is not None else ""
                            }
                            skus_list.append(recSkus)
                    else:
                        skus_list = []

                    # product: detail and sku
                    if products[i].categories_id == ProductCategoriesStatus.product:
                        rec = {
                            'categories_id': products[i].categories_id if products[i].categories_id is not None else "",
                            'categories_name': products[i].categories_name if products[i].categories_name is not None else "",
                            'products_id': products[i].products_id if products[i].products_id is not None else "",
                            'products_name': products[i].products_name if products[i].products_name is not None else "",
                            'products_discount': products[i].products_discount if products[i].products_discount is not None else "",
                            'products_intro': products[i].products_intro if products[i].products_intro is not None else "",
                            'products_description': products[i].products_description if products[i].products_description is not None else "",
                            'products_ingredient': products[i].products_ingredient if products[i].products_ingredient is not None else "",
                            'products_care': products[i].products_care if products[i].products_care is not None else "",
                            'products_notice': products[i].products_notice if products[i].products_notice is not None else "",
                            'products_tip': products[i].products_tip if products[i].products_tip is not None else "",
                            'products_status': products[i].products_status if products[i].products_status is not None else "",
                            'skus': skus_list
                        }
                        products_list.append(rec)

                    # assemble: detail and sku and assemble
                    elif products[i].categories_id == 2:
                        assembleList = []

                        assembleEntity = self.session.query(
                            AssembleEntity.id.label('id'),
                            AssembleEntity.sku_id.label('sku_id'),
                            AssembleEntity.sort.label('sort')
                        ).filter(
                            AssembleEntity.delete_time == None,
                            AssembleEntity.parent_sku_id == SkusEntity.id,
                            SkusEntity.product_id == products[i].products_id
                        ).order_by(AssembleEntity.sort.asc()).all()

                        for j in range(len(assembleEntity)):
                            recServiceParts = {
                                'id': assembleEntity[j].id if assembleEntity[j].id is not None else "",
                                'sku_id': assembleEntity[j].sku_id if assembleEntity[j].sku_id is not None else "",
                                'sort': assembleEntity[j].sort if assembleEntity[j].sort is not None else ""
                            }
                            assembleList.append(recServiceParts)

                        rec = {
                            'categories_id': products[i].categories_id if products[i].categories_id is not None else "",
                            'categories_name': products[i].categories_name if products[i].categories_name is not None else "",
                            'products_id': products[i].products_id if products[i].products_id is not None else "",
                            'products_name': products[i].products_name if products[i].products_name is not None else "",
                            'products_discount': products[i].products_discount if products[i].products_discount is not None else "",
                            'products_intro': products[i].products_intro if products[i].products_intro is not None else "",
                            'products_description': products[i].products_description if products[i].products_description is not None else "",
                            'products_ingredient': products[i].products_ingredient if products[i].products_ingredient is not None else "",
                            'products_care': products[i].products_care if products[i].products_care is not None else "",
                            'products_notice': products[i].products_notice if products[i].products_notice is not None else "",
                            'products_tip': products[i].products_tip if products[i].products_tip is not None else "",
                            'products_status': products[i].products_status if products[i].products_status is not None else "",
                            'skus': skus_list,
                            'assemble': assembleList
                        }
                        products_list.append(rec)

                    # service: detail and sku and service part
                    else:
                        servicePartsList = []
                        servicePartsEntity = self.session.query(ServicePartsEntity).filter(
                            ServicePartsEntity.product_id == products[i].products_id
                        ).all()

                        for j in range(len(servicePartsEntity)):
                            recServiceParts = {
                                'serviceParts_id': servicePartsEntity[j].id if servicePartsEntity[j].id is not None else "",
                                'employee_grade': servicePartsEntity[j].employee_grade if servicePartsEntity[j].employee_grade is not None else "",
                                'number': servicePartsEntity[j].number if servicePartsEntity[j].number is not None else "",
                                'technical_fee': servicePartsEntity[j].technical_fee if servicePartsEntity[j].technical_fee is not None else ""
                            }
                            servicePartsList.append(recServiceParts)

                        rec = {

                            'categories_id': products[i].categories_id if products[i].categories_id is not None else "",
                            'categories_name': products[i].categories_name if products[i].categories_name is not None else "",
                            'products_id': products[i].products_id if products[i].products_id is not None else "",
                            'products_name': products[i].products_name if products[i].products_name is not None else "",
                            'products_discount': products[i].products_discount if products[i].products_discount is not None else "",
                            'products_intro': products[i].products_intro if products[i].products_intro is not None else "",
                            'products_description': products[i].products_description if products[i].products_description is not None else "",
                            'products_ingredient': products[i].products_ingredient if products[i].products_ingredient is not None else "",
                            'products_care': products[i].products_care if products[i].products_care is not None else "",
                            'products_notice': products[i].products_notice if products[i].products_notice is not None else "",
                            'products_tip': products[i].products_tip if products[i].products_tip is not None else "",
                            'products_status': products[i].products_status if products[i].products_status is not None else "",
                            'skus': skus_list,
                            'serviceParts': servicePartsList
                        }
                        products_list.append(rec)

                return {"products_list": products_list}
            else:
                return {"products_list": []}
        except (
            TargetIdDoesNotExistException,
            Exception,
            RuntimeError
        ) as e:
            raise e
        finally:
            self.session.close()
            self.connection.close()

    def getProductDetailByCategoryIdForFrontend(self, category_id):
        products_list = []
        try:
            self.session = super(BusinessRuleInstance, self).get_session()

            # product and assemble
            if category_id == ProductCategoriesStatus.product or category_id == ProductCategoriesStatus.assemble:
                products = self.session.query(
                    ProductCategoriesEntity.id.label('categories_id'),
                    ProductCategoriesEntity.name.label('categories_name'),
                    ProductsEntity.id.label('products_id'),
                    ProductsEntity.name.label('products_name'),
                    ProductsEntity.discount.label('products_discount'),
                    ProductsEntity.intro.label('products_intro'),
                    ProductsEntity.description.label('products_description'),
                    ProductsEntity.ingredient.label('products_ingredient'),
                    ProductsEntity.care.label('products_care'),
                    ProductsEntity.notice.label('products_notice'),
                    ProductsEntity.faq.label('products_faq'),
                    ProductsEntity.note.label('products_note'),
                    ProductsEntity.tip.label('products_tip'),
                    ProductsEntity.status.label('products_status')
                ).filter(
                    ProductCategoriesEntity.id == ProductsEntity.category_id,
                    ProductCategoriesEntity.id == category_id,
                    ProductsEntity.status == ProductStatus.activity
                ).order_by(ProductsEntity.sort).all()

            # service
            else:
                products = self.session.query(
                    ProductCategoriesEntity.id.label('categories_id'),
                    ProductCategoriesEntity.name.label('categories_name'),
                    ProductsEntity.id.label('products_id'),
                    ProductsEntity.name.label('products_name'),
                    ProductsEntity.discount.label('products_discount'),
                    ProductsEntity.intro.label('products_intro'),
                    ProductsEntity.description.label('products_description'),
                    ProductsEntity.ingredient.label('products_ingredient'),
                    ProductsEntity.care.label('products_care'),
                    ProductsEntity.notice.label('products_notice'),
                    ProductsEntity.faq.label('products_faq'),
                    ProductsEntity.note.label('products_note'),
                    ProductsEntity.tip.label('products_tip'),
                    ProductsEntity.status.label('products_status')
                ).filter(
                    ProductCategoriesEntity.id == ProductsEntity.category_id,
                    ~ProductCategoriesEntity.id.in_([ProductCategoriesStatus.product, ProductCategoriesStatus.assemble, ProductCategoriesStatus.experience_ticket]),
                    ProductsEntity.status == ProductStatus.activity
                ).order_by(ProductsEntity.sort).all()

            if products != []:
                for i in range(len(products)):
                    if products[i].products_id is not None:
                        skus_list = []
                        skusEntity = self.session.query(SkusEntity).filter(
                            SkusEntity.product_id == products[i].products_id,
                            SkusEntity.status == SkuStatus.activity
                        ).all()
                        for j in range(len(skusEntity)):
                            recSkus = {
                                'skus_id': skusEntity[j].id if skusEntity[j].id is not None else "",
                                'number': skusEntity[j].number if skusEntity[j].number is not None else 1,
                                'price': skusEntity[j].price if skusEntity[j].price is not None else 0,
                                'expire': skusEntity[j].expire if skusEntity[j].expire is not None else 0,
                                'product_serial_number': skusEntity[j].product_serial_number if skusEntity[j].product_serial_number is not None else "",
                                'only_for_assemable': skusEntity[j].only_for_assemable if skusEntity[j].only_for_assemable is not None else ""
                            }
                            skus_list.append(recSkus)
                    else:
                        skus_list = []

                    # product: detail and sku
                    if products[i].categories_id == ProductCategoriesStatus.product:
                        rec = {
                            'categories_id': products[i].categories_id if products[i].categories_id is not None else "",
                            'categories_name': products[i].categories_name if products[i].categories_name is not None else "",
                            'products_id': products[i].products_id if products[i].products_id is not None else "",
                            'products_name': products[i].products_name if products[i].products_name is not None else "",
                            'products_discount': products[i].products_discount if products[i].products_discount is not None else "",
                            'products_intro': products[i].products_intro if products[i].products_intro is not None else "",
                            'products_description': products[i].products_description if products[i].products_description is not None else "",
                            'products_ingredient': products[i].products_ingredient if products[i].products_ingredient is not None else "",
                            'products_care': products[i].products_care if products[i].products_care is not None else "",
                            'products_notice': products[i].products_notice if products[i].products_notice is not None else "",
                            'products_tip': products[i].products_tip if products[i].products_tip is not None else "",
                            'products_status': products[i].products_status if products[i].products_status is not None else "",
                            'skus': skus_list
                        }
                        products_list.append(rec)

                    # assemble: detail and sku and assemble
                    elif products[i].categories_id == 2:
                        assembleList = []

                        assembleEntity = self.session.query(AssembleEntity).filter(
                            AssembleEntity.parent_sku_id == products[i].products_id,
                            AssembleEntity.delete_time == None
                        ).order_by(AssembleEntity.sort.asc()).all()

                        for j in range(len(assembleEntity)):
                            recServiceParts = {
                                'id': assembleEntity[j].id if assembleEntity[j].id is not None else "",
                                'sku_id': assembleEntity[j].sku_id if assembleEntity[j].sku_id is not None else "",
                                'sort': assembleEntity[j].sort if assembleEntity[j].sort is not None else ""
                            }
                            assembleList.append(recServiceParts)

                        rec = {
                            'categories_id': products[i].categories_id if products[i].categories_id is not None else "",
                            'categories_name': products[i].categories_name if products[i].categories_name is not None else "",
                            'products_id': products[i].products_id if products[i].products_id is not None else "",
                            'products_name': products[i].products_name if products[i].products_name is not None else "",
                            'products_discount': products[i].products_discount if products[i].products_discount is not None else "",
                            'products_intro': products[i].products_intro if products[i].products_intro is not None else "",
                            'products_description': products[i].products_description if products[i].products_description is not None else "",
                            'products_ingredient': products[i].products_ingredient if products[i].products_ingredient is not None else "",
                            'products_care': products[i].products_care if products[i].products_care is not None else "",
                            'products_notice': products[i].products_notice if products[i].products_notice is not None else "",
                            'products_tip': products[i].products_tip if products[i].products_tip is not None else "",
                            'products_status': products[i].products_status if products[i].products_status is not None else "",
                            'skus': skus_list,
                            'assemble': assembleList
                        }
                        products_list.append(rec)

                    # service: detail and sku and service part
                    else:
                        servicePartsList = []
                        servicePartsEntity = self.session.query(ServicePartsEntity).filter(
                            ServicePartsEntity.product_id == products[i].products_id
                        ).all()

                        for j in range(len(servicePartsEntity)):
                            recServiceParts = {
                                'serviceParts_id': servicePartsEntity[j].id if servicePartsEntity[j].id is not None else "",
                                'employee_grade': servicePartsEntity[j].employee_grade if servicePartsEntity[j].employee_grade is not None else "",
                                'number': servicePartsEntity[j].number if servicePartsEntity[j].number is not None else "",
                                'technical_fee': servicePartsEntity[j].technical_fee if servicePartsEntity[j].technical_fee is not None else ""
                            }
                            servicePartsList.append(recServiceParts)

                        rec = {

                            'categories_id': products[i].categories_id if products[i].categories_id is not None else "",
                            'categories_name': products[i].categories_name if products[i].categories_name is not None else "",
                            'products_id': products[i].products_id if products[i].products_id is not None else "",
                            'products_name': products[i].products_name if products[i].products_name is not None else "",
                            'products_discount': products[i].products_discount if products[i].products_discount is not None else "",
                            'products_intro': products[i].products_intro if products[i].products_intro is not None else "",
                            'products_description': products[i].products_description if products[i].products_description is not None else "",
                            'products_ingredient': products[i].products_ingredient if products[i].products_ingredient is not None else "",
                            'products_care': products[i].products_care if products[i].products_care is not None else "",
                            'products_notice': products[i].products_notice if products[i].products_notice is not None else "",
                            'products_tip': products[i].products_tip if products[i].products_tip is not None else "",
                            'products_status': products[i].products_status if products[i].products_status is not None else "",
                            'skus': skus_list,
                            'serviceParts': servicePartsList
                        }
                        products_list.append(rec)

                return {"products_list": products_list}
            else:
                return {"products_list": []}
        except (
            TargetIdDoesNotExistException,
            Exception,
            RuntimeError
        ) as e:
            raise e
        finally:
            self.session.close()
            self.connection.close()

    def getProductDetailByProductsId(self, products_id):
        try:
            self.session = super(BusinessRuleInstance, self).get_session()

            products = self.session.query(
                ProductCategoriesEntity.id.label('categories_id'),
                ProductCategoriesEntity.name.label('categories_name'),
                ProductsEntity.id.label('products_id'),
                ProductsEntity.name.label('products_name'),
                ProductsEntity.image.label('products_image'),
                ProductsEntity.discount.label('products_discount'),
                ProductsEntity.intro.label('products_intro'),
                ProductsEntity.description.label('products_description'),
                ProductsEntity.ingredient.label('products_ingredient'),
                ProductsEntity.care.label('products_care'),
                ProductsEntity.notice.label('products_notice'),
                ProductsEntity.faq.label('products_faq'),
                ProductsEntity.note.label('products_note'),
                ProductsEntity.tip.label('products_tip'),
                ProductsEntity.sort.label('products_sort'),
                ProductsEntity.status.label('products_status')
            ).filter(
                ProductCategoriesEntity.id == ProductsEntity.category_id,
                ProductsEntity.id == products_id
            ).first()

            if products is not None:
                if products.products_id is not None:
                    skus_list = []

                    skusEntity = self.session.query(SkusEntity).filter(
                        SkusEntity.product_id == products.products_id,
                        SkusEntity.status == SkuStatus.activity
                    ).all()

                    for j in range(len(skusEntity)):
                        recSkus = {
                            'skus_id': skusEntity[j].id if skusEntity[j].id is not None else "",
                            'number': skusEntity[j].number if skusEntity[j].number is not None else 1,
                            'price': skusEntity[j].price if skusEntity[j].price is not None else 0,
                            'expire': skusEntity[j].expire if skusEntity[j].expire is not None else 0,
                            'product_serial_number': skusEntity[j].product_serial_number if skusEntity[j].product_serial_number is not None else "",
                            'only_for_assemable': skusEntity[j].only_for_assemable if skusEntity[j].only_for_assemable is not None else ""
                        }
                        skus_list.append(recSkus)
                else:
                    skus_list = []

                # product: detail and sku
                if products.categories_id == ProductCategoriesStatus.product:
                    products = {
                        'categories_id': products.categories_id if products.categories_id is not None else "",
                        'categories_name': products.categories_name if products.categories_name is not None else "",
                        'products_id': products.products_id if products.products_id is not None else "",
                        'products_name': products.products_name if products.products_name is not None else "",
                        'products_image': products.products_image if products.products_image is not None else "",
                        'products_discount': products.products_discount if products.products_discount is not None else "",
                        'products_intro': products.products_intro if products.products_intro is not None else "",
                        'products_description': products.products_description if products.products_description is not None else "",
                        'products_ingredient': products.products_ingredient if products.products_ingredient is not None else "",
                        'products_care': products.products_care if products.products_care is not None else "",
                        'products_faq': products.products_faq if products.products_faq is not None else "",
                        'products_notice': products.products_notice if products.products_notice is not None else "",
                        'products_note': products.products_note if products.products_note is not None else "",
                        'products_tip': products.products_tip if products.products_tip is not None else "",
                        'products_status': products.products_status if products.products_status is not None else "",
                        'products_sort': products.products_sort if products.products_sort is not None else "",
                        'skus': skus_list
                    }
                    return {"products": products}

                # assemble: detail and sku and assemble
                elif products.categories_id == 2:
                    assembleList = []

                    assembleEntity = self.session.query(
                        AssembleEntity.id.label('id'),
                        AssembleEntity.sku_id.label('sku_id'),
                        AssembleEntity.sort.label('sort')
                    ).filter(
                        AssembleEntity.delete_time == None,
                        AssembleEntity.parent_sku_id == SkusEntity.id,
                        SkusEntity.product_id == products_id
                    ).order_by(AssembleEntity.sort.asc()).all()

                    for j in range(len(assembleEntity)):
                        recServiceParts = {
                            'id': assembleEntity[j].id if assembleEntity[j].id is not None else "",
                            'sku_id': assembleEntity[j].sku_id if assembleEntity[j].sku_id is not None else "",
                            'sort': assembleEntity[j].sort if assembleEntity[j].sort is not None else ""
                        }
                        assembleList.append(recServiceParts)

                    products = {
                        'categories_id': products.categories_id if products.categories_id is not None else "",
                        'categories_name': products.categories_name if products.categories_name is not None else "",
                        'products_id': products.products_id if products.products_id is not None else "",
                        'products_name': products.products_name if products.products_name is not None else "",
                        'products_image': products.products_image if products.products_image is not None else "",
                        'products_discount': products.products_discount if products.products_discount is not None else "",
                        'products_intro': products.products_intro if products.products_intro is not None else "",
                        'products_description': products.products_description if products.products_description is not None else "",
                        'products_ingredient': products.products_ingredient if products.products_ingredient is not None else "",
                        'products_care': products.products_care if products.products_care is not None else "",
                        'products_faq': products.products_faq if products.products_faq is not None else "",
                        'products_notice': products.products_notice if products.products_notice is not None else "",
                        'products_note': products.products_note if products.products_note is not None else "",
                        'products_tip': products.products_tip if products.products_tip is not None else "",
                        'products_status': products.products_status if products.products_status is not None else "",
                        'products_sort': products.products_sort if products.products_sort is not None else "",
                        'skus': skus_list,
                        'assemble': assembleList
                    }
                    return {"products": products}

                # service: detail and sku and service part
                else:
                    servicePartsList = []

                    servicePartsEntity = self.session.query(ServicePartsEntity).filter(
                        ServicePartsEntity.product_id == products.products_id
                    ).all()

                    for j in range(len(servicePartsEntity)):
                        recServiceParts = {
                            'serviceParts_id': servicePartsEntity[j].id if servicePartsEntity[j].id is not None else "",
                            'employee_grade': servicePartsEntity[j].employee_grade if servicePartsEntity[j].employee_grade is not None else "",
                            'number': servicePartsEntity[j].number if servicePartsEntity[j].number is not None else "",
                            'technical_fee': servicePartsEntity[j].technical_fee if servicePartsEntity[j].technical_fee is not None else ""
                        }
                        servicePartsList.append(recServiceParts)

                    products = {
                        'categories_id': products.categories_id if products.categories_id is not None else "",
                        'categories_name': products.categories_name if products.categories_name is not None else "",
                        'products_id': products.products_id if products.products_id is not None else "",
                        'products_name': products.products_name if products.products_name is not None else "",
                        'products_image': products.products_image if products.products_image is not None else "",
                        'products_discount': products.products_discount if products.products_discount is not None else "",
                        'products_intro': products.products_intro if products.products_intro is not None else "",
                        'products_description': products.products_description if products.products_description is not None else "",
                        'products_ingredient': products.products_ingredient if products.products_ingredient is not None else "",
                        'products_care': products.products_care if products.products_care is not None else "",
                        'products_faq': products.products_faq if products.products_faq is not None else "",
                        'products_notice': products.products_notice if products.products_notice is not None else "",
                        'products_note': products.products_note if products.products_note is not None else "",
                        'products_tip': products.products_tip if products.products_tip is not None else "",
                        'products_status': products.products_status if products.products_status is not None else "",
                        'products_sort': products.products_sort if products.products_sort is not None else "",
                        'skus': skus_list,
                        'serviceParts': servicePartsList
                    }
                    return {"products": products}
            else:
                raise TargetIdDoesNotExistException()
        except (
            TargetIdDoesNotExistException,
            Exception,
            RuntimeError
        ) as e:
            raise e
        finally:
            self.session.close()
            self.connection.close()

    def getServiceCategoryByID(self, id):
        try:
            self.session = super(BusinessRuleInstance, self).get_session()

            productCategoriesEntity = self.session.query(
                ProductCategoriesEntity
            ).filter(
                ProductCategoriesEntity.id == id
            ).scalar()

            if productCategoriesEntity is not None:
                productCategories = {
                    'id': productCategoriesEntity.id if productCategoriesEntity.id is not None else "",
                    'name': productCategoriesEntity.name if productCategoriesEntity.name is not None else "",
                    'description': productCategoriesEntity.description if productCategoriesEntity.description is not None else "",
                    'type': productCategoriesEntity.type if productCategoriesEntity.type is not None else ""
                }
                return {"productCategoriesList": productCategories}
            else:
                raise TargetIdDoesNotExistException()
        except (
            TargetIdDoesNotExistException,
            Exception,
            RuntimeError
        ) as e:
            raise e
        finally:
            self.session.close()
            self.connection.close()


    def getEventNumByMonthAllDays(self, targetYear, targetMonth):
        eventNumByMonthAllDays = []
        try:
            self.session = super(BusinessRuleInstance, self).get_session()

            # list all days in target month
            num_days = calendar.monthrange(targetYear, targetMonth)[1]
            days = [(date(targetYear, targetMonth, day)).strftime("%Y-%m-%d") for day in range(1, num_days+1)]

            eventNum = self.session.query(
                cast(EventsEntity.start_time, DATE).label('start_time'),
                func.count(EventsEntity.id).label('num')
            ).filter(
                extract('year', EventsEntity.start_time) == int(targetYear),
                extract('month', EventsEntity.start_time) == int(targetMonth),
                EventsEntity.status == EventStatus.activity
            ).group_by(cast(EventsEntity.start_time, DATE)).all()

            if eventNum != []:
                dateEventNumListByMonth = []
                totalEventNumListByMonth = []
                for i in range(len(eventNum)):
                    dateEventNumListByMonth.append((eventNum[i].start_time).strftime("%Y-%m-%d"))
                    totalEventNumListByMonth.append(int(eventNum[i].num))

                for i in range(len(days)):
                    # if days in eventNum[i].date:
                    if days[i] in dateEventNumListByMonth:
                        EventNum_index = dateEventNumListByMonth.index(days[i])
                        rec = {
                            'date': dateEventNumListByMonth[EventNum_index],
                            'num': int(totalEventNumListByMonth[EventNum_index])
                        }
                    else:
                        rec = {
                            'date': days[i],
                            'num': 0
                        }
                    eventNumByMonthAllDays.append(rec)
            else:
                for i in range(len(days)):
                    rec = {
                        'date': days[i],
                        'num': 0
                    }
                    eventNumByMonthAllDays.append(rec)
            return eventNumByMonthAllDays
        except (
                Exception,
                RuntimeError
        ) as e:
            raise e
        finally:
            self.session.close()
            self.connection.close()

    def getUnusedEventNumByDayAllProducts(self, productList):
        unusedEventNumByDayAllProducts = []
        try:
            self.session = super(BusinessRuleInstance, self).get_session()

            unusedEventNum = self.session.query(
                SkusEntity.product_id.label('product_id'),
                ProductsEntity.name.label('product_name'),
                func.sum(OrderItemsEntity.total_amount).label('total_amount'),
                func.sum(OrderItemsEntity.used).label('used')
            ).filter(
                OrdersEntity.cancel_employee_id == None,
                OrdersEntity.id == OrderItemsEntity.order_id,
                OrderItemsEntity.sku_id == SkusEntity.id,
                SkusEntity.product_id == ProductsEntity.id,
                ProductsEntity.category_id != ProductCategoriesStatus.product,
                ProductsEntity.category_id != ProductCategoriesStatus.assemble
            ).group_by(
                SkusEntity.product_id
            ).all()

            if unusedEventNum != []:
                product_id_list = []
                product_name_list = []
                total_amount_list = []
                used_list = []
                for i in range(len(unusedEventNum)):
                    product_id_list.append(int(unusedEventNum[i].product_id))
                    product_name_list.append(unusedEventNum[i].product_name)
                    total_amount_list.append(int(unusedEventNum[i].total_amount))
                    used_list.append(int(unusedEventNum[i].used))

                for i in range(len(productList)):
                    if productList[i]['category_id'] != ProductCategoriesStatus.product and productList[i]['category_id'] != ProductCategoriesStatus.assemble:
                        if productList[i]['product_id'] in product_id_list:
                            product_index = product_id_list.index(productList[i]['product_id'])

                            unusedEventNumByDayAllProducts.append({
                                'product_id': product_id_list[product_index],
                                'product_name': product_name_list[product_index],
                                'total_amount': int(total_amount_list[product_index]),
                                'used': int(used_list[product_index]),
                                'unused': int(total_amount_list[product_index]) - int(used_list[product_index])
                            })

                        else:
                            unusedEventNumByDayAllProducts.append({
                                'product_id': productList[i]['product_id'],
                                'product_name': productList[i]['product_name'],
                                'total_amount': 0,
                                'used': 0,
                                'unused': 0
                            })
                    # else:
                        # nothing
            else:
                for i in range(len(productList)):
                    unusedEventNumByDayAllProducts.append({
                        'product_id': productList[i]['product_id'],
                        'product_name': productList[i]['product_name'],
                        'total_amount': 0,
                        'used': 0,
                        'unused': 0
                    })
            return unusedEventNumByDayAllProducts
        except (
                Exception,
                RuntimeError
        ) as e:
            raise e
        finally:
            self.session.close()
            self.connection.close()
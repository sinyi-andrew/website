# -*- coding: utf-8 -*-
from datetime import datetime, timedelta, date
from monthdelta import monthdelta
import math
import uuid
from sqlalchemy.sql import func
from sqlalchemy import desc

from Entity.Entity import *
from Utility.Status import *
from Model.Exception import *
from Model.Member import MemberInstance
from Model.SuperModel import SuperModel

global engine
global connection
global trans


class OrderInstance(SuperModel):
    session = None

    def __init__(self):
        super(OrderInstance, self).__init__()

    def getOrderIDByOrderUuid(self, uuid):
        try:
            self.session = super(OrderInstance, self).get_session()

            ordersEntity = self.session.query(OrdersEntity).filter(
                OrdersEntity.product_serial_number == uuid
            ).scalar()

            if ordersEntity is None:
                raise TargetIdDoesNotExistException()
            else:
                return ordersEntity.id
        except (
                TargetIdDoesNotExistException,
                Exception,
                RuntimeError
        ) as e:
            raise e
        finally:
            self.session.close()
            self.connection.close()

    def getOrderInfoByOrderID(self, order_id):
        inventory_addition_list = []
        try:
            self.session = super(OrderInstance, self).get_session()

            ordersEntity = self.session.query(OrdersEntity).filter(
                OrdersEntity.id == int(order_id)
            ).first()

            if ordersEntity is not None:

                orderItemsEntity = self.session.query(
                    OrderItemsEntity.id.label('order_items_id'),
                    OrderItemsEntity.sku_id.label('sku_id'),
                    OrderItemsEntity.used.label('used'),
                    ProductsEntity.category_id.label('category_id'),
                    SuppliesEntity.id.label('supplies_id')
                ).filter(
                    OrderItemsEntity.order_id == ordersEntity.id,
                    OrderItemsEntity.sku_id == SkusEntity.id,
                    SkusEntity.product_id == ProductsEntity.id,
                    ProductsEntity.id == SuppliesEntity.product_id
                ).all()

                for i in range(len(orderItemsEntity)):
                    if orderItemsEntity[i].category_id == ProductCategoriesStatus.product:
                        inventory_addition_list.append({
                            'used': orderItemsEntity[i].used,
                            'supplies_id': orderItemsEntity[i].supplies_id
                        })

                # Only increase, can not be reduced
                order_info = {
                    'user_id': ordersEntity.user_id if ordersEntity.user_id is not None else "",
                    'total_price': ordersEntity.total_price if ordersEntity.total_price is not None else "",
                    'payment_type': ordersEntity.payment_type if ordersEntity.payment_type is not None else "",
                    'shipping_type': ordersEntity.shipping_type if ordersEntity.shipping_type is not None else "",
                    'shipping_fee': ordersEntity.shipping_fee if ordersEntity.shipping_fee is not None else "",
                    'inventory_addition_list': inventory_addition_list
                }
                return order_info
            else:
                raise TargetIdDoesNotExistException()
        except (
                TargetIdDoesNotExistException,
                Exception,
                RuntimeError
        ) as e:
            raise e
        finally:
            self.session.close()
            self.connection.close()

    def getOrderInfoByOrderItemID(self, order_item_id):
        try:
            self.session = super(OrderInstance, self).get_session()

            orderItemsEntity = self.session.query(
                OrderItemsEntity.id.label('order_item_id'),
                OrderItemsEntity.order_id.label('order_id'),
                OrderItemsEntity.sku_id.label('sku_id'),
                OrderItemsEntity.used.label('used'),
                OrderItemsEntity.price.label('price'),
                OrdersEntity.user_id.label('user_id'),
                OrdersEntity.total_price.label('order_total_price'),
                OrdersEntity.payment_type.label('order_payment_type'),
                SkusEntity.product_id.label('product_id'),
                ProductsEntity.category_id.label('category_id'),
                ProductsEntity.discount.label('discount')
            ).filter(
                OrderItemsEntity.id == int(order_item_id),
                OrderItemsEntity.order_id == OrdersEntity.id,
                SkusEntity.id == OrderItemsEntity.sku_id,
                SkusEntity.product_id == ProductsEntity.id
            ).first()

            if orderItemsEntity is not None:
                order_item_info = {
                    'order_item_id': orderItemsEntity.order_item_id if orderItemsEntity.order_item_id is not None else "",
                    'order_id': orderItemsEntity.order_id if orderItemsEntity.order_id is not None else "",
                    'order_user_id': orderItemsEntity.user_id if orderItemsEntity.user_id is not None else "",
                    'order_item_sku_id': orderItemsEntity.sku_id if orderItemsEntity.sku_id is not None else "",
                    'order_item_price': orderItemsEntity.price if orderItemsEntity.price is not None else "",
                    'order_item_product_id': orderItemsEntity.product_id if orderItemsEntity.product_id is not None else "",
                    'order_item_category_id': orderItemsEntity.category_id if orderItemsEntity.category_id is not None else "",
                    'order_item_discount': orderItemsEntity.discount if orderItemsEntity.discount is not None else "",
                    'order_item_used': orderItemsEntity.used if orderItemsEntity.used is not None else "",
                    'order_total_price': orderItemsEntity.order_total_price if orderItemsEntity.order_total_price is not None else "",
                    'order_payment_type': orderItemsEntity.order_payment_type if orderItemsEntity.order_payment_type is not None else ""
                }
                return order_item_info
            else:
                raise TargetIdDoesNotExistException()
        except (
                TargetIdDoesNotExistException,
                Exception,
                RuntimeError
        ) as e:
            raise e
        finally:
            self.session.close()
            self.connection.close()

    def getOrder(self):
        orderList = []
        try:
            self.session = super(OrderInstance, self).get_session()
            ordersEntity = self.session.query(
                OrdersEntity.id.label('id'),
                OrdersEntity.product_serial_number.label('uuid'),
                OrdersEntity.order_number.label('order_number'),
                OrdersEntity.user_id.label('user_id'),
                OrdersEntity.saler_id.label('saler_id'),
                OrdersEntity.employee_id.label('employee_id'),
                OrdersEntity.total_price.label('total_price'),
                OrdersEntity.payment_type.label('payment_type'),
                OrdersEntity.shipping_type.label('shipping_type'),
                OrdersEntity.shipping_fee.label('shipping_fee'),
                OrdersEntity.status.label('status'),
                OrdersEntity.date.label('date'),
                OrdersEntity.cancel_employee_id.label('cancel_employee_id'),
                UsersEntity.name.label('user_name'),
                UsersEntity.mobile_phone.label('mobile_phone')
            ).filter(
                OrdersEntity.user_id == UsersEntity.id,
                OrdersEntity.cancel_employee_id == None
            ).order_by(OrdersEntity.date.desc()).all()

            employeesEntity = self.session.query(
                EmployeesEntity
            ).filter().all()

            emplyee_id_list = []
            emplyee_name_list = []
            if employeesEntity != []:
                for i in range(len(employeesEntity)):
                    emplyee_id_list.append(employeesEntity[i].id)
                    emplyee_name_list.append(employeesEntity[i].name)
                if ordersEntity != []:
                    count = len(ordersEntity)
                    for i in range(len(ordersEntity)):
                        employee1_id = emplyee_id_list.index(ordersEntity[i].saler_id)
                        employee2_id = emplyee_id_list.index(ordersEntity[i].employee_id)
                        if ordersEntity[i].cancel_employee_id is not None:
                            cancel_employee_id = emplyee_id_list.index(ordersEntity[i].cancel_employee_id)
                            cancel_employee_name = emplyee_name_list[cancel_employee_id]
                        else:
                            cancel_employee_name = ""

                        if ordersEntity[i].payment_type == OrdersPaymentTypeStatus.cash:
                            payment_type = '現金'
                        elif ordersEntity[i].payment_type == OrdersPaymentTypeStatus.points:
                            payment_type = '點數'
                        elif ordersEntity[i].payment_type == OrdersPaymentTypeStatus.creditcard:
                            payment_type = '信用卡'
                        elif ordersEntity[i].payment_type == OrdersPaymentTypeStatus.creditloan:
                            payment_type = '信貸'
                        else:
                            payment_type = ''

                        if ordersEntity[i].shipping_type == OrdersShippingTypeStatus.self:
                            shipping_type = '自取'
                        elif ordersEntity[i].shipping_type == OrdersShippingTypeStatus.homedelivery:
                            shipping_type = '宅配'
                        elif ordersEntity[i].shipping_type == OrdersShippingTypeStatus.conveniencestore:
                            shipping_type = '超商取貨'
                        else:
                            shipping_type = ''

                        if ordersEntity[i].status == OrdersTypeStatus.undone:
                            order_status = '未完成'
                        elif ordersEntity[i].status == OrdersTypeStatus.overdue:
                            order_status = '已逾期'
                        elif ordersEntity[i].status == OrdersTypeStatus.completed:
                            order_status = '已完成'
                        elif ordersEntity[i].status == OrdersTypeStatus.deleted:
                            order_status = '已註銷'
                        else:
                            order_status = ''

                        rec = {
                            'id': ordersEntity[i].id if ordersEntity[i].id is not None else "",
                            'order_num': ordersEntity[i].order_number if ordersEntity[i].order_number is not None else "",
                            'user_id': ordersEntity[i].user_id if ordersEntity[i].user_id is not None else "",
                            'user_name': ordersEntity[i].user_name if ordersEntity[i].user_name is not None else "",
                            'user_mobile_phone': ordersEntity[i].mobile_phone if ordersEntity[i].mobile_phone is not None else "",
                            'saler_id': ordersEntity[i].saler_id if ordersEntity[i].saler_id is not None else "",
                            'saler_name': emplyee_name_list[employee1_id] if emplyee_name_list[employee1_id] is not None else "",
                            'employee_id': ordersEntity[i].employee_id if ordersEntity[i].employee_id is not None else "",
                            'employee_name': emplyee_name_list[employee2_id] if emplyee_name_list[employee2_id] is not None else "",
                            'total_price': ordersEntity[i].total_price if ordersEntity[i].total_price is not None else "",
                            'payment_type': ordersEntity[i].payment_type if ordersEntity[i].payment_type is not None else "",
                            'payment_type_str': payment_type if payment_type is not None else "",
                            'shipping_type': ordersEntity[i].shipping_type if ordersEntity[i].shipping_type is not None else "",
                            'shipping_type_str': shipping_type if shipping_type is not None else "",
                            'shipping_fee': ordersEntity[i].shipping_fee if ordersEntity[i].shipping_fee is not None else "",
                            'status': ordersEntity[i].status if ordersEntity[i].status is not None else "",
                            'status_str': order_status if order_status is not None else "",
                            'date': ordersEntity[i].date.__str__() if ordersEntity[i].date.__str__() is not None else "",
                            'cancel': OrdersCancelTypeStatus.nocancel if ordersEntity[i].cancel_employee_id is None else OrdersCancelTypeStatus.canceled,
                            'cancel_employee_id': ordersEntity[i].cancel_employee_id if ordersEntity[i].cancel_employee_id is not None else "",
                            'cancel_employee_name': cancel_employee_name,
                            'href': '/redirect/' + ordersEntity[i].uuid
                            # 'href': '<a class="btn btn-warning btn-xs" target="_blank" href="/redirect/' + ordersEntity[i].uuid + '" >詳情</a>'
                        }
                        orderList.append(rec)
                else:
                    orderList = []
                    count = 0
                return {'draw': 1, 'data': orderList, 'recordsTotal': count}
            else:
                raise EmployeeListEmptyException()
        except (
                EmployeeListEmptyException,
                Exception,
                RuntimeError
        ) as e:
            raise e
        finally:
            self.session.close()
            self.connection.close()

    def getOrderBySearch(self, search):
        orderList = []
        try:
            self.session = super(OrderInstance, self).get_session()
            ordersEntity = self.session.query(
                OrdersEntity.id.label('id'),
                OrdersEntity.product_serial_number.label('uuid'),
                OrdersEntity.order_number.label('order_number'),
                OrdersEntity.user_id.label('user_id'),
                OrdersEntity.saler_id.label('saler_id'),
                OrdersEntity.employee_id.label('employee_id'),
                OrdersEntity.total_price.label('total_price'),
                OrdersEntity.payment_type.label('payment_type'),
                OrdersEntity.shipping_type.label('shipping_type'),
                OrdersEntity.shipping_fee.label('shipping_fee'),
                OrdersEntity.status.label('status'),
                OrdersEntity.date.label('date'),
                OrdersEntity.cancel_employee_id.label('cancel_employee_id'),
                UsersEntity.name.label('user_name'),
                UsersEntity.mobile_phone.label('mobile_phone')
            ).filter(
                or_(OrdersEntity.order_number.like("%" + search + "%"),
                    UsersEntity.name.like("%" + search + "%"),
                    UsersEntity.mobile_phone.like("%" + search + "%")
                    ),
                OrdersEntity.cancel_employee_id == None,
                OrdersEntity.user_id == UsersEntity.id
            ).order_by(OrdersEntity.date.desc()).all()

            employeesEntity = self.session.query(
                EmployeesEntity
            ).filter().all()

            emplyee_id_list = []
            emplyee_name_list = []
            if employeesEntity != []:
                for i in range(len(employeesEntity)):
                    emplyee_id_list.append(employeesEntity[i].id)
                    emplyee_name_list.append(employeesEntity[i].name)

                if ordersEntity != []:
                    count = len(ordersEntity)
                    for i in range(len(ordersEntity)):
                        employee1_id = emplyee_id_list.index(ordersEntity[i].saler_id)
                        employee2_id = emplyee_id_list.index(ordersEntity[i].employee_id)
                        if ordersEntity[i].cancel_employee_id is not None:
                            cancel_employee_id = emplyee_id_list.index(ordersEntity[i].cancel_employee_id)
                            cancel_employee_name = emplyee_name_list[cancel_employee_id]
                        else:
                            cancel_employee_name = ""

                        if ordersEntity[i].payment_type == OrdersPaymentTypeStatus.cash:
                            payment_type = '現金'
                        elif ordersEntity[i].payment_type == OrdersPaymentTypeStatus.points:
                            payment_type = '點數'
                        elif ordersEntity[i].payment_type == OrdersPaymentTypeStatus.creditcard:
                            payment_type = '信用卡'
                        elif ordersEntity[i].payment_type == OrdersPaymentTypeStatus.creditloan:
                            payment_type = '信貸'
                        else:
                            payment_type = ''

                        if ordersEntity[i].shipping_type == OrdersShippingTypeStatus.self:
                            shipping_type = '自取'
                        elif ordersEntity[i].shipping_type == OrdersShippingTypeStatus.homedelivery:
                            shipping_type = '宅配'
                        elif ordersEntity[i].shipping_type == OrdersShippingTypeStatus.conveniencestore:
                            shipping_type = '超商取貨'
                        else:
                            shipping_type = ''

                        if ordersEntity[i].status == OrdersTypeStatus.undone:
                            order_status = '未完成'
                        elif ordersEntity[i].status == OrdersTypeStatus.overdue:
                            order_status = '已逾期'
                        elif ordersEntity[i].status == OrdersTypeStatus.completed:
                            order_status = '已完成'
                        elif ordersEntity[i].status == OrdersTypeStatus.deleted:
                            order_status = '已註銷'
                        else:
                            order_status = ''

                        rec = {
                            'id': ordersEntity[i].id if ordersEntity[i].id is not None else "",
                            'order_num': ordersEntity[i].order_number if ordersEntity[i].order_number is not None else "",
                            'user_id': ordersEntity[i].user_id if ordersEntity[i].user_id is not None else "",
                            'user_name': ordersEntity[i].user_name if ordersEntity[i].user_name is not None else "",
                            'user_mobile_phone': ordersEntity[i].mobile_phone if ordersEntity[i].mobile_phone is not None else "",
                            'saler_id': ordersEntity[i].saler_id if ordersEntity[i].saler_id is not None else "",
                            'saler_name': emplyee_name_list[employee1_id] if emplyee_name_list[employee1_id] is not None else "",
                            'employee_id': ordersEntity[i].employee_id if ordersEntity[i].employee_id is not None else "",
                            'employee_name': emplyee_name_list[employee2_id] if emplyee_name_list[employee2_id] is not None else "",
                            'total_price': ordersEntity[i].total_price if ordersEntity[i].total_price is not None else "",
                            'payment_type': ordersEntity[i].payment_type if ordersEntity[i].payment_type is not None else "",
                            'payment_type_str': payment_type if payment_type is not None else "",
                            'shipping_type': ordersEntity[i].shipping_type if ordersEntity[i].shipping_type is not None else "",
                            'shipping_type_str': shipping_type if shipping_type is not None else "",
                            'shipping_fee': ordersEntity[i].shipping_fee if ordersEntity[i].shipping_fee is not None else "",
                            'status': ordersEntity[i].status if ordersEntity[i].status is not None else "",
                            'status_str': order_status if order_status is not None else "",
                            'date': ordersEntity[i].date.__str__() if ordersEntity[i].date.__str__() is not None else "",
                            'cancel': OrdersCancelTypeStatus.nocancel if ordersEntity[i].cancel_employee_id is None else OrdersCancelTypeStatus.canceled,
                            'cancel_employee_id': ordersEntity[i].cancel_employee_id if ordersEntity[i].cancel_employee_id is not None else "",
                            'cancel_employee_name': cancel_employee_name,
                            'href': ordersEntity[i].uuid
                            # 'href': '<a class="btn btn-warning btn-xs order-info" data-uuid="' + ordersEntity[i].uuid + '" >詳情</a>'
                        }
                        orderList.append(rec)
                else:
                    orderList = []
                    count = 0
                return {'data': orderList, 'recordsTotal': count, 'recordsFiltered ': count}
            else:
                raise EmployeeListEmptyException()
        except (
                EmployeeListEmptyException,
                Exception,
                RuntimeError
        ) as e:
            raise e
        finally:
            self.session.close()
            self.connection.close()

    def getOrderByOrderID(self, order_id):
        try:
            self.session = super(OrderInstance, self).get_session()

            ordersEntity = self.session.query(
                OrdersEntity.id.label('id'),
                OrdersEntity.user_id.label('user_id'),
                OrdersEntity.saler_id.label('saler_id'),
                OrdersEntity.employee_id.label('employee_id'),
                OrdersEntity.total_price.label('total_price'),
                OrdersEntity.payment_type.label('payment_type'),
                OrdersEntity.shipping_type.label('shipping_type'),
                OrdersEntity.shipping_fee.label('shipping_fee'),
                OrdersEntity.status.label('status'),
                OrdersEntity.date.label('date'),
                OrdersEntity.cancel_employee_id.label('cancel_employee_id'),
                OrdersEntity.refund.label('refund'),
                OrdersEntity.creditcard_note.label('creditcard_note'),
                UsersEntity.name.label('user_name'),
                UsersEntity.mobile_phone.label('mobile_phone')
            ).filter(
                OrdersEntity.id == int(order_id),
                OrdersEntity.user_id == UsersEntity.id
            ).first()

            employeesEntity = self.session.query(
                EmployeesEntity
            ).filter().all()

            emplyee_id_list = []
            emplyee_name_list = []

            if employeesEntity != []:
                for i in range(len(employeesEntity)):
                    emplyee_id_list.append(employeesEntity[i].id)
                    emplyee_name_list.append(employeesEntity[i].name)

                if ordersEntity is not None:
                    if ordersEntity.id is not None:
                        order_items_single_list = []
                        order_items_assemble_list = []

                        orderItemsEntity = self.session.query(OrderItemsEntity).filter(
                            OrderItemsEntity.order_id == ordersEntity.id
                        ).order_by(OrderItemsEntity.name.asc()).all()

                        for i in range(len(orderItemsEntity)):

                            skusEntity = self.session.query(
                                SkusEntity.id.label('sku_id'),
                                SkusEntity.sku_number.label('sku_number'),
                                ProductsEntity.name.label('products_name'),
                                ProductsEntity.category_id.label('item_category_id')
                            ).filter(
                                SkusEntity.id == int(orderItemsEntity[i].sku_id),
                                ProductsEntity.id == SkusEntity.product_id
                            ).first()

                            if orderItemsEntity[i].status == OrdersItemTypeStatus.completed:
                                status_str = 'completed'
                            elif orderItemsEntity[i].status == OrdersItemTypeStatus.unfinished:
                                status_str = 'unfinished'
                            else:
                                status_str = ''

                            recOrderItems = {
                                'order_items_id': orderItemsEntity[i].id if orderItemsEntity[i].id is not None else "",
                                'order_id': orderItemsEntity[i].order_id if orderItemsEntity[i].order_id is not None else "",
                                'sku_id': orderItemsEntity[i].sku_id if orderItemsEntity[i].sku_id is not None else "",
                                'sku_number': skusEntity.sku_number if skusEntity.sku_number is not None else "",
                                'price': orderItemsEntity[i].price if orderItemsEntity[i].price is not None else "",
                                'number': orderItemsEntity[i].number if orderItemsEntity[i].number is not None else "",
                                'quantity': orderItemsEntity[i].quantity if orderItemsEntity[i].quantity is not None else "",
                                'total_amount': orderItemsEntity[i].total_amount if orderItemsEntity[i].total_amount is not None else "",
                                'status': orderItemsEntity[i].status if orderItemsEntity[i].status is not None else "",
                                'status_str': status_str if status_str is not None else "",
                                'date': orderItemsEntity[i].date.__str__() if orderItemsEntity[i].date.__str__() is not None else "",
                                'used': orderItemsEntity[i].used if orderItemsEntity[i].used is not None else "",
                                'exp_date': orderItemsEntity[i].exp_date.__str__() if orderItemsEntity[i].exp_date.__str__() is not None else "",
                                'assemble_name': orderItemsEntity[i].name if orderItemsEntity[i].name is not None else "",
                                'name': skusEntity.products_name if skusEntity.products_name is not None else "",
                                'note': orderItemsEntity[i].note if orderItemsEntity[i].note is not None else "",
                                'category_id': skusEntity.item_category_id if skusEntity.item_category_id is not None else "",
                                'input_personnel': orderItemsEntity[i].input_personnel if orderItemsEntity[i].input_personnel is not None else "",
                                'price_diff': orderItemsEntity[i].price_diff if orderItemsEntity[i].price_diff is not None else ""
                            }

                            if orderItemsEntity[i].name == "":
                                order_items_single_list.append(recOrderItems)
                            else:
                                order_items_assemble_list.append(recOrderItems)
                    else:
                        order_items_single_list = []
                        order_items_assemble_list = []

                    employee1_id = emplyee_id_list.index(ordersEntity.saler_id)
                    employee2_id = emplyee_id_list.index(ordersEntity.employee_id)
                    if ordersEntity.cancel_employee_id is not None:
                        cancel_employee_id = emplyee_id_list.index(ordersEntity.cancel_employee_id)
                        cancel_employee_name = emplyee_name_list[cancel_employee_id]
                    else:
                        cancel_employee_name = ""

                    if ordersEntity.payment_type == OrdersPaymentTypeStatus.cash:
                        payment_type = '現金'
                    elif ordersEntity.payment_type == OrdersPaymentTypeStatus.points:
                        payment_type = '點數'
                    elif ordersEntity.payment_type == OrdersPaymentTypeStatus.creditcard:
                        payment_type = '信用卡'
                    elif ordersEntity.payment_type == OrdersPaymentTypeStatus.creditloan:
                        payment_type = '信貸'
                    else:
                        payment_type = ''

                    if ordersEntity.shipping_type == OrdersShippingTypeStatus.self:
                        shipping_type = '自取'
                    elif ordersEntity.shipping_type == OrdersShippingTypeStatus.homedelivery:
                        shipping_type = '宅配'
                    elif ordersEntity.shipping_type == OrdersShippingTypeStatus.conveniencestore:
                        shipping_type = '超商取貨'
                    else:
                        shipping_type = ''

                    if ordersEntity.status == OrdersTypeStatus.undone:
                        order_status = '未完成'
                    elif ordersEntity.status == OrdersTypeStatus.overdue:
                        order_status = '已逾期'
                    elif ordersEntity.status == OrdersTypeStatus.completed:
                        order_status = '已完成'
                    elif ordersEntity.status == OrdersTypeStatus.deleted:
                        order_status = '已註銷'
                    else:
                        order_status = ''

                    if ordersEntity.refund == OrdersRefundStatus.refund:
                        order_refund_status = OrdersRefundStatus.refund
                        order_refund_status_str = '已歸還退款'
                    elif ordersEntity.refund == OrdersRefundStatus.norefund:
                        order_refund_status = OrdersRefundStatus.norefund
                        order_refund_status_str = '未歸還退款'
                    else:
                        order_refund_status = ''
                        order_refund_status_str = ''

                    rec = {
                        'id': ordersEntity.id if ordersEntity.id is not None else "",
                        'order_num': 'D' + ordersEntity.id.__str__().rjust(8, '0') if ordersEntity.id is not None else "",
                        'user_id': ordersEntity.user_id if ordersEntity.user_id is not None else "",
                        'user_name': ordersEntity.user_name if ordersEntity.user_name is not None else "",
                        'user_mobile_phone': ordersEntity.mobile_phone if ordersEntity.mobile_phone is not None else "",
                        'saler_id': ordersEntity.saler_id if ordersEntity.saler_id is not None else "",
                        'saler_name': emplyee_name_list[employee1_id] if emplyee_name_list[employee1_id] is not None else "",
                        'employee_id': ordersEntity.employee_id if ordersEntity.employee_id is not None else "",
                        'employee_name': emplyee_name_list[employee2_id] if emplyee_name_list[employee2_id] is not None else "",
                        'total_price': ordersEntity.total_price if ordersEntity.total_price is not None else "",
                        'payment_type': ordersEntity.payment_type if ordersEntity.payment_type is not None else "",
                        'payment_type_str': payment_type if payment_type is not None else "",
                        'shipping_type': ordersEntity.shipping_type if ordersEntity.shipping_type is not None else "",
                        'shipping_type_str': shipping_type if shipping_type is not None else "",
                        'shipping_fee': ordersEntity.shipping_fee if ordersEntity.shipping_fee is not None else "",
                        'status': ordersEntity.status if ordersEntity.status is not None else "",
                        'status_str': order_status if order_status is not None else "",
                        'date': ordersEntity.date.__str__() if ordersEntity.date.__str__() is not None else "",
                        'cancel': OrdersCancelTypeStatus.nocancel if ordersEntity.cancel_employee_id is None else OrdersCancelTypeStatus.canceled,
                        'cancel_employee_id': ordersEntity.cancel_employee_id if ordersEntity.cancel_employee_id is not None else "",
                        'cancel_employee_name': cancel_employee_name,
                        'order_items_single_list': order_items_single_list,
                        'order_items_assemble_list': order_items_assemble_list,
                        'refund': order_refund_status if order_refund_status is not None else "",
                        'refund_str': order_refund_status_str if order_refund_status_str is not None else "",
                        'creditcard_note': ordersEntity.creditcard_note if ordersEntity.creditcard_note is not None else ""
                    }
                    return {"data": rec}
                else:
                    raise TargetIdDoesNotExistException()
            else:
                raise EmployeeListEmptyException()
        except (
                TargetIdDoesNotExistException,
                EmployeeListEmptyException,
                Exception,
                RuntimeError
        ) as e:
            raise e
        finally:
            self.session.close()
            self.connection.close()

    def getOrderByUserID(self, user_id):
        orderList = []
        try:
            self.session = super(OrderInstance, self).get_session()
            ordersEntity = self.session.query(
                OrdersEntity.id.label('id'),

                OrdersEntity.product_serial_number.label('uuid'),
                OrdersEntity.user_id.label('user_id'),
                OrdersEntity.saler_id.label('saler_id'),
                OrdersEntity.employee_id.label('employee_id'),
                OrdersEntity.total_price.label('total_price'),
                OrdersEntity.payment_type.label('payment_type'),
                OrdersEntity.shipping_type.label('shipping_type'),
                OrdersEntity.shipping_fee.label('shipping_fee'),
                OrdersEntity.status.label('status'),
                OrdersEntity.date.label('date'),
                OrdersEntity.cancel_employee_id.label('cancel_employee_id'),
                UsersEntity.name.label('user_name'),
                UsersEntity.mobile_phone.label('mobile_phone')
            ).filter(
                OrdersEntity.user_id == int(user_id),
                OrdersEntity.cancel_employee_id == None,
                OrdersEntity.user_id == UsersEntity.id
            ).order_by(OrdersEntity.date.asc()).all()

            employeesEntity = self.session.query(
                EmployeesEntity
            ).filter().all()

            emplyee_id_list = []
            emplyee_name_list = []
            if employeesEntity != []:
                for i in range(len(employeesEntity)):
                    emplyee_id_list.append(employeesEntity[i].id)
                    emplyee_name_list.append(employeesEntity[i].name)

                if ordersEntity != []:
                    for i in range(len(ordersEntity)):
                        if ordersEntity[i].id is not None:
                            order_items_list = []
                            orderItemsEntity = self.session.query(OrderItemsEntity).filter(
                                OrderItemsEntity.order_id == ordersEntity[i].id
                            ).all()
                            for j in range(len(orderItemsEntity)):
                                recOrderItems = {
                                    'order_items_id': orderItemsEntity[j].id if orderItemsEntity[j].id is not None else "",
                                    'order_id': orderItemsEntity[j].order_id if orderItemsEntity[j].order_id is not None else "",
                                    'sku_id': orderItemsEntity[j].sku_id if orderItemsEntity[j].sku_id is not None else "",
                                    'price': orderItemsEntity[j].price if orderItemsEntity[j].price is not None else "",
                                    'number': orderItemsEntity[j].number if orderItemsEntity[j].number is not None else "",
                                    'quantity': orderItemsEntity[j].quantity if orderItemsEntity[j].quantity is not None else "",
                                    'total_amount': orderItemsEntity[j].total_amount if orderItemsEntity[j].total_amount is not None else "",
                                    'status': orderItemsEntity[j].status if orderItemsEntity[j].status is not None else "",
                                    'date': orderItemsEntity[j].date.__str__() if orderItemsEntity[j].date.__str__() is not None else ""
                                }
                                order_items_list.append(recOrderItems)
                        else:
                            order_items_list = []

                        employee1_id = emplyee_id_list.index(ordersEntity[i].saler_id)
                        employee2_id = emplyee_id_list.index(ordersEntity[i].employee_id)
                        if ordersEntity[i].cancel_employee_id is not None:
                            cancel_employee_id = emplyee_id_list.index(ordersEntity[i].cancel_employee_id)
                            cancel_employee_name = emplyee_name_list[cancel_employee_id]
                        else:
                            cancel_employee_name = ""

                        if ordersEntity[i].payment_type == OrdersPaymentTypeStatus.cash:
                            payment_type = '現金'
                        elif ordersEntity[i].payment_type == OrdersPaymentTypeStatus.points:
                            payment_type = '點數'
                        elif ordersEntity[i].payment_type == OrdersPaymentTypeStatus.creditcard:
                            payment_type = '信用卡'
                        elif ordersEntity[i].payment_type == OrdersPaymentTypeStatus.creditloan:
                            payment_type = '信貸'
                        else:
                            payment_type = ''

                        if ordersEntity[i].shipping_type == OrdersShippingTypeStatus.self:
                            shipping_type = '自取'
                        elif ordersEntity[i].shipping_type == OrdersShippingTypeStatus.homedelivery:
                            shipping_type = '宅配'
                        elif ordersEntity[i].shipping_type == OrdersShippingTypeStatus.conveniencestore:
                            shipping_type = '超商取貨'
                        else:
                            shipping_type = ''

                        if ordersEntity[i].status == OrdersTypeStatus.undone:
                            order_status = '未完成'
                        elif ordersEntity[i].status == OrdersTypeStatus.overdue:
                            order_status = '已逾期'
                        elif ordersEntity[i].status == OrdersTypeStatus.completed:
                            order_status = '已完成'
                        elif ordersEntity[i].status == OrdersTypeStatus.deleted:
                            order_status = '已註銷'
                        else:
                            order_status = ''

                        if ordersEntity[i].date.strftime("%Y-%m-%d") is not None:
                            date = ordersEntity[i].date.strftime("%Y-%m-%d")
                        else:
                            date = ""

                        rec = {
                            'id': ordersEntity[i].id if ordersEntity[i].id is not None else "",
                            'order_num': 'D' + ordersEntity[i].id.__str__().rjust(8, '0') if ordersEntity[i].id is not None else "",
                            'user_id': ordersEntity[i].user_id if ordersEntity[i].user_id is not None else "",
                            'user_name': ordersEntity[i].user_name if ordersEntity[i].user_name is not None else "",
                            'user_mobile_phone': ordersEntity[i].mobile_phone if ordersEntity[i].mobile_phone is not None else "",
                            'saler_id': ordersEntity[i].saler_id if ordersEntity[i].saler_id is not None else "",
                            'saler_name': emplyee_name_list[employee1_id] if emplyee_name_list[employee1_id] is not None else "",
                            'employee_id': ordersEntity[i].employee_id if ordersEntity[i].employee_id is not None else "",
                            'employee_name': emplyee_name_list[employee2_id] if emplyee_name_list[employee2_id] is not None else "",
                            'total_price': ordersEntity[i].total_price if ordersEntity[i].total_price is not None else "",
                            'payment_type': ordersEntity[i].payment_type if ordersEntity[i].payment_type is not None else "",
                            'payment_type_str': payment_type if payment_type is not None else "",
                            'shipping_type': ordersEntity[i].shipping_type if ordersEntity[i].shipping_type is not None else "",
                            'shipping_type_str': shipping_type if shipping_type is not None else "",
                            'shipping_fee': ordersEntity[i].shipping_fee if ordersEntity[i].shipping_fee is not None else "",
                            'status': ordersEntity[i].status if ordersEntity[i].status is not None else "",
                            'status_str': order_status if order_status is not None else "",
                            'date': date if date is not None else "",
                            'cancel': OrdersCancelTypeStatus.nocancel if ordersEntity[i].cancel_employee_id is None else OrdersCancelTypeStatus.canceled,
                            'cancel_employee_id': ordersEntity[i].cancel_employee_id if ordersEntity[i].cancel_employee_id is not None else "",
                            'cancel_employee_name': cancel_employee_name,
                            'order_items_list': order_items_list,
                            'href': '<a class="btn btn-primary btn-xs" target="_blank" href="/redirect/' + ordersEntity[i].uuid + '" >詳情</a><a class="btn btn-primary btn-xs" onclick="editOrder($(this),\''+ordersEntity[i].uuid+'\')">編輯</a>'
                        }
                        orderList.append(rec)
                else:
                    orderList = []
                return {"data": orderList}
            else:
                raise EmployeeListEmptyException()
        except (
                EmployeeListEmptyException,
                Exception,
                RuntimeError
        ) as e:
            raise e
        finally:
            self.session.close()
            self.connection.close()

    def getOrderItemsByUserID(self, user_id):
        productOrderItemsList = []
        productNotPickupList = []
        productNotSentList = []
        serviceOrderItemsList = []
        try:
            self.session = super(OrderInstance, self).get_session()

            ordersEntity = self.session.query(
                OrdersEntity.shipping_type.label('shipping_type'),
                OrderItemsEntity.id.label('order_items_id'),
                OrderItemsEntity.sku_id.label('sku_id'),
                OrderItemsEntity.total_amount.label('total_amount'),
                OrderItemsEntity.used.label('used'),
                OrderItemsEntity.exp_date.label('exp_date'),
                SkusEntity.product_id.label('product_id'),
                SkusEntity.number.label('skus_number'),
                ProductsEntity.category_id.label('category_id'),
                ProductsEntity.name.label('product_name')
            ).filter(
                OrdersEntity.user_id == int(user_id),
                OrdersEntity.cancel_employee_id == None,
                OrdersEntity.status != OrdersTypeStatus.completed,
                OrderItemsEntity.status != OrdersItemTypeStatus.completed,
                OrderItemsEntity.total_amount != OrderItemsEntity.used,
                OrderItemsEntity.order_id == OrdersEntity.id,
                OrderItemsEntity.sku_id == SkusEntity.id,
                SkusEntity.product_id == ProductsEntity.id
            ).all()

            if ordersEntity != []:
                for i in range(len(ordersEntity)):
                    servicepart_list = []
                    if type(ordersEntity[i].total_amount) is int and type(ordersEntity[i].used) is int:
                        quota = int(ordersEntity[i].total_amount) - int(ordersEntity[i].used)
                    else:
                        quota = 0

                    # product: for receive goods
                    if ordersEntity[i].category_id == ProductCategoriesStatus.product:
                        event_list = [""]
                        # Not picked up
                        if ordersEntity[i].shipping_type == OrdersShippingTypeStatus.self:
                            rec_product = {
                                'order_items_id': ordersEntity[i].order_items_id if ordersEntity[i].order_items_id is not None else "",
                                'order_item_status': '未取貨',
                                'sku_id': ordersEntity[i].sku_id if ordersEntity[i].sku_id is not None else "",
                                'skus_number': ordersEntity[i].skus_number if ordersEntity[i].skus_number is not None else "",
                                'product_id': ordersEntity[i].product_id if ordersEntity[i].product_id is not None else "",
                                'product_name': ordersEntity[i].product_name if ordersEntity[i].product_name is not None else "",
                                'quota': quota,
                                'servicepart_list': servicepart_list,
                                'event_list': event_list
                            }
                            productNotPickupList.append(rec_product)
                        # Not sent
                        elif ordersEntity[i].shipping_type == OrdersShippingTypeStatus.homedelivery:
                            rec_product = {
                                'order_items_id': ordersEntity[i].order_items_id if ordersEntity[i].order_items_id is not None else "",
                                'order_item_status': '未寄送',
                                'sku_id': ordersEntity[i].sku_id if ordersEntity[i].sku_id is not None else "",
                                'skus_number': ordersEntity[i].skus_number if ordersEntity[i].skus_number is not None else "",
                                'product_id': ordersEntity[i].product_id if ordersEntity[i].product_id is not None else "",
                                'product_name': ordersEntity[i].product_name if ordersEntity[i].product_name is not None else "",
                                'quota': quota,
                                'servicepart_list': servicepart_list,
                                'event_list': event_list
                            }
                            productNotSentList.append(rec_product)
                    # assemble
                    # service
                    if ordersEntity[i].category_id >= ProductCategoriesStatus.service_starting_value:
                        event_list = [""]
                        # get service part
                        servicePartsEntity = self.session.query(ServicePartsEntity).filter(
                            ServicePartsEntity.product_id == int(ordersEntity[i].product_id)
                        ).all()
                        if servicePartsEntity != []:
                            for j in range(len(servicePartsEntity)):
                                rec_serviceparts = {
                                    'employee_grade': servicePartsEntity[j].employee_grade,
                                    'number': servicePartsEntity[j].number
                                }
                                servicepart_list.append(rec_serviceparts)

                        # get evnet employee
                        eventsEntity = self.session.query(EventsEntity).filter(
                            EventsEntity.order_item_id == int(ordersEntity[i].order_items_id)
                        ).all()
                        if eventsEntity != []:
                            for j in range(len(eventsEntity)):
                                event_list = []
                                event_employee_list = []
                                eventEmployeeEntity = self.session.query(
                                    EventEmployeeEntity.id.label('event_employee_id'),
                                    EventEmployeeEntity.event_id.label('event_id'),
                                    EventEmployeeEntity.grade_id.label('grade_id'),
                                    EventEmployeeEntity.employee_id.label('employee_id')
                                ).filter(
                                    EventEmployeeEntity.event_id == int(eventsEntity[j].id)
                                ).all()
                                if eventEmployeeEntity != []:
                                    for k in range(len(eventEmployeeEntity)):
                                        rec_eventemployee = {
                                            'event_employee_id': eventEmployeeEntity[k].event_employee_id,
                                            'event_id': eventEmployeeEntity[k].event_id,
                                            'grade_id': eventEmployeeEntity[k].grade_id,
                                            'employee_id': eventEmployeeEntity[k].employee_id
                                        }
                                        event_employee_list.append(rec_eventemployee)
                                else:
                                    rec_eventemployee = {
                                        'event_employee_id': "",
                                        'event_id': "",
                                        'grade_id': "",
                                        'employee_id': ""
                                    }
                                    event_employee_list.append(rec_eventemployee)
                                event_list.append(event_employee_list)

                        # check order items exp_date
                        if ordersEntity[i].exp_date is not None:
                            exp_date_with_time = datetime(ordersEntity[i].exp_date.year, ordersEntity[i].exp_date.month, ordersEntity[i].exp_date.day)
                            if exp_date_with_time >= datetime.now():
                                order_item_status = "activity"
                            elif exp_date_with_time < datetime.now():
                                order_item_status = "overdue"
                            else:
                                order_item_status = ""
                        else:
                            order_item_status = ""

                        rec_service = {
                            'order_items_id': ordersEntity[i].order_items_id if ordersEntity[i].order_items_id is not None else "",
                            'order_item_status': order_item_status,
                            'sku_id': ordersEntity[i].sku_id if ordersEntity[i].sku_id is not None else "",
                            'skus_number': ordersEntity[i].skus_number if ordersEntity[i].skus_number is not None else "",
                            'product_id': ordersEntity[i].product_id if ordersEntity[i].product_id is not None else "",
                            'product_name': ordersEntity[i].product_name if ordersEntity[i].product_name is not None else "",
                            'quota': quota,
                            'servicepart_list': servicepart_list,
                            'event_list': event_list
                        }
                        serviceOrderItemsList.append(rec_service)
                productOrderItemsList.append({'productNotPickupList': productNotPickupList})
                productOrderItemsList.append({'productNotSentList': productNotSentList})
            return {
                'productOrderItemsList': productOrderItemsList,
                'serviceOrderItemsList': serviceOrderItemsList
            }
        except (
                Exception,
                RuntimeError
        ) as e:
            raise e
        finally:
            self.session.close()
            self.connection.close()

    def getOrderItemsByUserIDWithoutFilter(self, user_id):
        productOrderItemsList = []
        productNotPickupList = []
        productNotSentList = []
        serviceOrderItemsList = []
        try:
            self.session = super(OrderInstance, self).get_session()

            ordersEntity = self.session.query(
                OrdersEntity.shipping_type.label('shipping_type'),
                OrdersEntity.status.label('orders_status'),
                OrderItemsEntity.status.label('order_items_status'),
                OrderItemsEntity.id.label('order_items_id'),
                OrderItemsEntity.sku_id.label('sku_id'),
                OrderItemsEntity.total_amount.label('total_amount'),
                OrderItemsEntity.used.label('used'),
                OrderItemsEntity.exp_date.label('exp_date'),
                SkusEntity.product_id.label('product_id'),
                SkusEntity.number.label('skus_number'),
                ProductsEntity.category_id.label('category_id'),
                ProductsEntity.name.label('product_name')
            ).filter(
                OrdersEntity.user_id == int(user_id),
                OrdersEntity.cancel_employee_id == None,
                OrderItemsEntity.order_id == OrdersEntity.id,
                OrderItemsEntity.sku_id == SkusEntity.id,
                SkusEntity.product_id == ProductsEntity.id
            ).all()

            if ordersEntity != []:
                for i in range(len(ordersEntity)):
                    servicepart_list = []
                    if type(ordersEntity[i].total_amount) is int and type(ordersEntity[i].used) is int:
                        quota = int(ordersEntity[i].total_amount) - int(ordersEntity[i].used)
                    else:
                        quota = 0

                    # product: for receive goods
                    if ordersEntity[i].category_id == ProductCategoriesStatus.product:
                        event_list = [""]
                        # Not picked up
                        if ordersEntity[i].shipping_type == OrdersShippingTypeStatus.self:
                            if ordersEntity[i].order_items_status == OrdersItemTypeStatus.unfinished:
                                order_item_status = "未取貨"
                            elif ordersEntity[i].order_items_status == OrdersItemTypeStatus.completed:
                                order_item_status = "已取貨"
                            else:
                                order_item_status = ""
                            rec_product = {
                                'order_items_id': ordersEntity[i].order_items_id if ordersEntity[i].order_items_id is not None else "",
                                'order_item_status': order_item_status,
                                'sku_id': ordersEntity[i].sku_id if ordersEntity[i].sku_id is not None else "",
                                'skus_number': ordersEntity[i].skus_number if ordersEntity[i].skus_number is not None else "",
                                'product_id': ordersEntity[i].product_id if ordersEntity[i].product_id is not None else "",
                                'product_name': ordersEntity[i].product_name if ordersEntity[i].product_name is not None else "",
                                'quota': quota,
                                'servicepart_list': servicepart_list,
                                'event_list': event_list
                            }
                            productNotPickupList.append(rec_product)
                        # Not sent
                        elif ordersEntity[i].shipping_type == OrdersShippingTypeStatus.homedelivery:
                            if ordersEntity[i].order_items_status == OrdersItemTypeStatus.unfinished:
                                order_item_status = "未寄送"
                            elif ordersEntity[i].order_items_status == OrdersItemTypeStatus.completed:
                                order_item_status = "已寄送"
                            else:
                                order_item_status = ""
                            rec_product = {
                                'order_items_id': ordersEntity[i].order_items_id if ordersEntity[i].order_items_id is not None else "",
                                'order_item_status': order_item_status,
                                'sku_id': ordersEntity[i].sku_id if ordersEntity[i].sku_id is not None else "",
                                'skus_number': ordersEntity[i].skus_number if ordersEntity[i].skus_number is not None else "",
                                'product_id': ordersEntity[i].product_id if ordersEntity[i].product_id is not None else "",
                                'product_name': ordersEntity[i].product_name if ordersEntity[i].product_name is not None else "",
                                'quota': quota,
                                'servicepart_list': servicepart_list,
                                'event_list': event_list
                            }
                            productNotSentList.append(rec_product)

                    # assemble
                    # (no assemble)

                    # service
                    if ordersEntity[i].category_id >= ProductCategoriesStatus.service_starting_value:
                        event_list = [""]
                        # get service part
                        servicePartsEntity = self.session.query(ServicePartsEntity).filter(
                            ServicePartsEntity.product_id == int(ordersEntity[i].product_id)
                        ).all()
                        if servicePartsEntity != []:
                            for j in range(len(servicePartsEntity)):
                                rec_serviceparts = {
                                    'employee_grade': servicePartsEntity[j].employee_grade,
                                    'number': servicePartsEntity[j].number
                                }
                                servicepart_list.append(rec_serviceparts)

                        # get evnet employee
                        eventsEntity = self.session.query(EventsEntity).filter(
                            EventsEntity.order_item_id == int(ordersEntity[i].order_items_id)
                        ).all()
                        if eventsEntity != []:
                            for j in range(len(eventsEntity)):
                                event_list = []
                                event_employee_list = []
                                eventEmployeeEntity = self.session.query(
                                    EventEmployeeEntity.id.label('event_employee_id'),
                                    EventEmployeeEntity.event_id.label('event_id'),
                                    EventEmployeeEntity.grade_id.label('grade_id'),
                                    EventEmployeeEntity.employee_id.label('employee_id')
                                ).filter(
                                    EventEmployeeEntity.event_id == int(eventsEntity[j].id)
                                ).all()
                                if eventEmployeeEntity != []:
                                    for k in range(len(eventEmployeeEntity)):
                                        rec_eventemployee = {
                                            'event_employee_id': eventEmployeeEntity[k].event_employee_id,
                                            'event_id': eventEmployeeEntity[k].event_id,
                                            'grade_id': eventEmployeeEntity[k].grade_id,
                                            'employee_id': eventEmployeeEntity[k].employee_id
                                        }
                                        event_employee_list.append(rec_eventemployee)
                                else:
                                    rec_eventemployee = {
                                        'event_employee_id': "",
                                        'event_id': "",
                                        'grade_id': "",
                                        'employee_id': ""
                                    }
                                    event_employee_list.append(rec_eventemployee)
                                event_list.append(event_employee_list)

                        # check order items exp_date
                        if ordersEntity[i].exp_date is not None:
                            exp_date_with_time = datetime(ordersEntity[i].exp_date.year, ordersEntity[i].exp_date.month, ordersEntity[i].exp_date.day)
                            if exp_date_with_time >= datetime.now():
                                order_item_status = "activity"
                            elif exp_date_with_time < datetime.now():
                                order_item_status = "overdue"
                            else:
                                order_item_status = ""
                        else:
                            order_item_status = ""

                        # if event_list == []:
                        #     event_list

                        rec_service = {
                            'order_items_id': ordersEntity[i].order_items_id if ordersEntity[i].order_items_id is not None else "",
                            'order_item_status': order_item_status,
                            'sku_id': ordersEntity[i].sku_id if ordersEntity[i].sku_id is not None else "",
                            'skus_number': ordersEntity[i].skus_number if ordersEntity[i].skus_number is not None else "",
                            'product_id': ordersEntity[i].product_id if ordersEntity[i].product_id is not None else "",
                            'product_name': ordersEntity[i].product_name if ordersEntity[i].product_name is not None else "",
                            'quota': quota,
                            'servicepart_list': servicepart_list,
                            'event_list': event_list
                        }
                        serviceOrderItemsList.append(rec_service)
                productOrderItemsList.append({'productNotPickupList': productNotPickupList})
                productOrderItemsList.append({'productNotSentList': productNotSentList})
            return {
                'productOrderItemsList': productOrderItemsList,
                'serviceOrderItemsList': serviceOrderItemsList
            }
        except (
                Exception,
                RuntimeError
        ) as e:
            raise e
        finally:
            self.session.close()
            self.connection.close()

    # check recipient input when shipping_type is not self, and check shipping_fee is 0 when shipping_type is self
    def check_recipient(self, shipping_type, recipient, recipient_phone_number, recipient_deliver_address, shipping_fee):
        try:
            self.session = super(OrderInstance, self).get_session()

            if shipping_type != OrdersShippingTypeStatus.self:
                if recipient is None or recipient_phone_number is None or recipient_deliver_address is None or \
                                recipient == '' or recipient_phone_number == '' or recipient_deliver_address == '' or \
                                recipient == ' ' or recipient_phone_number == ' ' or recipient_deliver_address == ' ':
                    raise MissingRequiredParametersException()
                else:
                    return shipping_fee
            else:
                shipping_fee = 0
                return shipping_fee
        except (
                MissingRequiredParametersException,
                Exception,
                RuntimeError
        ) as e:
            raise e
        finally:
            self.session.close()
            self.connection.close()

    # check creditcard_note when payment_type is creditcard (OrdersPaymentTypeStatus.creditcard -> 3)
    def check_creditcard_note(self, payment_type, creditcard_note):
        try:
            self.session = super(OrderInstance, self).get_session()

            if payment_type == OrdersPaymentTypeStatus.creditcard:
                if creditcard_note is None or creditcard_note == '' or creditcard_note == ' ':
                    raise CreditcardNoteWrongFormatException()
                else:
                    return creditcard_note
            else:
                return creditcard_note
        except (
            CreditcardNoteWrongFormatException,
            MissingRequiredParametersException,
            Exception,
            RuntimeError
        ) as e:
            raise e
        finally:
            self.session.close()
            self.connection.close()

    # check skus_list duplication
    def check_skus_list_duplication(self, skus_list):
        try:
            self.session = super(OrderInstance, self).get_session()

            sku_id_list = []

            for i in range(len(skus_list)):
                sku_id_list.append(skus_list[i]['sku_id'])

            # sku_id_list duplicate judgment
            if len(sku_id_list) != len(set(sku_id_list)):
                raise WrongFormatException()
            else:
                return sku_id_list
        except (
            WrongFormatException,
            Exception,
            RuntimeError
        ) as e:
            raise e
        finally:
            self.session.close()
            self.connection.close()

    # check skus_list category 7(Experience ticket)
    def checkCategory7Unused(self, user_id, sku_id_list):
        try:
            self.session = super(OrderInstance, self).get_session()

            orderItemsEntity = self.session.query(
                OrderItemsEntity.id.label('order_item_id'),
                OrderItemsEntity.sku_id.label('sku_id'),
                OrdersEntity.id.label('order_id'),
                ProductsEntity.category_id.label('category_id')
            ).filter(
                OrderItemsEntity.order_id == OrdersEntity.id,
                OrderItemsEntity.sku_id == SkusEntity.id,
                SkusEntity.product_id == ProductsEntity.id,
                OrderItemsEntity.sku_id.in_(sku_id_list),
                OrdersEntity.user_id == int(user_id),
                OrdersEntity.status != OrdersTypeStatus.deleted,
                ProductsEntity.category_id == ProductCategoriesStatus.experience_ticket
            ).all()

            if orderItemsEntity == []:
                return 1
            else:
                raise ThisExperienceTicketAlreadyUsedException()
        except (
            ThisExperienceTicketAlreadyUsedException,
            Exception,
            RuntimeError
        ) as e:
            raise e
        finally:
            self.session.close()
            self.connection.close()

    # processing skus_list
    def processing_skus_list(self, skus_list, user_discount, shipping_setting, shipping_fee):
        inventory_deduction_list = []
        try:
            self.session = super(OrderInstance, self).get_session()

            # judgment and processing skus_list
            sku_id_list = []
            quantity_list = []
            immediate_list = []
            assemble_name_list = []
            product_price_list = []

            total_price = 0

            # processing skus_list one by one
            for i in range(len(skus_list)):
                if skus_list[i]['sku_id'] and skus_list[i]['quantity']:

                    # check sku_id is assemble or not
                    checkAssembleSkus = self.session.query(
                        ProductsEntity.name.label('product_name'),
                        ProductsEntity.category_id.label('category_id'),
                        SkusEntity.price.label('price'),
                        SkusEntity.number.label('skus_number'),
                        ProductsEntity.discount.label('discount')
                    ).filter(
                        SkusEntity.id == int(skus_list[i]['sku_id']),
                        SkusEntity.product_id == ProductsEntity.id
                    ).first()

                    # sku_id exist
                    if checkAssembleSkus is not None:


                        # single product amount
                        total_amount = int(skus_list[i]['quantity']) * int(checkAssembleSkus.skus_number)
                        # discount
                        if (checkAssembleSkus.discount):
                            total_price = total_price + int(math.ceil((checkAssembleSkus.price) * float(user_discount))) * int(skus_list[i]['quantity'])
                        # no discount
                        else:
                            total_price = total_price + int(math.ceil((checkAssembleSkus.price) * int(skus_list[i]['quantity'])))


                        # sku_id is assemble class
                        if checkAssembleSkus.category_id == ProductCategoriesStatus.assemble:

                            childAssembleSkus = self.session.query(
                                AssembleEntity.sku_id.label('child_sku_id')
                            ).filter(
                                AssembleEntity.parent_sku_id == int(skus_list[i]['sku_id']),
                                AssembleEntity.delete_time == None
                            ).all()

                            if childAssembleSkus != []:
                                # correct all assemble sku
                                for j in range(len(childAssembleSkus)):
                                    sku_id_list.append(childAssembleSkus[j].child_sku_id)
                                    quantity_list.append(skus_list[i]['quantity'])
                                    immediate_list.append(skus_list[i]['immediate'])
                                    assemble_name_list.append(str(checkAssembleSkus.product_name))
                                    product_price_list.append(checkAssembleSkus.price)
                            else:
                                raise InvaildSkuInAssembleException()

                        # sku_id is not assemble class
                        else:
                            sku_id_list.append(skus_list[i]['sku_id'])
                            quantity_list.append(skus_list[i]['quantity'])
                            immediate_list.append(skus_list[i]['immediate'])
                            assemble_name_list.append("")
                            product_price_list.append(checkAssembleSkus.price)

                    # sku_id does not exist
                    else:
                        raise TargetIdDoesNotExistException()
                else:
                    raise WrongFormatException()

            skus_list = []
            for i in range(len(sku_id_list)):

                skusEntity = self.session.query(
                    SkusEntity.id.label('skus_id'),
                    SkusEntity.product_id.label('product_id'),
                    SkusEntity.number.label('skus_number'),
                    SkusEntity.price.label('skus_price'),
                    SkusEntity.expire.label('skus_expire'),
                    SkusEntity.product_serial_number.label('skus_product_serial_number'),
                    SkusEntity.only_for_assemable.label('skus_only_for_assemable'),
                    SkusEntity.status.label('skus_status'),
                    ProductsEntity.category_id.label('category_id'),
                    ProductsEntity.name.label('product_name'),
                    ProductsEntity.discount.label('discount')
                ).filter(
                    SkusEntity.id == sku_id_list[i],
                    SkusEntity.product_id == ProductsEntity.id,
                ).first()

                # inventory check (only product category)
                if skusEntity.category_id == ProductCategoriesStatus.product:
                    suppliesEntity = self.session.query(SuppliesEntity).filter(
                        SuppliesEntity.product_id == int(skusEntity.product_id)
                    ).scalar()
                    if suppliesEntity is not None:
                        sum = self.session.query(func.sum(SuppliesInventoryEntity.quantity)).filter(
                            SuppliesInventoryEntity.supplies_id == suppliesEntity.id
                        ).scalar()
                        if sum is not None:
                            num_inventory = sum
                        else:
                            num_inventory = 0
                        # inventory number check
                        if num_inventory >= total_amount:
                            if int(immediate_list[i]) != 0:
                                take_num = int(immediate_list[i]) * int(skusEntity.skus_number)
                                inventory_deduction_list.append({
                                    'supplies_id': suppliesEntity.id,
                                    'quantity': take_num,
                                    'type': InventoryTypeStatus.counter
                                })
                            else:
                                without_immediate = int(immediate_list[i])
                        else:
                            raise InsufficientInventoryException()
                    else:
                        raise InsufficientInventoryException()
                rec = {
                    'skus_id': skusEntity.skus_id if skusEntity.skus_id is not None else "",
                    'product_id': skusEntity.product_id if skusEntity.product_id is not None else "",
                    'skus_number': skusEntity.skus_number if skusEntity.skus_number is not None else "",
                    'skus_price': product_price_list[i] if product_price_list[i] is not None else "",
                    'skus_expire': skusEntity.skus_expire if skusEntity.skus_expire is not None else "",
                    'skus_product_serial_number': skusEntity.skus_product_serial_number if skusEntity.skus_product_serial_number is not None else "",
                    'skus_only_for_assemable': skusEntity.skus_only_for_assemable if skusEntity.skus_only_for_assemable is not None else "",
                    'skus_status': skusEntity.skus_status if skusEntity.skus_status is not None else "",
                    'category_id': skusEntity.category_id if skusEntity.category_id is not None else "",
                    'product_name': skusEntity.product_name if skusEntity.product_name is not None else "",
                    'quantity': int(quantity_list[i]),
                    'immediate': int(immediate_list[i]),
                    'assemble_name': str(assemble_name_list[i]),
                    'total_amount': total_amount
                }
                skus_list.append(rec)

            if int(total_price) >= int(shipping_setting):
                total_price = total_price
            else:
                total_price = total_price + int(shipping_fee)
            return {
                'skus_list': skus_list,
                'total_price': total_price,
                'inventory_deduction_list': inventory_deduction_list
            }
        except (
                InvaildSkuInAssembleException,
                WrongFormatException,
                InsufficientInventoryException,
                Exception,
                RuntimeError
        ) as e:
            raise e
        finally:
            self.session.close()
            self.connection.close()

    # processing sku_id
    def processing_sku_id_single(self, sku_info):
        inventory_deduction_list = []
        try:
            self.session = super(OrderInstance, self).get_session()

            # inventory check (only product category)
            suppliesEntity = self.session.query(SuppliesEntity).filter(
                SuppliesEntity.product_id == int(sku_info['product_id'])
            ).scalar()

            if suppliesEntity is not None:
                sum = self.session.query(func.sum(SuppliesInventoryEntity.quantity)).filter(
                    SuppliesInventoryEntity.supplies_id == suppliesEntity.id
                ).scalar()
                if sum is not None:
                    num_inventory = sum
                else:
                    num_inventory = 0
                # inventory number check
                total_amount = int(sku_info['skus_number'])*int(sku_info['quantity'])
                if num_inventory >= total_amount:
                    if int(sku_info['immediate']) != 0:
                        take_num = int(sku_info['immediate']) * int(sku_info['skus_number'])
                        inventory_deduction_list.append({
                            'supplies_id': suppliesEntity.id,
                            'quantity': take_num,
                            'type': InventoryTypeStatus.counter
                        })
                    else:
                        without_immediate = int(sku_info['immediate'])
                else:
                    raise InsufficientInventoryException()
            else:
                raise InsufficientInventoryException()

            return inventory_deduction_list
        except (
                InsufficientInventoryException,
                Exception,
                RuntimeError
        ) as e:
            raise e
        finally:
            self.session.close()
            self.connection.close()

    # processing sku_id
    def get_inventory_return_by_sku_id_single(self, sku_info, used):
        inventory_return_list = []
        try:
            self.session = super(OrderInstance, self).get_session()

            # inventory check (only product category)
            suppliesEntity = self.session.query(SuppliesEntity).filter(
                SuppliesEntity.product_id == int(sku_info['product_id'])
            ).scalar()

            if suppliesEntity is not None:
                inventory_return_list.append({
                    'supplies_id': suppliesEntity.id,
                    'quantity': used,
                    'type': InventoryTypeStatus.counter
                })
            else:
                raise TargetIdDoesNotExistException()

            return inventory_return_list
        except (
                TargetIdDoesNotExistException,
                Exception,
                RuntimeError
        ) as e:
            raise e
        finally:
            self.session.close()
            self.connection.close()

    # check customer's quota
    def check_quota(self, payment_type, user_uuid, total_price):
        try:
            self.session = super(OrderInstance, self).get_session()

            # only check point payment type
            if payment_type == OrdersPaymentTypeStatus.points:
                user = self.session.query(UsersEntity).filter(
                    UsersEntity.uuid == user_uuid
                ).scalar()

                if user is not None:
                    data = MemberInstance().getUserData(user.uuid)
                    user_point = data.point
                    # pass
                    if total_price <= user_point:
                        return user.uuid
                    # no pass
                    else:
                        raise InsufficientBalanceException()
                else:
                    raise TargetIdDoesNotExistException()
            else:
                return 1
        except (
                InsufficientBalanceException,
                TargetIdDoesNotExistException,
                Exception,
                RuntimeError
        ) as e:
            raise e
        finally:
            self.session.close()
            self.connection.close()

    # new order create
    def create_order(self, input_personnel, user_uuid, employee_uuid1, employee_uuid2, payment_type, shipping_type, shipping_fee, order_status,
                     total_price, recipient, recipient_phone_number, recipient_deliver_address, shipping_date, creditcard_note):
        try:
            self.session = super(OrderInstance, self).get_session()

            now = datetime.now()

            newOrders = OrdersEntity()
            newOrders.store_id = 1  # multiple tenant
            newOrders.user_id = user_uuid
            newOrders.saler_id = employee_uuid1
            newOrders.employee_id = employee_uuid2
            newOrders.total_price = total_price
            newOrders.payment_type = payment_type
            newOrders.shipping_type = shipping_type
            newOrders.shipping_fee = shipping_fee
            newOrders.status = order_status
            newOrders.date = now
            newOrders.input_personnel = input_personnel
            newOrders.product_serial_number = str(uuid.uuid4())
            newOrders.recipient = recipient
            newOrders.recipient_phone_number = recipient_phone_number
            newOrders.recipient_deliver_address = recipient_deliver_address
            newOrders.shipping_date = shipping_date
            newOrders.modify_date = now
            newOrders.sales_reocrd_date = now
            newOrders.creditcard_note = creditcard_note
            self.session.add(newOrders)
            self.session.commit()
            order_id_str = str(newOrders.id)
            newOrders.order_number = 'D' + order_id_str.rjust(8, '0')
            self.session.commit()
            order_id = newOrders.id
            order_uuid = newOrders.product_serial_number

            order_info = {
                'order_id': order_id,
                'order_uuid': order_uuid
            }
            return order_info
        except (
                Exception,
                RuntimeError
        ) as e:
            raise e
        finally:
            self.session.close()
            self.connection.close()

    def update_order(self, input_personnel, order_id, recipient, recipient_phone_number, recipient_deliver_address, shipping_date):
        try:
            self.session = super(OrderInstance, self).get_session()

            ordersEntity = self.session.query(OrdersEntity).filter(
                OrdersEntity.id == int(order_id)
            ).scalar()

            if ordersEntity is not None:
                ordersEntity.recipient = recipient
                ordersEntity.recipient_phone_number = recipient_phone_number
                ordersEntity.recipient_deliver_address = recipient_deliver_address
                ordersEntity.shipping_date = shipping_date
                ordersEntity.input_personnel = input_personnel
                self.session.commit()
                return order_id
            else:
                raise TargetIdDoesNotExistException()
        except (
            WrongFormatException,
            TargetIdDoesNotExistException,
            Exception,
            RuntimeError
        ) as e:
            raise e
        finally:
            self.session.close()
            self.connection.close()

    # def update_order_total_price(self, input_personnel, order_id, total_price):
    #     try:
    #         self.session = super(OrderInstance, self).get_session()
    #
    #         ordersEntity = self.session.query(OrdersEntity).filter(
    #             OrdersEntity.id == int(order_id)
    #         ).scalar()
    #
    #         if ordersEntity is not None:
    #             ordersEntity.total_price = total_price
    #             ordersEntity.input_personnel = input_personnel
    #             self.session.commit()
    #             return order_id
    #         else:
    #             raise TargetIdDoesNotExistException()
    #     except (
    #         WrongFormatException,
    #         TargetIdDoesNotExistException,
    #         Exception,
    #         RuntimeError
    #     ) as e:
    #         raise e
    #     finally:
    #         self.session.close()
    #         self.connection.close()

    # new item create
    def create_item(self, skus, order_id):
        num_completed_item = 0
        num_unfinished_item = 0
        try:
            self.session = super(OrderInstance, self).get_session()

            for i in range(len(skus)):

                if skus[i]['category_id'] == ProductCategoriesStatus.product:
                    if int(skus[i]['immediate']) != 0:
                        status = OrdersItemTypeStatus.completed
                        used = skus[i]['total_amount']
                        num_completed_item = num_completed_item + 1
                    else:
                        status = OrdersItemTypeStatus.unfinished
                        num_unfinished_item = num_unfinished_item + 1
                        used = 0
                else:
                    status = OrdersItemTypeStatus.unfinished
                    num_unfinished_item = num_unfinished_item + 1
                    used = 0

                exp_datetime = datetime.now() + monthdelta(int(skus[i]['skus_expire'])) + timedelta(days=1)
                exp_date = exp_datetime.strftime("%Y-%m-%d")

                newOrderItems = OrderItemsEntity()
                newOrderItems.order_id = order_id
                newOrderItems.sku_id = skus[i]['skus_id']
                newOrderItems.price = skus[i]['skus_price']
                newOrderItems.number = skus[i]['skus_number']
                newOrderItems.quantity = skus[i]['quantity']
                newOrderItems.total_amount = skus[i]['total_amount']
                newOrderItems.status = status
                newOrderItems.date = datetime.now()
                newOrderItems.used = used
                newOrderItems.exp_date = exp_date
                newOrderItems.name = skus[i]['assemble_name']
                self.session.add(newOrderItems)
                self.session.commit()
            if num_completed_item == len(skus):
                order_status = OrdersTypeStatus.completed
            else:
                order_status = OrdersTypeStatus.undone
            return order_status
        except (
                TargetIdDoesNotExistException,
                WrongFormatException,
                Exception,
                RuntimeError
        ) as e:
            raise e
        finally:
            self.session.close()
            self.connection.close()

    def update_order_item(self, input_personnel, order_item_id, used, exp_date, note, category_id):
        try:
            self.session = super(OrderInstance, self).get_session()

            orderItemsEntity = self.session.query(OrderItemsEntity).filter(
                OrderItemsEntity.id == int(order_item_id)
            ).scalar()

            if orderItemsEntity is not None:
                # product pickup
                if category_id == ProductCategoriesStatus.product:

                    old_used = orderItemsEntity.used
                    new_used = used
                    used_diff = int(new_used) - int(old_used)

                    if orderItemsEntity.total_amount == used:
                        orderItemsEntity.status = OrdersItemTypeStatus.completed
                    elif orderItemsEntity.total_amount >= used:
                        orderItemsEntity.status = OrdersItemTypeStatus.unfinished
                    else:
                        raise WrongFormatException()

                    if used_diff >= 0:
                        orderItemsEntity.used = used
                        orderItemsEntity.note = note
                        orderItemsEntity.input_personnel = input_personnel
                        self.session.commit()
                        return order_item_id, used_diff
                    else:
                        raise WrongFormatException()
                # service exp_date extension
                else:
                    used_diff = 0
                    orderItemsEntity.exp_date = exp_date
                    orderItemsEntity.note = note
                    orderItemsEntity.input_personnel = input_personnel
                    self.session.commit()
                    return order_item_id, used_diff
            else:
                raise TargetIdDoesNotExistException()
        except (
            WrongFormatException,
            TargetIdDoesNotExistException,
            Exception,
            RuntimeError
        ) as e:
            raise e
        finally:
            self.session.close()
            self.connection.close()

    def change_order_item(self, input_personnel, sku_info, order_item_info, user_discount):
        try:
            self.session = super(OrderInstance, self).get_session()

            orderItemsEntity = self.session.query(OrderItemsEntity).filter(
                OrderItemsEntity.id == int(order_item_info['order_item_id'])
            ).scalar()

            if orderItemsEntity is not None:
                if sku_info['category_id'] == ProductCategoriesStatus.product:
                    if int(sku_info['immediate']) != 0:
                        status = OrdersItemTypeStatus.completed
                        used = int(sku_info['immediate'])*int(sku_info['skus_number'])
                    else:
                        status = OrdersItemTypeStatus.unfinished
                        used = 0
                else:
                    status = OrdersItemTypeStatus.unfinished
                    used = 0

                exp_datetime = datetime.now() + monthdelta(int(sku_info['expire'])) + timedelta(days=1)
                exp_date = exp_datetime.strftime("%Y-%m-%d")

                if (order_item_info['order_item_discount']):
                    old_price = int(math.ceil(order_item_info['order_item_price'] * float(user_discount))) * int(sku_info['quantity'])
                else:
                    old_price = int(order_item_info['order_item_price']) * int(sku_info['quantity'])

                if (sku_info['discount']):
                    new_price = int(math.ceil(sku_info["skus_price"] * float(user_discount))) * int(sku_info['quantity'])
                else:
                    new_price = int(sku_info["skus_price"]) * int(sku_info['quantity'])

                price_diff = new_price - old_price

                orderItemsEntity.order_id = order_item_info['order_id']
                orderItemsEntity.sku_id = sku_info['skus_id']
                orderItemsEntity.price = sku_info['skus_price']
                orderItemsEntity.number = sku_info['skus_number']
                orderItemsEntity.quantity = sku_info['quantity']
                orderItemsEntity.total_amount = int(sku_info['skus_number'])*int(sku_info['quantity'])
                orderItemsEntity.status = status
                orderItemsEntity.date = datetime.now()
                orderItemsEntity.used = used
                orderItemsEntity.exp_date = exp_date
                orderItemsEntity.name = "換貨"
                orderItemsEntity.input_personnel = input_personnel
                orderItemsEntity.price_diff = price_diff
                self.session.commit()

                ordersEntity = self.session.query(OrdersEntity).filter(
                    OrdersEntity.id == int(order_item_info['order_id'])
                ).scalar()

                if ordersEntity is not None:
                    ordersEntity.modify_date = datetime.now()

                return order_item_info['order_item_id']
            else:
                raise TargetIdDoesNotExistException()
        except (
            WrongFormatException,
            TargetIdDoesNotExistException,
            Exception,
            RuntimeError
        ) as e:
            raise e
        finally:
            self.session.close()
            self.connection.close()

    # update order status
    def update_order_status(self, order_id, order_status):
        try:
            self.session = super(OrderInstance, self).get_session()

            ordersEntity = self.session.query(OrdersEntity).filter(
                OrdersEntity.id == int(order_id)
            ).scalar()

            if ordersEntity is not None:
                ordersEntity.status = order_status
                self.session.commit()
            return order_id
        except (
                Exception,
                RuntimeError
        ) as e:
            raise e
        finally:
            self.session.close()
            self.connection.close()

    def getOrderCancel(self):
        orderList = []
        try:
            self.session = super(OrderInstance, self).get_session()
            ordersEntity = self.session.query(
                OrdersEntity.id.label('id'),
                OrdersEntity.product_serial_number.label('uuid'),
                OrdersEntity.order_number.label('order_number'),
                OrdersEntity.user_id.label('user_id'),
                OrdersEntity.saler_id.label('saler_id'),
                OrdersEntity.employee_id.label('employee_id'),
                OrdersEntity.total_price.label('total_price'),
                OrdersEntity.payment_type.label('payment_type'),
                OrdersEntity.shipping_type.label('shipping_type'),
                OrdersEntity.shipping_fee.label('shipping_fee'),
                OrdersEntity.status.label('status'),
                OrdersEntity.date.label('date'),
                OrdersEntity.cancel_employee_id.label('cancel_employee_id'),
                OrdersEntity.refund.label('refund'),
                UsersEntity.name.label('user_name'),
                UsersEntity.mobile_phone.label('mobile_phone')
            ).filter(
                OrdersEntity.user_id == UsersEntity.id,
                OrdersEntity.cancel_employee_id != None
            ).order_by(OrdersEntity.date.desc()).all()

            employeesEntity = self.session.query(
                EmployeesEntity
            ).filter().all()

            emplyee_id_list = []
            emplyee_name_list = []
            if employeesEntity != []:
                for i in range(len(employeesEntity)):
                    emplyee_id_list.append(employeesEntity[i].id)
                    emplyee_name_list.append(employeesEntity[i].name)
                if ordersEntity != []:
                    count = len(ordersEntity)
                    for i in range(len(ordersEntity)):
                        employee1_id = emplyee_id_list.index(ordersEntity[i].saler_id)
                        employee2_id = emplyee_id_list.index(ordersEntity[i].employee_id)
                        if ordersEntity[i].cancel_employee_id is not None:
                            cancel_employee_id = emplyee_id_list.index(ordersEntity[i].cancel_employee_id)
                            cancel_employee_name = emplyee_name_list[cancel_employee_id]
                        else:
                            cancel_employee_name = ""

                        if ordersEntity[i].payment_type == OrdersPaymentTypeStatus.cash:
                            payment_type = '現金'
                        elif ordersEntity[i].payment_type == OrdersPaymentTypeStatus.points:
                            payment_type = '點數'
                        elif ordersEntity[i].payment_type == OrdersPaymentTypeStatus.creditcard:
                            payment_type = '信用卡'
                        elif ordersEntity[i].payment_type == OrdersPaymentTypeStatus.creditloan:
                            payment_type = '信貸'
                        else:
                            payment_type = ''

                        if ordersEntity[i].shipping_type == OrdersShippingTypeStatus.self:
                            shipping_type = '自取'
                        elif ordersEntity[i].shipping_type == OrdersShippingTypeStatus.homedelivery:
                            shipping_type = '宅配'
                        elif ordersEntity[i].shipping_type == OrdersShippingTypeStatus.conveniencestore:
                            shipping_type = '超商取貨'
                        else:
                            shipping_type = ''

                        if ordersEntity[i].status == OrdersTypeStatus.undone:
                            order_status = '未完成'
                        elif ordersEntity[i].status == OrdersTypeStatus.overdue:
                            order_status = '已逾期'
                        elif ordersEntity[i].status == OrdersTypeStatus.completed:
                            order_status = '已完成'
                        elif ordersEntity[i].status == OrdersTypeStatus.deleted:
                            order_status = '已註銷'
                        else:
                            order_status = ''

                        if ordersEntity[i].refund == OrdersRefundStatus.refund:
                            order_refund_status = OrdersRefundStatus.refund
                            order_refund_status_str = '已歸還退款'
                        elif ordersEntity[i].refund == OrdersRefundStatus.norefund:
                            order_refund_status = OrdersRefundStatus.norefund
                            order_refund_status_str = '未歸還退款'
                        else:
                            order_refund_status = ''
                            order_refund_status_str = ''

                        rec = {
                            'id': ordersEntity[i].id if ordersEntity[i].id is not None else "",
                            'order_num': ordersEntity[i].order_number if ordersEntity[i].order_number is not None else "",
                            'user_id': ordersEntity[i].user_id if ordersEntity[i].user_id is not None else "",
                            'user_name': ordersEntity[i].user_name if ordersEntity[i].user_name is not None else "",
                            'user_mobile_phone': ordersEntity[i].mobile_phone if ordersEntity[i].mobile_phone is not None else "",
                            'saler_id': ordersEntity[i].saler_id if ordersEntity[i].saler_id is not None else "",
                            'saler_name': emplyee_name_list[employee1_id] if emplyee_name_list[employee1_id] is not None else "",
                            'employee_id': ordersEntity[i].employee_id if ordersEntity[i].employee_id is not None else "",
                            'employee_name': emplyee_name_list[employee2_id] if emplyee_name_list[employee2_id] is not None else "",
                            'total_price': ordersEntity[i].total_price if ordersEntity[i].total_price is not None else "",
                            'payment_type': ordersEntity[i].payment_type if ordersEntity[i].payment_type is not None else "",
                            'payment_type_str': payment_type if payment_type is not None else "",
                            'shipping_type': ordersEntity[i].shipping_type if ordersEntity[i].shipping_type is not None else "",
                            'shipping_type_str': shipping_type if shipping_type is not None else "",
                            'shipping_fee': ordersEntity[i].shipping_fee if ordersEntity[i].shipping_fee is not None else "",
                            'status': ordersEntity[i].status if ordersEntity[i].status is not None else "",
                            'status_str': order_status if order_status is not None else "",
                            'date': ordersEntity[i].date.__str__() if ordersEntity[i].date.__str__() is not None else "",
                            'cancel': OrdersCancelTypeStatus.nocancel if ordersEntity[i].cancel_employee_id is None else OrdersCancelTypeStatus.canceled,
                            'cancel_employee_id': ordersEntity[i].cancel_employee_id if ordersEntity[i].cancel_employee_id is not None else "",
                            'cancel_employee_name': cancel_employee_name if cancel_employee_name is not None else "",
                            'href': '<a class="btn btn-primary btn-xs" target="_blank" href="/redirect/' + ordersEntity[i].uuid + '" >詳情</a><a class="btn btn-primary btn-xs" onclick="editOrder($(this),\''+ordersEntity[i].uuid+'\')">編輯</a>',
                            'refund': order_refund_status if order_refund_status is not None else "",
                            'refund_str': order_refund_status_str if order_refund_status_str is not None else ""
                        }
                        orderList.append(rec)
                else:
                    orderList = []
                    count = 0
                return {'draw': 1, 'data': orderList, 'recordsTotal': count}
            else:
                raise EmployeeListEmptyException()
        except (
                EmployeeListEmptyException,
                Exception,
                RuntimeError
        ) as e:
            raise e
        finally:
            self.session.close()
            self.connection.close()

    # cancel order
    def cancel_order(self, input_personnel, order_id):
        try:
            self.session = super(OrderInstance, self).get_session()

            ordersEntity = self.session.query(OrdersEntity).filter(
                OrdersEntity.id == order_id
            ).scalar()

            if ordersEntity is not None:
                now = datetime.now()
                order_id = ordersEntity.id
                ordersEntity.status = OrdersTypeStatus.deleted
                ordersEntity.cancel_employee_id = input_personnel
                ordersEntity.modify_date = now
                ordersEntity.sales_reocrd_date = now
                self.session.commit()

                return order_id
            else:
                raise TargetIdDoesNotExistException()
        except (
                TargetIdDoesNotExistException,
                PermissionDeniedException,
                Exception,
                RuntimeError
        ) as e:
            raise e
        finally:
            self.session.close()
            self.connection.close()

    def modify_order_refund(self, order_id, refund):
        try:
            self.session = super(OrderInstance, self).get_session()

            ordersEntity = self.session.query(OrdersEntity).filter(
                OrdersEntity.id == int(order_id),
                OrdersEntity.cancel_employee_id != None
            ).scalar()
            if ordersEntity is not None:
                ordersEntity.refund = refund
                self.session.commit()
                return order_id
            else:
                raise TargetIdDoesNotExistException()
        except (
            TargetIdDoesNotExistException,
            Exception,
            RuntimeError
        ) as e:
            raise e
        finally:
            self.session.close()
            self.connection.close()

    # processing event_employee_list
    def processing_event_employee_list(self, order_item_id, event_employee_list):
        event_employee_insert_list = []
        try:
            self.session = super(OrderInstance, self).get_session()

            # judgment and processing event_employee_list
            orderItemsEntity = self.session.query(
                OrderItemsEntity.status.label('orderItems_status'),
                ProductsEntity.category_id.label('category_id'),
                ProductsEntity.id.label('products_id'),
                SkusEntity.id.label('skus_id')
            ).filter(
                OrderItemsEntity.id == int(order_item_id),
                OrderItemsEntity.status == OrdersItemTypeStatus.unfinished,
                OrderItemsEntity.sku_id == SkusEntity.id,
                SkusEntity.product_id == ProductsEntity.id
            ).first()

            if orderItemsEntity is not None:

                checkServicePartsEntity = self.session.query(ServicePartsEntity).filter(
                    ServicePartsEntity.product_id == orderItemsEntity.products_id
                ).all()

                if checkServicePartsEntity != []:

                    serviceparts_gradeid_list = []
                    serviceparts_number_list = []

                    for i in range(len(checkServicePartsEntity)):
                        serviceparts_gradeid_list.append(checkServicePartsEntity[i].employee_grade)
                        serviceparts_number_list.append(checkServicePartsEntity[i].number)

                    if orderItemsEntity.category_id != ProductCategoriesStatus.product and orderItemsEntity.category_id != ProductCategoriesStatus.assemble:
                        employee_grade_list = []
                        employee_id_list = []
                        for i in range(len(event_employee_list)):
                            if event_employee_list[i]['employee_grade'] and event_employee_list[i]['employee_id']:
                                employee_grade_list.append(event_employee_list[i]['employee_grade'])
                                employee_id_list.append(event_employee_list[i]['employee_id'])
                            else:
                                raise WrongFormatException()

                        # employee_grade_list duplicate judgment
                        if len(employee_grade_list) != len(set(employee_grade_list)):
                            raise WrongFormatException()

                        for i in range(len(employee_grade_list)):
                            list_index = serviceparts_gradeid_list.index(employee_grade_list[i])
                            # check match service parts number
                            temp1 = len(employee_id_list[i])
                            temp2 = serviceparts_number_list[list_index]
                            if len(employee_id_list[i]) == serviceparts_number_list[list_index]:
                                for j in range(len(employee_id_list[i])):
                                    rec = {
                                        'grade_id': employee_grade_list[i],
                                        'employee_id': employee_id_list[i][j]
                                    }
                                    event_employee_insert_list.append(rec)
                            else:
                                raise ServicePartUnmatchException()
                        return event_employee_insert_list
                    else:
                        raise TargetIdDoesNotExistException()
                else:
                    raise TargetIdDoesNotExistException()
            else:
                raise TargetIdDoesNotExistException()
        except (
                ServicePartUnmatchException,
                WrongFormatException,
                TargetIdDoesNotExistException,
                Exception,
                RuntimeError
        ) as e:
            raise e
        finally:
            self.session.close()
            self.connection.close()

    # processing event_employee_list update
    def processing_event_employee_list_update(self, order_item_id, event_employee_list):
        event_employee_update_list = []
        try:
            self.session = super(OrderInstance, self).get_session()

            # judgment and processing event_employee_list
            orderItemsEntity = self.session.query(
                OrderItemsEntity.status.label('orderItems_status'),
                ProductsEntity.category_id.label('category_id'),
                ProductsEntity.id.label('products_id'),
                SkusEntity.id.label('skus_id')
            ).filter(
                OrderItemsEntity.id == int(order_item_id),
                OrderItemsEntity.sku_id == SkusEntity.id,
                SkusEntity.product_id == ProductsEntity.id
            ).first()

            if orderItemsEntity is not None:

                checkServicePartsEntity = self.session.query(ServicePartsEntity).filter(
                    ServicePartsEntity.product_id == orderItemsEntity.products_id
                ).all()

                if checkServicePartsEntity != []:

                    serviceparts_gradeid_list = []
                    serviceparts_number_list = []

                    for i in range(len(checkServicePartsEntity)):
                        serviceparts_gradeid_list.append(checkServicePartsEntity[i].employee_grade)
                        serviceparts_number_list.append(checkServicePartsEntity[i].number)

                    if orderItemsEntity.category_id != ProductCategoriesStatus.product and orderItemsEntity.category_id != ProductCategoriesStatus.assemble:
                        employee_grade_list = []
                        employee_id_list = []
                        event_employee_id_list = []
                        for i in range(len(event_employee_list)):
                            if event_employee_list[i]['employee_grade'] and event_employee_list[i]['employee_id'] and event_employee_list[i]['event_employee_id']:
                                employee_grade_list.append(event_employee_list[i]['employee_grade'])
                                employee_id_list.append(event_employee_list[i]['employee_id'])
                                event_employee_id_list.append(event_employee_list[i]['event_employee_id'])
                            else:
                                raise WrongFormatException()

                        # employee_grade_list duplicate judgment
                        if len(employee_grade_list) != len(set(employee_grade_list)):
                            raise WrongFormatException()

                        for i in range(len(employee_grade_list)):
                            list_index = serviceparts_gradeid_list.index(employee_grade_list[i])
                            # check match service parts number
                            if len(employee_id_list[i]) == serviceparts_number_list[list_index]:
                                for j in range(len(employee_id_list[i])):
                                    rec = {
                                        'grade_id': employee_grade_list[i],
                                        'employee_id': employee_id_list[i][j],
                                        'event_employee_id': event_employee_id_list[i][j]
                                    }
                                    event_employee_update_list.append(rec)
                            else:
                                raise ServicePartUnmatchException()
                        return event_employee_update_list
                    else:
                        raise TargetIdDoesNotExistException()
                else:
                    raise TargetIdDoesNotExistException()
            else:
                raise TargetIdDoesNotExistException()
        except (
                ServicePartUnmatchException,
                WrongFormatException,
                TargetIdDoesNotExistException,
                Exception,
                RuntimeError
        ) as e:
            raise e
        finally:
            self.session.close()
            self.connection.close()

    # check customer's order item quota
    def check_quota_order_item(self, user_id, order_item_id):
        try:
            self.session = super(OrderInstance, self).get_session()

            orderItemsEntity = self.session.query(
                OrderItemsEntity
            ).filter(
                OrderItemsEntity.id == int(order_item_id),
                OrdersEntity.user_id == int(user_id),
                OrdersEntity.id == OrderItemsEntity.order_id
            ).scalar()

            now_date = datetime.now().date()

            if orderItemsEntity is not None:
                if orderItemsEntity.total_amount > orderItemsEntity.used:
                    if orderItemsEntity.exp_date >= now_date:
                        return 1
                    else:
                        raise ItemExpiredException()
                elif orderItemsEntity.total_amount == orderItemsEntity.used:
                    orderItemsEntity.status = OrdersItemTypeStatus.completed
                    self.session.commit()
                    raise InsufficientBalanceException()
                elif orderItemsEntity.total_amount < orderItemsEntity.used:
                    raise InsufficientBalanceException()
            # no pass
            else:
                raise InsufficientBalanceException()
        except (
            ItemExpiredException,
            InsufficientBalanceException,
            Exception,
            RuntimeError
        ) as e:
            raise e
        finally:
            self.session.close()
            self.connection.close()

    # used = used + 1
    def add_order_item_used(self, user_id, order_item_id):
        try:
            self.session = super(OrderInstance, self).get_session()

            orderItemsEntity = self.session.query(
                OrderItemsEntity
            ).filter(
                OrderItemsEntity.id == int(order_item_id),
                OrdersEntity.user_id == int(user_id),
                OrdersEntity.id == OrderItemsEntity.order_id,
            ).scalar()

            if orderItemsEntity is not None:
                orderItemsEntity.used = orderItemsEntity.used + 1
                if orderItemsEntity.total_amount == orderItemsEntity.used:
                    orderItemsEntity.status = OrdersItemTypeStatus.completed
                self.session.commit()
                return 1
            else:
                raise TargetIdDoesNotExistException()
        except (
            TargetIdDoesNotExistException,
            Exception,
            RuntimeError
        ) as e:
            raise e
        finally:
            self.session.close()
            self.connection.close()

    def getEventByEventID(self, event_id):
        try:
            self.session = super(OrderInstance, self).get_session()

            eventsEntity = self.session.query(
                EventsEntity.id.label('event_id'),
                EventsEntity.order_item_id.label('order_item_id'),
                EventsEntity.user_id.label('user_id'),
                EventsEntity.product_name.label('product_name'),
                EventsEntity.start_time.label('start_time'),
                EventsEntity.end_time.label('end_time'),
                EventsEntity.note.label('note'),
                EventsEntity.input_personnel.label('input_personnel'),
                UsersEntity.user_number.label('user_number'),
                UsersEntity.name.label('user_name'),
                UsersEntity.mobile_phone.label('mobile_phone'),
                EmployeesEntity.employee_number.label('employee_number')
            ).filter(
                EventsEntity.id == int(event_id),
                EventEmployeeEntity.event_id == EventsEntity.id,
                EventsEntity.user_id == UsersEntity.id,
                EventsEntity.input_personnel == EmployeesEntity.id
            ).first()

            if eventsEntity is not None:
                eventEmployeeList = []

                eventEmployeeEntity = self.session.query(
                    EventEmployeeEntity.id.label('event_employee_id'),
                    EventEmployeeEntity.grade_id.label('grade_id'),
                    EventEmployeeEntity.employee_id.label('employee_id'),
                    EmployeesEntity.employee_number.label('employee_number'),
                    EmployeesEntity.name.label('employee_name'),
                    EmployeeGradesEntity.name.label('employee_grades_name')
                ).filter(
                    EventEmployeeEntity.event_id == int(event_id),
                    EventEmployeeEntity.employee_id == EmployeesEntity.id,
                    EventEmployeeEntity.grade_id == EmployeeGradesEntity.id
                ).all()

                if eventEmployeeEntity != []:
                    for i in range(len(eventEmployeeEntity)):
                        rec = {
                            'event_employee_id': eventEmployeeEntity[i].event_employee_id if eventEmployeeEntity[i].event_employee_id is not None else "",
                            'grade_id': eventEmployeeEntity[i].grade_id if eventEmployeeEntity[i].grade_id is not None else "",
                            'employee_id': eventEmployeeEntity[i].employee_id if eventEmployeeEntity[i].employee_id is not None else "",
                            'employee_number': eventEmployeeEntity[i].employee_number if eventEmployeeEntity[i].employee_number is not None else "",
                            'employee_name': eventEmployeeEntity[i].employee_name if eventEmployeeEntity[i].employee_name is not None else "",
                            'employee_grades_name': eventEmployeeEntity[i].employee_grades_name if eventEmployeeEntity[i].employee_grades_name is not None else ""
                        }
                        eventEmployeeList.append(rec)
                else:
                    eventEmployeeList = []

                resposne = {
                    'event_id': eventsEntity.event_id if eventsEntity.event_id is not None else "",
                    'order_item_id': eventsEntity.order_item_id if eventsEntity.order_item_id is not None else "",
                    'user_id': eventsEntity.user_id if eventsEntity.user_id is not None else "",
                    'user_number': eventsEntity.user_number if eventsEntity.user_number is not None else "",
                    'user_name': eventsEntity.user_name if eventsEntity.user_name is not None else "",
                    'mobile_phone': eventsEntity.mobile_phone if eventsEntity.mobile_phone is not None else "",
                    'product_name': eventsEntity.product_name if eventsEntity.product_name is not None else "",
                    'start_time': eventsEntity.start_time.__str__() if eventsEntity.start_time.__str__() is not None else "",
                    'end_time': eventsEntity.end_time.__str__() if eventsEntity.end_time.__str__() is not None else "",
                    'note': eventsEntity.note if eventsEntity.note is not None else "",
                    'input_personnel': eventsEntity.input_personnel if eventsEntity.input_personnel is not None else "",
                    'employee_number': eventsEntity.employee_number if eventsEntity.employee_number is not None else "",
                    'eventEmployeeList': eventEmployeeList if eventEmployeeList != [] else []
                }
                return resposne
            else:
                raise TargetIdDoesNotExistException()
        except (
                TargetIdDoesNotExistException,
                Exception,
                RuntimeError
        ) as e:
            raise e
        finally:
            self.session.close()
            self.connection.close()

    def getEventByUserID(self, user_id):
        eventsList = []
        try:
            self.session = super(OrderInstance, self).get_session()

            eventsEntity = self.session.query(
                EventsEntity.id.label('event_id'),
                EventsEntity.store_id.label('store_id'),
                EventsEntity.order_item_id.label('order_item_id'),
                EventsEntity.user_id.label('user_id'),
                EventsEntity.product_name.label('product_name'),
                EventsEntity.start_time.label('start_time'),
                EventsEntity.end_time.label('end_time'),
                EventsEntity.note.label('note'),
                EventsEntity.input_personnel.label('input_personnel'),
                EventsEntity.status.label('status'),
                EventsEntity.registered.label('registered'),
                UsersEntity.user_number.label('user_number'),
                UsersEntity.name.label('user_name'),
                EmployeesEntity.id.label('employees_id'),
                EmployeesEntity.employee_number.label('employee_number'),
                EmployeesEntity.name.label('employees_name'),
                OrderItemsEntity.id.label('orderItems_id'),
                OrderItemsEntity.name.label('OrderItems_name'),
                OrderItemsEntity.sku_id.label('sku_id'),
                SkusEntity.number.label('skus_quantity'),
                ProductsEntity.name.label('products_name')
            ).filter(
                EventsEntity.user_id == int(user_id),
                EventsEntity.status != EventStatus.deleted,
                EventsEntity.user_id == UsersEntity.id,
                EventsEntity.input_personnel == EmployeesEntity.id,
                EventsEntity.order_item_id == OrderItemsEntity.id,
                OrderItemsEntity.sku_id == SkusEntity.id,
                SkusEntity.product_id == ProductsEntity.id
            ).all()

            if eventsEntity != []:
                for i in range(len(eventsEntity)):
                    event_employee_id_list = []

                    eventEmployeeEntity = self.session.query(
                        EventEmployeeEntity.id.label('event_employee_id'),
                        EventEmployeeEntity.event_id.label('event_id'),
                        EventEmployeeEntity.grade_id.label('grade_id'),
                        EventEmployeeEntity.employee_id.label('employee_id'),
                        EmployeesEntity.employee_number.label('employee_number'),
                        EmployeesEntity.name.label('employee_name')
                    ).filter(
                        EventEmployeeEntity.event_id == int(eventsEntity[i].event_id),
                        EventEmployeeEntity.employee_id == EmployeesEntity.id
                    ).all()

                    if eventEmployeeEntity is not None:
                        for j in range(len(eventEmployeeEntity)):
                            event_employee_id_list.append(eventEmployeeEntity[j].employee_id)

                        for j in range(len(eventEmployeeEntity)):

                            if eventsEntity[i].status == EventStatus.deleted:
                                status = '刪除'
                            elif eventsEntity[i].status == EventStatus.activity:
                                status = '未報到'
                            elif eventsEntity[i].status == EventStatus.registered:
                                status = '已報到'
                            elif eventsEntity[i].status == EventStatus.doctor_check:
                                status = '醫師確認'
                            else:
                                status = ''
                            rec = {
                                'event_id': eventsEntity[i].event_id if eventsEntity[i].event_id is not None else "",
                                'store_id': eventsEntity[i].store_id if eventsEntity[i].store_id is not None else "",
                                'order_item_id': eventsEntity[i].order_item_id if eventsEntity[i].order_item_id is not None else "",
                                # 'product_name': eventsEntity[i].product_name if eventsEntity[i].product_name is not None else "",
                                'start': eventsEntity[i].start_time.__str__() if eventsEntity[i].start_time.__str__() is not None else "",
                                'end': eventsEntity[i].end_time.__str__() if eventsEntity[i].end_time.__str__() is not None else "",
                                'note': eventsEntity[i].note if eventsEntity[i].note is not None else "",
                                'input_personnel': eventsEntity[i].input_personnel if eventsEntity[i].input_personnel is not None else "",
                                'status': eventsEntity[i].status if eventsEntity[i].status is not None else "",
                                'status_str': status if status is not None else "",
                                'registered': eventsEntity[i].registered if eventsEntity[i].registered is not None else "",
                                'user_id': eventsEntity[i].user_id if eventsEntity[i].user_id is not None else "",
                                'user_name': eventsEntity[i].user_name if eventsEntity[i].user_name is not None else "",
                                'user_number': eventsEntity[i].user_number if eventsEntity[i].user_number is not None else "",
                                'employees_id': eventsEntity[i].employees_id if eventsEntity[i].employees_id is not None else "",
                                'employee_number': eventsEntity[i].employee_number if eventsEntity[i].employee_number is not None else "",
                                # 'product_show_name': product_show_name if product_show_name is not None else "",
                                'skus_quantity': eventsEntity[i].skus_quantity if eventsEntity[i].skus_quantity is not None else "",
                                'sku_id': eventsEntity[i].sku_id if eventsEntity[i].sku_id is not None else "",
                                'title': eventEmployeeEntity[j].employee_name if eventEmployeeEntity[j].employee_name is not None else "",
                                'title_grade_id': eventEmployeeEntity[j].grade_id if eventEmployeeEntity[j].grade_id is not None else "",
                                'title_employee_number': eventEmployeeEntity[j].employee_number if eventEmployeeEntity[j].employee_number is not None else "",
                                'event_employee_id_list': event_employee_id_list
                            }
                            eventsList.append(rec)
            else:
                eventsList = []
            return {"eventsList": eventsList}
        except (
                Exception,
                RuntimeError
        ) as e:
            raise e
        finally:
            self.session.close()
            self.connection.close()

    def getEventWithRange(self, start, end):
        eventsList = []
        try:
            self.session = super(OrderInstance, self).get_session()

            start_time = datetime.strptime(start, "%Y-%m-%d")
            end_time = datetime.strptime(end, "%Y-%m-%d")

            eventsEntity = self.session.query(
                EventsEntity.id.label('event_id'),
                EventsEntity.store_id.label('store_id'),
                EventsEntity.order_item_id.label('order_item_id'),
                EventsEntity.user_id.label('user_id'),
                EventsEntity.product_name.label('product_name'),
                EventsEntity.start_time.label('start_time'),
                EventsEntity.end_time.label('end_time'),
                EventsEntity.note.label('note'),
                EventsEntity.input_personnel.label('input_personnel'),
                EventsEntity.status.label('status'),
                EventsEntity.registered.label('registered'),
                UsersEntity.user_number.label('user_number'),
                UsersEntity.name.label('user_name'),
                EmployeesEntity.id.label('employees_id'),
                EmployeesEntity.employee_number.label('employee_number'),
                EmployeesEntity.name.label('employees_name'),
                OrderItemsEntity.id.label('orderItems_id'),
                OrderItemsEntity.name.label('OrderItems_name'),
                OrderItemsEntity.sku_id.label('sku_id'),
                SkusEntity.number.label('skus_quantity'),
                ProductsEntity.name.label('products_name')
            ).filter(
                EventsEntity.status != EventStatus.deleted,
                EventsEntity.user_id == UsersEntity.id,
                EventsEntity.input_personnel == EmployeesEntity.id,
                EventsEntity.order_item_id == OrderItemsEntity.id,
                OrderItemsEntity.sku_id == SkusEntity.id,
                SkusEntity.product_id == ProductsEntity.id,
                and_(
                    EventsEntity.start_time >= start_time,
                    EventsEntity.start_time <= end_time
                )
            ).all()

            if eventsEntity != []:
                for i in range(len(eventsEntity)):
                    event_employee_id_list = []
                    # if eventsEntity[i].products_name is not None and eventsEntity[i].skus_quantity is not None:
                    #     product_show_name = str(eventsEntity[i].products_name) + ' * ' + str(eventsEntity[i].skus_quantity)
                    # else:
                    #     product_show_name = ""
                    #
                    # eventEmployee_count = self.session.query(
                    #     EventEmployeeEntity.grade_id.label('grade_id'),
                    #     EventEmployeeEntity.employee_id.label('employee_id'),
                    #     func.count(EventEmployeeEntity.grade_id).label('sum')
                    # ).filter(
                    #     EventEmployeeEntity.event_id == int(eventsEntity[i].event_id)
                    # ).group_by(EventEmployeeEntity.grade_id).all()
                    #
                    # service_part_list = []
                    # if eventEmployee_count != []:
                    #     for j in range(len(eventEmployee_count)):
                    #         service_part_list.append({
                    #             'employee_grade': eventEmployee_count[j].grade_id,
                    #             'number': eventEmployee_count[j].sum
                    #         })
                    # else:
                    #     service_part_list = []

                    eventEmployeeEntity = self.session.query(
                        EventEmployeeEntity.id.label('event_employee_id'),
                        EventEmployeeEntity.event_id.label('event_id'),
                        EventEmployeeEntity.grade_id.label('grade_id'),
                        EventEmployeeEntity.employee_id.label('employee_id'),
                        EmployeesEntity.employee_number.label('employee_number'),
                        EmployeesEntity.name.label('employee_name')
                    ).filter(
                        EventEmployeeEntity.event_id == int(eventsEntity[i].event_id),
                        EventEmployeeEntity.employee_id == EmployeesEntity.id
                    ).all()

                    if eventEmployeeEntity is not None:
                        for j in range(len(eventEmployeeEntity)):
                            event_employee_id_list.append(eventEmployeeEntity[j].employee_id)

                        for j in range(len(eventEmployeeEntity)):

                            if eventsEntity[i].status == EventStatus.deleted:
                                status = '刪除'
                            elif eventsEntity[i].status == EventStatus.activity:
                                status = '未報到'
                            elif eventsEntity[i].status == EventStatus.registered:
                                status = '已報到'
                            elif eventsEntity[i].status == EventStatus.doctor_check:
                                status = '醫師確認'
                            else:
                                status = ''
                            rec = {
                                'event_id': eventsEntity[i].event_id if eventsEntity[i].event_id is not None else "",
                                'store_id': eventsEntity[i].store_id if eventsEntity[i].store_id is not None else "",
                                'order_item_id': eventsEntity[i].order_item_id if eventsEntity[i].order_item_id is not None else "",
                                # 'product_name': eventsEntity[i].product_name if eventsEntity[i].product_name is not None else "",
                                'start': eventsEntity[i].start_time.__str__() if eventsEntity[i].start_time.__str__() is not None else "",
                                'end': eventsEntity[i].end_time.__str__() if eventsEntity[i].end_time.__str__() is not None else "",
                                'note': eventsEntity[i].note if eventsEntity[i].note is not None else "",
                                'input_personnel': eventsEntity[i].input_personnel if eventsEntity[i].input_personnel is not None else "",
                                'status': eventsEntity[i].status if eventsEntity[i].status is not None else "",
                                'status_str': status if status is not None else "",
                                'registered': eventsEntity[i].registered if eventsEntity[i].registered is not None else "",
                                'user_id': eventsEntity[i].user_id if eventsEntity[i].user_id is not None else "",
                                'user_name': eventsEntity[i].user_name if eventsEntity[i].user_name is not None else "",
                                'user_number': eventsEntity[i].user_number if eventsEntity[i].user_number is not None else "",
                                'employees_id': eventsEntity[i].employees_id if eventsEntity[i].employees_id is not None else "",
                                'employee_number': eventsEntity[i].employee_number if eventsEntity[i].employee_number is not None else "",
                                # 'product_show_name': product_show_name if product_show_name is not None else "",
                                'skus_quantity': eventsEntity[i].skus_quantity if eventsEntity[i].skus_quantity is not None else "",
                                'sku_id': eventsEntity[i].sku_id if eventsEntity[i].sku_id is not None else "",
                                'title': eventEmployeeEntity[j].employee_name if eventEmployeeEntity[j].employee_name is not None else "",
                                'title_grade_id': eventEmployeeEntity[j].grade_id if eventEmployeeEntity[j].grade_id is not None else "",
                                'title_employee_number': eventEmployeeEntity[j].employee_number if eventEmployeeEntity[j].employee_number is not None else "",
                                # 'service_part_list': service_part_list,
                                'event_employee_id_list': event_employee_id_list
                            }
                            eventsList.append(rec)
            else:
                eventsList = []
            return {"eventsList": eventsList}
        except (
                Exception,
                RuntimeError
        ) as e:
            raise e
        finally:
            self.session.close()
            self.connection.close()

    def getEvent(self):
        eventsList = []
        try:
            self.session = super(OrderInstance, self).get_session()

            eventsEntity = self.session.query(
                EventsEntity.id.label('event_id'),
                EventsEntity.store_id.label('store_id'),
                EventsEntity.order_item_id.label('order_item_id'),
                EventsEntity.user_id.label('user_id'),
                EventsEntity.product_name.label('product_name'),
                EventsEntity.start_time.label('start_time'),
                EventsEntity.end_time.label('end_time'),
                EventsEntity.note.label('note'),
                EventsEntity.input_personnel.label('input_personnel'),
                UsersEntity.user_number.label('user_number'),
                UsersEntity.name.label('user_name'),
                EmployeesEntity.id.label('employees_id'),
                EmployeesEntity.employee_number.label('employee_number'),
                EmployeesEntity.name.label('employees_name'),
                OrderItemsEntity.id.label('orderItems_id'),
                OrderItemsEntity.name.label('OrderItems_name'),
                OrderItemsEntity.sku_id.label('sku_id'),
                SkusEntity.number.label('skus_quantity'),
                ProductsEntity.name.label('products_name')
            ).filter(
                EventsEntity.status != EventStatus.deleted,
                EventsEntity.user_id == UsersEntity.id,
                EventsEntity.input_personnel == EmployeesEntity.id,
                EventsEntity.order_item_id == OrderItemsEntity.id,
                OrderItemsEntity.sku_id == SkusEntity.id,
                SkusEntity.product_id == ProductsEntity.id
            ).all()

            if eventsEntity != []:
                for i in range(len(eventsEntity)):
                    event_employee_id_list = []
                    # if eventsEntity[i].products_name is not None and eventsEntity[i].skus_quantity is not None:
                    #     product_show_name = str(eventsEntity[i].products_name) + ' * ' + str(eventsEntity[i].skus_quantity)
                    # else:
                    #     product_show_name = ""
                    #
                    # eventEmployee_count = self.session.query(
                    #     EventEmployeeEntity.grade_id.label('grade_id'),
                    #     EventEmployeeEntity.employee_id.label('employee_id'),
                    #     func.count(EventEmployeeEntity.grade_id).label('sum')
                    # ).filter(
                    #     EventEmployeeEntity.event_id == int(eventsEntity[i].event_id)
                    # ).group_by(EventEmployeeEntity.grade_id).all()
                    #
                    # service_part_list = []
                    # if eventEmployee_count != []:
                    #     for j in range(len(eventEmployee_count)):
                    #         service_part_list.append({
                    #             'employee_grade': eventEmployee_count[j].grade_id,
                    #             'number': eventEmployee_count[j].sum
                    #         })
                    # else:
                    #     service_part_list = []

                    eventEmployeeEntity = self.session.query(
                        EventEmployeeEntity.id.label('event_employee_id'),
                        EventEmployeeEntity.event_id.label('event_id'),
                        EventEmployeeEntity.grade_id.label('grade_id'),
                        EventEmployeeEntity.employee_id.label('employee_id'),
                        EmployeesEntity.employee_number.label('employee_number'),
                        EmployeesEntity.name.label('employee_name')
                    ).filter(
                        EventEmployeeEntity.event_id == int(eventsEntity[i].event_id),
                        EventEmployeeEntity.employee_id == EmployeesEntity.id
                    ).all()

                    if eventEmployeeEntity is not None:
                        for j in range(len(eventEmployeeEntity)):
                            event_employee_id_list.append(eventEmployeeEntity[j].employee_id)

                        for j in range(len(eventEmployeeEntity)):
                            rec = {
                                'event_id': eventsEntity[i].event_id if eventsEntity[i].event_id is not None else "",
                                'store_id': eventsEntity[i].store_id if eventsEntity[i].store_id is not None else "",
                                'order_item_id': eventsEntity[i].order_item_id if eventsEntity[i].order_item_id is not None else "",
                                # 'product_name': eventsEntity[i].product_name if eventsEntity[i].product_name is not None else "",
                                'start': eventsEntity[i].start_time.__str__() if eventsEntity[i].start_time.__str__() is not None else "",
                                'end': eventsEntity[i].end_time.__str__() if eventsEntity[i].end_time.__str__() is not None else "",
                                'note': eventsEntity[i].note if eventsEntity[i].note is not None else "",
                                'input_personnel': eventsEntity[i].input_personnel if eventsEntity[i].input_personnel is not None else "",
                                'user_id': eventsEntity[i].user_id if eventsEntity[i].user_id is not None else "",
                                'user_name': eventsEntity[i].user_name if eventsEntity[i].user_name is not None else "",
                                'user_number': eventsEntity[i].user_number if eventsEntity[i].user_number is not None else "",
                                'employees_id': eventsEntity[i].employees_id if eventsEntity[i].employees_id is not None else "",
                                'employee_number': eventsEntity[i].employee_number if eventsEntity[i].employee_number is not None else "",
                                # 'product_show_name': product_show_name if product_show_name is not None else "",
                                'skus_quantity': eventsEntity[i].skus_quantity if eventsEntity[i].skus_quantity is not None else "",
                                'sku_id': eventsEntity[i].sku_id if eventsEntity[i].sku_id is not None else "",
                                'title': eventEmployeeEntity[j].employee_name if eventEmployeeEntity[j].employee_name is not None else "",
                                'title_grade_id': eventEmployeeEntity[j].grade_id if eventEmployeeEntity[j].grade_id is not None else "",
                                'title_employee_number': eventEmployeeEntity[j].employee_number if eventEmployeeEntity[j].employee_number is not None else "",
                                # 'service_part_list': service_part_list,
                                'event_employee_id_list': event_employee_id_list
                            }
                            eventsList.append(rec)
            else:
                eventsList = []
            return {"eventsList": eventsList}
        except (
                Exception,
                RuntimeError
        ) as e:
            raise e
        finally:
            self.session.close()
            self.connection.close()

    # new event create
    def create_event(self, input_personnel, user_id, order_item_id, product_name, start_time, end_time, note):
        try:
            self.session = super(OrderInstance, self).get_session()

            newEvents = EventsEntity()
            newEvents.store_id = 1  # multiple tenant
            newEvents.order_item_id = order_item_id
            newEvents.user_id = user_id
            newEvents.product_name = product_name
            newEvents.start_time = start_time
            newEvents.end_time = end_time
            newEvents.note = note
            newEvents.input_personnel = input_personnel
            newEvents.status = EventStatus.activity
            self.session.add(newEvents)
            self.session.commit()
            reservation_id = newEvents.id

            return reservation_id
        except (
                Exception,
                RuntimeError
        ) as e:
            raise e
        finally:
            self.session.close()
            self.connection.close()

    def update_event(self, input_personnel, event_id, order_item_id, product_name, start_time, end_time, note):
        try:
            self.session = super(OrderInstance, self).get_session()

            eventsEntity = self.session.query(EventsEntity).filter(
                EventsEntity.id == int(event_id)
            ).scalar()

            if eventsEntity is not None:
                # already checked by doctor
                if eventsEntity.status == EventStatus.doctor_check:
                    raise EventAlreadyCheckedByDoctorException()
                # uncheck by doctor
                else:
                    eventsEntity.order_item_id = order_item_id
                    eventsEntity.product_name = product_name
                    eventsEntity.start_time = start_time
                    eventsEntity.end_time = end_time
                    eventsEntity.note = note
                    eventsEntity.input_personnel = input_personnel
                    self.session.commit()
                    return event_id
            else:
                raise TargetIdDoesNotExistException()
        except (
            EventAlreadyCheckedByDoctorException,
            TargetIdDoesNotExistException,
            Exception,
            RuntimeError
        ) as e:
            raise e
        finally:
            self.session.close()
            self.connection.close()

    def delete_all_old_event(self, event_id):
        try:
            self.session = super(OrderInstance, self).get_session()

            eventEmployeeEntity = self.session.query(EventEmployeeEntity).filter(
                EventEmployeeEntity.event_id == int(event_id)
            ).all()

            if eventEmployeeEntity != []:
                for i in range(len(eventEmployeeEntity)):
                    self.session.delete(eventEmployeeEntity[i])
                    self.session.commit()
                return event_id
            else:
                raise TargetIdDoesNotExistException()
        except (
            EventAlreadyCheckedByDoctorException,
            TargetIdDoesNotExistException,
            Exception,
            RuntimeError
        ) as e:
            raise e
        finally:
            self.session.close()
            self.connection.close()

    def doctor_change_service_event(self, input_personnel, event_id, start_time, end_time, note):
        try:
            self.session = super(OrderInstance, self).get_session()

            eventsEntity = self.session.query(EventsEntity).filter(
                EventsEntity.id == int(event_id)
            ).scalar()

            if eventsEntity is not None:
                # already checked by doctor
                if eventsEntity.status == EventStatus.doctor_check:
                    raise EventAlreadyCheckedByDoctorException()
                # uncheck by doctor
                else:
                    eventsEntity.start_time = start_time
                    eventsEntity.end_time = end_time
                    eventsEntity.note = note
                    eventsEntity.input_personnel = input_personnel
                    self.session.commit()
                    return event_id
            else:
                raise TargetIdDoesNotExistException()
        except (
            EventAlreadyCheckedByDoctorException,
            TargetIdDoesNotExistException,
            Exception,
            RuntimeError
        ) as e:
            raise e
        finally:
            self.session.close()
            self.connection.close()

    def registered_event(self, input_personnel, event_id):
        try:
            self.session = super(OrderInstance, self).get_session()

            eventsEntity = self.session.query(EventsEntity).filter(
                EventsEntity.id == int(event_id)
            ).scalar()

            if eventsEntity is not None:
                # already checked by doctor
                if eventsEntity.status == EventStatus.doctor_check:
                    raise EventAlreadyCheckedByDoctorException()
                # uncheck by doctor
                else:
                    eventsEntity.status = EventStatus.registered
                    eventsEntity.registered = input_personnel
                    self.session.commit()
                    return event_id
            else:
                raise TargetIdDoesNotExistException()
        except (
            TargetIdDoesNotExistException,
            Exception,
            RuntimeError
        ) as e:
            raise e
        finally:
            self.session.close()
            self.connection.close()

    def doctor_check_event(self, input_personnel, event_id):
        try:
            self.session = super(OrderInstance, self).get_session()

            eventsEntity = self.session.query(EventsEntity).filter(
                EventsEntity.id == int(event_id)
            ).scalar()

            if eventsEntity is not None:
                # unregistered
                if eventsEntity.status == EventStatus.deleted:
                    raise EventAlreadyCanceledException()
                elif eventsEntity.status == EventStatus.activity:
                    raise EventNotYetRegisterException()
                elif eventsEntity.status == EventStatus.doctor_check:
                    raise EventAlreadyCheckedByDoctorException()
                else:
                    eventsEntity.doctor_check = input_personnel
                    eventsEntity.status = EventStatus.doctor_check
                    self.session.commit()

                    user_id = eventsEntity.user_id
                    order_item_id = eventsEntity.order_item_id
                    return user_id, order_item_id
            else:
                raise TargetIdDoesNotExistException()
        except (
            EventAlreadyCanceledException,
            EventNotYetRegisterException,
            EventAlreadyCheckedByDoctorException,
            TargetIdDoesNotExistException,
            Exception,
            RuntimeError
        ) as e:
            raise e
        finally:
            self.session.close()
            self.connection.close()

    # cancel event
    def cancel_event(self, input_personnel, event_id):
        try:
            self.session = super(OrderInstance, self).get_session()

            eventsEntity = self.session.query(EventsEntity).filter(
                EventsEntity.id == int(event_id),
                EventsEntity.status == EventStatus.activity
            ).scalar()

            if eventsEntity is not None:
                # registered can not deleted
                if eventsEntity.status == EventStatus.registered:
                    raise EventAlreadyRegisteredException()

                else:
                    # return order_item
                    orderItemsEntity = self.session.query(OrderItemsEntity).filter(
                        OrderItemsEntity.id == int(eventsEntity.order_item_id)
                    ).scalar()

                    if orderItemsEntity is not None:
                        if orderItemsEntity.used > 0:
                            orderItemsEntity.used = orderItemsEntity.used - 1
                        else:
                            orderItemsEntity.used = 0
                        orderItemsEntity.status = OrdersItemTypeStatus.unfinished
                        self.session.commit()
                    else:
                        raise TargetIdDoesNotExistException()

                    eventsEntity.status = EventStatus.deleted
                    eventsEntity.cancel_employee_id = input_personnel
                    self.session.commit()

                    return event_id
            else:
                raise TargetIdDoesNotExistException()
        except (
                EventAlreadyCheckedByDoctorException,
                EventAlreadyRegisteredException,
                TargetIdDoesNotExistException,
                PermissionDeniedException,
                Exception,
                RuntimeError
        ) as e:
            raise e
        finally:
            self.session.close()
            self.connection.close()

    # new item create
    def create_event_employee(self, event_employee_insert_list, event_id):
        try:
            self.session = super(OrderInstance, self).get_session()

            for i in range(len(event_employee_insert_list)):
                newEventEmployee = EventEmployeeEntity()
                newEventEmployee.event_id = event_id
                newEventEmployee.grade_id = event_employee_insert_list[i]['grade_id']
                newEventEmployee.employee_id = event_employee_insert_list[i]['employee_id']
                self.session.add(newEventEmployee)
                self.session.commit()
            return 1
        except (
                TargetIdDoesNotExistException,
                WrongFormatException,
                Exception,
                RuntimeError
        ) as e:
            raise e
        finally:
            self.session.close()
            self.connection.close()

    # update event employee
    def update_event_employee(self, event_employee_update_list):
        try:
            self.session = super(OrderInstance, self).get_session()

            for i in range(len(event_employee_update_list)):

                eventEmployeeEntity = self.session.query(
                    EventEmployeeEntity
                ).filter(
                    EventEmployeeEntity.id == int(event_employee_update_list[i]['event_employee_id'])
                ).scalar()

                if eventEmployeeEntity is not None:
                    eventEmployeeEntity.employee_id = event_employee_update_list[i]['employee_id']
                    self.session.commit()
            return 1
        except (
                TargetIdDoesNotExistException,
                WrongFormatException,
                Exception,
                RuntimeError
        ) as e:
            raise e
        finally:
            self.session.close()
            self.connection.close()

    # delete event employee
    def delete_event_employee(self, event_id):
        try:
            self.session = super(OrderInstance, self).get_session()

            eventEmployeeEntity = self.session.query(EventEmployeeEntity).filter(
                EventEmployeeEntity.event_id == int(event_id)
            ).all()

            if eventEmployeeEntity != []:
                for i in range(len(eventEmployeeEntity)):
                    self.session.delete(eventEmployeeEntity[i])
                    self.session.commit()
            return 1
        except (
                TargetIdDoesNotExistException,
                WrongFormatException,
                Exception,
                RuntimeError
        ) as e:
            raise e
        finally:
            self.session.close()
            self.connection.close()

    def getOrderAllDay(self, year, month, day):
        order_all_day_list = []
        income_cash = 0
        income_points = 0
        income_creditcard = 0
        income_creditloan = 0
        income_total = 0
        try:
            self.session = super(OrderInstance, self).get_session()

            ordersEntity = self.session.query(
                OrdersEntity.id.label('id'),
                OrdersEntity.user_id.label('user_id'),
                OrdersEntity.saler_id.label('saler_id'),
                OrdersEntity.employee_id.label('employee_id'),
                OrdersEntity.total_price.label('total_price'),
                OrdersEntity.payment_type.label('payment_type'),
                OrdersEntity.shipping_type.label('shipping_type'),
                OrdersEntity.shipping_fee.label('shipping_fee'),
                OrdersEntity.status.label('status'),
                OrdersEntity.date.label('date'),
                OrdersEntity.input_personnel.label('input_personnel'),
                OrdersEntity.cancel_employee_id.label('cancel_employee_id'),
                OrdersEntity.refund.label('refund'),
                OrdersEntity.modify_date.label('modify_date'),
                UsersEntity.user_number.label('user_number'),
                UsersEntity.name.label('user_name'),
                UsersEntity.mobile_phone.label('mobile_phone')
            ).filter(
                OrdersEntity.user_id == UsersEntity.id,
                extract('year', OrdersEntity.sales_reocrd_date) == int(year),
                extract('month', OrdersEntity.sales_reocrd_date) == int(month),
                extract('day', OrdersEntity.sales_reocrd_date) == int(day)
            ).all()

            employeesEntity = self.session.query(
                EmployeesEntity
            ).filter().all()

            emplyee_id_list = []
            emplyee_name_list = []

            if employeesEntity != []:
                for i in range(len(employeesEntity)):
                    emplyee_id_list.append(employeesEntity[i].id)
                    emplyee_name_list.append(employeesEntity[i].name)

                if ordersEntity != []:
                    for i in range(len(ordersEntity)):
                        order_items_single_list = []
                        order_items_assemble_list = []

                        orderItemsEntity = self.session.query(OrderItemsEntity).filter(
                            OrderItemsEntity.order_id == ordersEntity[i].id
                        ).order_by(OrderItemsEntity.name.asc()).all()

                        for j in range(len(orderItemsEntity)):

                            skusEntity = self.session.query(
                                SkusEntity.id.label('sku_id'),
                                SkusEntity.sku_number.label('sku_number'),
                                ProductsEntity.name.label('products_name'),
                                ProductsEntity.category_id.label('item_category_id')
                            ).filter(
                                SkusEntity.id == int(orderItemsEntity[j].sku_id),
                                ProductsEntity.id == SkusEntity.product_id
                            ).first()

                            if skusEntity.sku_number is not None:
                                sku_number = skusEntity.sku_number
                            else:
                                sku_number = ""

                            if orderItemsEntity[j].total_amount is not None:
                                total_amount = orderItemsEntity[j].total_amount
                            else:
                                total_amount = ""

                            if orderItemsEntity[j].status == OrdersItemTypeStatus.completed:
                                status_str = '完成項目'
                            elif orderItemsEntity[j].status == OrdersItemTypeStatus.unfinished:
                                status_str = '未完成項目'
                            else:
                                status_str = ''

                            if orderItemsEntity[j].used is not None:
                                used = orderItemsEntity[j].used
                            else:
                                used = ""

                            if skusEntity.item_category_id >= 3:
                                exp_date = orderItemsEntity[j].exp_date.__str__()
                            else:
                                exp_date = ""

                            if orderItemsEntity[j].name is not None:
                                assemble_name = orderItemsEntity[j].name
                            else:
                                assemble_name = ""

                            if skusEntity.products_name is not None:
                                products_name = skusEntity.products_name
                            else:
                                products_name = ""

                            if orderItemsEntity[j].note is not None:
                                note = orderItemsEntity[j].note
                            else:
                                note = ""

                            if orderItemsEntity[j].input_personnel is not None:
                                order_item_input_personnel_id = emplyee_id_list.index(orderItemsEntity[j].input_personnel)
                                order_item_input_personnel_name = emplyee_name_list[order_item_input_personnel_id]
                            else:
                                order_item_input_personnel_name = ""

                            recOrderItems = '產品編號:' + str(sku_number) \
                                            + ',項目總數量:' + str(total_amount) \
                                            + ',項目狀態:' + str(status_str) \
                                            + ',項目已取貨數量/已使用次數:' + str(used) \
                                            + ',服務使用期限:' + str(exp_date) \
                                            + ',類別名稱:' + str(assemble_name) \
                                            + ',項目名稱:' + str(products_name) \
                                            + ',附註:' + str(note) \
                                            + ',換貨經手人:' + str(order_item_input_personnel_name) \
                                            + '\n'

                            if orderItemsEntity[j].name == "":
                                # order_items_single_list.append(recOrderItems)
                                order_items_single_list.append(recOrderItems)
                            else:
                                # order_items_assemble_list.append(recOrderItems + '\n')
                                order_items_assemble_list.append(recOrderItems)

                        employee1_id = emplyee_id_list.index(ordersEntity[i].saler_id)
                        employee2_id = emplyee_id_list.index(ordersEntity[i].employee_id)
                        if ordersEntity[i].cancel_employee_id is not None:
                            cancel_employee_id = emplyee_id_list.index(ordersEntity[i].cancel_employee_id)
                            cancel_employee_name = emplyee_name_list[cancel_employee_id]
                        else:
                            cancel_employee_name = ""

                        if ordersEntity[i].input_personnel is not None:
                            order_input_personnel_id = emplyee_id_list.index(ordersEntity[i].input_personnel)
                            order_input_personnel_name = emplyee_name_list[order_input_personnel_id]
                        else:
                            order_input_personnel_name = ""

                        dateTarget = date(year, month, day)

                        if ordersEntity[i].total_price is not None:

                            total_price = ordersEntity[i].total_price

                            total_price_add = total_price

                            if ordersEntity[i].status == OrdersTypeStatus.deleted:
                                order_date = ordersEntity[i].date.date()
                                if order_date == dateTarget:
                                    total_price_add = 0
                                else:
                                    total_price_add = -int(total_price)
                            else:
                                total_price_add = total_price
                        else:
                            total_price_add = 0

                        income_total = income_total + total_price_add

                        if ordersEntity[i].payment_type == OrdersPaymentTypeStatus.cash:
                            payment_type = '現金'
                            income_cash = income_cash + total_price_add
                        elif ordersEntity[i].payment_type == OrdersPaymentTypeStatus.points:
                            payment_type = '點數'
                            income_points = income_points + total_price_add
                        elif ordersEntity[i].payment_type == OrdersPaymentTypeStatus.creditcard:
                            payment_type = '信用卡'
                            income_creditcard = income_creditcard + total_price_add
                        elif ordersEntity[i].payment_type == OrdersPaymentTypeStatus.creditloan:
                            payment_type = '信貸'
                            income_creditloan = income_creditloan + total_price_add
                        else:
                            payment_type = ''

                        if ordersEntity[i].shipping_type == OrdersShippingTypeStatus.self:
                            shipping_type = '自取'
                        elif ordersEntity[i].shipping_type == OrdersShippingTypeStatus.homedelivery:
                            shipping_type = '宅配'
                        elif ordersEntity[i].shipping_type == OrdersShippingTypeStatus.conveniencestore:
                            shipping_type = '超商取貨'
                        else:
                            shipping_type = ''

                        if ordersEntity[i].status == OrdersTypeStatus.undone:
                            order_status = '未完成'
                        elif ordersEntity[i].status == OrdersTypeStatus.overdue:
                            order_status = '已逾期'
                        elif ordersEntity[i].status == OrdersTypeStatus.completed:
                            order_status = '已完成'
                        elif ordersEntity[i].status == OrdersTypeStatus.deleted:
                            order_status = '已註銷'
                        else:
                            order_status = ''

                        if ordersEntity[i].refund == OrdersRefundStatus.refund:
                            order_refund_status = OrdersRefundStatus.refund
                            order_refund_status_str = '已歸還退款'
                        elif ordersEntity[i].refund == OrdersRefundStatus.norefund:
                            order_refund_status = OrdersRefundStatus.norefund
                            order_refund_status_str = '未歸還退款'
                        else:
                            order_refund_status = ''
                            order_refund_status_str = ''

                        rec = {
                            'order_num': 'D' + ordersEntity[i].id.__str__().rjust(8, '0') if ordersEntity[i].id is not None else "",
                            'user_number': ordersEntity[i].user_number if ordersEntity[i].user_number is not None else "",
                            'user_name': ordersEntity[i].user_name if ordersEntity[i].user_name is not None else "",
                            'user_mobile_phone': ordersEntity[i].mobile_phone if ordersEntity[i].mobile_phone is not None else "",
                            'saler_name': emplyee_name_list[employee1_id] if emplyee_name_list[employee1_id] is not None else "",
                            'employee_name': emplyee_name_list[employee2_id] if emplyee_name_list[employee2_id] is not None else "",
                            'total_price': total_price,
                            'payment_type_str': payment_type if payment_type is not None else "",
                            'shipping_type_str': shipping_type if shipping_type is not None else "",
                            'shipping_fee': ordersEntity[i].shipping_fee if ordersEntity[i].shipping_fee is not None else "",
                            'status_str': order_status if order_status is not None else "",
                            'date': ordersEntity[i].date.__str__() if ordersEntity[i].date.__str__() is not None else "",
                            'input_personnel': order_input_personnel_name,
                            'cancel_employee_name': cancel_employee_name,
                            'order_items_single_list': order_items_single_list,
                            'order_items_assemble_list': order_items_assemble_list,
                            'refund_str': order_refund_status_str if order_refund_status_str is not None else "",
                            'modify_date': ordersEntity[i].modify_date.__str__() if ordersEntity[i].modify_date.__str__() is not None else "",

                        }
                        order_all_day_list.append(rec)
                income_all_day = {
                    'income_cash': income_cash,
                    'income_cash_str': '現金訂單總金額',
                    'income_points': income_points,
                    'income_points_str': '點數訂單總金額',
                    'income_creditcard': income_creditcard,
                    'income_creditcard_str': '信用卡訂單總金額',
                    'income_creditloan': income_creditloan,
                    'income_creditloan_str': '信貸訂單總金額',
                    'income_total': income_total,
                    'income_total_str': '本日訂單總金額'
                }
                return order_all_day_list, income_all_day
            else:
                raise EmployeeListEmptyException()
        except (
                TargetIdDoesNotExistException,
                EmployeeListEmptyException,
                Exception,
                RuntimeError
        ) as e:
            raise e
        finally:
            self.session.close()
            self.connection.close()

    def getEventAllDay(self, year, month, day):
        event_all_day_list = []
        try:
            self.session = super(OrderInstance, self).get_session()

            eventsEntity = self.session.query(
                EventsEntity.id.label('id'),
                EventsEntity.order_item_id.label('order_item_id'),
                EventsEntity.user_id.label('user_id'),
                EventsEntity.product_name.label('product_name'),
                EventsEntity.status.label('status'),
                UsersEntity.user_number.label('user_number'),
                UsersEntity.name.label('user_name'),
                UsersEntity.mobile_phone.label('mobile_phone'),
                UserGradesEntity.name.label('user_grade_name')
            ).filter(
                EventsEntity.user_id == UsersEntity.id,
                UsersEntity.grade_id == UserGradesEntity.id,
                extract('year', EventsEntity.start_time) == int(year),
                extract('month', EventsEntity.start_time) == int(month),
                extract('day', EventsEntity.start_time) == int(day)
            ).all()

            employeesEntity = self.session.query(
                EmployeesEntity
            ).filter().all()

            emplyee_id_list = []
            emplyee_name_list = []

            if employeesEntity != []:
                for i in range(len(employeesEntity)):
                    emplyee_id_list.append(employeesEntity[i].id)
                    emplyee_name_list.append(employeesEntity[i].name)

                if eventsEntity != []:
                    for i in range(len(eventsEntity)):
                        doctors = []
                        managers = []
                        medical_artists = []
                        nurses = []

                        eventEmployeeEntity = self.session.query(
                            EventEmployeeEntity.employee_id.label('employee_id'),
                            EventEmployeeEntity.grade_id.label('grade_id')
                        ).filter(
                            EventEmployeeEntity.event_id == eventsEntity[i].id
                        ).all()

                        for j in range(len(eventEmployeeEntity)):

                            if eventEmployeeEntity[j].employee_id is not None:
                                event_employee_id = emplyee_id_list.index(eventEmployeeEntity[j].employee_id)
                                event_employee_name = emplyee_name_list[event_employee_id]
                            else:
                                event_employee_name = ""

                            if eventEmployeeEntity[j].grade_id is not None:
                                grade_id = eventEmployeeEntity[j].grade_id

                                if grade_id == EmployeeGradeStatus.doctor:
                                    doctors.append(str(event_employee_name))
                                elif grade_id == EmployeeGradeStatus.manager:
                                    managers.append(str(event_employee_name))
                                elif grade_id == EmployeeGradeStatus.medical_artist:
                                    medical_artists.append(str(event_employee_name))
                                elif grade_id == EmployeeGradeStatus.nurse:
                                    nurses.append(str(event_employee_name))

                        if eventsEntity[i].status == EventStatus.activity:
                            status_str = '未報到'
                        elif eventsEntity[i].status == EventStatus.registered:
                            status_str = '已報到'
                        elif eventsEntity[i].status == EventStatus.doctor_check:
                            status_str = '醫師確認'
                        elif eventsEntity[i].status == EventStatus.deleted:
                            status_str = '已取消'
                        else:
                            status_str = ''

                        rec = {
                            'user_number': eventsEntity[i].user_number if eventsEntity[i].user_number is not None else "",
                            'user_name': eventsEntity[i].user_name if eventsEntity[i].user_name is not None else "",
                            'user_grade': eventsEntity[i].user_grade_name if eventsEntity[i].user_grade_name is not None else "",
                            'user_mobile_phone': eventsEntity[i].mobile_phone if eventsEntity[i].mobile_phone is not None else "",
                            'evnet_product': eventsEntity[i].product_name if eventsEntity[i].product_name is not None else "",
                            'status': status_str,
                            'doctors': doctors,
                            'managers': managers,
                            'medical_artists': medical_artists,
                            'nurses': nurses
                        }
                        event_all_day_list.append(rec)
                    return event_all_day_list
                else:
                    return event_all_day_list
            else:
                raise EmployeeListEmptyException()
        except (
                TargetIdDoesNotExistException,
                EmployeeListEmptyException,
                Exception,
                RuntimeError
        ) as e:
            raise e
        finally:
            self.session.close()
            self.connection.close()

    def getPointAllDay(self, year, month, day):
        point_all_day_list = []
        try:
            self.session = super(OrderInstance, self).get_session()

            userAccountsEntity = self.session.query(
                UserAccountsEntity.id.label('id'),
                UserAccountsEntity.user_id.label('user_id'),
                UserAccountsEntity.order_id.label('order_id'),
                UserAccountsEntity.employee_id.label('employee_id'),
                UserAccountsEntity.point.label('point'),
                UserAccountsEntity.number.label('number'),
                UserAccountsEntity.date.label('date'),
                UsersEntity.user_number.label('user_number'),
                UsersEntity.name.label('user_name'),
                UsersEntity.mobile_phone.label('mobile_phone'),
                UserGradesEntity.name.label('user_grade_name')
            ).filter(
                UserAccountsEntity.user_id == UsersEntity.id,
                UsersEntity.grade_id == UserGradesEntity.id,
                extract('year', UserAccountsEntity.date) == int(year),
                extract('month', UserAccountsEntity.date) == int(month),
                extract('day', UserAccountsEntity.date) == int(day)
            ).all()

            employeesEntity = self.session.query(
                EmployeesEntity
            ).filter().all()

            emplyee_id_list = []
            emplyee_name_list = []

            if employeesEntity != []:
                for i in range(len(employeesEntity)):
                    emplyee_id_list.append(employeesEntity[i].id)
                    emplyee_name_list.append(employeesEntity[i].name)

                if userAccountsEntity != []:
                    for i in range(len(userAccountsEntity)):
                        if userAccountsEntity[i].employee_id is not None:
                            input_personnel_id = emplyee_id_list.index(userAccountsEntity[i].employee_id)
                            input_personnel_name = emplyee_name_list[input_personnel_id]
                        else:
                            input_personnel_name = ""

                        if userAccountsEntity[i].order_id is not None:
                            point_number = ''
                            order_number = userAccountsEntity[i].number
                        else:
                            point_number = userAccountsEntity[i].number
                            order_number = ''

                        rec = {
                            'user_number': userAccountsEntity[i].user_number if userAccountsEntity[i].user_number is not None else "",
                            'user_name': userAccountsEntity[i].user_name if userAccountsEntity[i].user_name is not None else "",
                            'user_grade': userAccountsEntity[i].user_grade_name if userAccountsEntity[i].user_grade_name is not None else "",
                            'user_mobile_phone': userAccountsEntity[i].mobile_phone if userAccountsEntity[i].mobile_phone is not None else "",
                            'input_personnel': input_personnel_name,
                            'point': userAccountsEntity[i].point if userAccountsEntity[i].point is not None else "",
                            'point_number': point_number,
                            'order_number': order_number,
                            'date': userAccountsEntity[i].date.__str__() if userAccountsEntity[i].date.__str__() is not None else ""
                        }
                        point_all_day_list.append(rec)
                    return point_all_day_list
                else:
                    return point_all_day_list
            else:
                raise EmployeeListEmptyException()
        except (
                TargetIdDoesNotExistException,
                EmployeeListEmptyException,
                Exception,
                RuntimeError
        ) as e:
            raise e
        finally:
            self.session.close()
            self.connection.close()

    def getSupplyAllDay(self, year, month, day):
        supply_all_day_list = []
        try:
            self.session = super(OrderInstance, self).get_session()

            suppliesInventoryEntity = self.session.query(
                SuppliesInventoryEntity.id.label('id'),
                SuppliesInventoryEntity.supplies_id.label('supplies_id'),
                SuppliesInventoryEntity.employee_id.label('employee_id'),
                SuppliesInventoryEntity.quantity.label('quantity'),
                SuppliesInventoryEntity.note.label('note'),
                SuppliesInventoryEntity.date.label('date'),
                SuppliesInventoryEntity.type.label('type'),
                SuppliesEntity.name.label('supply_name'),
                SuppliesEntity.product_id.label('product_id')
            ).filter(
                SuppliesInventoryEntity.supplies_id == SuppliesEntity.id,
                extract('year', SuppliesInventoryEntity.date) == int(year),
                extract('month', SuppliesInventoryEntity.date) == int(month),
                extract('day', SuppliesInventoryEntity.date) == int(day),
                SuppliesInventoryEntity.type == InventoryTypeStatus.counter
            ).order_by(
                SuppliesInventoryEntity.supplies_id
            ).all()

            employeesEntity = self.session.query(
                EmployeesEntity
            ).filter().all()

            emplyee_id_list = []
            emplyee_name_list = []

            if employeesEntity != []:
                for i in range(len(employeesEntity)):
                    emplyee_id_list.append(employeesEntity[i].id)
                    emplyee_name_list.append(employeesEntity[i].name)

                if suppliesInventoryEntity != []:
                    for i in range(len(suppliesInventoryEntity)):
                        if suppliesInventoryEntity[i].employee_id is not None:
                            input_personnel_id = emplyee_id_list.index(suppliesInventoryEntity[i].employee_id)
                            input_personnel_name = emplyee_name_list[input_personnel_id]
                        else:
                            input_personnel_name = ""

                        if suppliesInventoryEntity[i].type == InventoryTypeStatus.warehouse:
                            type = '倉庫'
                        elif suppliesInventoryEntity[i].type == InventoryTypeStatus.counter:
                            type = '櫃檯'
                        else:
                            type = ''

                        rec = {
                            'supply_name': suppliesInventoryEntity[i].supply_name if suppliesInventoryEntity[i].supply_name is not None else "",
                            'supplies_id': suppliesInventoryEntity[i].supplies_id if suppliesInventoryEntity[i].supplies_id is not None else "",
                            'product_id': suppliesInventoryEntity[i].product_id if suppliesInventoryEntity[i].product_id is not None else "",
                            'quantity': suppliesInventoryEntity[i].quantity if suppliesInventoryEntity[i].quantity is not None else "",
                            'input_personnel': input_personnel_name,
                            'note': suppliesInventoryEntity[i].note if suppliesInventoryEntity[i].note is not None else "",
                            'date': suppliesInventoryEntity[i].date.__str__() if suppliesInventoryEntity[i].date.__str__() is not None else "",
                            'type': type
                        }
                        supply_all_day_list.append(rec)
                    return supply_all_day_list
                else:
                    return supply_all_day_list
            else:
                raise EmployeeListEmptyException()
        except (
                TargetIdDoesNotExistException,
                EmployeeListEmptyException,
                Exception,
                RuntimeError
        ) as e:
            raise e
        finally:
            self.session.close()
            self.connection.close()
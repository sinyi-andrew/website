from Model.Exception import *
from Model.SuperModel import SuperModel
from Entity.Entity import *
from Utility.Email import EmailHelper
from Utility.Host import Host
from Utility.Status import *
import uuid
import hashlib
import json
import time
from datetime import datetime, timedelta
from sqlalchemy.sql import func

global engine
global connection
global trans


class EmployeeInstance(SuperModel):
    session = None

    def __init__(self):
        super(EmployeeInstance, self).__init__()

    def getCalendar(self):
        employee_calendar_list = []
        try:
            self.session = super(EmployeeInstance, self).get_session()

            employeeCalendarEntity = self.session.query(
                EmployeeCalendarEntity.id.label('employee_calendar_id'),
                EmployeeCalendarEntity.employee_id.label('employee_id'),
                EmployeeCalendarEntity.start_time.label('start_time'),
                EmployeeCalendarEntity.end_time.label('end_time'),
                EmployeeCalendarEntity.status.label('status'),
                EmployeeCalendarEntity.note.label('note'),
                EmployeesEntity.employee_number.label('employee_number'),
                EmployeesEntity.name.label('employee_name')
            ).filter(
                EmployeeCalendarEntity.status == EmployeeCalendarStatus.activity,
                EmployeeCalendarEntity.employee_id == EmployeesEntity.id
            ).all()
            if employeeCalendarEntity != []:
                for i in range(len(employeeCalendarEntity)):

                    if employeeCalendarEntity[i].status == EmployeeCalendarStatus.activity:
                        status = 'activity'
                    else:
                        status = 'deleted'
                    rec = {
                        'employee_calendar_id': employeeCalendarEntity[i].employee_calendar_id if employeeCalendarEntity[i].employee_calendar_id is not None else "",
                        'employee_id': employeeCalendarEntity[i].employee_id if employeeCalendarEntity[i].employee_id is not None else "",
                        'start': employeeCalendarEntity[i].start_time if employeeCalendarEntity[i].start_time is not None else "",
                        'end': employeeCalendarEntity[i].end_time if employeeCalendarEntity[i].end_time is not None else "",
                        'status': employeeCalendarEntity[i].status if employeeCalendarEntity[i].status is not None else "",
                        'status_str': status if status is not None else "",
                        'note': employeeCalendarEntity[i].note if employeeCalendarEntity[i].note is not None else "",
                        'employee_number': employeeCalendarEntity[i].employee_number if employeeCalendarEntity[i].employee_number is not None else "",
                        'title': employeeCalendarEntity[i].employee_name if employeeCalendarEntity[i].employee_name is not None else ""
                    }
                    employee_calendar_list.append(rec)
            else:
                employee_calendar_list = []
            return {'employee_calendar_list': employee_calendar_list}
        except (
            Exception,
            RuntimeError
        ) as e:
            raise e
        finally:
            self.session.close()
            self.connection.close()

    # new calendar create
    def create_calendar(self, input_personnel, employee_id, start_time, end_time, note):
        try:
            self.session = super(EmployeeInstance, self).get_session()

            newEmployeeCalendar = EmployeeCalendarEntity()
            newEmployeeCalendar.employee_id = employee_id
            newEmployeeCalendar.start_time = start_time
            newEmployeeCalendar.end_time = end_time
            newEmployeeCalendar.input_personnel = input_personnel
            newEmployeeCalendar.status = EmployeeCalendarStatus.activity
            newEmployeeCalendar.note = note
            self.session.add(newEmployeeCalendar)
            self.session.commit()
            calendar_id = newEmployeeCalendar.id
            return calendar_id
        except (
            Exception,
            RuntimeError
        ) as e:
            raise e
        finally:
            self.session.close()
            self.connection.close()

    def update_calendar(self, calendar_id, input_personnel, start_time, end_time, note):
        try:
            self.session = super(EmployeeInstance, self).get_session()

            employeeCalendarEntity = self.session.query(EmployeeCalendarEntity).filter(
                EmployeeCalendarEntity.id == int(calendar_id)
            ).scalar()

            if employeeCalendarEntity is not None:
                employeeCalendarEntity.start_time = start_time
                employeeCalendarEntity.end_time = end_time
                employeeCalendarEntity.input_personnel = input_personnel
                employeeCalendarEntity.note = note
                self.session.commit()
                return calendar_id
            else:
                raise TargetIdDoesNotExistException()
        except (
            TargetIdDoesNotExistException,
            Exception,
            RuntimeError
        ) as e:
            raise e
        finally:
            self.session.close()
            self.connection.close()

    def delete_calendar(self, input_personnel, calendar_id):
        try:
            self.session = super(EmployeeInstance, self).get_session()

            employeeCalendarEntity = self.session.query(EmployeeCalendarEntity).filter(
                EmployeeCalendarEntity.id == int(calendar_id)
            ).scalar()

            if employeeCalendarEntity is not None:
                employeeCalendarEntity.input_personnel = input_personnel
                employeeCalendarEntity.status = EmployeeCalendarStatus.deleted
                self.session.commit()
                return calendar_id
            else:
                raise TargetIdDoesNotExistException()
        except (
            TargetIdDoesNotExistException,
            Exception,
            RuntimeError
        ) as e:
            raise e
        finally:
            self.session.close()
            self.connection.close()

    def getFunctionData(self):
        try:
            self.session = super(EmployeeInstance, self).get_session()
            _data = self.session.query(FunctionEntity).all()

            if _data is None:
                raise DataDoesNotExistException()
            else:
                result = _data
            return result
        except (
                AuthenticateFailedException,
                Exception,
                RuntimeError
        ) as e:
            raise e
        finally:
            self.session.close()
            self.connection.close()

    def createFunction(self, store_id, name, description):
        response = {}
        try:
            self.session = super(EmployeeInstance, self).get_session()
            data = FunctionEntity()
            data.store_id = store_id
            data.name = name
            data.description = description

            self.session.add(data)
            self.session.commit()
            response_data = {
                'id': self.session.query(FunctionEntity).filter(
                    FunctionEntity.id == data.id
                ).scalar()
            }

            return response_data
        except (
                AuthenticateFailedException,
                Exception,
                RuntimeError
        ) as e:
            raise e
        finally:
            self.session.close()
            self.connection.close()

    def getRoleList(self):

        try:
            self.session = super(EmployeeInstance, self).get_session()
            _data = self.session.query(EmployeeGradesEntity).all()

            if _data is None:
                raise DataDoesNotExistException()
            else:
                result = _data
            return _data
        except (
                AuthenticateFailedException,
                Exception,
                RuntimeError
        ) as e:
            raise e
        finally:
            self.session.close()
            self.connection.close()

    def getRoleData(self, id):

        try:
            self.session = super(EmployeeInstance, self).get_session()
            _data = self.session.query(EmployeeGradesEntity).filter(
                EmployeeGradesEntity.id == id
            ).scalar()
            permission = self.session.query(PermissionEntity).filter(
                PermissionEntity.grade_id == id
            ).all()

            if _data is None or permission is None:
                raise DataDoesNotExistException()
            else:
                _data.permission = permission
                result = _data
            return result
        except (
                AuthenticateFailedException,
                Exception,
                RuntimeError
        ) as e:
            raise e
        finally:
            self.session.close()
            self.connection.close()

    def roleData(self, id, name, description, sale, permission):
        response = {}
        try:
            if id == "" or id is None:  # new
                self.session = super(EmployeeInstance, self).get_session()
                data = EmployeeGradesEntity()

                data.name = name
                data.description = description
                data.sale = sale

                self.session.add(data)
                self.session.commit()

                for i in range(len(permission)):
                    p_data = PermissionEntity()
                    p_data.store_id = '1'
                    p_data.grade_id = data.id
                    p_data.function_id = permission[i]["id"]
                    p_data.permission = permission[i]["value"]
                    self.session.add(p_data)
                    self.session.commit()
            else:  # update
                self.session = super(EmployeeInstance, self).get_session()

                data = self.session.query(
                    EmployeeGradesEntity
                ).filter(
                    EmployeeGradesEntity.id == id
                ).scalar()

                if data is None:
                    raise DataDoesNotExistException()
                else:
                    if data.id == 1:
                        raise PermissionDeniedException()
                    else:
                        data.name = name
                        data.description = description
                        data.sale = sale
                        self.session.flush()
                        self.session.commit()
                        for i in range(len(permission)):
                            p_data = self.session.query(PermissionEntity).filter(PermissionEntity.id == permission[i]["id"]).scalar()
                            p_data.permission = permission[i]["value"]
                            self.session.add(p_data)
                            self.session.commit()

            response_data = {
                'id': self.session.query(EmployeeGradesEntity).filter(
                    EmployeeGradesEntity.id == data.id
                ).scalar()
            }

            return response_data
        except (
                DataDoesNotExistException,
                PermissionDeniedException,
                AuthenticateFailedException,
                Exception,
                RuntimeError
        ) as e:
            raise e
        finally:
            self.session.close()
            self.connection.close()

    def getEmployeeIDListOnlySeller(self):
        employeeIDList = []
        employeeNameList = []
        try:
            self.session = super(EmployeeInstance, self).get_session()

            employeesEntity = self.session.query(
                EmployeesEntity.id.label('employee_id'),
                EmployeesEntity.name.label('employee_name')
            ).filter(
                EmployeesEntity.status == EmployeeStatus.activity,
                or_(
                    EmployeeGradesEntity.id == EmployeeGradeStatus.manager,
                    EmployeeGradesEntity.id == EmployeeGradeStatus.medical_artist
                ),
                EmployeesEntity.grade_id == EmployeeGradesEntity.id
            ).all()

            if employeesEntity != []:
                for i in range(len(employeesEntity)):
                    employeeIDList.append(employeesEntity[i].employee_id)
                    employeeNameList.append(employeesEntity[i].employee_name)
            else:
                raise DataDoesNotExistException()
            employeeList = {
                'employeeIDList': employeeIDList,
                'employeeNameList': employeeNameList
            }
            return employeeList
        except (
            DataDoesNotExistException,
            AuthenticateFailedException,
            Exception,
            RuntimeError
        ) as e:
            raise e
        finally:
            self.session.close()
            self.connection.close()

    def getEmployeeIDList(self):
        employeeIDList = []
        employeeNameList = []
        try:
            self.session = super(EmployeeInstance, self).get_session()

            employeesEntity = self.session.query(
                EmployeesEntity.id.label('employee_id'),
                EmployeesEntity.name.label('employee_name')
            ).filter(
                EmployeesEntity.status == EmployeeStatus.activity
            ).all()

            if employeesEntity != []:
                for i in range(len(employeesEntity)):
                    employeeIDList.append(employeesEntity[i].employee_id)
                    employeeNameList.append(employeesEntity[i].employee_name)
            else:
                raise DataDoesNotExistException()
            employeeList = {
                'employeeIDList': employeeIDList,
                'employeeNameList': employeeNameList
            }
            return employeeList
        except (
                AuthenticateFailedException,
                Exception,
                RuntimeError
        ) as e:
            raise e
        finally:
            self.session.close()
            self.connection.close()

    def getEmployeeList(self):

        try:
            self.session = super(EmployeeInstance, self).get_session()
            _data = self.session.query(EmployeesEntity).all()

            if _data is None:
                raise DataDoesNotExistException()
            else:
                result = _data
            return _data
        except (
                AuthenticateFailedException,
                Exception,
                RuntimeError
        ) as e:
            raise e
        finally:
            self.session.close()
            self.connection.close()

    def getEmployeeData(self, id):

        try:
            self.session = super(EmployeeInstance, self).get_session()
            _data = self.session.query(EmployeesEntity).filter(
                EmployeesEntity.id == id
            ).scalar()
            grade = self.session.query(EmployeeGradesEntity).filter(
                EmployeeGradesEntity.id == _data.grade_id
            ).all()

            if _data is None or grade is None:
                raise DataDoesNotExistException()
            else:
                _data.grade = grade
                result = _data
            return result
        except (
                AuthenticateFailedException,
                Exception,
                RuntimeError
        ) as e:
            raise e
        finally:
            self.session.close()
            self.connection.close()

    def idNumbercConfirm(self, id, IDNumber):
        response = {}
        try:
            self.session = super(EmployeeInstance, self).get_session()
            data = self.session.query(EmployeesEntity).filter(
                EmployeesEntity.IDNumber == IDNumber
            ).scalar()
            if data is not None:
                if id == data.id:
                    response = 0
                else:
                    response = 1
            else:
                response = 0

            return response
        except (
                Exception,
                RuntimeError
        ) as e:
            raise e
        finally:
            self.session.close()
            self.connection.close()

    def employeeData(self, id, grade_id, store_id, name, IDNumber, email,
                     phone_number, mobile_phone, birthday, bloodtype, gender,
                     address, height, weight, marriage, skill,
                     position_change, education, experience, certificate, note, images, contact, contact_phone, status):
        response = {}
        try:
            if id == "" or id is None:  # new
                self.session = super(EmployeeInstance, self).get_session()

                checkEmployeesEntity = self.session.query(EmployeesEntity).filter(
                    EmployeesEntity.email == email
                ).scalar()

                if checkEmployeesEntity is not None:
                    raise EmailDuplicationException()
                else:
                    data = EmployeesEntity()

                    default_password = 'diazy'

                    data.grade_id = grade_id
                    data.store_id = store_id
                    data.uuid = str(uuid.uuid4())
                    data.name = name
                    data.IDNumber = IDNumber
                    data.email = email
                    data.password = hashlib.sha256(bytes(default_password.encode('utf-8'))).hexdigest()
                    data.phone_number = phone_number
                    data.mobile_phone = mobile_phone
                    data.birthday = birthday
                    data.bloodtype = bloodtype
                    data.gender = gender
                    data.address = address
                    data.height = height
                    data.weight = weight
                    data.marriage = marriage
                    data.skill = skill
                    data.position_change = position_change
                    data.education = education
                    data.experience = experience
                    data.certificate = certificate
                    data.note = note
                    data.image = images
                    data.contact = contact
                    data.contact_phone = contact_phone
                    data.status = EmployeeStatus.activity
                    data.create_date = datetime.now()

                    self.session.add(data)
                    self.session.commit()
                    id_str = str(data.id)
                    data.employee_number = 'E' + id_str.rjust(6, '0')
                    self.session.commit()

                    employee_email = data.email
                    employee_name = data.name
                    employee_uuid = data.uuid
                    employee_number = data.employee_number

                    # authentication email sending.
                    email_helper = EmailHelper()
                    email_helper.send_employee_activation_notification(
                        employee_email,
                        employee_name,
                        employee_number,
                        Host().fqdn_name + "/employee-activation?g=" + employee_uuid
                    )

            else:  # update
                self.session = super(EmployeeInstance, self).get_session()
                data = self.session.query(EmployeesEntity).filter(EmployeesEntity.id == id).scalar()
                if data is None:
                    raise DataDoesNotExistException()
                else:
                    if status == '' or status is None:
                        raise MissingCriticalParametersException()
                    data.grade_id = grade_id
                    data.name = name
                    data.IDNumber = IDNumber
                    data.email = email
                    data.phone_number = phone_number
                    data.mobile_phone = mobile_phone
                    data.birthday = birthday
                    data.bloodtype = bloodtype
                    data.gender = gender
                    data.address = address
                    data.height = height
                    data.weight = weight
                    data.marriage = marriage
                    data.skill = skill
                    data.position_change = position_change
                    data.education = education
                    data.experience = experience
                    data.certificate = certificate
                    data.note = note
                    data.image = images
                    data.contact = contact
                    data.contact_phone = contact_phone
                    data.status = status
                    self.session.flush()
                    self.session.commit()

            response_data = {
                'id': self.session.query(EmployeesEntity).filter(
                    EmployeesEntity.id == data.id
                ).scalar()
            }

            return response_data
        except (
            MissingCriticalParametersException,
            EmailDuplicationException,
            Exception,
            RuntimeError
        ) as e:
            raise e
        finally:
            self.session.close()
            self.connection.close()

    def authenticate(self, account, password):
        hash_password = hashlib.sha256(bytes(password.encode('utf-8'))).hexdigest()
        try:
            self.session = super(EmployeeInstance, self).get_session()

            employeesEntity = self.session.query(EmployeesEntity).filter(
                or_(EmployeesEntity.email == account,
                    EmployeesEntity.IDNumber == account
                    ),
                EmployeesEntity.password == hash_password,
                EmployeesEntity.status == EmployeeStatus.activity
            ).scalar()

            if employeesEntity is not None:  # found
                response = {
                    "uuid": employeesEntity.uuid if employeesEntity.uuid is not None else "",
                    "name": employeesEntity.name if employeesEntity.name is not None else "",
                    "email": employeesEntity.email if employeesEntity.email is not None else "",
                    "ID_Number": employeesEntity.IDNumber if employeesEntity.IDNumber is not None else "",
                    "grade_id": employeesEntity.grade_id if employeesEntity.grade_id is not None else ""
                }
                return response
            else:
                raise AuthenticateFailedException()
        except (
                AuthenticateFailedException,
                Exception,
                RuntimeError
        ) as e:
            raise e
        finally:
            self.session.close()
            self.connection.close()

    def checkPassword(self, uuid, password):
        hash_password = hashlib.sha256(bytes(password.encode('utf-8'))).hexdigest()
        try:
            self.session = super(EmployeeInstance, self).get_session()

            employeesEntity = self.session.query(EmployeesEntity).filter(
                EmployeesEntity.uuid == uuid,
                EmployeesEntity.password == hash_password
            ).scalar()

            if employeesEntity is not None:  # found
                return {"employee_id": employeesEntity.id if employeesEntity.id is not None else ""}
            else:
                raise AuthenticateFailedException()
        except (
                AuthenticateFailedException,
                Exception,
                RuntimeError
        ) as e:
            raise e
        finally:
            self.session.close()
            self.connection.close()

    def updatePassword(self, number, old_password, new_password):
        old_password = hashlib.sha256(bytes(old_password.encode('utf-8'))).hexdigest()
        new_password = hashlib.sha256(bytes(new_password.encode('utf-8'))).hexdigest()
        try:
            self.session = super(EmployeeInstance, self).get_session()

            employeesEntity = self.session.query(EmployeesEntity).filter(
                EmployeesEntity.employee_number == number,
                EmployeesEntity.password == old_password
            ).scalar()
            if employeesEntity is None:
                raise AuthenticateFailedException()
            else:
                employeesEntity.password = new_password
                self.session.flush()
                self.session.commit()
                return {"employee_id": employeesEntity.id if employeesEntity.id is not None else ""}
        except (
                AuthenticateFailedException,
                Exception,
                RuntimeError
        ) as e:
            raise e
        finally:
            self.session.close()
            self.connection.close()

    def searchEmployee(self, search):
        try:
            self.session = super(EmployeeInstance, self).get_session()

            user = self.session.query(
                EmployeesEntity.id.label('id'),
                EmployeesEntity.uuid.label('uuid'),
                EmployeesEntity.name.label('name'),
                EmployeesEntity.employee_number.label('employee_num'),
            ).filter(
                or_(EmployeesEntity.employee_number == search,
                    EmployeesEntity.IDNumber == search,
                    )
            ).first()

            if user is not None:
                response_data = {
                    'code': 0,
                    'uuid': user.uuid if user.uuid is not None else "",
                    'name': user.name if user.name is not None else "",
                    'employee_number': user.employee_num if user.employee_num is not None else ""
                }
            else:
                response_data = {
                    'code': 2001,
                    'msg': '查無員工帳號資料，請重新輸入!'
                }
            return response_data
        except (
                AuthenticateFailedException,
                Exception,
                RuntimeError
        ) as e:
            raise e
        finally:
            self.session.close()
            self.connection.close()

    def getPermissionByGradeID(self, input_personnel_grade):
        permission_list = []
        try:
            self.session = super(EmployeeInstance, self).get_session()

            permissionEntity = self.session.query(PermissionEntity).filter(
                PermissionEntity.grade_id == input_personnel_grade
            ).all()

            if permissionEntity != []:
                for i in range(len(permissionEntity)):
                    permission_list.append({
                        'function_id': permissionEntity[i].function_id,
                        'grade_id': permissionEntity[i].grade_id
                    })
            else:
                permission_list.append({
                        'function_id': "",
                        'grade_id': ""
                    })
            return {'permission_list': permission_list}
        except (
                TargetIdDoesNotExistException,
                Exception,
                RuntimeError
        ) as e:
            raise e
        finally:
            self.session.close()
            self.connection.close()

    def getPermissionByGradeID(self, grade):
        permissionList = []
        try:
            self.session = super(EmployeeInstance, self).get_session()

            permissionEntity = self.session.query(PermissionEntity).filter(
                PermissionEntity.grade_id == int(grade)
            ).all()

            if permissionEntity != []:
                for i in range(len(permissionEntity)):
                    permissionList.append({
                        'permission': permissionEntity[i].permission,
                        'function_id': permissionEntity[i].function_id
                    })
                return permissionList
            else:
                raise AuthenticateFailedException()
        except (
                AuthenticateFailedException,
                Exception,
                RuntimeError
        ) as e:
            raise e
        finally:
            self.session.close()
            self.connection.close()

    def getPermissionByGradeIDAndFunctionID(self, input_personnel_grade, function):
        try:
            self.session = super(EmployeeInstance, self).get_session()

            permissionEntity = self.session.query(PermissionEntity).filter(
                PermissionEntity.grade_id == int(input_personnel_grade),
                PermissionEntity.function_id == int(function)
            ).scalar()

            if permissionEntity is not None:
                if permissionEntity.permission is not None:
                    return int(permissionEntity.permission)
                else:
                    return 0
            else:
                raise TargetIdDoesNotExistException()
        except (
                TargetIdDoesNotExistException,
                Exception,
                RuntimeError
        ) as e:
            raise e
        finally:
            self.session.close()
            self.connection.close()

    def getEmployeeIDByUuid(self, uuid):
        try:
            self.session = super(EmployeeInstance, self).get_session()
            employeesEntity = self.session.query(EmployeesEntity).filter(
                EmployeesEntity.uuid == uuid
            ).scalar()

            if employeesEntity is None:
                raise TargetIdDoesNotExistException()
            else:
                return employeesEntity.id
        except (
                TargetIdDoesNotExistException,
                Exception,
                RuntimeError
        ) as e:
            raise e
        finally:
            self.session.close()
            self.connection.close()

    def getEmployeeDataByUuid(self, uuid):
        try:
            self.session = super(EmployeeInstance, self).get_session()

            employeesEntity = self.session.query(EmployeesEntity).filter(
                EmployeesEntity.uuid == uuid
            ).scalar()

            if employeesEntity is None:
                raise TargetIdDoesNotExistException()
            else:
                response = {
                    'employee_id': employeesEntity.id,
                    'employee_name': employeesEntity.name,
                    'employee_image': employeesEntity.image,
                    'employee_number': employeesEntity.employee_number
                }
                return response
        except (
                TargetIdDoesNotExistException,
                Exception,
                RuntimeError
        ) as e:
            raise e
        finally:
            self.session.close()
            self.connection.close()

    def send_forgetpassword_email(self, email):
        try:
            self.session = super(EmployeeInstance, self).get_session()

            employeesEntity = self.session.query(EmployeesEntity).filter(
                EmployeesEntity.email == email
            ).scalar()

            if employeesEntity is None:
                raise TargetIdDoesNotExistException()

            now = int(time.time())
            dateform1 = datetime.fromtimestamp(now)        ##datetime
            date = dateform1 + timedelta(days=1)         ##datetime
            datestamp = int(time.mktime(datetime.strptime(str(date), "%Y-%m-%d %H:%M:%S").timetuple()))

            new_authentication = AuthenticationEntity()
            new_authentication.auth_create_date = now
            new_authentication.auth_token = str(uuid.uuid4())
            new_authentication.auth_valid_date = datestamp
            new_authentication.auth_account_id = employeesEntity.id
            new_authentication.auth_flag = AuthenticationFlagStatus.unused

            self.session.add(new_authentication)
            self.session.commit()

            target_email = employeesEntity.email
            employee_name = employeesEntity.name
            token = new_authentication.auth_token

            EmailHelper().send_forget_password_email(target_email, employee_name, Host().fqdn_name + "/admin/employee/api/forget-password/authenticate?token=" + token)
            return {"email": email}
        except (
            TargetIdDoesNotExistException,
            Exception,
            RuntimeError
        ) as e:
            raise e
        finally:
            self.session.close()
            self.connection.close()


    ##forget password token authenticate
    def authenticate_forgetword_token(self, token):
        try:
            self.session = super(EmployeeInstance, self).get_session()

            now = int(time.time())

            authenticate_token = self.session.query(AuthenticationEntity).filter(
                AuthenticationEntity.auth_token == token,
                AuthenticationEntity.auth_flag == AuthenticationFlagStatus.unused,
                and_(
                    AuthenticationEntity.auth_create_date <= now,
                    AuthenticationEntity.auth_valid_date >= now
                )
            ).scalar()

            if authenticate_token is not None:
                employee_id = authenticate_token.auth_account_id

                employeesEntity = self.session.query(EmployeesEntity).filter(
                    EmployeesEntity.id == employee_id
                ).scalar()

                if employeesEntity is not None:
                    response = {
                        "token": token,
                        "employee_id": employeesEntity.id
                    }
                    return response
                else:
                    raise TokenAuthenticationIsFailedException()
            else:
                raise TokenAuthenticationIsFailedException()
        except(
            TokenAuthenticationIsFailedException,
            Exception,
            RuntimeError
        ) as e:
            raise e

    ##password
    def password(self, employee_id, password, token):
        hash_password = hashlib.sha256(bytes(password.encode('utf-8'))).hexdigest()
        try:
            self.session = super(EmployeeInstance, self).get_session()

            employeesEntity = self.session.query(EmployeesEntity).filter(
                EmployeesEntity.id == int(employee_id)
            ).scalar()

            if employeesEntity is not None:      ##found
                employeesEntity.password = hash_password
                self.session.commit()

                now = int(time.time())

                authenticationEntity = self.session.query(AuthenticationEntity).filter(
                    AuthenticationEntity.auth_token == token,
                    AuthenticationEntity.auth_flag == AuthenticationFlagStatus.unused,
                    and_(
                        AuthenticationEntity.auth_create_date <= now,
                        AuthenticationEntity.auth_valid_date >= now
                    )
                ).scalar()

                if authenticationEntity is None:
                    raise TokenAuthenticationIsFailedException()

                authenticationEntity.auth_flag = AuthenticationFlagStatus.used
                self.session.commit()

                response = {
                    "employee_id": employeesEntity.id
                }
                return response
            else:
                raise TargetIdDoesNotExistException()
        except (
            TokenAuthenticationIsFailedException,
            TargetIdDoesNotExistException,
            Exception,
            RuntimeError
        ) as e:
            raise e
        finally:
            self.session.close()
            self.connection.close()
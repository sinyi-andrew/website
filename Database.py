# -*- coding: utf-8 -*-
from sqlalchemy import create_engine
from sqlalchemy.exc import *


connection_string = "mysql+pymysql://diazy:54706525@192.168.20.112:3306/diazy_test?charset=utf8"
# connection_string = "mysql+pymysql://root:Sinyitech54706525@35.229.223.196:3306/diazy_test?charset=utf8"

class Singleton(object):
    _instance = None

    def __new__(class_, *args, **kwargs):
        if not isinstance(class_._instance, class_):
            class_._instance = object.__new__(class_, *args, **kwargs)
        return class_._instance


class DatabaseFactory(Singleton):
    engine = None

    def get_db_engine(self):
        try:
            if self.engine is None:
                self.engine = create_engine(connection_string, pool_size=30, max_overflow=10, pool_recycle=3600)
            return self.engine
        except (Exception, DBAPIError, SQLAlchemyError) as e:
            raise e
